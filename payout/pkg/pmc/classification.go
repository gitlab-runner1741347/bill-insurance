package pmc

import (
	"git.kafefin.net/backend/central-payout/internal/models"
	"git.kafefin.net/backend/central-payout/pb"
)

func ClassifyByStatus(payments []*models.PaymentRequest) map[pb.PaymentStatus][]*models.PaymentRequest {
	ret := make(map[pb.PaymentStatus][]*models.PaymentRequest)

	for k := range pb.PaymentStatus_name {
		ret[pb.PaymentStatus(k)] = make([]*models.PaymentRequest, 0)
	}

	for _, payment := range payments {
		ret[payment.Status] = append(ret[payment.Status], payment)
	}

	return ret
}

func ClassifyByTxnStatus(payments []*models.PaymentRequest) map[pb.PaymentTransactionStatus][]*models.PaymentRequest {
	ret := make(map[pb.PaymentTransactionStatus][]*models.PaymentRequest)

	for k := range pb.PaymentTransactionStatus_name {
		ret[pb.PaymentTransactionStatus(k)] = make([]*models.PaymentRequest, 0)
	}

	for _, payment := range payments {
		ret[payment.TxnStatus] = append(ret[payment.TxnStatus], payment)
	}

	return ret
}

func ClassifyByTxnQuota(payments []*models.PaymentRequest, quota int64) (higher []*models.PaymentRequest, lower []*models.PaymentRequest) {
	higher, lower = make([]*models.PaymentRequest, 0), make([]*models.PaymentRequest, 0)

	for _, payment := range payments {
		if payment.PaymentAmount > quota {
			higher = append(higher, payment)
		} else {
			lower = append(lower, payment)
		}
	}

	return higher, lower
}

func ExtractID(payments []*models.PaymentRequest) []int64 {
	ids := make([]int64, len(payments))
	for i := range payments {
		ids[i] = payments[i].ID
	}

	return ids
}
