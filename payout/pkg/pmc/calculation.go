package pmc

import "git.kafefin.net/backend/central-payout/internal/models"

func GetTotalPaymentAmount(payments []*models.PaymentRequest) (total int64) {
	for _, payment := range payments {
		total += payment.PaymentAmount
	}

	return
}
