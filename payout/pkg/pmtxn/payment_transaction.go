package pmtxn

import (
	"fmt"
	"time"
)

func GenPaymentRequestID() string {
	return "10X" + fmt.Sprint(time.Now().UnixMilli())
}
