package sldmath

func GenericMax[T int | uint | int32 | uint32 | int64 | uint64](args ...T) T {
	if len(args) == 0 {
		return 0
	}

	ret := args[0]
	for _, v := range args {
		if v > ret {
			ret = v
		}
	}

	return ret
}
