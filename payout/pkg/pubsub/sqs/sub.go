package sqs

import (
	"context"
	"fmt"
	"sync"

	"git.kafefin.net/backend/central-payout/pkg/pubsub"
	"git.kafefin.net/backend/kitchen/l"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sqs"
)

type SubscriberMessage struct {
	raw      *sqs.Message
	svc      *sqs.SQS
	queueUrl string
}

func (m *SubscriberMessage) Value() string {
	return aws.StringValue(m.raw.Body)
}

func (m *SubscriberMessage) Key() string {
	return aws.StringValue(m.raw.MessageId)
}

func (m *SubscriberMessage) Commit(metadata string) error {
	_, err := m.svc.DeleteMessage(&sqs.DeleteMessageInput{
		QueueUrl:      aws.String(m.queueUrl),
		ReceiptHandle: m.raw.ReceiptHandle,
	})
	if err != nil {
		return err
	}
	return nil
}

type SubscriberOption func(*Subscriber)

type Subscriber struct {
	logger    l.Logger
	ctx       context.Context
	queueUrl  string
	svc       *sqs.SQS
	workerNum int
	maxNumMsg int64
	handler   pubsub.SubscriberHandler
}

func NewSubscriber(ctx context.Context, svc *sqs.SQS, queueUrl string, logger l.Logger,
	handler pubsub.SubscriberHandler,
	opts ...SubscriberOption,
) (pubsub.Subscriber, error) {
	s := &Subscriber{
		ctx:       ctx,
		logger:    logger,
		svc:       svc,
		workerNum: 4,
		maxNumMsg: 8,
		handler:   handler,
	}
	s.queueUrl = queueUrl

	for _, opt := range opts {
		opt(s)
	}
	return s, nil
}

func WithWorkerNum(num int) SubscriberOption {
	return func(s *Subscriber) {
		s.workerNum = num
	}
}

func WithMaxNumMsg(max int64) SubscriberOption {
	return func(s *Subscriber) {
		s.maxNumMsg = max
	}
}

func (s *Subscriber) Run() error {
	if s.handler == nil {
		return fmt.Errorf("handler subscriber message invalid")
	}

	msgChan := make(chan pubsub.SubscriberMessage)

	go func() {
		defer close(msgChan)
		for {
			select {
			case <-s.ctx.Done():
				return
			default:
				result, err := s.svc.ReceiveMessage(&sqs.ReceiveMessageInput{
					QueueUrl:        aws.String(s.queueUrl),
					WaitTimeSeconds: aws.Int64(20),
				})
				if err != nil {
					s.logger.Error("receive message from queue failed", l.Object("queueUrl", s.queueUrl), l.Error(err))
					return
				}

				for _, msg := range result.Messages {
					msgChan <- &SubscriberMessage{
						raw:      msg,
						svc:      s.svc,
						queueUrl: s.queueUrl,
					}
				}
			}
		}
	}()

	var wg sync.WaitGroup
	for idx := 0; idx < s.workerNum; idx++ {
		s.logger.Info(fmt.Sprintf("subscriber index [%v] is starting..", idx))
		wg.Add(1)
		go func() {
			defer wg.Done()
			for {
				msg, ok := <-msgChan
				if !ok {
					return
				}
				if msg.Value() == "" {
					continue
				}
				s.handler(msg)
			}
		}()
	}
	wg.Wait()
	return nil
}

func (s Subscriber) Close() error {
	return nil
}
