package sqs

import (
	"context"
	"fmt"

	"git.kafefin.net/backend/central-payout/pkg/pubsub"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sqs"
)

type PublishMessageOption func(*PublishMessage)

type PublishMessage struct {
	body                   string
	attrs                  map[string]*sqs.MessageAttributeValue
	messageId              *string
	delaySeconds           *int64
	messageDeduplicationId *string
	groupId                *string
}

func NewPublishMessage(body string, opts ...PublishMessageOption) *PublishMessage {
	msg := &PublishMessage{
		body: body,
	}
	for _, opt := range opts {
		opt(msg)
	}
	return msg
}

func WithDelaySeconds(sec int64) PublishMessageOption {
	return func(m *PublishMessage) {
		m.delaySeconds = &sec
	}
}

func WithMessageDeduplicationId(id string) PublishMessageOption {
	return func(m *PublishMessage) {
		m.messageDeduplicationId = &id
	}
}

func WithGroupId(groupId string) PublishMessageOption {
	return func(m *PublishMessage) {
		m.groupId = &groupId
	}
}

func WithMessageAttributes(attrs map[string]string) PublishMessageOption {
	return func(m *PublishMessage) {
		for k, v := range attrs {
			m.attrs[k] = &sqs.MessageAttributeValue{
				DataType:    aws.String("String"),
				StringValue: aws.String(v),
			}
		}
	}
}

func (m *PublishMessage) Output() (interface{}, error) {
	return m.messageId, nil
}

type Publisher struct {
	queueUrl string
	svc      *sqs.SQS
}

func New(svc *sqs.SQS) (*Publisher, error) {
	return &Publisher{
		svc: svc,
	}, nil
}

func NewPublisher(svc *sqs.SQS, queueUrl string) (*Publisher, error) {
	return &Publisher{
		queueUrl: queueUrl,
		svc:      svc,
	}, nil
}

func (p *Publisher) Publish(ctx context.Context, dest string, msg pubsub.PublishMessage) error {
	v, ok := msg.(*PublishMessage)
	if !ok {
		return fmt.Errorf("format message invalid")
	}

	msgOutput, err := p.svc.SendMessageWithContext(ctx, &sqs.SendMessageInput{
		MessageAttributes: v.attrs,
		MessageBody:       aws.String(v.body),
		QueueUrl:          aws.String(dest),
	})
	if err != nil {
		return err
	}
	v.messageId = msgOutput.MessageId
	return nil
}
