package pubsub

import "context"

type (
	PublishMessageOption func(PublishMessage)

	PublishMessage interface {
		Output() (interface{}, error)
	}

	Publisher interface {
		Publish(ctx context.Context, dest string, message PublishMessage) error
	}

	SubscriberHandler func(SubscriberMessage)

	SubscriberMessage interface {
		Value() string
		Key() string
		Commit(metadata string) error
	}

	Subscriber interface {
		Run() error
		Close() error
	}
)
