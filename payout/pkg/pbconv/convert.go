package pbconv

import "strings"

type EnumOption func(s string) string

func EnumToName(str string, opts ...EnumOption) string {
	enumStrParts := strings.Split(str, "_")
	if len(enumStrParts) == 0 {
		return ""
	}

	name := enumStrParts[len(enumStrParts)-1]

	for _, opt := range opts {
		name = opt(name)
	}

	return name
}

func MapUnspecified(str string) string {
	if str == "UNSPECIFIED" {
		return ""
	}

	return str
}
