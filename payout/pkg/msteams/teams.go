package msteams

import (
	"bytes"
	"encoding/json"

	"net/http"
)

type TeamsNotificaton struct {
	webhook string
}

func NewTeamsNoti(wh string) *TeamsNotificaton {
	return &TeamsNotificaton{
		webhook: wh,
	}
}

type textRequest struct {
	Text string `json:"text"`
}

func (t *TeamsNotificaton) Send(s string) error {
	reqBody, _ := json.Marshal(textRequest{
		Text: s,
	})

	request, err := http.NewRequest(http.MethodPost, t.webhook, bytes.NewBuffer(reqBody))
	if err != nil {
		return err
	}

	_, err = http.DefaultClient.Do(request)
	return err
}
