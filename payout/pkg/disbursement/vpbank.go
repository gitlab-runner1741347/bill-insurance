package disbursement

import (
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	_ "embed"
	"encoding/base64"
	"encoding/pem"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"strconv"
	"time"

	"git.kafefin.net/backend/central-payout/pb"
	"git.kafefin.net/backend/central-payout/pkg/models"
	"git.kafefin.net/backend/central-payout/pkg/types"
	"git.kafefin.net/backend/kitchen/l"
	"git.kafefin.net/backend/kitchen/nap"
	kitutils "git.kafefin.net/backend/kitchen/utils"
	"github.com/patrickmn/go-cache"
	"go.opentelemetry.io/contrib/instrumentation/net/http/otelhttp"
	"go.opentelemetry.io/otel/trace"
	"moul.io/http2curl"
)

const (
	tokenPath               = "/security/token"
	listBanksPath           = "/payments/v1/banks/"
	internalBeneficiaryPath = "/payments/v1/beneficiary/info?benType=internal"
	externalBeneficiaryPath = "/payments/v1/beneficiary/info?benType=external"
	internalTransferPath    = "/payments/v1/transfer/internal"
	externalTransferPath    = "/payments/v1/transfer/external"
	ftListPath              = "/payments/v1/transfer/ftlist"
	accountInfoPath         = "/payments/v1/partner-account/info"

	keyListBank = "central-payout:list-bank"
)

type VpbankPayment struct {
	host   string
	bearer string
	nap    *nap.Nap
	cache  *cache.Cache
	logger types.CommonLogger
	cer    []byte
	pem    []byte
}

func NewVpbankPayment(host, idn, bearer string, tp trace.TracerProvider, logger types.CommonLogger, rawPem, rawCer string) PaymentOut {
	var n *nap.Nap
	if tp != nil {
		n = nap.NewOtel(otelhttp.WithTracerProvider(tp))
	} else {
		n = nap.New()
	}

	n.Base(host)
	n.CreatePrometheusVec(nap.NapCounterVec())
	n.SetHeader("IDN-App", idn)

	decodedPem, err := base64.StdEncoding.DecodeString(rawPem)
	if err != nil {
		log.Fatal("Error decoding vpbank pem:", l.Error(err))
	}

	decodedCer, err := base64.StdEncoding.DecodeString(rawCer)
	if err != nil {
		log.Fatal("Error decoding vpbank cer:", l.Error(err))
	}

	return &VpbankPayment{
		host:   host,
		bearer: bearer,
		nap:    n,
		cache:  cache.New(1*time.Hour, 2*time.Hour),
		logger: logger,
		pem:    decodedPem,
		cer:    decodedCer,
	}
}

type GrantAccessResponse struct {
	AccessToken string `json:"access_token"`
	Scope       string `json:"scope"`
	TokenType   string `json:"token_type"`
	ExpiresIn   int64  `json:"expires_in"`
}

type GetAccountInfoResponse struct {
	AccountNumber          string `json:"accountNumber"`
	Currency               string `json:"currency"`
	AccountName            string `json:"accountName"`
	AvailableBalance       string `json:"availableBalance"`
	availableBalanceAmount *float64
}

func (s *GetAccountInfoResponse) GetAvailableBlance() float64 {
	if s.availableBalanceAmount != nil {
		return *s.availableBalanceAmount
	}

	balance, _ := strconv.ParseFloat(s.AvailableBalance, 64)
	s.availableBalanceAmount = &balance

	return *s.availableBalanceAmount
}

func (s *GetAccountInfoResponse) GetAvailableBlanceCurrency() string {

	return kitutils.FormatVNDCurrency(s.GetAvailableBlance())
}

type FTDetail struct {
	FTDetail        interface{} `json:"FTDetail"`
	DebitAccount    string      `json:"debitAccount"`
	DebitAmount     string      `json:"debitAmount"`
	DebitCurrency   string      `json:"debitCurrency"`
	CreditAccount   string      `json:"creditAccount"`
	CreditAmount    string      `json:"creditAmount"`
	CreditCurrency  string      `json:"creditCurrency"`
	ChargeAccount   string      `json:"chargeAccount"`
	ChargeAmount    string      `json:"chargeAmount"`
	ChargeCurrency  string      `json:"chargeCurrency"`
	BookingBranchNo string      `json:"bookingBranchNo"`
	TransactionId   string      `json:"transactionId"`
	TransactionDate string      `json:"transactionDate"`
	Status          string      `json:"status"`
	Remark          string      `json:"remark"`
	Inputter        string      `json:"inputter"`
}

type GetTransactionInfoResponse = []*FTDetail

type InternalTransferRequest struct {
	ReferenceNumber string `json:"referenceNumber"`
	Service         string `json:"service"`
	TransactionType string `json:"transactionType"`
	SourceNumber    string `json:"sourceNumber"`
	TargetNumber    string `json:"targetNumber"`
	BenName         string `json:"benName"`
	Amount          int64  `json:"amount"`
	Currency        string `json:"currency"`
	Remark          string `json:"remark"`
	Signature       string `json:"signature"`
}

type ExternalTransferRequest struct {
	ReferenceNumber string `json:"referenceNumber"`
	Service         string `json:"service"`
	TransactionType string `json:"transactionType"`
	SourceNumber    string `json:"sourceNumber"`
	TargetNumber    string `json:"targetNumber"`
	BenName         string `json:"benName"`
	Amount          int64  `json:"amount"`
	Currency        string `json:"currency"`
	Remark          string `json:"remark"`
	BankID          string `json:"bankId"`
	CallbackURL     string `json:"callbackUrl"`
	TargetAddress   string `json:"targetAddress"`
	Signature       string `json:"signature"`
}

type TransferResponse struct {
	ReferenceNumber string `json:"referenceNumber"`
	TransactionID   string `json:"transactionId"`
	TransferResult  string `json:"transferResult"`
	TransactionDate string `json:"transactionDate"`
	TransferDate    string `json:"transferDate"`
	Signature       string `json:"signature"`
}

type FailedTransferResponse struct {
	Errormessage string `json:"errormessage"`
	Message      string `json:"message"`
	ErrorMessage string `json:"errorMessage"`
}

func (f *FailedTransferResponse) AllMessage() string {
	if f == nil {
		return ""
	}

	return f.ErrorMessage + f.Message + f.ErrorMessage
}

var vpBankDefault = &pb.Bank{
	Id:        0,
	Name:      "Ngân hàng TMCP Việt Nam Thịnh Vượng",
	ShortName: "VPBANK",
	Bin:       "970466",
}

func IsVPBank(bin string) bool {
	return bin == vpBankDefault.Bin
}

func (v *VpbankPayment) getAccessToken() string {
	resp := GrantAccessResponse{}
	var failureResp interface{}

	v.nap.SetHeader("Authorization", "Basic "+v.bearer)
	r, err := v.nap.Post(tokenPath).BodyUrlEncode(map[string]string{
		"scope":      "make_internal_transfer init_payments_data_read make_external_fund_transfer own_trasfer_history_read",
		"grant_type": "client_credentials",
	}).Receive(&resp, &failureResp)

	if err != nil || r.StatusCode != http.StatusOK {
		var (
			bodyBytes   []byte
			readBodyErr error
		)
		if r != nil {
			bodyBytes, readBodyErr = io.ReadAll(r.Body)
		}
		v.logger.Error("getAccessToken error", l.Error(err), l.String("resp", string(bodyBytes)), l.String("readBodyErr", readBodyErr.Error()), l.Any("failed", failureResp))
	}

	return resp.AccessToken
}

func (v *VpbankPayment) genSignature(data string) string {
	block, _ := pem.Decode(v.pem)
	if block == nil {
		log.Printf("failed to decode PEM block containing private key")
		return ""
	}
	privateKeyRaw, err := x509.ParsePKCS8PrivateKey(block.Bytes)
	if err != nil {
		log.Printf("generateSignature() data %s ex: %s", data, err)
		return ""
	}

	// Convert to specific RSA private key type
	privateKey, ok := privateKeyRaw.(*rsa.PrivateKey)
	if !ok {
		log.Printf("failed to convert to RSA private key")
		return ""
	}

	// Base64 encode the data
	base64Data := base64.StdEncoding.EncodeToString([]byte(data))

	// Create a signature
	hash := sha256.New()
	hash.Write([]byte(base64Data))
	hashed := hash.Sum(nil)
	signature, err := rsa.SignPKCS1v15(rand.Reader, privateKey, crypto.SHA256, hashed)
	if err != nil {
		log.Printf("generateSignature() data %s ex: %s", data, err)
		return ""
	}

	// Base64 encode the signature
	base64Signature := base64.StdEncoding.EncodeToString(signature)
	return base64Signature
}

func (v *VpbankPayment) UpdateListBank() error {
	accessToken := v.getAccessToken()
	if accessToken == "" {
		log.Printf("can not get access token")
		return errors.New("can not get access token to update list bank")
	}

	resp := ListBanksResponse{}

	v.nap.SetHeader("x-request-id", strconv.Itoa(int(time.Now().Unix())))
	v.nap.SetAuthToken(accessToken)
	v.nap.Get(listBanksPath).ReceiveSuccess(&resp)

	banks := make([]*pb.Bank, len(resp.BankList))
	for i := range resp.BankList {
		banks[i] = resp.BankList[i].ToProto()
	}

	if len(banks) > 0 {
		banks = append([]*pb.Bank{vpBankDefault}, banks...)
		v.cache.Set(keyListBank, banks, cache.NoExpiration)
		return nil
	} else {
		log.Printf("list bank is empty")
		return errors.New("list bank is empty")
	}
}

func (v *VpbankPayment) ListBanks() []*pb.Bank {
	if c, found := v.cache.Get(keyListBank); found {
		return c.([]*pb.Bank)
	}

	accessToken := v.getAccessToken()
	if accessToken == "" {
		log.Printf("Can not get access token")
		return nil
	}

	resp := ListBanksResponse{}

	v.nap.SetHeader("x-request-id", strconv.Itoa(int(time.Now().Unix())))
	v.nap.SetAuthToken(accessToken)
	v.nap.Get(listBanksPath).ReceiveSuccess(&resp)

	banks := make([]*pb.Bank, len(resp.BankList))
	for i := range resp.BankList {
		banks[i] = resp.BankList[i].ToProto()
	}

	if len(banks) > 0 {
		banks = append([]*pb.Bank{vpBankDefault}, banks...)
		v.cache.Set(keyListBank, banks, cache.NoExpiration)
	}

	return banks
}

func (v *VpbankPayment) GetBankInfoByBin(bin string) *pb.Bank {
	banks := v.ListBanks()
	for _, bank := range banks {
		if bank.Bin == bin {
			return bank
		}
	}

	return nil
}

func (v *VpbankPayment) GetBeneficiaryInfo(bin, benNumber, debitName string) *models.AccountVerificationVPBankReturn {
	accessToken := v.getAccessToken()
	if accessToken == "" {
		return nil
	}

	var outputInterface interface{}
	//v.nap.SetAuthToken(accessToken)
	v.nap.SetHeader("Authorization", "Bearer "+accessToken)
	v.nap.SetHeader("X-Request-Id", strconv.Itoa(int(time.Now().Unix())))

	v.nap.SetHeader("benNumber", benNumber)
	v.nap.SetHeader("debitName", debitName)

	if !IsVPBank(bin) {
		v.nap.SetHeader("bankId", bin)
		v.nap.Get(externalBeneficiaryPath).ReceiveSuccess(&outputInterface)
	} else {
		v.nap.Get(internalBeneficiaryPath).ReceiveSuccess(&outputInterface)
	}

	if outputInterface != nil {

		beneficiaryName, ok := outputInterface.(map[string]interface{})["beneficiaryName"]
		if !ok {
			beneficiaryName = ""
		}

		customerNumber, ok := outputInterface.(map[string]interface{})["customerNumber"]
		if !ok {
			customerNumber = ""
		}

		return &models.AccountVerificationVPBankReturn{
			BeneficiaryName: beneficiaryName.(string),
			CustomerNumber:  customerNumber.(string),
		}

	}
	return nil
}

func (v *VpbankPayment) GetAccountInfo(accountNumber string) *GetAccountInfoResponse {
	accessToken := v.getAccessToken()
	if accessToken == "" {
		return nil
	}

	successResp := GetAccountInfoResponse{}
	var failureResp interface{}

	v.nap.SetHeader("x-request-id", strconv.Itoa(int(time.Now().Unix())))
	v.nap.SetAuthToken(accessToken)
	v.nap.SetHeader("accountNumber", accountNumber)

	r := v.nap.Get(accountInfoPath)
	resp, err := r.Receive(&successResp, &failureResp)
	if err != nil || resp.StatusCode != http.StatusOK {
		request, _ := r.Request()
		command, _ := http2curl.GetCurlCommand(request)
		v.logger.Error("GetAccountInfo failed", l.Any("accountNumber", accountNumber), l.Any("failure_resp", failureResp), l.Any("curl_command", command))
	}

	v.logger.Info("ReceiveSuccess", l.Any("successResp", successResp))

	return &successResp
}

func (v *VpbankPayment) GetTransactionInfo(referenceNumber, accountNumber string) GetTransactionInfoResponse {
	accessToken := v.getAccessToken()
	if accessToken == "" {
		return nil
	}

	successResp := GetTransactionInfoResponse{}
	var failureResp interface{}

	v.nap.SetHeader("x-request-id", strconv.Itoa(int(time.Now().Unix())))
	v.nap.SetAuthToken(accessToken)
	v.nap.SetHeader("referenceNumber", referenceNumber)
	v.nap.SetHeader("accountNumber", accountNumber)

	resp, err := v.nap.Get(ftListPath).Clone().
		AutoRetry(nap.WithRetryTimes(3), nap.WithRetryWaitMin(5*time.Minute)).Receive(&successResp, &failureResp)
	if err != nil {
		v.logger.Error("GetTransactionInfo error", l.Error(err))
	}

	if resp.StatusCode != http.StatusOK {
		v.logger.Error("GetTransactionInfo failed", l.Any("failure_resp", failureResp))
	}

	return successResp
}

func (v *VpbankPayment) InternalTransfer(req InternalTransferRequest) (*TransferResponse, error) {
	accessToken := v.getAccessToken()
	if accessToken == "" {
		return nil, errors.New("invalid token")
	}

	var successResp TransferResponse
	var failureResp FailedTransferResponse
	v.nap.SetHeader("Authorization", "Bearer "+accessToken)
	v.nap.SetHeader("X-Request-Id", strconv.Itoa(int(time.Now().Unix())))

	req.Signature = v.genSignature(fmt.Sprintf("%s%s%s%d", req.ReferenceNumber, req.SourceNumber, req.TargetNumber, req.Amount))
	resp, err := v.nap.Post(internalTransferPath).BodyJSON(req).Receive(&successResp, &failureResp)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		v.logger.Error("InternalTransfer failed", l.Any("req", req), l.Any("failure_resp", failureResp))
		return nil, errors.New(failureResp.AllMessage())
	}

	return &successResp, nil
}

func (v *VpbankPayment) ExternalTransfer(req ExternalTransferRequest) (*TransferResponse, error) {
	accessToken := v.getAccessToken()
	if accessToken == "" {
		return nil, errors.New("invalid token")
	}

	var successResp TransferResponse
	var failureResp FailedTransferResponse

	v.nap.SetHeader("Authorization", "Bearer "+accessToken)
	v.nap.SetHeader("X-Request-Id", strconv.Itoa(int(time.Now().Unix())))

	req.Signature = v.genSignature(fmt.Sprintf("%s%s%s%d", req.ReferenceNumber, req.SourceNumber, req.TargetNumber, req.Amount))
	n := v.nap.Post(externalTransferPath).BodyJSON(req)

	resp, err := n.Receive(&successResp, &failureResp)
	if err != nil {
		request, _ := n.Request()
		command, _ := http2curl.GetCurlCommand(request)
		v.logger.Error("ExternalTransfer failed", l.Any("req", req), l.Any("failure_resp", failureResp), l.Any("curl_command", command))
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		v.logger.Error("ExternalTransfer failed", l.Any("req", req), l.Any("failure_resp", failureResp))
		return nil, errors.New(failureResp.AllMessage())
	}

	return &successResp, nil
}
