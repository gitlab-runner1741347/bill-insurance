package disbursement

import (
	"git.kafefin.net/backend/central-payout/pb"
	"git.kafefin.net/backend/central-payout/pkg/models"
)

type PaymentOut interface {
	ListBanks() []*pb.Bank
	UpdateListBank() error
	GetBeneficiaryInfo(bankId, benNumber, debitName string) *models.AccountVerificationVPBankReturn
	GetBankInfoByBin(bin string) *pb.Bank
	GetAccountInfo(accountNumber string) *GetAccountInfoResponse
	GetTransactionInfo(referenceNumber, accountNumber string) GetTransactionInfoResponse
	InternalTransfer(req InternalTransferRequest) (*TransferResponse, error)
	ExternalTransfer(req ExternalTransferRequest) (*TransferResponse, error)
}

type BankInfo struct {
	ID           int64  `json:"id"`
	BankName     string `json:"bankName"`
	ShortName    string `json:"shortName"`
	Bin          string `json:"bin"`
	NapasCard    bool   `json:"napasCard"`
	NapasAccount bool   `json:"napasAccount"`
}

func (b BankInfo) ToProto() *pb.Bank {
	return &pb.Bank{
		Id:        b.ID,
		Code:      b.ShortName,
		Name:      b.BankName,
		ShortName: b.ShortName,
		NameEn:    "",
		Bin:       b.Bin,
		Icon:      "", // TODO
	}
}

type ListBanksResponse struct {
	BankList []BankInfo `json:"bankList"`
}
