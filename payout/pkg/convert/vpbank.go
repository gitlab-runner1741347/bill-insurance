package convert

import "git.kafefin.net/backend/central-payout/pb"

var VpbTransferSttTxnSttMap = map[string]pb.PaymentTransactionStatus{
	"complete": pb.PaymentTransactionStatus_PTS_SUCCESS,
	"pending":  pb.PaymentTransactionStatus_PTS_PENDING,
	"failed":   pb.PaymentTransactionStatus_PTS_FAILED,
}

func VPBTxnSttToOurStt(stt string) pb.PaymentTransactionStatus {
	return VpbTransferSttTxnSttMap[stt]
}
