package convert

import (
	"git.kafefin.net/backend/central-payout/internal/models"
	"git.kafefin.net/backend/central-payout/pb"
)

func PaymentTransactionToAdminPaymentTransactionPb(m *models.PaymentTransaction) *pb.AdminPaymentTransaction {
	return &pb.AdminPaymentTransaction{
		ReferenceNumber: m.ID,
		Status:          m.Status,
		TransactionDate: m.CreatedAt.UnixMilli(),
		ErrorMessage:    m.ErrorMessage,
	}
}

func PaymentTransactionsToAdminPaymentTransactionPbs(transactions []*models.PaymentTransaction) []*pb.AdminPaymentTransaction {
	ret := make([]*pb.AdminPaymentTransaction, len(transactions))

	for i := range transactions {
		ret[i] = PaymentTransactionToAdminPaymentTransactionPb(transactions[i])
	}

	return ret
}
