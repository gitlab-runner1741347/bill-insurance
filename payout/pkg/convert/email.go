package convert

import sldMessagePb "git.kafefin.net/backend/saladin-central-proto/saladin_message"

type SendEmailRequest struct {
	TemplateId    string
	Name          string
	Email         string
	Source        string
	ReqBodyParams []*sldMessagePb.Values
	Attachments   []Attachment
	Cc            []EmailTo
}

type Attachment struct {
	FileName string
	FileURL  string
}

type EmailTo struct {
	Name  string
	Email string
}
