package convert

import (
	"git.kafefin.net/backend/central-payout/internal/models"
	"git.kafefin.net/backend/central-payout/pb"
)

func PaymentRequestToPb(m models.PaymentRequest, bank *pb.Bank) *pb.Payment {
	return &pb.Payment{
		Id:            m.ID,
		SrcId:         m.SrcID,
		PaymentType:   m.PaymentType,
		Beneficiary:   ToBeneficiaryPb(m, bank),
		Debtor:        m.Debtor,
		CampaignCode:  m.CampaignCode,
		CampaignName:  m.CampaignName,
		Status:        m.Status,
		PaymentStatus: m.TxnStatus,
	}
}
