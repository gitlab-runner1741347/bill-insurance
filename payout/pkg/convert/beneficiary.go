package convert

import (
	"git.kafefin.net/backend/central-payout/internal/models"
	"git.kafefin.net/backend/central-payout/pb"
)

func ToBeneficiaryPb(m models.PaymentRequest, bank *pb.Bank) *pb.Beneficiary {
	return &pb.Beneficiary{
		SldBankId:     m.SldBankID,
		BankName:      bank.Name,
		ShortName:     bank.ShortName,
		AccountNumber: m.AccountNumber,
		AccountName:   m.AccountName,
		Bin:           bank.Bin,
	}
}
