package convert

import (
	"fmt"

	"git.kafefin.net/backend/central-payout/internal/models"
	"git.kafefin.net/backend/central-payout/pb"
	"git.kafefin.net/backend/central-payout/pkg/pmtype"
)

func PaymentRequestToAdminPb(m models.PaymentRequest, host string) *pb.AdminPayment {
	detailURL := m.DetailURL
	if detailURLPattern := pmtype.PaymentTypeMap[m.PaymentType]; detailURLPattern != "" {
		detailURL = fmt.Sprintf(detailURLPattern, host, m.SrcID)
	}

	return &pb.AdminPayment{
		PaymentRequestId:          m.ID,
		OriginRequestId:           m.SrcID,
		OriginRequestType:         m.PaymentType,
		BankName:                  m.BankName,
		BeneficiaryName:           m.AccountNumber,
		Debtor:                    m.GetDebtorName(),
		Campaign:                  m.CampaignName,
		Amount:                    m.PaymentAmount,
		Status:                    m.Status,
		DisbursementOverallStatus: m.GetTxnStatusName(),
		TxnStatus:                 m.TxnStatus,
		CreatedAt:                 m.CreatedAt.UnixMilli(),
		UrlDetail:                 detailURL,
	}
}

func PaymentRequestsToAdminPbs(payments []*models.PaymentRequest, host string) []*pb.AdminPayment {
	ret := make([]*pb.AdminPayment, 0)

	for _, payment := range payments {
		ret = append(ret, PaymentRequestToAdminPb(*payment, host))
	}

	return ret
}

func ToAdminBeneficiaryPb(m models.PaymentRequest, bank *pb.Bank) *pb.AdminBeneficiary {
	return &pb.AdminBeneficiary{
		Name:          m.AccountName,
		Phone:         m.BeneficiaryPhone,
		BankName:      m.BankName,
		BankBranch:    m.BankBranch,
		BankId:        m.SldBankID,
		ShortBankName: bank.GetShortName(),
		BankAccount:   m.AccountNumber,
		Bin:           bank.GetBin(),
	}
}

func PaymentRequestToAdminDetailPb(m models.PaymentRequestDetail, bank *pb.Bank, host string) *pb.AdminPaymentDetail {
	urlDetail := ""
	if urlDetailPattern := pmtype.PaymentTypeMap[m.PaymentType]; urlDetailPattern != "" {
		urlDetail = fmt.Sprintf(urlDetailPattern, host, m.SrcID)
	}

	return &pb.AdminPaymentDetail{
		PaymentRequestId:          m.ID,
		OriginRequestId:           m.SrcID,
		OriginRequestType:         m.PaymentType,
		Debtor:                    m.GetDebtorName(),
		CampaignCode:              m.CampaignCode,
		Campaign:                  m.CampaignName,
		Amount:                    m.PaymentAmount,
		Status:                    m.Status,
		DisbursementOverallStatus: m.GetTxnStatusName(),
		TxnStatus:                 m.TxnStatus,
		CreatedAt:                 m.CreatedAt.UnixMilli(),
		BeneficiaryInfo:           ToAdminBeneficiaryPb(m.PaymentRequest, bank),
		NoteOfOriginRequest:       m.SrcNote,
		DisbursementTransactions:  PaymentTransactionsToAdminPaymentTransactionPbs(m.PaymentTransactions),
		NoteOfAccountant:          m.AccountantNote,
		NoteOfCeo:                 m.CeoNote,
		UrlDetail:                 urlDetail,
	}
}
