package convert

import (
	"fmt"

	"git.kafefin.net/backend/central-payout/internal/models"
	"git.kafefin.net/backend/central-payout/pkg/datetimeutils"
	messagePb "git.kafefin.net/backend/saladin-central-proto/saladin_message"
)

func PaymentRequestToMessageValues(urlDetal string, paymentRequest *models.PaymentRequest) []*messagePb.Values {
	return []*messagePb.Values{
		{
			Key:    "pr_id",
			Values: []string{fmt.Sprintf("%d", paymentRequest.ID)},
		},
		{
			Key:    "payment_type",
			Values: []string{paymentRequest.PaymentType},
		},
		{
			Key:    "pr_amount",
			Values: []string{fmt.Sprintf("%d", paymentRequest.PaymentAmount)},
		},
		{
			Key:    "pr_status",
			Values: []string{paymentRequest.Status.String()},
		},
		{
			Key:    "pr_detail_url",
			Values: []string{urlDetal},
		},
		{
			Key:    "created_date",
			Values: []string{datetimeutils.TimeToStringTime(paymentRequest.CreatedAt, datetimeutils.DDMMYYYYYLayout)},
		},
	}
}
