package datetimeutils

import (
	"time"
)

func TimeToString(t *time.Time, layout string) string {
	if t == nil {
		return ""
	}

	return t.Format(layout)
}
