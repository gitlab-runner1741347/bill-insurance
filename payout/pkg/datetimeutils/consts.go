package datetimeutils

import (
	"time"

	"google.golang.org/protobuf/types/known/timestamppb"
)

const (
	DateLayout               = "2006-01-02"
	DateHMLayout             = "2006-01-02 15:04"
	DateTimeLayout           = "2006-01-02 15:04:05"
	DDMMYYYYYHHMMLayout      = "02/01/2006 15:04"
	DDMMYYYYYLayout          = "02/01/2006"
	VPBTransferDateLayout    = "2006-01-02"
	VPBTransactionDateLayout = "2006-01-02 15:04:05"
)

const (
	DayDuration = time.Hour * 24
)

func UnixMilliToPTime(i int64) *time.Time {
	if i == 0 {
		return nil
	}

	t := time.UnixMilli(i)
	return &t
}

func PTimeToUnixMilli(t *time.Time) int64 {
	if t == nil {
		return 0
	}

	return t.UnixMilli()
}

func TimeToStringTime(t time.Time, format string) string {
	return t.Format(format)
}

func PTimeToStringTime(t *time.Time, format string) string {
	if t == nil {
		return ""
	}

	return (*t).Format(format)
}

func TimeToStringDate(t time.Time) string {
	return t.Format(DDMMYYYYYLayout)
}

func PTimeToStringDate(t *time.Time) string {
	if t == nil {
		return ""
	}

	return (*t).Format(DDMMYYYYYLayout)
}

func TimeToP7StringTime(t time.Time, format string) string {
	return t.Add(7 * time.Hour).Format(format)
}

func PTimeToP7StringTime(t *time.Time, format string) string {
	if t == nil {
		return ""
	}

	return (*t).Add(7 * time.Hour).Format(format)
}

func TimeToP7StringDate(t time.Time) string {
	return t.Add(7 * time.Hour).Format(DDMMYYYYYLayout)
}

func PTimeToP7StringDate(t *time.Time) string {
	if t == nil {
		return ""
	}

	return (*t).Add(7 * time.Hour).Format(DDMMYYYYYLayout)
}

func TimeIn(t time.Time, name string) (time.Time, error) {
	loc, err := time.LoadLocation(name)
	if err == nil {
		t = t.In(loc)
	}
	return t, err
}

func TimeInVN(t time.Time) time.Time {
	t, _ = TimeIn(t, "Asia/Ho_Chi_Minh")

	return t
}

func TimeToProto(t *time.Time) *timestamppb.Timestamp {
	if t == nil {
		return nil
	}

	return timestamppb.New(*t)
}

func ProtoToTime(t *timestamppb.Timestamp) *time.Time {
	if t == nil {
		return nil
	}

	ret := t.AsTime()

	return &ret
}

func ConvertProtoTimestamp(protoTimestamp *timestamppb.Timestamp) *time.Time {
	if protoTimestamp == nil {
		return nil
	}

	timeValue := protoTimestamp.AsTime()
	if !protoTimestamp.IsValid() {
		return nil
	}

	return &timeValue
}

func ConvertToProtoTimestamp(timestamp time.Time) *timestamppb.Timestamp {
	protoTimestamp := timestamppb.New(timestamp)
	if !protoTimestamp.IsValid() {
		return nil
	}
	return protoTimestamp
}

func FormatToYYYYMMDD(timestamp time.Time) string {
	formattedTime := timestamp.Format("2006-01-02")
	return formattedTime
}

func ParseFromStringToTime(input string) (time.Time, error) {
	layout := "2006-01-02"
	parsedTime, err := time.Parse(layout, input)
	if err != nil {
		return time.Time{}, err
	}
	return parsedTime, nil
}
