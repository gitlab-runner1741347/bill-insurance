package datetimeutils

import (
	"errors"
	"math"
	"time"
)

func StringDateToTime(s string) (time.Time, error) {
	return time.Parse(DateLayout, s)
}

func StringDateHMToTime(s string) (time.Time, error) {
	return time.Parse(DateHMLayout, s)
}

func StringDateHMToTimeWithTz(s string, l *time.Location) (time.Time, error) {
	if l == nil {
		return time.Time{}, errors.New("invalid location")
	}
	return time.ParseInLocation(DateHMLayout, s, l)
}

func ToDateString(t time.Time) string {
	return t.Format(DateLayout)
}

func ToDateTimeString(t time.Time) string {
	return t.Format(DateTimeLayout)
}

func DurationToDays(d time.Duration) uint32 {
	return uint32(math.Floor(float64(d)/float64(DayDuration))) + 1
}

func ToDDMMYYYHHMM(s int64) string {
	result := time.UnixMilli(s)
	return result.Format(DDMMYYYYYHHMMLayout)
}

func ToDDMMYYYY(s int64) string {
	result := time.UnixMilli(s)
	return result.Format(DDMMYYYYYLayout)
}

func StringToTime(v string, layout string) *time.Time {
	t, _ := time.Parse(layout, v)

	if t.IsZero() {
		return nil
	}

	return &t
}

func StringToTimeWithTz(v string, layout string, l *time.Location) *time.Time {
	var t time.Time

	if l != nil {
		t, _ = time.ParseInLocation(layout, v, l)
	} else {
		t, _ = time.Parse(layout, v)
	}

	if t.IsZero() {
		return nil
	}

	return &t
}
