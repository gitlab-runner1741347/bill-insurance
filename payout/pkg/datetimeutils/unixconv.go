package datetimeutils

import "time"

func UnixToTime(value int64, unit time.Duration) *time.Time {
	if value == 0 {
		return nil
	}

	var t time.Time
	switch unit {
	case time.Second:
		t = time.Unix(value, 0)
	case time.Microsecond:
		t = time.UnixMicro(value)
	case time.Millisecond:
		t = time.UnixMilli(value)
	case time.Nanosecond:
		t = time.Unix(0, value)
	}

	return &t
}

func TimeToUnix(t *time.Time, unit time.Duration) int64 {
	if t == nil {
		return 0
	}

	switch unit {
	case time.Second:
		return t.Unix()
	case time.Microsecond:
		return t.UnixMicro()
	case time.Millisecond:
		return t.UnixMilli()
	case time.Nanosecond:
		return t.UnixNano()
	default:
		return 0
	}
}
