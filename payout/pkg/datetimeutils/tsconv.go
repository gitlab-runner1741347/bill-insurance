package datetimeutils

import (
	"time"

	"google.golang.org/protobuf/types/known/timestamppb"
)

func ProtoTimestampToTime(protoTimestamp *timestamppb.Timestamp) *time.Time {
	if !protoTimestamp.IsValid() {
		return nil
	}

	timeValue := protoTimestamp.AsTime()

	return &timeValue
}

func TimeToProtoTimestamp(timestamp *time.Time) *timestamppb.Timestamp {
	if timestamp == nil {
		return nil
	}

	protoTimestamp := timestamppb.New(*timestamp)
	if !protoTimestamp.IsValid() {
		return nil
	}

	return protoTimestamp
}
