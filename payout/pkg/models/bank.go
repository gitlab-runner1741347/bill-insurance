package models

type AccountVerificationVPBankReturn struct {
	BeneficiaryName string `json:"beneficiaryName"`
	CustomerNumber  string `json:"customerNumber"`
}
