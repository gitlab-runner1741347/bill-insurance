package pmtype

const deliveryClaimDashboardURL = "%s/dashboard/claims/delivery/%s"
const travalClaimDashboardURL = "%s/dashboard/travel/claims/%s"
const tripClaimDashboardURL = "%s/dashboard/trip/claims/%s"

var PaymentTypeMap = map[string]string{
	"ClaimGITI":          deliveryClaimDashboardURL,
	"ClaimGITIDelayed":   deliveryClaimDashboardURL,
	"ClaimTravelDelayed": travalClaimDashboardURL,
	"ClaimTripCancelled": tripClaimDashboardURL,
}
