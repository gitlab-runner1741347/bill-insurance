package publisher

import (
	"context"
	"encoding/json"

	cpsub "git.kafefin.net/backend/central-payout/internal/subscribers/central-payout"
	"git.kafefin.net/backend/central-payout/pkg/pubsub"
	sqsPubsub "git.kafefin.net/backend/central-payout/pkg/pubsub/sqs"

	"github.com/hashicorp/go-multierror"
)

func PublishProcessPaymentTransactionMessage(ctx context.Context, pub pubsub.Publisher, queueURL string, paymentRequestID int64) error {
	msgBody, err := json.Marshal(cpsub.ProcessPaymentTransactionMessage{Params: struct {
		PaymentRequestID int64 `json:"payment_request_id"`
	}{
		PaymentRequestID: paymentRequestID,
	}})
	if err != nil {
		return multierror.Prefix(err, "Marshal materialSub.ProcessPaymentTransactionMessage")
	}
	message := sqsPubsub.NewPublishMessage(string(msgBody))
	if err := pub.Publish(ctx, queueURL, message); err != nil {
		return multierror.Prefix(err, "pub.Publish")
	}

	return nil
}
