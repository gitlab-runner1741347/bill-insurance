package consts

import (
	"fmt"
	"time"
)

const (
	PrefixService = "central-payout"
)

const (
	DefaultExpirationAccountNumber = 12 * time.Hour
)

func GenKeyForCacheBankAccount(bankId int64, accountNumber string) string {
	return fmt.Sprintf("%s:%d:%s", PrefixService, bankId, accountNumber)
}
