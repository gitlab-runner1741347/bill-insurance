package consts

import "git.kafefin.net/backend/central-payout/pb"

const (
	WithdrawFirstOrder = "withdraw_first_order"
	ClaimGITI          = "ClaimGITI"
	ClaimGITIDelayed   = "ClaimGITIDelayed"
	ClaimTravelDelayed = "ClaimTravelDelayed"
	ClaimTripCancelled = "ClaimTripCancelled"
)

var PaymentTypes = []*pb.PaymentType{
	{
		Code: WithdrawFirstOrder,
		Name: "Chi trả đơn hàng",
	},
	{
		Code: ClaimGITI,
		Name: "Bồi thường (GITI)",
	},
	{
		Code: ClaimGITIDelayed,
		Name: "Bồi thường giao trễ (GITI)",
	},
	{
		Code: ClaimTravelDelayed,
		Name: "Bồi thường trễ hủy chuyến (Travel)",
	},
	{
		Code: ClaimTripCancelled,
		Name: "Bồi thường hủy chuyến (Trip)",
	},
}
