package consts

import (
	"fmt"

	"git.kafefin.net/backend/kitchen/ecode"
	"google.golang.org/grpc/codes"
)

var (
	InvalidBalanceAmount = func(args ...any) *ecode.Status {
		return ecode.Error(codes.Code(36001), fmt.Sprintf("Số dư tài khoản (<strong>%v</strong> đ) không đủ để tiếp tục", args...))
	}
	InvalidTotalTxnAmount = func(args ...any) *ecode.Status {
		return ecode.Error(codes.Code(36002), fmt.Sprintf("Yêu cầu thanh toán vượt quá hạn mức <strong>%v</strong> đ/Ngày. Các giao dịch sẽ cần cấp duyệt tiếp theo (CEO).", args...))
	}
	InvalidSomeTxnAmount = func(args ...any) *ecode.Status {
		return ecode.Error(codes.Code(36003), fmt.Sprintf("Các Yêu cầu thanh toán vượt quá hạn mức <strong>%v</strong> đ/GD. Các các Yêu cầu thanh toán trên <strong>%v</strong> đ sẽ cần cấp duyệt tiếp theo (CEO).", args...))
	}
)
