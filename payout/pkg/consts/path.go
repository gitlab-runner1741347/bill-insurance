package consts

import "fmt"

const (
	PathAcountantPaymentRequest = "dashboard/accountant/payment-requests-new"
)

func DetailUrlPaymentRequest(host, source string, id int64) string {
	lastChar := host[len(host)-1]
	if lastChar != byte('/') {
		host += "/"
	}
	return fmt.Sprintf("%s%s/%d/%s", host, PathAcountantPaymentRequest, id, source)
}
