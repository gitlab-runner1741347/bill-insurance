package stringutils

import "testing"

func Test_trim_spaces_and_remove_diacritics(t *testing.T) {
	input := "  café  "
	expected := "cafe"

	result := Normalize(input)

	if result != expected {
		t.Errorf("Expected %s, but got %s", expected, result)
	}
}

func Test_remove_special_characters(t *testing.T) {
	input := "Hello!@#$%^&*()_+123 -World"
	expected := "Hello123 -World"

	result := Normalize(input)

	if result != expected {
		t.Errorf("Expected %s, but got %s", expected, result)
	}
}

func Test_replace_special_characters(t *testing.T) {
	input := "Café"
	expected := "Cafe"

	result := Normalize(input)

	if result != expected {
		t.Errorf("Expected %s, but got %s", expected, result)
	}
}

func Test_empty_string_input(t *testing.T) {
	input := ""
	expected := ""

	result := Normalize(input)

	if result != expected {
		t.Errorf("Expected %s, but got %s", expected, result)
	}
}

func Test_only_whitespace_input(t *testing.T) {
	input := "     "
	expected := ""

	result := Normalize(input)

	if result != expected {
		t.Errorf("Expected %s, but got %s", expected, result)
	}
}

func Test_only_special_characters_input(t *testing.T) {
	input := "!@#$%^&*()"
	expected := ""

	result := Normalize(input)

	if result != expected {
		t.Errorf("Expected %s, but got %s", expected, result)
	}
}
