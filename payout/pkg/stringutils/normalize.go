package stringutils

import (
	"regexp"
	"sort"
	"strings"
	"unicode"

	"golang.org/x/text/runes"
	"golang.org/x/text/transform"
	"golang.org/x/text/unicode/norm"
)

func Normalize(input string) string {
	input = strings.TrimSpace(input)

	rMapping := func(r rune) rune {
		sortedSpecialRunes := []rune{'Đ', 'đ'}
		replacedByRunes := []rune{'D', 'd'}

		pos := sort.Search(len(sortedSpecialRunes), func(i int) bool { return sortedSpecialRunes[i] >= r })
		if pos != -1 && r == sortedSpecialRunes[pos] {
			return replacedByRunes[pos]
		}
		return r
	}

	t := transform.Chain(norm.NFD, runes.Remove(runes.In(unicode.Mn)), norm.NFC, runes.Map(rMapping))
	strTransform, _, _ := transform.String(t, input)

	str := regexp.MustCompile(`[^a-zA-Z0-9 \-]+`).ReplaceAllString(strTransform, "")

	return str
}
