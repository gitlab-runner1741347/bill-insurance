package stringutils

import (
	"math/rand"
	"regexp"
	"strings"
	"time"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
var lowerLetterRunes = []rune("abcdefghijklmnopqrstuvwxyz")

func RandStringRunes(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))] //nolint
	}
	return string(b)
}

func RandLowwerStringRunes(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = lowerLetterRunes[rand.Intn(len(lowerLetterRunes))] //nolint
	}
	return string(b)
}

/** * Bo dau 1 chuoi * * @param s * @return */

func LowerAndRemoveAccent(s string) string {
	s = strings.ToLower(s)

	var RegexpA = `à|á|ạ|ã|ả|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ`
	var RegexpE = `è|ẻ|ẽ|é|ẹ|ê|ề|ể|ễ|ế|ệ`
	var RegexpI = `ì|ỉ|ĩ|í|ị`
	var RegexpU = `ù|ủ|ũ|ú|ụ|ư|ừ|ử|ữ|ứ|ự`
	var RegexpY = `ỳ|ỷ|ỹ|ý|ỵ`
	var RegexpO = `ò|ỏ|õ|ó|ọ|ô|ồ|ổ|ỗ|ố|ộ|ơ|ờ|ở|ỡ|ớ|ợ`
	var RegexpD = `Đ|đ`
	regA := regexp.MustCompile(RegexpA)
	regE := regexp.MustCompile(RegexpE)
	regI := regexp.MustCompile(RegexpI)
	regO := regexp.MustCompile(RegexpO)
	regU := regexp.MustCompile(RegexpU)
	regY := regexp.MustCompile(RegexpY)
	regD := regexp.MustCompile(RegexpD)
	s = regA.ReplaceAllLiteralString(s, "a")
	s = regE.ReplaceAllLiteralString(s, "e")
	s = regI.ReplaceAllLiteralString(s, "i")
	s = regO.ReplaceAllLiteralString(s, "o")
	s = regU.ReplaceAllLiteralString(s, "u")
	s = regY.ReplaceAllLiteralString(s, "y")
	s = regD.ReplaceAllLiteralString(s, "d")

	// regexp remove symbols
	var RegexpPara = `[^\w\s]`
	regPara := regexp.MustCompile(RegexpPara)
	s = regPara.ReplaceAllLiteralString(s, "")

	return s
}

var SOURCE_RUNES = []rune(`ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚÝỲỸàáâãèéêìíòóôõùúýĂăĐđĨĩŨũƠơƯưẠạẢảẤấẦầẨẩẪẫẬậẮắẰằẲẳẴẵẶặẸẹẺẻẼẽẾếỀềỂểỄễỆệỈỉỊịỌọỎỏỐốỒồỔổỖỗỘộỚớỜờỞởỠỡỢợỤụỦủỨứỪừỬửỮữỰự`)
var DESTINATION_RUNES = []rune(`AAAAEEEIIOOOOUUYYYaaaaeeeiioooouuyAaDdIiUuOoUuAaAaAaAaAaAaAaAaAaAaAaAaEeEeEeEeEeEeEeEeIiIiOoOoOoOoOoOoOoOoOoOoOoOoUuUuUuUuUuUuUu`)

func binarySearch(sortedArray []rune, key rune, low int, high int) int {

	var middle int = (low + high) / 2

	if high < low {
		return -1
	}

	switch {
	case key == sortedArray[middle]:
		return middle
	case key < sortedArray[middle]:
		return binarySearch(sortedArray, key, low, middle-1)
	default:
		return binarySearch(sortedArray, key, middle+1, high)
	}

}

/** * Bo dau 1 ky tu * * @param ch * @return */
func RemoveAccentChar(r rune) rune {

	var index int = binarySearch(SOURCE_RUNES, r, 0, len(SOURCE_RUNES))

	if index >= 0 {
		return DESTINATION_RUNES[index]
	}

	return r
}
