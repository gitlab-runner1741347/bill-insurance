# generate by sqlize

ALTER TABLE `payment_request` ADD COLUMN `detail_url` varchar(1000) COMMENT 'detail url' AFTER `payment_type`;
ALTER TABLE `payment_request` ADD COLUMN `idempotent_key` varchar(255) COMMENT 'idempotent key' AFTER `transaction_detail`;
CREATE UNIQUE INDEX `idx_src_service_idempotent_key` ON `payment_request`(`src_service`, `idempotent_key`);