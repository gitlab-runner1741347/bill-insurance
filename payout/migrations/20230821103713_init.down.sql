# generate by sqlize

DROP TABLE IF EXISTS `payment_request`;

DROP TABLE IF EXISTS `payment_transaction`;

DROP TABLE IF EXISTS `disbursement_reconcile`;

DROP TABLE IF EXISTS `daily_payout_total`;

DROP TABLE IF EXISTS `vpb_transaction`;