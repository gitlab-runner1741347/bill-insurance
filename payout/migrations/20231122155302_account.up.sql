# generate by sqlize

ALTER TABLE `payment_request` ADD COLUMN `disbursement_account_number` varchar(255) COMMENT 'disbursement account number' AFTER `idempotent_key`;