# generate by sqlize

ALTER TABLE `payment_request` DROP COLUMN `detail_url`;
ALTER TABLE `payment_request` DROP COLUMN `idempotent_key`;
DROP INDEX `idx_src_service_idempotent_key` ON `payment_request`;