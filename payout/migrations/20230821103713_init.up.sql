# generate by sqlize

CREATE TABLE `payment_request` (
 `id`                 bigint(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
 `src_id`             varchar(255) COMMENT 'src id',
 `payment_type`       varchar(255) COMMENT 'payment type',
 `sld_bank_id`        bigint(20) COMMENT 'sld bank id',
 `account_number`     varchar(255) COMMENT 'account number',
 `account_name`       varchar(255) COMMENT 'account name',
 `beneficiary_phone`  varchar(255) COMMENT 'beneficiary phone',
 `bank_name`          varchar(255) COMMENT 'bank name',
 `bank_branch`        varchar(255) COMMENT 'bank branch',
 `bank_bin`           varchar(255) COMMENT 'bank bin',
 `debtor`             int(11) COMMENT 'debtor',
 `campaign_code`      varchar(255) COMMENT 'campaign code',
 `campaign_name`      varchar(255) COMMENT 'campaign name',
 `payment_amount`     bigint(20) COMMENT 'payment amount',
 `src_service`        varchar(255) COMMENT 'src service',
 `callback_url`       varchar(1000) COMMENT 'callback url',
 `status`             int(11) COMMENT 'status',
 `txn_status`         int(11) COMMENT 'txn status',
 `src_note`           text COMMENT 'src note',
 `accountant_note`    text COMMENT 'accountant note',
 `ceo_note`           text COMMENT 'ceo note',
 `transaction_detail` text COMMENT 'transaction detail',
 `created_at`         datetime DEFAULT CURRENT_TIMESTAMP(),
 `updated_at`         datetime DEFAULT CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP()
);
CREATE INDEX `idx_campaign_code` ON `payment_request`(`campaign_code`);
CREATE INDEX `idx_created_at` ON `payment_request`(`created_at`);

CREATE TABLE `payment_transaction` (
 `id`                         varchar(50) PRIMARY KEY,
 `payment_request_id`         bigint(20) UNSIGNED COMMENT 'payment request id',
 `payment_service_provider`   int(11) COMMENT 'payment service provider',
 `sld_bank_id`                bigint(20) COMMENT 'sld bank id',
 `psp_bank_id`                bigint(20) COMMENT 'psp bank id',
 `psp_bank_code`              varchar(255) COMMENT 'psp bank code',
 `psp_bank_name`              varchar(255) COMMENT 'psp bank name',
 `psp_bank_short_name`        varchar(255) COMMENT 'psp bank short name',
 `psp_bin`                    varchar(255) COMMENT 'psp bin',
 `beneficiary_account_number` varchar(255) COMMENT 'beneficiary account number',
 `beneficiary_account_name`   varchar(255) COMMENT 'beneficiary account name',
 `debit_account_number`       varchar(255) COMMENT 'debit account number',
 `payment_amount`             bigint(20) COMMENT 'payment amount',
 `currency`                   varchar(255) COMMENT 'currency',
 `transaction_detail`         varchar(210) COMMENT 'transaction detail',
 `ref_txn_id`                 varchar(255) COMMENT 'ref txn id',
 `partner_transfer_result`    varchar(255) COMMENT 'partner transfer result',
 `partner_transaction_date`   datetime COMMENT 'partner transaction date',
 `partner_transfer_date`      datetime COMMENT 'partner transfer date',
 `error_message`              text COMMENT 'error message',
 `status`                     int(11) COMMENT 'status',
 `created_at`                 datetime DEFAULT CURRENT_TIMESTAMP(),
 `updated_at`                 datetime DEFAULT CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP()
);

CREATE TABLE `disbursement_reconcile` (
 `id`              bigint(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
 `created_by`      varchar(255) COMMENT 'created by',
 `export_file_url` varchar(255) COMMENT 'export file url',
 `reconcile_date`  varchar(255) COMMENT 'reconcile date',
 `updated_by`      varchar(255) COMMENT 'updated by',
 `created_at`      datetime DEFAULT CURRENT_TIMESTAMP(),
 `updated_at`      datetime DEFAULT CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP()
);

CREATE TABLE `daily_payout_total` (
 `id`           varchar(50) PRIMARY KEY,
 `total_amount` bigint(20) COMMENT 'total amount',
 `created_at`   datetime DEFAULT CURRENT_TIMESTAMP(),
 `updated_at`   datetime DEFAULT CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP()
);

CREATE TABLE `vpb_transaction` (
 `id`               bigint(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
 `reference_number` varchar(50) COMMENT 'reference number',
 `service`          varchar(255) COMMENT 'service',
 `type`             varchar(255) COMMENT 'type',
 `source_number`    varchar(255) COMMENT 'source number',
 `target_number`    varchar(255) COMMENT 'target number',
 `ben_name`         varchar(255) COMMENT 'ben name',
 `amount`           varchar(255) COMMENT 'amount',
 `currency`         varchar(255) COMMENT 'currency',
 `remark`           varchar(210) COMMENT 'remark',
 `bank_id`          varchar(50) COMMENT 'bank id',
 `transaction_id`   varchar(255) COMMENT 'transaction id',
 `transfer_result`  varchar(50) COMMENT 'transfer result',
 `transaction_date` varchar(50) COMMENT 'transaction date',
 `transfer_date`    varchar(50) COMMENT 'transfer date',
 `bank_name`        varchar(100) COMMENT 'bank name',
 `bank_bin`         varchar(100) COMMENT 'bank bin',
 `status`           varchar(50) COMMENT 'status',
 `ft_details`       json COMMENT 'ft details',
 `error_message`    text COMMENT 'error message',
 `created_at`       datetime DEFAULT CURRENT_TIMESTAMP(),
 `updated_at`       datetime DEFAULT CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP()
);
CREATE UNIQUE INDEX `idx_reference_number` ON `vpb_transaction`(`reference_number`);