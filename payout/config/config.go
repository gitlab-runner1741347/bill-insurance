// YOU CAN EDIT YOUR CUSTOM CONFIG HERE

package config

import (
	"bytes"
	_ "embed"
	"fmt"
	"strings"
	"time"

	kitconfig "git.kafefin.net/backend/kitchen/config"
	"git.kafefin.net/backend/kitchen/l"
	kitutils "git.kafefin.net/backend/kitchen/utils"
	"git.kafefin.net/backend/salad-kit/server"
	"github.com/go-sql-driver/mysql"
	"github.com/spf13/viper"
)

var (
	ll = l.New()
)

// Config holds all settings
//
//go:embed config.yaml
var defaultConfig []byte

type Config struct {
	Base                                  `mapstructure:",squash"`
	MySQL                                 *kitconfig.MySQL      `yaml:"mysql" mapstructure:"mysql"`
	Postgres                              *Postgres             `yaml:"postgres" mapstructure:"postgres"`
	Redis                                 *Redis                `yaml:"redis" mapstructure:"redis"`
	OtelCollectorAddress                  string                `yaml:"otel_collector_address" mapstructure:"otel_collector_address"`
	External                              *External             `yaml:"external" mapstructure:"external"`
	Quota                                 *Quota                `yaml:"quota" mapstructure:"quota"`
	Vpbank                                *Vpbank               `yaml:"vpbank" mapstructure:"vpbank"`
	VpbankReconciliation                  *VpbankReconciliation `yaml:"vpbank_reconciliation" mapstructure:"vpbank_reconciliation"`
	SaladinAdminDomain                    string                `yaml:"saladin_admin_domain" mapstructure:"saladin_admin_domain"`
	ProcessPaymentTransactionQueueUrl     string                `yaml:"process_payment_transaction_queue_url" mapstructure:"process_payment_transaction_queue_url"`
	CheckPaymentTransactionResultQueueUrl string                `yaml:"check_payment_transaction_result_queue_url" mapstructure:"check_payment_transaction_result_queue_url"`
	NameToReconcile                       string                `yaml:"name_to_reconcile" mapstructure:"name_to_reconcile"`
	AwsCloud                              *AwsCloud             `yaml:"aws_cloud" mapstructure:"aws_cloud"`
	Teams                                 *Teams                `yaml:"teams" mapstructure:"teams"`
	AccountingEmail                       string                `yaml:"accounting_email" mapstructure:"accounting_email"`
	IsVpBankProdAccountCheck              bool                  `yaml:"is_vp_bank_prod_account_check" mapstructure:"is_vp_bank_prod_account_check"`
}

type Base struct {
	Server      ServerConfig `yaml:"server" mapstructure:"server"`
	Environment string       `yaml:"environment" mapstructure:"environment"`
	ApiGateway  string       `yaml:"api_gateway" mapstructure:"api_gateway"`
}

type External struct {
	SaladinStaffAddress   string `yaml:"saladin_staff_address" mapstructure:"saladin_staff_address"`
	SaladinMessageAddress string `yaml:"saladin_message_address" mapstructure:"saladin_message_address"`
}

type Quota struct {
	QuotaByTranx int64 `yaml:"quota_by_tranx" mapstructure:"quota_by_tranx"`
	QuotaByDay   int64 `yaml:"quota_by_day" mapstructure:"quota_by_day"`
}

func (q Quota) GetQuotaByTranxCurrency() string {
	return kitutils.FormatVNDCurrency(q.QuotaByTranx)
}

func (q Quota) GetQuotaByDayCurrency() string {
	return kitutils.FormatVNDCurrency(q.QuotaByDay)
}

// MySQL is settings of a MySQL server. It contains almost same fields as mysql.Config,
// but with some different field names and tags.
type MySQL struct {
	Username  string            `yaml:"username" mapstructure:"username"`
	Password  string            `yaml:"password" mapstructure:"password"`
	Protocol  string            `yaml:"protocol" mapstructure:"protocol"`
	Address   string            `yaml:"address" mapstructure:"address"`
	Database  string            `yaml:"database" mapstructure:"database"`
	Params    map[string]string `yaml:"params" mapstructure:"params"`
	Collation string            `yaml:"collation" mapstructure:"collation"`
	Loc       *time.Location    `yaml:"location" mapstructure:"loc"`
	TLSConfig string            `yaml:"tls_config" mapstructure:"tls_config"`

	Timeout      time.Duration `yaml:"timeout" mapstructure:"timeout"`
	ReadTimeout  time.Duration `yaml:"read_timeout" mapstructure:"read_timeout"`
	WriteTimeout time.Duration `yaml:"write_timeout" mapstructure:"write_timeout"`

	AllowAllFiles           bool   `yaml:"allow_all_files" mapstructure:"allow_all_files"`
	AllowCleartextPasswords bool   `yaml:"allow_cleartext_passwords" mapstructure:"allow_cleartext_passwords"`
	AllowOldPasswords       bool   `yaml:"allow_old_passwords" mapstructure:"allow_old_passwords"`
	ClientFoundRows         bool   `yaml:"client_found_rows" mapstructure:"client_found_rows"`
	ColumnsWithAlias        bool   `yaml:"columns_with_alias" mapstructure:"columns_with_alias"`
	InterpolateParams       bool   `yaml:"interpolate_params" mapstructure:"interpolate_params"`
	MultiStatements         bool   `yaml:"multi_statements" mapstructure:"multi_statements"`
	ParseTime               bool   `yaml:"parse_time" mapstructure:"parse_time"`
	GoogleAuthFile          string `yaml:"google_auth_file" mapstructure:"google_auth_file"`
}

// FormatDSN returns MySQL DSN from settings.
func (m *MySQL) FormatDSN() string {
	um := &mysql.Config{
		User:                    m.Username,
		Passwd:                  m.Password,
		Net:                     m.Protocol,
		Addr:                    m.Address,
		DBName:                  m.Database,
		Params:                  m.Params,
		Collation:               m.Collation,
		Loc:                     m.Loc,
		TLSConfig:               m.TLSConfig,
		Timeout:                 m.Timeout,
		ReadTimeout:             m.ReadTimeout,
		WriteTimeout:            m.WriteTimeout,
		AllowAllFiles:           m.AllowAllFiles,
		AllowCleartextPasswords: m.AllowCleartextPasswords,
		AllowOldPasswords:       m.AllowOldPasswords,
		ClientFoundRows:         m.ClientFoundRows,
		ColumnsWithAlias:        m.ColumnsWithAlias,
		InterpolateParams:       m.InterpolateParams,
		MultiStatements:         m.MultiStatements,
		ParseTime:               m.ParseTime,
		AllowNativePasswords:    true,
	}
	return um.FormatDSN()
}

type Postgres struct {
	Username string `yaml:"username" mapstructure:"username"`
	Password string `yaml:"password" mapstructure:"password"`
	Address  string `yaml:"address" mapstructure:"address"`
	Port     int    `yaml:"port" mapstructure:"port"`
	Database string `yaml:"database" mapstructure:"database"`
	LogLevel string `yaml:"log_level" mapstructure:"log_level"`
}

func (m *Postgres) FormatDSN() string {
	return fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%d sslmode=disable TimeZone=Asia/Ho_Chi_Minh",
		m.Address, m.Username, m.Password, m.Database, m.Port)
}

// Redis ...
type Redis struct {
	Address  string `yaml:"address" mapstructure:"address"`
	Password string `yaml:"password" mapstructure:"password"`
	DB       int    `yaml:"db" mapstructure:"db"`
}

type Vpbank struct {
	Host                               string `json:"host" mapstructure:"host" yaml:"host"`
	Idn                                string `json:"idn" mapstructure:"idn" yaml:"idn"`
	BearerToken                        string `json:"bearer_token" mapstructure:"bearer_token" yaml:"bearer_token"`
	ClaimDisbursementAccountNumber     string `json:"claim_disbursement_account_number" mapstructure:"claim_disbursement_account_number" yaml:"claim_disbursement_account_number"`
	AffiliateDisbursementAccountNumber string `json:"affiliate_disbursement_account_number" mapstructure:"affiliate_disbursement_account_number" yaml:"affiliate_disbursement_account_number"`
	Pem                                string `json:"pem" mapstructure:"pem" yaml:"pem"`
	Cer                                string `json:"cer" mapstructure:"cer" yaml:"cer"`
}

type VpbankReconciliation struct {
	Address  string `json:"address" mapstructure:"address" yaml:"address"`
	Port     int    `json:"port" mapstructure:"port" yaml:"port"`
	Username string `json:"username" mapstructure:"username" yaml:"username"`
	Password string `json:"password" mapstructure:"password" yaml:"password"`
}

// ServerConfig hold http/grpc server config
type ServerConfig struct {
	GRPC server.Listen `json:"grpc" mapstructure:"grpc" yaml:"grpc"`
	HTTP server.Listen `json:"http" mapstructure:"http" yaml:"http"`
}

func Load() *Config {
	var cfg = &Config{}

	viper.SetConfigType("yaml")
	err := viper.ReadConfig(bytes.NewBuffer(defaultConfig))
	if err != nil {
		ll.Fatal("Failed to read viper config", l.Error(err))
	}

	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "__"))
	viper.AutomaticEnv()

	err = viper.Unmarshal(&cfg)
	if err != nil {
		ll.Fatal("Failed to unmarshal config", l.Error(err))
	}

	ll.Info("Config loaded", l.Object("config", cfg))
	return cfg
}

type AwsCloud struct {
	Region                 string `yaml:"region" mapstructure:"region"`
	AccessKey              string `yaml:"access_key" mapstructure:"access_key"`
	SecretKey              string `yaml:"secret_key" mapstructure:"secret_key"`
	Bucket                 string `yaml:"bucket" mapstructure:"bucket"`
	ReconcileBucket        string `yaml:"reconcile_bucket" mapstructure:"reconcile_bucket"`
	ReconcilePrivateBucket string `yaml:"reconcile_private_bucket" mapstructure:"reconcile_private_bucket"`
}

type Teams struct {
	OperationAlertWebhook string `yaml:"operation_alert_webhook" mapstructure:"operation_alert_webhook"`
}
