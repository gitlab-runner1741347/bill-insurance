package models

import (
	"database/sql"
	"time"

	"git.kafefin.net/backend/central-payout/pb"
	"git.kafefin.net/backend/central-payout/pkg/pbconv"
)

type PaymentRequest struct {
	ID                        int64                       `sql:"type:BIGINT UNSIGNED;primary_key;auto_increment"`
	SrcID                     string                      `sql:"type:VARCHAR(255)"`
	PaymentType               string                      `sql:"type:VARCHAR(255)"`
	DetailURL                 string                      `sql:"type:VARCHAR(1000)"`
	SldBankID                 int64                       `sql:"type:BIGINT"`
	AccountNumber             string                      `sql:"type:VARCHAR(255)"`
	AccountName               string                      `sql:"type:VARCHAR(255)"`
	BeneficiaryPhone          string                      `sql:"type:VARCHAR(255)"`
	BankName                  string                      `sql:"type:VARCHAR(255)"`
	BankBranch                string                      `sql:"type:VARCHAR(255)"`
	BankBin                   string                      `sql:"type:VARCHAR(255)"`
	Debtor                    pb.Debtor                   `sql:"type:INT"`
	CampaignCode              string                      `sql:"type:VARCHAR(255);index"`
	CampaignName              string                      `sql:"type:VARCHAR(255)"`
	PaymentAmount             int64                       `sql:"type:BIGINT"`
	SrcService                string                      `sql:"type:VARCHAR(255)"`
	CallbackURL               string                      `sql:"type:VARCHAR(1000)"`
	Status                    pb.PaymentStatus            `sql:"type:INT"`
	TxnStatus                 pb.PaymentTransactionStatus `sql:"type:INT"`
	SrcNote                   string                      `sql:"type:TEXT"`
	AccountantNote            string                      `sql:"type:TEXT"`
	CeoNote                   string                      `sql:"type:TEXT"`
	TransactionDetail         string                      `sql:"type:TEXT"`
	IdempotentKey             sql.NullString              `sql:"type:VARCHAR(255);unique;index_columns:src_service,idempotent_key"`
	DisbursementAccountNumber string                      `sql:"type:VARCHAR(255)"`
	CreatedAt                 time.Time                   `sql:"type:DATETIME;default:CURRENT_TIMESTAMP;index" json:"created_at"`
	UpdatedAt                 time.Time                   `sql:"type:DATETIME;default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP" json:"updated_at"`
}

func (PaymentRequest) TableName() string {
	return "payment_request"
}

func (m PaymentRequest) GetDebtorName() string {
	return pbconv.EnumToName(m.Debtor.String())
}

func (m PaymentRequest) GetStatusName() string {
	return pbconv.EnumToName(m.Status.String())
}

func (m PaymentRequest) GetTxnStatusName() string {
	return pbconv.EnumToName(m.TxnStatus.String(), pbconv.MapUnspecified)
}

type PaymentRequestDetail struct {
	PaymentRequest
	PaymentTransactions []*PaymentTransaction `gorm:"foreignKey:PaymentRequestID;references:ID"`
}
