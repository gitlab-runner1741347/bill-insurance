package models

type DisbursementReconcile struct {
	Id            string `sql:"type:BIGINT UNSIGNED;primary_key;auto_increment"`
	CreatedBy     string `sql:"type:VARCHAR(255)"`
	ExportFileUrl string `sql:"type:VARCHAR(255)"`
	ReconcileDate string `sql:"type:VARCHAR(255)"`
	UpdatedBy     string `sql:"type:VARCHAR(255)"`
	Model         `sql:"squash"`
}

func (DisbursementReconcile) TableName() string {
	return "disbursement_reconcile"
}
