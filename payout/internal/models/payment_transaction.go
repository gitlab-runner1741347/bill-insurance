package models

import (
	"time"

	"git.kafefin.net/backend/central-payout/pb"
)

type PaymentTransaction struct {
	ID                       string                      `sql:"type:VARCHAR(50);primary_key"`
	PaymentRequestID         int64                       `sql:"type:BIGINT UNSIGNED"`
	PaymentServiceProvider   pb.PaymentServiceProvider   `sql:"type:INT"`
	SldBankID                int64                       `sql:"type:BIGINT"`
	PspBankID                int64                       `sql:"type:BIGINT"`
	PspBankCode              string                      `sql:"type:VARCHAR(255)"`
	PspBankName              string                      `sql:"type:VARCHAR(255)"`
	PspBankShortName         string                      `sql:"type:VARCHAR(255)"`
	PspBin                   string                      `sql:"type:VARCHAR(255)"`
	BeneficiaryAccountNumber string                      `sql:"type:VARCHAR(255)"`
	BeneficiaryAccountName   string                      `sql:"type:VARCHAR(255)"`
	DebitAccountNumber       string                      `sql:"type:VARCHAR(255)"`
	PaymentAmount            int64                       `sql:"type:BIGINT"`
	Currency                 string                      `sql:"type:VARCHAR(255)"`
	TransactionDetail        string                      `sql:"type:VARCHAR(210)"`
	RefTxnID                 string                      `sql:"type:VARCHAR(255)"`
	PartnerTransferResult    string                      `sql:"type:VARCHAR(255)"`
	PartnerTransactionDate   *time.Time                  `sql:"type:DATETIME"`
	PartnerTransferDate      *time.Time                  `sql:"type:DATETIME"`
	ErrorMessage             string                      `sql:"type:TEXT"`
	Status                   pb.PaymentTransactionStatus `sql:"type:INT"`

	Model `sql:"squash"`
}

func (PaymentTransaction) TableName() string {
	return "payment_transaction"
}

func (p PaymentTransaction) GetPartnerTransactionDate() int64 {
	if p.PartnerTransactionDate == nil {
		return 0
	}

	return p.PartnerTransactionDate.UnixMilli()
}

func (p PaymentTransaction) GetPartnerTransferDate() int64 {
	if p.PartnerTransferDate == nil {
		return 0
	}

	return p.PartnerTransferDate.UnixMilli()
}

type PaymentTransactionDetail struct {
	PaymentTransaction
	PaymentRequest *PaymentRequest `gorm:"foreignKey:ID;references:PaymentRequestID"`
	VpbTransaction *VpbTransaction `gorm:"foreignKey:ReferenceNumber;references:ID"`
}
