package models

type DailyPayoutTotal struct {
	ID          string `sql:"type:VARCHAR(50);primary_key"`
	TotalAmount int64

	Model `sql:"squash"`
}

func (DailyPayoutTotal) TableName() string {
	return "daily_payout_total"
}
