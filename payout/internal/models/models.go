package models

import (
	"time"
)

type Model struct {
	CreatedAt time.Time `sql:"type:DATETIME;default:CURRENT_TIMESTAMP"`
	UpdatedAt time.Time `sql:"type:DATETIME;default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}

func AllModels() []interface{} {
	return []interface{}{
		PaymentRequest{},
		PaymentTransaction{},
		DisbursementReconcile{},
		DailyPayoutTotal{},
		VpbTransaction{},
	}
}
