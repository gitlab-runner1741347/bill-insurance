package models

import (
	"git.kafefin.net/backend/central-payout/pkg/disbursement"
	"gorm.io/datatypes"
)

type VpbTransaction struct {
	ID              int64                                        `sql:"type:BIGINT UNSIGNED;primary_key;auto_increment"`
	ReferenceNumber string                                       `sql:"type:VARCHAR(50);unique"`
	Service         string                                       `sql:"type:VARCHAR(255)"`
	Type            string                                       `sql:"type:VARCHAR(255)"`
	SourceNumber    string                                       `sql:"type:VARCHAR(255)"`
	TargetNumber    string                                       `sql:"type:VARCHAR(255)"`
	BenName         string                                       `sql:"type:VARCHAR(255)"`
	Amount          string                                       `sql:"type:VARCHAR(255)"`
	Currency        string                                       `sql:"type:VARCHAR(255)"`
	Remark          string                                       `sql:"type:VARCHAR(210)"`
	BankID          string                                       `sql:"type:VARCHAR(50)"`
	TransactionID   string                                       `sql:"type:VARCHAR(255)"`
	TransferResult  string                                       `sql:"type:VARCHAR(50)"`
	TransactionDate string                                       `sql:"type:VARCHAR(50)"`
	TransferDate    string                                       `sql:"type:VARCHAR(50)"`
	BankName        string                                       `sql:"type:VARCHAR(100)"`
	BankBin         string                                       `sql:"type:VARCHAR(100)"`
	Status          string                                       `sql:"type:VARCHAR(50)"`
	FTDetails       datatypes.JSONType[[]*disbursement.FTDetail] `sql:"type:JSON"`
	ErrorMessage    string                                       `sql:"type:TEXT"`

	Model `sql:"squash"`
}

func (VpbTransaction) TableName() string {
	return "vpb_transaction"
}
