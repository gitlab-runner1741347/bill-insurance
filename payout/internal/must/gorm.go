package must

import (
	"time"

	"git.kafefin.net/backend/central-payout/config"
	"git.kafefin.net/backend/kitchen/l"

	kitconfig "git.kafefin.net/backend/kitchen/config"
	"gorm.io/driver/mysql"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	zapgorm "moul.io/zapgorm2"
)

const (
	maxDBIdleConns  = 10
	maxDBOpenConns  = 100
	maxConnLifeTime = 30 * time.Minute
)

func ConnectMySQL(cfg *kitconfig.MySQL) *gorm.DB {
	if cfg.Address == "." {
		return nil
	}

	ll.Info("Connecting mysql")
	db, err := gorm.Open(mysql.Open(cfg.FormatDSN()), &gorm.Config{
		PrepareStmt: true,
		Logger:      zapgorm.New(ll.Logger).LogMode(cfg.GormLogLevel()),
	})
	if err != nil {
		ll.Fatal("Error open mysql", l.Error(err))
	}

	err = db.Raw("SELECT 1").Error
	if err != nil {
		ll.Fatal("Error querying SELECT 1", l.Error(err))
	}

	sqlDB, err := db.DB()
	if err != nil {
		ll.Fatal("Error get sql DB", l.Error(err))
	}

	sqlDB.SetMaxIdleConns(maxDBIdleConns)
	sqlDB.SetMaxOpenConns(maxDBOpenConns)
	sqlDB.SetConnMaxLifetime(maxConnLifeTime)
	ll.Info("Connected mysql successfully")
	return db
}

func ConnectPostgres(cfg *config.Postgres) *gorm.DB {
	if cfg.Address == "." {
		return nil
	}

	ll.Info("Connecting postgres")
	db, err := gorm.Open(postgres.Open(cfg.FormatDSN()), &gorm.Config{
		PrepareStmt: true,
		Logger:      zapgorm.New(ll.Logger).LogMode(logger.Silent),
	})
	if err != nil {
		ll.Fatal("ConnectPostgres")
	}

	ll.Info("Connected postgres successfully")
	return db
}
