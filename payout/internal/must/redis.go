package must

import (
	"context"

	"git.kafefin.net/backend/central-payout/config"
	"git.kafefin.net/backend/kitchen/l"

	"github.com/go-redis/redis/v8"
)

func ConnectRedis(cfg *config.Redis) *redis.Client {
	if cfg.Address == "." {
		return nil
	}

	ll.Info("Connecting redis")
	redisClient := redis.NewClient(&redis.Options{
		Addr: cfg.Address,
		DB:   cfg.DB,
	})

	_, err := redisClient.Ping(context.TODO()).Result()
	if err != nil {
		ll.Fatal("error pinging redis", l.Error(err))
	}

	ll.Info("Connected redis successfully")
	return redisClient
}
