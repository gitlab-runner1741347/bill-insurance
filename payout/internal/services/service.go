package services

import (
	"context"

	"git.kafefin.net/backend/central-payout/config"
	"git.kafefin.net/backend/central-payout/internal/external"
	"git.kafefin.net/backend/central-payout/internal/external/callback"
	"git.kafefin.net/backend/central-payout/internal/repositories"
	"git.kafefin.net/backend/central-payout/pb"
	"git.kafefin.net/backend/central-payout/pkg/disbursement"
	"git.kafefin.net/backend/central-payout/pkg/msteams"
	"git.kafefin.net/backend/central-payout/pkg/pubsub"
	"git.kafefin.net/backend/central-payout/pkg/types"
	pbCore "git.kafefin.net/backend/salad-kit/pb"
	"git.kafefin.net/backend/salad-kit/server"

	"github.com/go-redis/redis/v8"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"google.golang.org/grpc"
)

const (
	OkMessage = "OK"
	Version   = "1.0.0"
)

type serviceInterface interface {
	pb.CentralPayoutPublicServiceServer
	pb.CentralPayoutAdminServiceServer
	pb.CentralPayoutInternalServiceServer
	pbCore.HealthServiceServer
}

var _ serviceInterface = &service{}

type service struct {
	cfg             *config.Config
	log             types.CommonLogger
	repos           *repositories.Repositories
	dbs             disbursement.PaymentOut
	callbackClient  *callback.NapClient
	pub             pubsub.Publisher
	redis           *redis.Client
	teamsAlert      *msteams.TeamsNotificaton
	externalClients external.ExternalClients
}

func NewCentralPayoutService(
	ctx context.Context,
	cfg *config.Config,
	log types.CommonLogger,
	repos *repositories.Repositories,
	dbs disbursement.PaymentOut,
	callbackClient *callback.NapClient,
	pub pubsub.Publisher,
	redis *redis.Client,
	teamsAlert *msteams.TeamsNotificaton,
	externalClients external.ExternalClients,
) server.ServiceServer {
	return &service{
		cfg:             cfg,
		log:             log,
		repos:           repos,
		dbs:             dbs,
		callbackClient:  callbackClient,
		pub:             pub,
		redis:           redis,
		teamsAlert:      teamsAlert,
		externalClients: externalClients,
	}
}

func (s *service) Version(ctx context.Context, request *pbCore.VersionRequest) (*pbCore.VersionResponse, error) {
	return &pbCore.VersionResponse{Version: Version}, nil
}

func (s *service) Liveness(context context.Context, req *pbCore.LivenessRequest) (*pbCore.LivenessResponse, error) {
	return &pbCore.LivenessResponse{Message: OkMessage}, nil
}

func (s *service) ToggleReadiness(context context.Context, req *pbCore.ToggleReadinessRequest) (*pbCore.ToggleReadinessResponse, error) {
	return &pbCore.ToggleReadinessResponse{Message: OkMessage}, nil
}

func (s *service) Readiness(context context.Context, req *pbCore.ReadinessRequest) (*pbCore.ReadinessResponse, error) {
	return &pbCore.ReadinessResponse{Message: OkMessage}, nil
}

func (s *service) RegisterWithGrpcServer(server *grpc.Server) {
	pb.RegisterCentralPayoutPublicServiceServer(server, s)
	pb.RegisterCentralPayoutAdminServiceServer(server, s)
	pb.RegisterCentralPayoutInternalServiceServer(server, s)
	pbCore.RegisterHealthServiceServer(server, s)
}

func (s *service) RegisterWithMuxServer(ctx context.Context, mux *runtime.ServeMux, conn *grpc.ClientConn) error {
	if err := pb.RegisterCentralPayoutPublicServiceHandler(ctx, mux, conn); err != nil {
		return err
	}

	if err := pb.RegisterCentralPayoutAdminServiceHandler(ctx, mux, conn); err != nil {
		return err
	}

	if err := pb.RegisterCentralPayoutInternalServiceHandler(ctx, mux, conn); err != nil {
		return err
	}

	if err := pbCore.RegisterHealthServiceHandler(ctx, mux, conn); err != nil {
		return err
	}

	return nil
}

// Close ...
func (s *service) Close(ctx context.Context) {
}
