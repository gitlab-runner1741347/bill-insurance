package services

import (
	"context"
	_ "embed"
	"sync"

	"git.kafefin.net/backend/central-payout/internal/models"
	"git.kafefin.net/backend/central-payout/internal/repositories"
	"git.kafefin.net/backend/central-payout/pb"
	"git.kafefin.net/backend/central-payout/pkg/consts"
	"git.kafefin.net/backend/central-payout/pkg/convert"
	"git.kafefin.net/backend/central-payout/pkg/pmc"
	"git.kafefin.net/backend/kitchen/l"
	"git.kafefin.net/backend/salad-kit/grpckit"
	"github.com/hashicorp/go-multierror"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (s *service) getPayments(ctx context.Context, req *pb.AdminGetPaymentsRequest) (*pb.AdminGetPaymentsResponse, error) {
	payments, total, err := s.repos.PaymentRequestRepository.PaginatePaymentRequests(req)
	if err != nil {
		return nil, err
	}

	dailyTotal, err := s.repos.DailyPayoutTotalRepository.GetToday()
	if err != nil {
		s.log.Error("DailyPayoutTotalRepository.GetToday", l.Error(err))
		return nil, err
	}

	return &pb.AdminGetPaymentsResponse{
		Data: &pb.AdminGetPaymentsResponse_Data{
			Payments: convert.PaymentRequestsToAdminPbs(payments, s.cfg.SaladinAdminDomain),
			Paging: &pb.Paging{
				Total:  int32(total),
				Offset: req.GetOffset(),
			},
			Quota: &pb.AdminQuota{
				QuotaByTranx: s.cfg.Quota.QuotaByTranx,
				QuotaByDay:   s.cfg.Quota.QuotaByDay,
				TodayTotal:   dailyTotal.TotalAmount,
			},
		},
	}, nil
}

func (s *service) processPaymentTxnByIDs(ctx context.Context, paymentIDs []int64) error {
	errChan := make(chan error, len(paymentIDs))
	var wg sync.WaitGroup

	for _, id := range paymentIDs {
		wg.Add(1)
		go func(paymentID int64, wg *sync.WaitGroup, errChan chan<- error) {
			defer wg.Done()
			if err := s.publishProcessPaymentTransactionMessage(context.TODO(), paymentID); err != nil {
				s.log.Error("AdminAccountantReviewPayments publishProcessPaymentTransactionMessage", l.Error(err))
				errChan <- err
			}
		}(id, &wg, errChan)
	}

	wg.Wait()
	close(errChan)

	// Collect the results from the channel
	var errors error
	for err := range errChan {
		errors = multierror.Append(err, errors)
	}

	return errors
}

func (s *service) callbackRejectPaymentRequest(ctx context.Context, prs []*models.PaymentRequest) {
	for _, pr := range prs {
		if err := s.callbackClient.Callback(context.TODO(), pr.CallbackURL, &pb.PaymentCallback{
			SrcId:         pr.SrcID,
			PaymentId:     pr.ID,
			PaymentStatus: pb.PaymentStatus_PRS_REJECTED,
			CampaignCode:  pr.CampaignCode,
			IdempotentKey: pr.IdempotentKey.String,
		}); err != nil {
			s.log.Error("callbackClient.Callback", l.Error(err))
			return
		}
	}
}

func (s *service) AdminGetPayments(ctx context.Context, req *pb.AdminGetPaymentsRequest) (*pb.AdminGetPaymentsResponse, error) {
	return s.getPayments(ctx, req)
}

func (s *service) AdminAccountantGetPayments(ctx context.Context, req *pb.AdminGetPaymentsRequest) (*pb.AdminGetPaymentsResponse, error) {
	req.Status = pb.PaymentStatus_PRS_WAITING_FOR_ACCOUNTANT
	return s.getPayments(ctx, req)
}

func (s *service) AdminCeoGetPayments(ctx context.Context, req *pb.AdminGetPaymentsRequest) (*pb.AdminGetPaymentsResponse, error) {
	req.Status = pb.PaymentStatus_PRS_WAITING_FOR_CEO
	return s.getPayments(ctx, req)
}

func (s *service) AdminGetPayment(ctx context.Context, req *pb.AdminGetPaymentRequest) (*pb.AdminGetPaymentResponse, error) {
	payment, err := s.repos.PaymentRequestRepository.GetOneDetailByID(req.PaymentId)
	if err != nil {
		s.log.Error("PaymentRequestRepository.GetOneDetailByID", l.Error(err))
		return nil, err
	}

	dailyTotal, err := s.repos.DailyPayoutTotalRepository.GetToday()
	if err != nil {
		s.log.Error("DailyPayoutTotalRepository.GetToday", l.Error(err))
		return nil, err
	}

	return &pb.AdminGetPaymentResponse{
		Data: &pb.AdminGetPaymentResponse_Data{
			Payment: convert.PaymentRequestToAdminDetailPb(*payment, getSldBank(payment.SldBankID), s.cfg.SaladinAdminDomain),
			Quota: &pb.AdminQuota{
				QuotaByTranx: s.cfg.Quota.QuotaByTranx,
				QuotaByDay:   s.cfg.Quota.QuotaByDay,
				TodayTotal:   dailyTotal.TotalAmount,
			},
		},
	}, nil
}

func (s *service) GetDisbursementAccountNumber(paymentRequest []*models.PaymentRequest) string {

	if len(paymentRequest) == 0 {
		return ""
	}

	firstPaymentType := paymentRequest[0].PaymentType

	for _, pr := range paymentRequest {
		if pr.PaymentType != firstPaymentType {
			return ""
		}
	}

	return paymentRequest[0].DisbursementAccountNumber
}

func (s *service) accountantReviewPayments(ctx context.Context, req *pb.AdminReviewPaymentsRequest) (*pb.AdminReviewPaymentsResponse, error) {
	if err := s.repos.Transaction(func(r *repositories.Repositories) error {

		if len(req.PaymentIds) < 1 {
			return status.Error(codes.InvalidArgument, "numbers of payment requests must be > 0")
		}
		dailyTotal, err := r.DailyPayoutTotalRepository.GetUpdateToday()
		if err != nil {
			return multierror.Prefix(err, "DailyPayoutTotalRepository.GetUpdateToday")
		}

		payments, err := r.PaymentRequestRepository.GetUpdateByIDs(req.GetPaymentIds())
		if err != nil {
			return multierror.Prefix(err, "PaymentRequestRepository.GetUpdateByIDs")
		}

		classifiedPaymentMap := pmc.ClassifyByStatus(payments)
		if len(classifiedPaymentMap[pb.PaymentStatus_PRS_WAITING_FOR_ACCOUNTANT]) < len(req.PaymentIds) {
			return status.Error(codes.InvalidArgument, "invalid payment requests status")
		}

		if req.Action == ReviewActionReject {
			if err := r.PaymentRequestRepository.AccountantApproveByIDs(pmc.ExtractID(payments), pb.PaymentStatus_PRS_REJECTED, req.Note); err != nil {
				return multierror.Prefix(err, "PaymentRequestRepository.AccountantApproveByIDs")
			}

			go s.callbackRejectPaymentRequest(context.TODO(), payments)

			return nil
		}

		totalPaymentAmount := pmc.GetTotalPaymentAmount(payments)
		wfcPmts, approvedPmts := pmc.ClassifyByTxnQuota(payments, s.cfg.Quota.QuotaByTranx)

		disbursementAccountNumbers := s.GetDisbursementAccountNumber(payments)
		if disbursementAccountNumbers == "" {
			return status.Error(codes.InvalidArgument, "Hệ thống không tìm được tài khoản chuyên chi phù hợp")
		}

		sldBenInfo := s.dbs.GetAccountInfo(disbursementAccountNumbers)
		if sldBenInfo == nil {
			return status.Error(codes.FailedPrecondition, "Hệ thống không kiểm tra được số dư của tài khoản chuyên chi")
		}
		if float64(totalPaymentAmount) > sldBenInfo.GetAvailableBlance() {
			return consts.InvalidBalanceAmount(sldBenInfo.GetAvailableBlanceCurrency())
		}

		switch req.GetOverQuotaFlag() {
		case pb.OverQuotaFlag_OVER_QUOTA_FLAG_BY_DAY:
			if err := r.PaymentRequestRepository.AccountantApproveByIDs(req.PaymentIds, pb.PaymentStatus_PRS_WAITING_FOR_CEO, req.Note); err != nil {
				return multierror.Prefix(err, "PaymentRequestRepository.AccountantApproveByIDs")
			}
		case pb.OverQuotaFlag_OVER_QUOTA_FLAG_BY_TRANX:
			if err := r.PaymentRequestRepository.AccountantApproveByIDs(pmc.ExtractID(wfcPmts), pb.PaymentStatus_PRS_WAITING_FOR_CEO, req.Note); err != nil {
				return multierror.Prefix(err, "PaymentRequestRepository.AccountantApproveByIDs")
			}
			if err := r.PaymentRequestRepository.AccountantApproveByIDs(pmc.ExtractID(approvedPmts), pb.PaymentStatus_PRS_APPROVED, req.Note); err != nil {
				return multierror.Prefix(err, "PaymentRequestRepository.AccountantApproveByIDs")
			}
			if err := s.processPaymentTxnByIDs(context.TODO(), pmc.ExtractID(approvedPmts)); err != nil {
				s.log.Error("AdminAccountantReviewPayments processPaymentTxnByIDs", l.Error(err))
			}
		default:
			if totalPaymentAmount+dailyTotal.TotalAmount > s.cfg.Quota.QuotaByDay {
				return consts.InvalidTotalTxnAmount(s.cfg.Quota.GetQuotaByDayCurrency())
			}
			if len(wfcPmts) > 0 {
				return consts.InvalidSomeTxnAmount(s.cfg.Quota.GetQuotaByTranxCurrency(), s.cfg.Quota.GetQuotaByTranxCurrency())
			}
			if err := r.PaymentRequestRepository.AccountantApproveByIDs(pmc.ExtractID(approvedPmts), pb.PaymentStatus_PRS_APPROVED, req.Note); err != nil {
				return multierror.Prefix(err, "PaymentRequestRepository.AccountantApproveByIDs")
			}
			if err := s.processPaymentTxnByIDs(context.TODO(), req.PaymentIds); err != nil {
				s.log.Error("AdminAccountantReviewPayments processPaymentTxnByIDs", l.Error(err))
			}
		}

		if err := r.DailyPayoutTotalRepository.UpdateTotalAmount(dailyTotal.ID, dailyTotal.TotalAmount+totalPaymentAmount); err != nil {
			return multierror.Prefix(err, "DailyPayoutTotalRepository.UpdateTotalAmount")
		}

		return nil
	}); err != nil {
		s.log.Error("AdminUpdateOrderStatus Transaction", l.Error(err))
		return nil, err
	}

	return &pb.AdminReviewPaymentsResponse{}, nil
}

func (s *service) AdminAccountantReviewPayments(ctx context.Context, req *pb.AdminReviewPaymentsRequest) (*pb.AdminReviewPaymentsResponse, error) {
	staff, ok := grpckit.GetStaffPolicyFromContext(ctx).(grpckit.StaffPolicy)
	if !ok || staff == nil {
		s.log.Error("AdminAccountantReviewPayments staff is nil")
		return nil, status.Error(codes.PermissionDenied, codes.PermissionDenied.String())
	}

	return s.accountantReviewPayments(ctx, req)
}

func (s *service) AdminCeoReviewPayments(ctx context.Context, req *pb.AdminReviewPaymentsRequest) (*pb.AdminReviewPaymentsResponse, error) {
	if err := s.repos.Transaction(func(r *repositories.Repositories) error {
		payments, err := r.PaymentRequestRepository.GetUpdateByIDs(req.GetPaymentIds())
		if err != nil {
			return multierror.Prefix(err, "PaymentRequestRepository.GetUpdateByIDs")
		}
		classifiedPaymentMap := pmc.ClassifyByStatus(payments)
		if len(classifiedPaymentMap[pb.PaymentStatus_PRS_WAITING_FOR_CEO]) < len(req.PaymentIds) {
			return status.Error(codes.InvalidArgument, "invalid payment requests status")
		}

		if req.Action == ReviewActionReject {
			if err := r.PaymentRequestRepository.AccountantApproveByIDs(pmc.ExtractID(payments), pb.PaymentStatus_PRS_REJECTED, req.Note); err != nil {
				return multierror.Prefix(err, "PaymentRequestRepository.AccountantApproveByIDs")
			}

			go s.callbackRejectPaymentRequest(context.TODO(), payments)

			return nil
		}

		disbursementAccountNumbers := s.GetDisbursementAccountNumber(payments)
		if disbursementAccountNumbers == "" {
			return status.Error(codes.InvalidArgument, "invalid payment requests")
		}

		sldBenInfo := s.dbs.GetAccountInfo(disbursementAccountNumbers)
		totalPaymentAmount := pmc.GetTotalPaymentAmount(payments)

		if sldBenInfo == nil {
			return status.Error(codes.FailedPrecondition, "Hệ thống không kiểm tra được số dư của tài khoản chuyên chi")
		}
		if float64(totalPaymentAmount) > sldBenInfo.GetAvailableBlance() {
			return consts.InvalidBalanceAmount(sldBenInfo.GetAvailableBlanceCurrency())
		}

		if err := r.PaymentRequestRepository.CeoApproveByIDs(pmc.ExtractID(payments), pb.PaymentStatus_PRS_APPROVED, req.Note); err != nil {
			return multierror.Prefix(err, "PaymentRequestRepository.CeoApproveByIDs")
		}

		return nil
	}); err != nil {
		s.log.Error("AdminUpdateOrderStatus Transaction", l.Error(err))
		return nil, err
	}

	if err := s.processPaymentTxnByIDs(context.TODO(), req.PaymentIds); err != nil {
		s.log.Error("AdminAccountantReviewPayments processPaymentTxnByIDs", l.Error(err))
	}

	return &pb.AdminReviewPaymentsResponse{}, nil
}

func (s *service) AdminRetryPayments(ctx context.Context, req *pb.AdminRetryPaymentsRequest) (*pb.AdminRetryPaymentsResponse, error) {
	if err := s.repos.Transaction(func(r *repositories.Repositories) error {
		payments, err := r.PaymentRequestRepository.GetUpdateByIDs(req.GetPaymentIds())
		if err != nil {
			return multierror.Prefix(err, "PaymentRequestRepository.GetUpdateByIDs")
		}
		classifiedPaymentMap := pmc.ClassifyByTxnStatus(payments)
		if len(classifiedPaymentMap[pb.PaymentTransactionStatus_PTS_FAILED]) < len(req.PaymentIds) {
			return status.Error(codes.InvalidArgument, "invalid payment requests status")
		}

		disbursementAccountNumbers := s.GetDisbursementAccountNumber(payments)
		if disbursementAccountNumbers == "" {
			return status.Error(codes.InvalidArgument, "invalid payment requests")
		}

		sldBenInfo := s.dbs.GetAccountInfo(disbursementAccountNumbers)
		totalPaymentAmount := pmc.GetTotalPaymentAmount(payments)

		if sldBenInfo == nil {
			return status.Error(codes.FailedPrecondition, "Hệ thống không kiểm tra được số dư của tài khoản chuyên chi")
		}
		if float64(totalPaymentAmount) > sldBenInfo.GetAvailableBlance() {
			return consts.InvalidBalanceAmount(sldBenInfo.GetAvailableBlanceCurrency())
		}

		if err := r.PaymentRequestRepository.ResetTxnSttByIDs(req.PaymentIds); err != nil {
			return err
		}

		if err := s.processPaymentTxnByIDs(context.TODO(), req.PaymentIds); err != nil {
			s.log.Error("AdminAccountantReviewPayments processPaymentTxnByIDs", l.Error(err))
		}

		return nil
	}); err != nil {
		s.log.Error("Transaction", l.Error(err))
		return nil, err
	}

	return &pb.AdminRetryPaymentsResponse{}, nil
}

func (s *service) AdminGetConfigurations(ctx context.Context, req *pb.AdminGetConfigurationsRequest) (*pb.AdminGetConfigurationsResponse, error) {
	return &pb.AdminGetConfigurationsResponse{
		Data: &pb.AdminGetConfigurationsResponse_Data{
			PaymentTypes: consts.PaymentTypes,
		},
	}, nil
}
