package services

import (
	"context"
	"encoding/json"

	cpsub "git.kafefin.net/backend/central-payout/internal/subscribers/central-payout"
	sqsPubsub "git.kafefin.net/backend/central-payout/pkg/pubsub/sqs"

	"github.com/hashicorp/go-multierror"
)

func (s *service) publishProcessPaymentTransactionMessage(ctx context.Context, paymentRequestID int64) error {
	msgBody, err := json.Marshal(cpsub.ProcessPaymentTransactionMessage{Params: struct {
		PaymentRequestID int64 `json:"payment_request_id"`
	}{
		PaymentRequestID: paymentRequestID,
	}})
	if err != nil {
		return multierror.Prefix(err, "Marshal cpsub.ProcessPaymentTransactionMessage")
	}
	message := sqsPubsub.NewPublishMessage(string(msgBody))
	if err := s.pub.Publish(ctx, s.cfg.ProcessPaymentTransactionQueueUrl, message); err != nil {
		return multierror.Prefix(err, "pub.Publish")
	}

	return nil
}
