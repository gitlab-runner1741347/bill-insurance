package services

import (
	"context"

	"git.kafefin.net/backend/central-payout/internal/models"
	"git.kafefin.net/backend/central-payout/pkg/convert"
	"git.kafefin.net/backend/kitchen/l"
	sldMessagePb "git.kafefin.net/backend/saladin-central-proto/saladin_message"
)

const (
	TemplatePaymentRequestCreated = "d-97e670422d254b85a6637f278f7faacd"
)

const (
	sourceCentralPayout = "CP"
)

func (s *service) sendEmailWhenPaymentRequestCreated(urlDetail string, templateID string, paymentRequest *models.PaymentRequest) {
	switch templateID {
	case TemplatePaymentRequestCreated:
		values := convert.PaymentRequestToMessageValues(urlDetail, paymentRequest)

		req := &sldMessagePb.SendEmailTemplateRequest{
			To:         []*sldMessagePb.EmailTo{{Email: s.cfg.AccountingEmail}},
			TemplateId: templateID,
			Params:     values,
			Source:     sourceCentralPayout,
		}

		if _, err := s.externalClients.SldMessageSvc.ClientSendEmailTemplate(context.TODO(), req); err != nil {
			s.log.Error("sendEmailDeliveryClaim error send email", l.Error(err))
			return
		}
	}

}
