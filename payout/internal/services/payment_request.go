package services

import (
	"context"
	_ "embed"
	"errors"
	"fmt"

	"git.kafefin.net/backend/central-payout/internal/models"
	"git.kafefin.net/backend/central-payout/pb"
	"git.kafefin.net/backend/central-payout/pkg/consts"
	"git.kafefin.net/backend/central-payout/pkg/sqlconv"
	"git.kafefin.net/backend/kitchen/l"
	"gorm.io/gorm"
)

func getSldBank(sldBankID int64) *pb.Bank {
	for _, v := range pbBanks {
		if v.Id == sldBankID {
			return v
		}
	}

	return nil
}

func (s *service) InternalCreatePayment(ctx context.Context, req *pb.InternalCreatePaymentRequest) (*pb.InternalCreatePaymentResponse, error) {
	sldBank := getSldBank(req.SldBankId)
	if sldBank == nil {
		return nil, fmt.Errorf("cannot find bank with id: %v", req.SldBankId)
	}

	// bankWithBin := s.dbs.GetBankInfoByBin(sldBank.Bin)

	// if _, err := s.bankAccountVerification(ctx, &pb.AccountBankToVerifyRequest{
	// 	BankId:     sldBank.Id,
	// 	BankNumber: req.AccountNumber,
	// 	NameOwner:  req.AccountName,
	// }); err != nil {
	// 	s.log.Error("bankAccountVerification", l.Error(err))
	// 	return nil, err
	// }
	if req.IdempotentKey != "" {
		existedPR, err := s.repos.PaymentRequestRepository.GetOneByIdempotentKey(req.IdempotentKey, req.SrcService)
		if err != nil && !errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, err
		}
		if existedPR != nil && existedPR.ID != 0 {
			return &pb.InternalCreatePaymentResponse{
				PaymentId: existedPR.ID,
			}, nil
		}
	}

	paymentRequest, err := s.repos.PaymentRequestRepository.Create(models.PaymentRequest{
		SrcID:                     req.SrcId,
		PaymentType:               req.PaymentType,
		DetailURL:                 req.DetailUrl,
		SldBankID:                 req.SldBankId,
		AccountNumber:             req.AccountNumber,
		AccountName:               req.AccountName,
		BankName:                  sldBank.Name,
		BankBin:                   sldBank.Bin,
		Debtor:                    req.Debtor,
		CampaignCode:              req.CampaignCode,
		CampaignName:              req.CampaignName,
		PaymentAmount:             req.PaymentAmount,
		SrcService:                req.SrcService,
		CallbackURL:               req.CallbackUrl,
		SrcNote:                   req.SrcNote,
		Status:                    pb.PaymentStatus_PRS_WAITING_FOR_ACCOUNTANT,
		TransactionDetail:         req.TransactionDetail,
		IdempotentKey:             sqlconv.StringToNullString(req.IdempotentKey),
		DisbursementAccountNumber: s.FindDisbursementAccountNumberBasedOnType(req.GetPaymentType()),
	})
	if err != nil {
		s.log.Error("PaymentRequestRepository.Create", l.Error(err))
		return nil, err
	}

	if autoApprovePaymentType[req.PaymentType] {
		go func() {
			if _, err := s.accountantReviewPayments(context.TODO(), &pb.AdminReviewPaymentsRequest{
				PaymentIds: []int64{paymentRequest.ID},
				Note:       "Tự động phê duyệt.",
				Action:     ReviewActionApprove,
			}); err != nil {
				s.log.Error("accountantReviewPayments", l.Error(err))
			}
		}()
	} else {
		detailUrl := consts.DetailUrlPaymentRequest(s.cfg.SaladinAdminDomain, paymentRequest.SrcID, paymentRequest.ID)

		go func(paymentType string, id int64, urlDetail string) {
			if err := s.teamsAlert.Send(fmt.Sprintf("[%s] Có yêu cầu thanh toán %d cần được duyệt: %s ", paymentType, id, urlDetail)); err != nil {
				s.log.Error("notification via team error", l.Error(err))
			}
		}(paymentRequest.PaymentType, paymentRequest.ID, detailUrl)

		go s.sendEmailWhenPaymentRequestCreated(detailUrl, TemplatePaymentRequestCreated, paymentRequest)
	}

	return &pb.InternalCreatePaymentResponse{
		PaymentId: paymentRequest.ID,
	}, nil
}

func (s *service) InternalGetPayment(ctx context.Context, req *pb.InternalGetPaymentRequest) (*pb.InternalGetPaymentResponse, error) {
	return &pb.InternalGetPaymentResponse{}, nil
}
