package services

import (
	"bytes"
	"context"
	_ "embed"
	"encoding/csv"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strconv"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"git.kafefin.net/backend/central-payout/pb"
	"git.kafefin.net/backend/central-payout/pkg/consts"
	"git.kafefin.net/backend/kitchen/l"
)

//go:embed banks.csv
var banks []byte

var (
	// shortName -> name
	mapBanksName = map[string]string{}
	mapBanksIcon = map[string]string{}
	pbBanks      = []*pb.Bank{}
)

func init() {
	csvLines, _ := csv.NewReader(bytes.NewBuffer(banks)).ReadAll()
	pbBanks = make([]*pb.Bank, 0, len(csvLines)-1)

	for i, elem := range csvLines {
		if i == 0 || len(elem) == 0 {
			continue
		}

		id, _ := strconv.ParseInt(elem[0], 10, 64)
		pbBanks = append(pbBanks, &pb.Bank{
			Id:        id,
			Code:      elem[4],
			Name:      elem[3],
			ShortName: elem[2],
			NameEn:    "",
			Bin:       elem[1],
			Icon:      elem[5],
		})
		mapBanksName[elem[2]] = elem[3]
		mapBanksIcon[elem[2]] = elem[5]
	}
}

func ProductionBankValidate(req *pb.AccountBankToVerifyRequest) (*pb.AccountBankToVerifyResponse, error) {
	url := "https://gw.saladin.vn/api/v1/central-payout/public/bank-account-verification"

	commonError := errors.New("validate failed")
	// Create the request body as a Go struct
	payload := struct {
		BankID     int    `json:"bank_id"`
		BankNumber string `json:"bank_number"`
		NameOwner  string `json:"name_owner"`
	}{
		BankID:     int(req.BankId),
		BankNumber: req.BankNumber,
		NameOwner:  req.NameOwner,
	}

	// Convert the payload to JSON
	jsonPayload, err := json.Marshal(payload)
	if err != nil {
		fmt.Println("Error marshaling payload:", err)
		return nil, commonError
	}

	// Create the HTTP request
	reqq, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonPayload))
	if err != nil {
		fmt.Println("Error creating request:", err)
		return nil, commonError
	}

	// Set the request headers
	reqq.Header.Set("Content-Type", "application/json")

	// Send the request
	client := &http.Client{}
	resp, err := client.Do(reqq)
	if err != nil {
		fmt.Println("Error sending request:", err)
		return nil, commonError
	}
	defer resp.Body.Close()

	// Read the response body
	var outputInterface interface{}
	err = json.NewDecoder(resp.Body).Decode(&outputInterface)
	if outputInterface != nil {
		js, err := json.Marshal(outputInterface)
		if err != nil {
			return nil, commonError
		}
		var rt pb.AccountBankToVerifyResponse
		err = json.Unmarshal(js, &rt)

		if rt.Code != 0 {
			return &rt, errors.New("Thông tin tài khoản không chính xác")
		}

		return &rt, nil

	} else {
		return nil, errors.New("Number or name invalid")
	}

}

func (s *service) bankAccountVerification(ctx context.Context, req *pb.AccountBankToVerifyRequest) (*pb.AccountBankToVerifyResponse, error) {
	s.log.Info(fmt.Sprintf("Info bank: %d, %s, %s", req.GetBankId(), req.GetBankNumber(), req.GetNameOwner()))

	// check cache
	key := consts.GenKeyForCacheBankAccount(req.BankId, req.GetBankNumber())
	nameOwner, err := s.redis.Get(ctx, key).Result()
	// case hit cache
	if err == nil {
		return &pb.AccountBankToVerifyResponse{
			Code:    0,
			Message: codes.OK.String(),
			Data: &pb.AccountBankToVerifyResponse_Data{
				BeneficiaryName: nameOwner,
			},
		}, nil
	}
	// case miss cache

	if req.GetBankNumber() == "" || req.GetBankId() == int64(0) {
		if req.GetBankId() <= 0 {
			s.log.Error("Bank Id invalid")
			return nil, errors.New("Bank Id invalid")
		}
		s.log.Error("BankAccountVerification lack off fields error")
		return nil, errors.New("Lack of fields")
	}

	// hack
	if s.cfg.Environment != "P" && s.cfg.IsVpBankProdAccountCheck {
		return ProductionBankValidate(req)
	}

	// check exist
	sldBank := getSldBank(req.BankId)
	if sldBank == nil || sldBank.Bin == "" {
		s.log.Error("Bank Id invalie")
		return nil, errors.New("Bank Id invalid")
	}

	s.log.Info(fmt.Sprintf("Info bank designed by saladin: %v", sldBank))

	// check valid
	bankWithBin := s.dbs.GetBankInfoByBin(sldBank.Bin)
	if bankWithBin == nil {
		s.log.Error("bankWithBin invalid")
		return nil, errors.New("bankWithBin invalid")
	}

	s.log.Info(fmt.Sprintf("Info vpBank to call api: %v", bankWithBin))

	resultAfterVerify := s.dbs.GetBeneficiaryInfo(bankWithBin.Bin, req.BankNumber, req.GetNameOwner())

	if resultAfterVerify == nil {
		s.log.Error("resultAfterVerify nil", l.String("bin", bankWithBin.Bin), l.String("bankNumber", req.BankNumber), l.String("nameOwner", req.GetNameOwner()))
		return nil, status.Error(codes.InvalidArgument, "Thông tin tài khoản không chính xác")
	}
	resultAfterVerifyJson, err := json.Marshal(resultAfterVerify)
	if err != nil {
		s.log.Error("resultAfterVerifyJson unmarshal error", l.Error(err))
		return nil, err
	}

	var dataReturn pb.AccountBankToVerifyResponse_Data
	err = json.Unmarshal(resultAfterVerifyJson, &dataReturn)
	if err != nil {
		s.log.Error("resultAfterVerifyJson unmarshal error", l.Error(err))
		return nil, err
	}

	// set cache
	redisErr := s.redis.SetEX(ctx, key, dataReturn.BeneficiaryName, consts.DefaultExpirationAccountNumber).Err()
	if redisErr != nil {
		s.log.Warn("cache data error, bankAccountVerification", l.Error(redisErr))
	}

	return &pb.AccountBankToVerifyResponse{
		Code:    int64(codes.OK),
		Message: codes.OK.String(),
		Data:    &dataReturn,
	}, nil
}

func (s *service) ListBanks(ctx context.Context, req *pb.ListBanksRequest) (*pb.ListBanksResponse, error) {
	return &pb.ListBanksResponse{
		Banks: pbBanks,
	}, nil
}

func (s *service) ListDisbursementBanks(ctx context.Context, req *pb.ListDisbursementBanksRequest) (*pb.ListDisbursementBanksResponse, error) {
	vpBanks := s.dbs.ListBanks()
	if len(banks) == 0 {
		return nil, errors.New("empty data")
	}

	for i := range vpBanks {
		name := mapBanksName[vpBanks[i].Bin]
		if name == "" {
			name = vpBanks[i].Name
		}

		vpBanks[i].Name = name
		vpBanks[i].Icon = mapBanksIcon[vpBanks[i].Bin]
	}

	return &pb.ListDisbursementBanksResponse{
		Banks: vpBanks,
	}, nil
}

func (s *service) BankAccountVerification(ctx context.Context, req *pb.AccountBankToVerifyRequest) (*pb.AccountBankToVerifyResponse, error) {
	return s.bankAccountVerification(ctx, req)
}

func (s *service) InternalBankAccountValidate(ctx context.Context, req *pb.AccountBankToVerifyRequest) (*pb.AccountBankToVerifyResponse, error) {
	return s.bankAccountVerification(ctx, req)
}
