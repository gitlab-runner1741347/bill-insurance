package services

import "git.kafefin.net/backend/central-payout/pkg/consts"

var autoApprovePaymentType = map[string]bool{
	"ClaimGITIDelayed": false,
}

// Review Action
const (
	ReviewActionApprove = "approve"
	ReviewActionReject  = "reject"
)

const (
	ClaimPayment     = "payment_claim"
	AffiliatePayment = "affiliate_payment"
)

var mapTypePayment = map[string]string{
	consts.WithdrawFirstOrder: AffiliatePayment,
	consts.ClaimGITI:          ClaimPayment,
	consts.ClaimGITIDelayed:   ClaimPayment,
	consts.ClaimTravelDelayed: ClaimPayment,
	consts.ClaimTripCancelled: ClaimPayment,
}

func (s *service) FindDisbursementAccountNumberBasedOnType(tp string) string {
	payment := mapTypePayment[tp]
	switch payment {
	case ClaimPayment:
		return s.cfg.Vpbank.ClaimDisbursementAccountNumber
	case AffiliatePayment:
		return s.cfg.Vpbank.AffiliateDisbursementAccountNumber
	}

	return ""
}
