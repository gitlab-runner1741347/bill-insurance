package external

import (
	"context"
	"fmt"

	"git.kafefin.net/backend/central-payout/pkg/convert"
	sldMessagePb "git.kafefin.net/backend/saladin-central-proto/saladin_message"
	"google.golang.org/grpc"
)

type SldMessageSvc interface {
	SendEmailTemplate(
		ctx context.Context,
		request convert.SendEmailRequest,
	) (*sldMessagePb.SendEmailTemplateResponse, error)
	ClientSendEmailTemplate(
		ctx context.Context,
		request *sldMessagePb.SendEmailTemplateRequest,
	) (*sldMessagePb.SendEmailTemplateResponse, error)
}

type sldMessageClient struct {
	client sldMessagePb.SaladinMessageClient
}

func NewSaladinMessageService(conn *grpc.ClientConn) SldMessageSvc {
	return sldMessageClient{
		client: sldMessagePb.NewSaladinMessageClient(conn),
	}
}

func (p sldMessageClient) SendEmailTemplate(
	ctx context.Context,
	request convert.SendEmailRequest,
) (*sldMessagePb.SendEmailTemplateResponse, error) {
	req := &sldMessagePb.SendEmailTemplateRequest{
		To:         []*sldMessagePb.EmailTo{{Name: request.Name, Email: request.Email}},
		TemplateId: request.TemplateId,
		Params:     request.ReqBodyParams,
		Source:     request.Source,
	}
	if len(request.Attachments) > 0 {
		var att []*sldMessagePb.EmailAttachment
		for _, e := range request.Attachments {
			emailAttachment := &sldMessagePb.EmailAttachment{
				Name: fmt.Sprintf("%s.pdf", e.FileName),
				Type: "application/pdf",
				Url:  e.FileURL,
			}
			att = append(att, emailAttachment)
		}
		req.Attachments = att
	}
	if len(request.Cc) > 0 {
		var ccs []*sldMessagePb.EmailTo
		for _, e := range request.Cc {
			to := &sldMessagePb.EmailTo{
				Email: e.Email,
			}
			ccs = append(ccs, to)
		}
		req.Cc = ccs
	}
	return p.client.SendEmailTemplate(ctx, req)
}

func (p sldMessageClient) ClientSendEmailTemplate(
	ctx context.Context,
	request *sldMessagePb.SendEmailTemplateRequest,
) (*sldMessagePb.SendEmailTemplateResponse, error) {

	return p.client.SendEmailTemplate(ctx, request)
}
