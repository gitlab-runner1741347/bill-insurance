package external

import (
	pb "git.kafefin.net/backend/saladin-central-proto/saladin_staff"
	"google.golang.org/grpc"
)

type SaladinStaffClient struct {
	pb.SaladinStaffClient
}

func NewSaladinStaffClient(conn *grpc.ClientConn) SaladinStaffClient {
	return SaladinStaffClient{
		pb.NewSaladinStaffClient(conn),
	}
}
