package callback

import (
	"context"
	"fmt"
	"net/http"

	"git.kafefin.net/backend/central-payout/pkg/types"
	"git.kafefin.net/backend/kitchen/l"
	"git.kafefin.net/backend/kitchen/nap"
)

type Client interface {
	Callback(ctx context.Context, path string) error
}

type NapClient struct {
	cli    *nap.Nap
	logger types.CommonLogger
}

func New(logger types.CommonLogger) *NapClient {
	cli := nap.New()
	c := NapClient{
		cli:    cli,
		logger: logger,
	}
	return &c
}

func (c *NapClient) Callback(ctx context.Context, path string, req interface{}) error {
	var (
		err        error
		failureRaw nap.Raw
	)

	if path == "" {
		c.logger.Warn("callback path is empty")
		return nil
	}

	defer func() {
		if err != nil {
			c.logger.Error(
				"callback service failed",
				l.Error(err),
			)
		}
	}()

	resp, err := c.cli.SetContext(ctx).Post(path).BodyJSON(req).
		AutoRetry(nap.WithRetryTimes(3), nap.WithRetryBackoff(nap.DefaultBackoff)).
		Receive(nil, &failureRaw)
	if err != nil {
		return err
	}
	if failureRaw != nil {
		err = fmt.Errorf("exception error response: %v", string(failureRaw))
		return err
	}
	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf("response status code [%v] is not OK", resp.StatusCode)
		return err
	}
	return nil
}
