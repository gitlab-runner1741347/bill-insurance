package external

import (
	"context"
	"crypto/tls"
	"crypto/x509"

	"git.kafefin.net/backend/central-payout/config"
	"git.kafefin.net/backend/central-payout/pkg/types"
	"git.kafefin.net/backend/kitchen/l"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/credentials/insecure"
)

type ExternalClients struct {
	SaladinStaffClient SaladinStaffClient
	SldMessageSvc      SldMessageSvc
}

func NewExternalClients(cfg *config.External, logger types.CommonLogger) ExternalClients {
	return ExternalClients{
		SaladinStaffClient: NewSaladinStaffClient(newClientConnection(cfg.SaladinStaffAddress, true, logger)),
		SldMessageSvc:      NewSaladinMessageService(newClientConnection(cfg.SaladinMessageAddress, true, logger)),
	}
}

func newClientConnection(address string, isInsecured bool, logger types.CommonLogger) *grpc.ClientConn {
	var opts []grpc.DialOption
	if address != "" {
		opts = append(opts, grpc.WithAuthority(address))
	}

	if isInsecured {
		opts = append(opts, grpc.WithTransportCredentials(insecure.NewCredentials()))
	} else {
		systemRoots, err := x509.SystemCertPool()
		if err != nil {
			logger.Fatal("Failed get cert", l.Error(err))
		}

		cred := credentials.NewTLS(&tls.Config{
			RootCAs: systemRoots,
		})
		opts = append(opts, grpc.WithTransportCredentials(cred))
	}

	conn, err := grpc.DialContext(context.Background(), address, opts...)
	if err != nil {
		logger.Fatal("Failed to dial Base service", l.Error(err))
	}

	return conn
}
