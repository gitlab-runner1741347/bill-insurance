package central_payout

type ProcessPaymentTransactionMessage struct {
	Params struct {
		PaymentRequestID int64 `json:"payment_request_id"`
	} `json:"params"`
}

type CheckPaymentTransactionResultMessage struct {
	Params struct {
		PaymentTransactionID string `json:"payment_transaction_id"`
	} `json:"params"`
}
