package central_payout

import (
	"context"
	"encoding/json"
	"fmt"

	"git.kafefin.net/backend/central-payout/config"
	"git.kafefin.net/backend/central-payout/internal/external/callback"
	"git.kafefin.net/backend/central-payout/internal/repositories"
	"git.kafefin.net/backend/central-payout/pb"
	"git.kafefin.net/backend/central-payout/pkg/consts"
	"git.kafefin.net/backend/central-payout/pkg/disbursement"
	"git.kafefin.net/backend/central-payout/pkg/msteams"
	"git.kafefin.net/backend/central-payout/pkg/pubsub"
	sqs_pubsub "git.kafefin.net/backend/central-payout/pkg/pubsub/sqs"
	"git.kafefin.net/backend/kitchen/l"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/hashicorp/go-multierror"
	"gorm.io/datatypes"
)

type CheckPaymentResult struct {
	ctx            context.Context
	cfg            *config.Config
	logger         l.Logger
	repos          *repositories.Repositories
	dbs            disbursement.PaymentOut
	callbackClient *callback.NapClient
	svc            *sqs.SQS
	subs           pubsub.Subscriber
	teamsAlert     *msteams.TeamsNotificaton
}

func (s *CheckPaymentResult) Run() error {
	return s.subs.Run()
}

func (s *CheckPaymentResult) Close() error {
	return s.subs.Close()
}

func NewCheckPaymentResultSubscriber(
	ctx context.Context,
	cfg *config.Config,
	repos *repositories.Repositories,
	dbs disbursement.PaymentOut,
	callbackClient *callback.NapClient,
	sqsSess *session.Session,
	workerNo int,
	teamsAlert *msteams.TeamsNotificaton,
) (*CheckPaymentResult, error) {
	logger := l.New()
	s := &CheckPaymentResult{
		ctx:            ctx,
		logger:         logger,
		repos:          repos,
		cfg:            cfg,
		dbs:            dbs,
		callbackClient: callbackClient,
		teamsAlert:     teamsAlert,
	}

	svc := sqs.New(sqsSess)
	s.svc = svc

	subs, err := sqs_pubsub.NewSubscriber(ctx, svc, cfg.CheckPaymentTransactionResultQueueUrl, logger, s.handler, sqs_pubsub.WithWorkerNum(workerNo))
	if err != nil {
		return nil, err
	}
	s.subs = subs

	return s, nil
}

func (s *CheckPaymentResult) handler(msg pubsub.SubscriberMessage) {
	var (
		err error
		v   CheckPaymentTransactionResultMessage
	)
	defer func() {
		if err != nil {
			s.logger.Error("handle order message failed", l.Error(err))
		}
	}()
	// ctx, cancel := context.WithTimeout(s.ctx, 10*time.Second)
	// defer cancel()

	s.logger.Info("CheckPaymentResult handler", l.Any("msg", msg))

	if err = json.Unmarshal([]byte(msg.Value()), &v); err != nil {
		return
	}
	params := v.Params

	if params.PaymentTransactionID == "" {
		s.logger.Error("PaymentTransactionID is empty", l.Any("params", params))
		msg.Commit("")
		return
	}

	paymentTransaction, err := s.repos.PaymentTransactionRepository.GetOneDetailByID(params.PaymentTransactionID)
	if err != nil {
		s.logger.Error("PaymentTransactionRepository.GetOneDetailByID", l.Error(err))
		return
	}

	if paymentTransaction.Status == pb.PaymentTransactionStatus_PTS_FAILED || paymentTransaction.Status == pb.PaymentTransactionStatus_PTS_SUCCESS {
		s.logger.Info("payment transaction complete", l.Int32("status", int32(paymentTransaction.Status)))

		if err = s.callbackClient.Callback(context.TODO(), paymentTransaction.PaymentRequest.CallbackURL, &pb.PaymentCallback{
			SrcId:                    paymentTransaction.PaymentRequest.SrcID,
			PaymentId:                paymentTransaction.PaymentRequest.ID,
			PaymentStatus:            paymentTransaction.PaymentRequest.Status,
			PaymentTransactionStatus: paymentTransaction.Status,
			CampaignCode:             paymentTransaction.PaymentRequest.CampaignCode,
			IdempotentKey:            paymentTransaction.PaymentRequest.IdempotentKey.String,
		}); err != nil {
			s.logger.Error("callbackClient.Callback", l.Error(err), l.Any("payment transaction", paymentTransaction))
			return
		}

		err = msg.Commit("")
		return
	}

	resp := s.dbs.GetTransactionInfo(params.PaymentTransactionID, paymentTransaction.PaymentRequest.DisbursementAccountNumber)

	if len(resp) == 0 {
		s.logger.Warn("ft not found", l.Any("vpb ft detail", resp))

		paymentTransaction.PaymentTransaction.Status = pb.PaymentTransactionStatus_PTS_FAILED
		paymentTransaction.PaymentRequest.TxnStatus = pb.PaymentTransactionStatus_PTS_FAILED
	} else {
		ftDetail := resp[len(resp)-1]

		switch ftDetail.Status {
		case "AUTH":
			paymentTransaction.PaymentTransaction.Status = pb.PaymentTransactionStatus_PTS_SUCCESS
			paymentTransaction.PaymentRequest.TxnStatus = pb.PaymentTransactionStatus_PTS_SUCCESS
		case "INAUTH":
			paymentTransaction.PaymentTransaction.Status = pb.PaymentTransactionStatus_PTS_FAILED
			paymentTransaction.PaymentRequest.TxnStatus = pb.PaymentTransactionStatus_PTS_FAILED
		}
		paymentTransaction.VpbTransaction.Status = ftDetail.Status
		paymentTransaction.VpbTransaction.TransactionDate = ftDetail.TransactionDate
		paymentTransaction.VpbTransaction.TransactionID = ftDetail.TransactionId
		paymentTransaction.VpbTransaction.FTDetails = datatypes.NewJSONType(resp)
	}

	if err := s.repos.Transaction(func(r *repositories.Repositories) error {
		if err := r.PaymentTransactionRepository.Update(paymentTransaction.PaymentTransaction); err != nil {
			return multierror.Prefix(err, "PaymentTransactionRepository.Update")
		}

		if err := r.VpbTransactionRepository.Update(*paymentTransaction.VpbTransaction); err != nil {
			return multierror.Prefix(err, "VpbTransactionRepository.Update")
		}

		if err := r.PaymentRequestRepository.UpdateTxnStatusByID(paymentTransaction.PaymentRequest.ID, paymentTransaction.PaymentRequest.TxnStatus); err != nil {
			return multierror.Prefix(err, "PaymentRequestRepository.UpdateTxnStatusByID")
		}

		return nil
	}); err != nil {
		s.logger.Error("Transaction", l.Error(err))
		return
	}

	if err := s.callbackClient.Callback(context.TODO(), paymentTransaction.PaymentRequest.CallbackURL, &pb.PaymentCallback{
		SrcId:                    paymentTransaction.PaymentRequest.SrcID,
		PaymentId:                paymentTransaction.PaymentRequest.ID,
		PaymentStatus:            paymentTransaction.PaymentRequest.Status,
		PaymentTransactionStatus: paymentTransaction.Status,
		CampaignCode:             paymentTransaction.PaymentRequest.CampaignCode,
		IdempotentKey:            paymentTransaction.PaymentRequest.IdempotentKey.String,
	}); err != nil {
		s.logger.Error("callbackClient.Callback", l.Error(err))
		return
	}

	// notification
	if paymentTransaction.Status == pb.PaymentTransactionStatus_PTS_FAILED {

		detailUrl := consts.DetailUrlPaymentRequest(s.cfg.SaladinAdminDomain, paymentTransaction.PaymentRequest.SrcID, paymentTransaction.PaymentRequest.ID)
		go func(id int64, urlDetail string) {
			if err := s.teamsAlert.Send(fmt.Sprintf("Yêu cầu thanh toán số  %d thanh toán thất bại, vui lòng kiểm tra và thanh toán lại %s", id, urlDetail)); err != nil {
				s.logger.Error("notification via team error", l.Error(err))
			}
		}(paymentTransaction.PaymentRequest.ID, detailUrl)

	}

	err = msg.Commit("")
}
