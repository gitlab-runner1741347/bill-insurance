package central_payout

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
	"time"

	"git.kafefin.net/backend/central-payout/config"
	"git.kafefin.net/backend/central-payout/internal/external/callback"
	"git.kafefin.net/backend/central-payout/internal/models"
	"git.kafefin.net/backend/central-payout/internal/repositories"
	"git.kafefin.net/backend/central-payout/pb"
	"git.kafefin.net/backend/central-payout/pkg/convert"
	"git.kafefin.net/backend/central-payout/pkg/datetimeutils"
	"git.kafefin.net/backend/central-payout/pkg/disbursement"
	"git.kafefin.net/backend/central-payout/pkg/pmtxn"
	"git.kafefin.net/backend/central-payout/pkg/pubsub"
	sqspubsub "git.kafefin.net/backend/central-payout/pkg/pubsub/sqs"
	"git.kafefin.net/backend/central-payout/pkg/stringutils"
	"git.kafefin.net/backend/kitchen/l"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/hashicorp/go-multierror"
)

const VietnamTz = "Asia/Ho_Chi_Minh"

var vnLocation, _ = time.LoadLocation(VietnamTz)

// campain, origin_request_id, order_id, beneficiary_name
// const remark = "Saladin-%s-MKN %s-DH %s-%s-GITI Ahamove"

type ProcessPayment struct {
	ctx            context.Context
	cfg            *config.Config
	logger         l.Logger
	repos          *repositories.Repositories
	svc            *sqs.SQS
	subs           pubsub.Subscriber
	pub            pubsub.Publisher
	dbs            disbursement.PaymentOut
	callbackClient *callback.NapClient
}

func (s *ProcessPayment) Run() error {
	return s.subs.Run()
}

func (s *ProcessPayment) Close() error {
	return s.subs.Close()
}

func NewProcessPaymentSubscriber(
	ctx context.Context,
	cfg *config.Config,
	repos *repositories.Repositories,
	dbs disbursement.PaymentOut,
	callbackClient *callback.NapClient,
	sqsSess *session.Session,
	workerNo int,
) (*ProcessPayment, error) {
	logger := l.New()
	s := &ProcessPayment{
		ctx:            ctx,
		logger:         logger,
		repos:          repos,
		cfg:            cfg,
		dbs:            dbs,
		callbackClient: callbackClient,
	}

	svc := sqs.New(sqsSess)
	s.svc = svc

	subs, err := sqspubsub.NewSubscriber(ctx, svc, cfg.ProcessPaymentTransactionQueueUrl, logger, s.handler, sqspubsub.WithWorkerNum(workerNo))
	if err != nil {
		return nil, err
	}
	s.subs = subs

	pub, err := sqspubsub.New(svc)
	if err != nil {
		return nil, err
	}
	s.pub = pub

	return s, nil
}

func (s *ProcessPayment) handler(msg pubsub.SubscriberMessage) {
	var (
		v ProcessPaymentTransactionMessage
	)

	// ctx, cancel := context.WithTimeout(s.ctx, 10*time.Second)
	// defer cancel()

	s.logger.Info("ProcessPayment handler", l.Any("msg", msg))

	if err := json.Unmarshal([]byte(msg.Value()), &v); err != nil {
		s.logger.Error("json.Unmarshal", l.Error(err))
		return
	}
	params := v.Params

	// at most one strategy
	if err := msg.Commit(""); err != nil {
		s.logger.Error("msg.Commit", l.Error(err))
		return
	}

	var vpbTxn = models.VpbTransaction{}
	pmTxnID := pmtxn.GenPaymentRequestID()
	var paymentTxn models.PaymentTransaction
	if err := s.repos.Transaction(func(r *repositories.Repositories) error {
		paymentRequest, err := s.repos.PaymentRequestRepository.GetUpdateByID(params.PaymentRequestID)
		if err != nil {
			return err
		}

		if err := s.repos.PaymentRequestRepository.UpdateTxnStatusByID(paymentRequest.ID, pb.PaymentTransactionStatus_PTS_PENDING); err != nil {
			s.logger.Error("PaymentRequestRepository.UpdateTxnStatusByID pending", l.Error(err))
			return err
		}

		if paymentRequest.Status != pb.PaymentStatus_PRS_APPROVED && paymentRequest.TxnStatus != pb.PaymentTransactionStatus_PTS_PENDING {
			return errors.New("invalid payment request status")
		}

		bank := s.dbs.GetBankInfoByBin(paymentRequest.BankBin)
		if bank == nil {
			return errors.New("invalid bank bin")
		}

		paymentTxn = models.PaymentTransaction{
			ID:                       pmTxnID,
			PaymentRequestID:         paymentRequest.ID,
			PaymentServiceProvider:   pb.PaymentServiceProvider_PSP_VPB,
			SldBankID:                paymentRequest.SldBankID,
			PspBankID:                bank.Id,
			PspBankCode:              bank.Code,
			PspBankName:              bank.Name,
			PspBankShortName:         bank.ShortName,
			PspBin:                   bank.Bin,
			BeneficiaryAccountNumber: paymentRequest.AccountNumber,
			BeneficiaryAccountName:   paymentRequest.AccountName,
			DebitAccountNumber:       paymentRequest.DisbursementAccountNumber,
			PaymentAmount:            paymentRequest.PaymentAmount,
			Currency:                 "VND",
			TransactionDetail:        stringutils.Normalize(paymentRequest.TransactionDetail),
		}
		if _, err := r.PaymentTransactionRepository.Create(paymentTxn); err != nil {
			return multierror.Prefix(err, "PaymentTransactionRepository.Create")
		}

		vpbTxn.ReferenceNumber = paymentTxn.ID
		vpbTxn.Amount = strconv.FormatInt(paymentRequest.PaymentAmount, 10)
		vpbTxn.SourceNumber = paymentRequest.DisbursementAccountNumber
		vpbTxn.TargetNumber = paymentRequest.AccountNumber
		vpbTxn.BenName = paymentRequest.AccountName
		vpbTxn.Remark = paymentTxn.TransactionDetail
		vpbTxn.BankName = paymentRequest.BankName
		vpbTxn.BankBin = paymentRequest.BankBin
		vpbTxn.BankID = fmt.Sprint(bank.Id)
		if err := r.VpbTransactionRepository.CreateRef(&vpbTxn); err != nil {
			return multierror.Prefix(err, "VpbTransactionRepository.Create")
		}

		if err := s.verifyBankAccount(context.TODO(), bank.Bin, paymentRequest.AccountNumber, paymentRequest.AccountName); err != nil {
			s.logger.Error("verifyBankAccount", l.Error(err))
			paymentTxn.ErrorMessage = err.Error()
			if err := r.PaymentTransactionRepository.Update(paymentTxn); err != nil {
				s.logger.Error("PaymentTransactionRepository.Update", l.Error(err))
			}

			return nil
		}

		if disbursement.IsVPBank(bank.Bin) {
			vpbTxn.Service = "internal"
			vpbTxn.Type = "CACA"
			vpbTxn.Currency = "VND"

			resp, err := s.dbs.InternalTransfer(disbursement.InternalTransferRequest{
				ReferenceNumber: vpbTxn.ReferenceNumber,
				Service:         vpbTxn.Service,
				TransactionType: vpbTxn.Type,
				SourceNumber:    vpbTxn.SourceNumber,
				TargetNumber:    vpbTxn.TargetNumber,
				BenName:         vpbTxn.BenName,
				Amount:          paymentRequest.PaymentAmount,
				Currency:        vpbTxn.Currency,
				Remark:          paymentTxn.TransactionDetail,
			})
			if err != nil {
				s.logger.Error("InternalTransfer", l.Error(err))
				paymentTxn.ErrorMessage = err.Error()
				vpbTxn.ErrorMessage = err.Error()

				return nil
			}

			paymentTxn.RefTxnID = resp.TransactionID
			paymentTxn.PartnerTransferResult = resp.TransferResult
			paymentTxn.PartnerTransactionDate = datetimeutils.StringToTimeWithTz(resp.TransactionDate, datetimeutils.VPBTransactionDateLayout, vnLocation)
			paymentTxn.PartnerTransferDate = datetimeutils.StringToTimeWithTz(resp.TransferDate, datetimeutils.VPBTransferDateLayout, vnLocation)
			paymentTxn.Status = convert.VPBTxnSttToOurStt(resp.TransferResult)

			vpbTxn.TransferResult = resp.TransferResult
			vpbTxn.TransactionDate = resp.TransactionDate
			vpbTxn.TransferDate = resp.TransferDate
			vpbTxn.TransactionID = resp.TransactionID

		} else {
			vpbTxn.Service = "chiho"
			vpbTxn.Type = "FAST"
			vpbTxn.Currency = "VND"

			resp, err := s.dbs.ExternalTransfer(disbursement.ExternalTransferRequest{
				ReferenceNumber: vpbTxn.ReferenceNumber,
				Service:         vpbTxn.Service,
				TransactionType: vpbTxn.Type,
				SourceNumber:    vpbTxn.SourceNumber,
				TargetNumber:    vpbTxn.TargetNumber,
				BenName:         vpbTxn.BenName,
				Amount:          paymentRequest.PaymentAmount,
				Currency:        vpbTxn.Currency,
				Remark:          vpbTxn.Remark,
				BankID:          bank.Bin,
			})
			if err != nil {
				s.logger.Error("ExternalTransfer", l.Error(err))
				paymentTxn.ErrorMessage = err.Error()
				vpbTxn.ErrorMessage = err.Error()

				return nil
			}

			paymentTxn.RefTxnID = resp.TransactionID
			paymentTxn.PartnerTransferResult = resp.TransferResult
			paymentTxn.PartnerTransactionDate = datetimeutils.StringToTimeWithTz(resp.TransactionDate, datetimeutils.VPBTransactionDateLayout, vnLocation)
			paymentTxn.PartnerTransferDate = datetimeutils.StringToTimeWithTz(resp.TransferDate, datetimeutils.VPBTransferDateLayout, vnLocation)
			paymentTxn.Status = convert.VPBTxnSttToOurStt(resp.TransferResult)

			vpbTxn.TransferResult = resp.TransferResult
			vpbTxn.TransactionDate = resp.TransactionDate
			vpbTxn.TransferDate = resp.TransferDate
			vpbTxn.TransactionID = resp.TransactionID
		}

		if err := s.repos.PaymentRequestRepository.UpdateTxnStatusByID(paymentRequest.ID, paymentTxn.Status); err != nil {
			s.logger.Error("PaymentRequestRepository.UpdateTxnStatusByID", l.Error(err))
		}

		return nil
	}); err != nil {
		s.logger.Error("Transaction", l.Error(err), l.Any("params", params))
		return
	}

	if err := s.repos.PaymentTransactionRepository.Update(paymentTxn); err != nil {
		s.logger.Error("PaymentTransactionRepository.Update", l.Error(err))
	}

	if err := s.repos.VpbTransactionRepository.Update(vpbTxn); err != nil {
		s.logger.Error("VpbTransactionRepository.Update", l.Error(err))
	}

	if err := s.publishCheckPaymentTransactionResultMessage(context.TODO(), pmTxnID); err != nil {
		s.logger.Error("publishCheckPaymentTransactionResultMessage", l.Error(err))
	}
}

func (s *ProcessPayment) verifyBankAccount(ctx context.Context, bankBin, ownerNumber, ownerName string) error {

	// mock
	// if s.cfg.Environment != "P" {
	// 	return MockData(req)
	// }

	resultAfterVerify := s.dbs.GetBeneficiaryInfo(bankBin, ownerNumber, ownerName)
	if resultAfterVerify == nil {
		return errors.New("Thông tin tài khoản không chính xác")
	}

	return nil
}

func (s *ProcessPayment) publishCheckPaymentTransactionResultMessage(ctx context.Context,
	paymentTransactionID string) error {
	msgBody, err := json.Marshal(CheckPaymentTransactionResultMessage{Params: struct {
		PaymentTransactionID string `json:"payment_transaction_id"`
	}{
		PaymentTransactionID: paymentTransactionID,
	}})
	if err != nil {
		return multierror.Prefix(err, "Marshal materialSub.CheckPaymentTransactionResultQueueUrlMessage")
	}
	message := sqspubsub.NewPublishMessage(string(msgBody))
	if err := s.pub.Publish(ctx, s.cfg.CheckPaymentTransactionResultQueueUrl, message); err != nil {
		return multierror.Prefix(err, "pub.Publish")
	}

	return nil
}
