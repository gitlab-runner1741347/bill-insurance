package repositories

import (
	"time"

	"git.kafefin.net/backend/central-payout/internal/models"
	"git.kafefin.net/backend/central-payout/pb"
	"git.kafefin.net/backend/central-payout/pkg/sldmath"
	"github.com/hashicorp/go-multierror"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type PaymentRequestRepository struct {
	db        *gorm.DB
	tableName string
}

func NewPaymentRequestRepository(db *gorm.DB) *PaymentRequestRepository {
	return &PaymentRequestRepository{
		db:        db,
		tableName: models.PaymentRequest{}.TableName(),
	}
}

func (r *PaymentRequestRepository) Create(m models.PaymentRequest) (*models.PaymentRequest, error) {
	return &m, r.db.Table(r.tableName).
		Create(&m).Error
}

func (r *PaymentRequestRepository) GetOneDetailByID(id int64) (*models.PaymentRequestDetail, error) {
	m := models.PaymentRequestDetail{}

	return &m, r.db.Table(r.tableName).Where("id = ?", id).Preload("PaymentTransactions").First(&m).Error
}

func (r *PaymentRequestRepository) GetUpdateByID(id int64) (*models.PaymentRequest, error) {
	m := models.PaymentRequest{}

	return &m, r.db.Table(r.tableName).Clauses(clause.Locking{
		Strength: "UPDATE",
	}).Where("id = ?", id).First(&m).Error
}

func (r *PaymentRequestRepository) GetUpdateByIDs(ids []int64) ([]*models.PaymentRequest, error) {
	var m []*models.PaymentRequest

	return m, r.db.Table(r.tableName).Clauses(clause.Locking{
		Strength: "UPDATE",
	}).Where("id IN (?)", ids).Find(&m).Error
}

func (r *PaymentRequestRepository) GetTotalPaymentAmountByIDs(ids []int64) (int64, error) {
	var totalAmount int64

	return totalAmount, r.db.Table(r.tableName).Where("id IN (?)", ids).Select("SUM(payment_amount)").Scan(&totalAmount).Error
}

func (r *PaymentRequestRepository) PaginatePaymentRequests(req *pb.AdminGetPaymentsRequest) ([]*models.PaymentRequest, int64, error) {
	var payments []*models.PaymentRequest
	var total int64

	limit := sldmath.GenericMax(int(req.Limit), 10)

	q := r.db.Table(r.tableName)
	if req.GetPaymentRequestId() != 0 {
		q.Where("payment_request.id = ?", req.PaymentRequestId)
	}
	if req.GetSrcId() != "" {
		q.Where("payment_request.src_id = ?", req.SrcId)
	}
	if req.GetOriginRequestId() != "" {
		q.Where("payment_request.src_id = ?", req.OriginRequestId)
	}
	if req.GetFromDate() != nil {
		q.Where("payment_request.created_at >= ?", req.FromDate.AsTime())
	}
	if req.GetToDate() != nil {
		q.Where("DATE(payment_request.created_at) <= ?", req.ToDate.AsTime().Add(time.Hour*24))
	}
	if req.GetStatus() > 0 {
		q.Where("payment_request.status = ?", req.Status)
	}
	if req.GetDebtor() > 0 {
		q.Where("payment_request.debtor = ?", req.Debtor)
	}
	if req.GetPaymentType() != "" {
		q.Where("payment_request.payment_type = ?", req.PaymentType)
	}
	if req.GetCampaignCode() != "" {
		q.Where("payment_request.campaign_code = ?", req.CampaignCode)
	}

	q.Count(&total).Order("payment_request.created_at desc").Limit(limit).Offset(int(req.GetOffset()))

	return payments, total, q.Find(&payments).Error
}

func (r *PaymentRequestRepository) AccountantApproveByIDs(ids []int64, status pb.PaymentStatus, note string) error {
	return r.db.Table(r.tableName).Where("id IN (?)", ids).Updates(map[string]interface{}{
		"status":          status,
		"accountant_note": note,
	}).Error
}

func (r *PaymentRequestRepository) CeoApproveByIDs(ids []int64, status pb.PaymentStatus, note string) error {
	return r.db.Table(r.tableName).Where("id IN (?)", ids).Updates(map[string]interface{}{
		"status":   status,
		"ceo_note": note,
	}).Error
}

func (r *PaymentRequestRepository) ResetTxnSttByIDs(ids []int64) error {
	return multierror.Prefix(r.db.Table(r.tableName).Where("id IN (?) AND txn_status = ?", ids, pb.PaymentTransactionStatus_PTS_FAILED).Updates(map[string]interface{}{
		"txn_status": pb.PaymentTransactionStatus_PTS_UNSPECIFIED,
	}).Error, "ResetTxnSttByIDs")
}

func (r *PaymentRequestRepository) UpdateTxnStatusByID(id int64, txnStatus pb.PaymentTransactionStatus) error {
	return r.db.Table(r.tableName).Where("id = ?", id).Updates(map[string]interface{}{
		"txn_status": txnStatus,
	}).Error
}

func (r *PaymentRequestRepository) GetOneByIdempotentKey(idemKey, svc string) (*models.PaymentRequestDetail, error) {
	m := models.PaymentRequestDetail{}
	err := r.db.Table(r.tableName).Where("idempotent_key = ? AND src_service = ?", idemKey, svc).First(&m).Error
	if err != nil {
		return nil, err
	}

	return &m, nil
}
