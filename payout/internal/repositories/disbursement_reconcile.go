package repositories

import (
	"git.kafefin.net/backend/central-payout/internal/models"
	"gorm.io/gorm"
)

type DisbursementReconcileRepository struct {
	db        *gorm.DB
	tableName string
}

func NewDisbursementReconcileRepository(db *gorm.DB) *DisbursementReconcileRepository {
	return &DisbursementReconcileRepository{
		db:        db,
		tableName: models.DisbursementReconcile{}.TableName(),
	}
}

func (r *DisbursementReconcileRepository) Create(m models.DisbursementReconcile) (models.DisbursementReconcile, error) {
	return m, r.db.Table(r.tableName).
		Create(&m).Error
}

func (r *DisbursementReconcileRepository) Save(m models.DisbursementReconcile) (models.DisbursementReconcile, error) {
	return m, r.db.Table(r.tableName).
		Save(&m).Error
}
