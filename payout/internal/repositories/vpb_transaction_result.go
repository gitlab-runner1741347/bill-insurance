package repositories

import (
	"git.kafefin.net/backend/central-payout/internal/models"
	"gorm.io/gorm"
)

type VpbTransactionRepository struct {
	db        *gorm.DB
	tableName string
}

func NewVpbTransactionRepository(db *gorm.DB) *VpbTransactionRepository {
	return &VpbTransactionRepository{
		db:        db,
		tableName: models.VpbTransaction{}.TableName(),
	}
}

func (r *VpbTransactionRepository) Create(m models.VpbTransaction) (models.VpbTransaction, error) {
	return m, r.db.Table(r.tableName).
		Create(&m).Error
}

func (r *VpbTransactionRepository) CreateRef(m *models.VpbTransaction) error {
	return r.db.Table(r.tableName).
		Create(&m).Error
}

func (r *VpbTransactionRepository) Update(m models.VpbTransaction) error {
	return r.db.Table(r.tableName).Where("id = ?", m.ID).
		Updates(m).Error
}
