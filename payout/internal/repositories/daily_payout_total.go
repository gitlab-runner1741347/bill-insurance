package repositories

import (
	"errors"
	"time"

	"git.kafefin.net/backend/central-payout/internal/models"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type DailyPayoutTotalRepository struct {
	db        *gorm.DB
	tableName string
}

func NewDailyPayoutTotalRepository(db *gorm.DB) *DailyPayoutTotalRepository {
	return &DailyPayoutTotalRepository{
		db:        db,
		tableName: models.DailyPayoutTotal{}.TableName(),
	}
}

func (r *DailyPayoutTotalRepository) Create(m models.DailyPayoutTotal) (*models.DailyPayoutTotal, error) {
	return &m, r.db.Table(r.tableName).
		Create(&m).Error
}

func (r *DailyPayoutTotalRepository) CreateNoConflict(m models.DailyPayoutTotal) (*models.DailyPayoutTotal, error) {
	return &m, r.db.Table(r.tableName).Clauses(clause.OnConflict{DoNothing: true}).
		Create(&m).Error
}

func (r *DailyPayoutTotalRepository) GetUpdateToday() (*models.DailyPayoutTotal, error) {
	today := time.Now().Format("20060102")

	var dailyTotal *models.DailyPayoutTotal
	err := r.db.Clauses(clause.Locking{
		Strength: "UPDATE",
	}).Where("id = ?", today).First(&dailyTotal).Error
	if errors.Is(err, gorm.ErrRecordNotFound) {
		if _, err := r.CreateNoConflict(models.DailyPayoutTotal{ID: today}); err != nil {
			return nil, err
		}

		return r.GetUpdateToday()
	}
	if err != nil {
		return nil, err
	}

	return dailyTotal, err
}

func (r *DailyPayoutTotalRepository) GetToday() (*models.DailyPayoutTotal, error) {
	today := time.Now().Format("20060102")

	var dailyTotal *models.DailyPayoutTotal
	err := r.db.Where("id = ?", today).First(&dailyTotal).Error
	if errors.Is(err, gorm.ErrRecordNotFound) {
		newDailyTotal, err := r.CreateNoConflict(models.DailyPayoutTotal{ID: today})
		if err != nil {
			return nil, err
		}

		return newDailyTotal, nil
	}
	if err != nil {
		return nil, err
	}

	return dailyTotal, err
}

func (r *DailyPayoutTotalRepository) UpdateTotalAmount(id string, amount int64) error {
	return r.db.Table(r.tableName).Where("id = ?", id).Update("total_amount", amount).Error
}
