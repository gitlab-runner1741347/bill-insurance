package repositories

import (
	"git.kafefin.net/backend/central-payout/internal/models"
	"gorm.io/gorm"
)

type PaymentTransactionRepository struct {
	db        *gorm.DB
	tableName string
}

func NewPaymentTransactionRepository(db *gorm.DB) *PaymentTransactionRepository {
	return &PaymentTransactionRepository{
		db:        db,
		tableName: models.PaymentTransaction{}.TableName(),
	}
}

func (r *PaymentTransactionRepository) GetOneDetailByID(id string) (*models.PaymentTransactionDetail, error) {
	m := models.PaymentTransactionDetail{}

	return &m, r.db.Table(r.tableName).Where("id = ?", id).Preload("PaymentRequest").Preload("VpbTransaction").First(&m).Error
}

func (r *PaymentTransactionRepository) Create(m models.PaymentTransaction) (*models.PaymentTransaction, error) {
	return &m, r.db.Table(r.tableName).
		Create(&m).Error
}

func (r *PaymentTransactionRepository) Update(m models.PaymentTransaction) error {
	return r.db.Table(r.tableName).
		Updates(m).Error
}
