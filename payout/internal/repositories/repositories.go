package repositories

import (
	"gorm.io/gorm"
)

type Repositories struct {
	db                              *gorm.DB
	DisbursementReconcileRepository *DisbursementReconcileRepository
	DailyPayoutTotalRepository      *DailyPayoutTotalRepository
	PaymentRequestRepository        *PaymentRequestRepository
	PaymentTransactionRepository    *PaymentTransactionRepository
	VpbTransactionRepository        *VpbTransactionRepository
}

func NewRepositories(db *gorm.DB) *Repositories {
	return &Repositories{
		db:                              db,
		DisbursementReconcileRepository: NewDisbursementReconcileRepository(db),
		DailyPayoutTotalRepository:      NewDailyPayoutTotalRepository(db),
		PaymentRequestRepository:        NewPaymentRequestRepository(db),
		PaymentTransactionRepository:    NewPaymentTransactionRepository(db),
		VpbTransactionRepository:        NewVpbTransactionRepository(db),
	}
}

func (r Repositories) Transaction(txFunc func(*Repositories) error) (err error) {
	return r.db.Transaction(func(tx *gorm.DB) error {
		txRepositories := NewRepositories(tx)

		return txFunc(txRepositories)
	})
}
