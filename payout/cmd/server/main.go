package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"git.kafefin.net/backend/central-payout/config"
	"git.kafefin.net/backend/central-payout/internal/external"
	"git.kafefin.net/backend/central-payout/internal/external/callback"
	"git.kafefin.net/backend/central-payout/internal/must"
	"git.kafefin.net/backend/central-payout/internal/repositories"
	"git.kafefin.net/backend/central-payout/internal/services"
	cpsub "git.kafefin.net/backend/central-payout/internal/subscribers/central-payout"
	"git.kafefin.net/backend/central-payout/pkg/disbursement"
	"git.kafefin.net/backend/central-payout/pkg/msteams"
	sqsPubsub "git.kafefin.net/backend/central-payout/pkg/pubsub/sqs"
	"git.kafefin.net/backend/central-payout/pkg/types"
	"git.kafefin.net/backend/kitchen/l"
	"git.kafefin.net/backend/kitchen/oteljaeger"
	"git.kafefin.net/backend/salad-kit/grpckit"
	"git.kafefin.net/backend/salad-kit/server"
	staffpb "git.kafefin.net/backend/saladin-central-proto/saladin_staff"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
	"go.opentelemetry.io/otel/trace"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

func run(_ []string) error {
	var ctx = context.TODO()
	var cfg = config.Load()

	var ll types.CommonLogger
	var trace trace.TracerProvider
	if cfg.Environment == "D" {
		tp, shutdownFn, err := oteljaeger.JaegerProvider("delivery-insurance", cfg.OtelCollectorAddress)
		if err != nil {
			return err
		}
		trace = tp

		defer func(ctx context.Context) {
			if err := shutdownFn(ctx); err != nil {
				ll.Error("shutdown tracing provider", l.Error(err))
			}
		}(ctx)

		ll = l.NewWithOtel(tp)
	} else {
		ll = l.New()
	}

	db := must.ConnectMySQL(cfg.MySQL)
	repos := repositories.NewRepositories(db)
	redis := must.ConnectRedis(cfg.Redis)
	dbs := disbursement.NewVpbankPayment(cfg.Vpbank.Host, cfg.Vpbank.Idn, cfg.Vpbank.BearerToken, trace, ll, cfg.Vpbank.Pem, cfg.Vpbank.Cer)
	callbackClient := callback.New(ll)
	teamsAlert := msteams.NewTeamsNoti(cfg.Teams.OperationAlertWebhook)

	sqsSess := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
		Config: aws.Config{
			Region: aws.String("ap-southeast-1"),
		},
	}))
	sqsSvc := sqs.New(sqsSess)

	pub, err := sqsPubsub.New(sqsSvc)
	if err != nil {
		return fmt.Errorf("initialize publisher client %w", err)
	}

	processPaymentSubs, err := cpsub.NewProcessPaymentSubscriber(ctx, cfg, repos, dbs, callbackClient, sqsSess, 1)
	if err != nil {
		return err
	}
	go func() {
		ll.Info("processPaymentSubs subscriber starting..")
		if err = processPaymentSubs.Run(); err != nil {
			ll.Error("start subscriber failed", l.Error(err))
		}
	}()

	checkPaymentSubs, err := cpsub.NewCheckPaymentResultSubscriber(ctx, cfg, repos, dbs, callbackClient, sqsSess, 1, teamsAlert)
	if err != nil {
		return err
	}
	go func() {
		ll.Info("checkPaymentSubs subscriber starting..")
		if err = checkPaymentSubs.Run(); err != nil {
			ll.Error("start subscriber failed", l.Error(err))
		}
	}()

	externalClients := external.NewExternalClients(cfg.External, ll)

	mw := NewCustomMiddleware()
	srv, err := server.New(
		server.WithGrpcAddrListen(cfg.Server.GRPC),
		server.WithGatewayAddrListen(cfg.Server.HTTP),
		server.WithServiceServer(
			// Your service server init goes here
			// example: services.NewServer(cfg),
			services.NewCentralPayoutService(ctx, cfg, ll, repos, dbs, callbackClient, pub, redis, teamsAlert, externalClients),
		),
		server.WithGatewayServerMiddlewares(mw.MiddlewareHandleFunc),
		server.WithGrpcServerUnaryInterceptors(grpckit.MappingStaffResourceInterceptor(srnMap, validateAdminRsFn(ll, externalClients.SaladinStaffClient))),
	)
	if err != nil {
		return fmt.Errorf("initialize server %w", err)
	}

	if err := srv.Serve(ctx); err != nil {
		return fmt.Errorf("serving %w", err)
	}

	go func() {
		ll.Info("update list bank starting..")
		for {
			time.Sleep(time.Hour * 4)
			if err = dbs.UpdateListBank(); err != nil {
				ll.Error("start subscriber failed", l.Error(err))
			}
		}
	}()

	return nil
}

var srnMap = map[string]grpckit.ResourceAction{
	"/central_payout.CentralPayoutAdminService/AdminGetPayments": {
		Resource: "srn:central-payout:payment/list",
		Action:   "read",
	},
	"/central_payout.CentralPayoutAdminService/AdminAccountantGetPayments": {
		Resource: "srn:central-payout:accountant:payment/list",
		Action:   "read",
	},
	"/central_payout.CentralPayoutAdminService/AdminCeoGetPayments": {
		Resource: "srn:central-payout:ceo:payment/list",
		Action:   "write",
	},
	"/central_payout.CentralPayoutAdminService/AdminGetPayment": {
		Resource: "srn:central-payout:payment/get",
		Action:   "read",
	},
	"/central_payout.CentralPayoutAdminService/AdminAccountantReviewPayments": {
		Resource: "srn:central-payout:accountant:payment/review",
		Action:   "write",
	},
	"/central_payout.CentralPayoutAdminService/AdminCeoReviewPayments": {
		Resource: "srn:central-payout:ceo:payment/review",
		Action:   "write",
	},
	"/central_payout.CentralPayoutAdminService/AdminRetryPayments": {
		Resource: "srn:central-payout:payment/retry",
		Action:   "write",
	},
}

func validateAdminRsFn(log types.CommonLogger, staffClient external.SaladinStaffClient) func(context.Context, grpckit.ResourceAction) (grpckit.StaffPolicy, error) {
	return func(ctx context.Context, rs grpckit.ResourceAction) (grpckit.StaffPolicy, error) {
		md, ok := metadata.FromIncomingContext(ctx)
		if !ok {
			return nil, status.Error(codes.Internal, "invalid metadata")
		}

		mdAuthorizations := md.Get("authorization")
		if len(mdAuthorizations) == 0 {
			return nil, status.Error(codes.PermissionDenied, "invalid Authorization header")
		}
		tokenStrings := strings.Split(mdAuthorizations[0], "Bearer ")
		if len(tokenStrings) != 2 {
			return nil, status.Error(codes.PermissionDenied, "invalid Authorization header")
		}

		request := &staffpb.TokenPolicyRequest{
			Token:    tokenStrings[1],
			Action:   rs.Action,
			Resource: rs.Resource,
		}
		policy, err := staffClient.TokenPolicy(context.Background(), request)
		if err != nil {
			log.Error("validateAdminRsFn call saladin-staff error", l.Error(err))
			return nil, status.Error(codes.PermissionDenied, err.Error())
		}

		if !policy.Active {
			return nil, status.Error(codes.PermissionDenied, codes.PermissionDenied.String())
		}

		return policy, nil
	}
}

func main() {
	if err := run(os.Args); err != nil {
		log.Fatal(err)
	}
}
