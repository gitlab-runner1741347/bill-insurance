package main

import (
	"net/http"
)

type middleware struct {
}

func NewCustomMiddleware() *middleware {
	return &middleware{}
}

func (m *middleware) MiddlewareHandleFunc(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		next.ServeHTTP(w, r)
		return
	})
}
