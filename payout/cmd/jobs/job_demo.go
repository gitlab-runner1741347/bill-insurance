package main

import (
	"context"

	"github.com/spf13/cobra"
)

var (
	demoCmd = &cobra.Command{
		Use:   "demo",
		Short: "job demo",
		RunE:  demoFunc,
	}
)

func demoFunc(cmd *cobra.Command, args []string) error {
	job := NewDemoJob()
	return job.Run(context.TODO())
}

type DemoJob struct {
}

func NewDemoJob() *DemoJob {
	return &DemoJob{}
}

func (j *DemoJob) Run(ctx context.Context) error {
	ll.Warn("Start DemoJob")
	return nil
}
