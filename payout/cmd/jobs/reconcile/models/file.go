package models

import "os"

type File struct {
	IsPublic bool
	Bucket   string
	Key      string
	Name     string
	Type     string
	Reader   *os.File
}
