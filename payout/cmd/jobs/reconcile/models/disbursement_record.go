package models

type DisbursementRecord struct {
	RecordType   interface{}
	RefNum       interface{}
	BankId       interface{}
	TransferType interface{}
	DebitAccount string
	BenAccount   interface{}
	BenName      string
	BenBankName  string
	BenBankCode  string
	Amount       interface{}
	BankTime     string
	Checksum     string
}
