package reconcile

import (
	"context"
	"errors"
	"time"

	"git.kafefin.net/backend/central-payout/cmd/jobs/reconcile/biz"
	"git.kafefin.net/backend/central-payout/cmd/jobs/reconcile/common"
	"git.kafefin.net/backend/central-payout/cmd/jobs/reconcile/storage"
	"git.kafefin.net/backend/central-payout/config"
	"git.kafefin.net/backend/central-payout/internal/must"
	"git.kafefin.net/backend/kitchen/l"
	"git.kafefin.net/backend/kitchen/oteljaeger"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/spf13/cobra"
)

var (
	ReconcilationCmd = &cobra.Command{
		Use:   "reconcile",
		Short: "job reconcile",
		RunE:  reconcileFunc,
	}
)

func reconcileFunc(cmd *cobra.Command, args []string) error {
	job := NewReconcileJob()
	return job.Run(context.TODO(), args)
}

type ReconcileJob struct {
}

func NewReconcileJob() *ReconcileJob {
	return &ReconcileJob{}
}

func (j *ReconcileJob) Run(ctx context.Context, args []string) error {
	var err error
	timeCurrent := time.Now()
	time := &timeCurrent
	if len(args) > 1 {
		return errors.New("receied only one argument")
	} else if len(args) == 1 {
		time, err = common.ConvertFullToTime(args[0])
		if err != nil {
			return errors.New("cannot parse time")
		}
	}

	cfg := config.Load()

	tp, shutdownFn, err := oteljaeger.JaegerProvider("saladin-user", cfg.OtelCollectorAddress)
	var ll = l.NewWithOtel(tp)
	defer func(ctx context.Context) {
		if err := shutdownFn(ctx); err != nil {
			ll.Error("shutdown tracing provider", l.Error(err))
		}
	}(ctx)

	if err != nil {
		return err
	}

	db := storage.NewStore(must.ConnectMySQL(cfg.MySQL))
	// Create a new session
	awsConfig := &aws.Config{
		Region:      aws.String(cfg.AwsCloud.Region), // Replace with your desired region
		Credentials: credentials.NewStaticCredentials(cfg.AwsCloud.AccessKey, cfg.AwsCloud.SecretKey, ""),
	}

	sess, _ := session.NewSession(awsConfig)

	s3Repo := storage.NewS3Client(sess)
	configLocal := storage.InitConfig(cfg)
	ll.Info("Config loaded", l.Object("config_local", configLocal))
	bizJob := biz.NewVPBDisbursementReconcileBiz(db, db, s3Repo, configLocal, ll)

	if err := bizJob.JobPushFileSpecificDay(context.TODO(), time); err != nil {
		return err
	}

	return nil
}
