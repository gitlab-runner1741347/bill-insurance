package common

import (
	"fmt"
	"strings"
	"time"

	models_ "git.kafefin.net/backend/central-payout/cmd/jobs/reconcile/models"
)

func ToReconcileBankID(ftTxtID string) string {
	if ftTxtID == "" {
		return ""
	}

	separatorIdx := strings.Index(ftTxtID, "/")
	if separatorIdx == -1 {
		return ftTxtID
	}

	return ftTxtID[:separatorIdx]
}

func ToReconcileTransferType(tp string) int {
	if tp == "CACA" {
		return 0
	} else if tp == "FAST" {
		return 2
	}
	return -1
}

func FormatTime(time time.Time) string {
	currentTime := time

	year := currentTime.Format("2006")
	month := currentTime.Format("01")
	day := currentTime.Format("02")
	hour := currentTime.Format("15")
	minute := currentTime.Format("04")
	second := currentTime.Format("05")

	formattedTime := year + month + day + hour + minute + second
	return fmt.Sprintf("%s", formattedTime)
}

func ConvertStructSliceToStringSlice(slice []*models_.DisbursementRecord) ([][]string, error) {
	matrix := make([][]string, len(slice))
	for i, item := range slice {
		if i == len(slice)-1 {
			matrix[i] = []string{
				fmt.Sprintf("%v", item.RecordType),
				fmt.Sprintf("%v", item.RefNum),
				fmt.Sprintf("%v", item.BankId),
				fmt.Sprintf("%v", item.TransferType),
				fmt.Sprintf("%v", item.DebitAccount),
				fmt.Sprintf("%v", item.BenAccount),
			}
		} else {
			matrix[i] = []string{
				fmt.Sprintf("%v", item.RecordType),
				fmt.Sprintf("%v", item.RefNum),
				fmt.Sprintf("%v", item.BankId),
				fmt.Sprintf("%v", item.TransferType),
				fmt.Sprintf("%v", item.DebitAccount),
				fmt.Sprintf("%v", item.BenAccount),
				fmt.Sprintf("%v", item.BenName),
				fmt.Sprintf("%v", item.BenBankName),
				fmt.Sprintf("%v", item.BenBankCode),
				fmt.Sprintf("%v", item.Amount),
				fmt.Sprintf("%v", item.BankTime),
				fmt.Sprintf("%v", item.Checksum),
			}
		}

	}
	return matrix, nil
}

func ConvertToYYYYMMDD(inputTimeStr string) (string, error) {
	layout := "2006-01-02 15:04:05"

	inputTime, err := time.Parse(layout, inputTimeStr)
	if err != nil {
		return "", err
	}

	outputLayout := "20060102"
	outputTimeStr := inputTime.Format(outputLayout)

	return outputTimeStr, nil
}

func ConvertYYYYMMDDToTime(input string) (*time.Time, error) {
	layout := "2006-01-02"
	parsedTime, err := time.Parse(layout, input)
	if err != nil {
		fmt.Println("Error:", err)
		return nil, err
	}
	return &parsedTime, nil
}

func ConvertFullToTime(input string) (*time.Time, error) {
	layout := "2006-01-02-15-04-05"
	parsedTime, err := time.Parse(layout, input)
	if err != nil {
		fmt.Println("Error:", err)
		return nil, err
	}
	return &parsedTime, nil
}

func GenerateFileName(partnerName string, isDailyReport bool, currentTime time.Time) string {
	var fileName string

	if isDailyReport {
		fileName = fmt.Sprintf("%s_CHIHO_%s.csv", partnerName, currentTime.Format("20060102150405"))
	} else {
		yesterday := currentTime.Add(-24 * time.Hour)
		fileName = fmt.Sprintf("%s_CHIHO_%s%s.csv", partnerName, yesterday.Format("20060102"), currentTime.Format("150405"))
	}

	return fileName
}
