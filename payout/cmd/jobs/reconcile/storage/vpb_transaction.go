package storage

import (
	"context"
	"time"

	"git.kafefin.net/backend/central-payout/internal/models"
)

func (s *store) GetVpbTransactionResult(ctx context.Context, reconcileDateStart *time.Time, reconcileDateEnd *time.Time) ([]*models.VpbTransaction, error) {
	var disbursementTxts []*models.VpbTransaction
	err := s.db.Where("transfer_result IN (?, ?) AND transaction_date BETWEEN ? AND ?", "complete", "pending", reconcileDateStart, reconcileDateEnd).Find(&disbursementTxts).Error
	return disbursementTxts, err
}
