package storage

import (
	"bytes"
	"net/http"

	"git.kafefin.net/backend/kitchen/l"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
)

type S3Client struct {
	sess   *session.Session
	logger l.Logger
}

func NewS3Client(sess *session.Session) *S3Client {
	logger := l.New()
	c := &S3Client{
		sess:   sess,
		logger: logger,
	}
	return c
}

func (s3 S3Client) UploadS3(isPublic bool, keyObject, bucket string, data []byte) error {

	fileBytes := bytes.NewReader(data)

	fileType := http.DetectContentType(data)

	uploader := s3manager.NewUploader(s3.sess)

	_, err := uploader.Upload(&s3manager.UploadInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(keyObject),
		//ACL:         acl,
		Body:        fileBytes,
		ContentType: aws.String(fileType),
	})
	if err != nil {
		return err
	}

	return nil
}
