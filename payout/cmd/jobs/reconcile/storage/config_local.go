package storage

import (
	"git.kafefin.net/backend/central-payout/config"
	kitconfig "git.kafefin.net/backend/kitchen/config"
)

type ConfigLocal struct {
	MySQL                *kitconfig.MySQL             `yaml:"mysql" mapstructure:"mysql"`
	VpbankReconciliation *config.VpbankReconciliation `yaml:"vpbank_reconciliation" mapstructure:"vpbank_reconciliation"`
	NameToReconcile      string                       `yaml:"name_to_reconcile" mapstructure:"name_to_reconcile"`
	AwsCloud             *config.AwsCloud             `yaml:"aws_cloud" mapstructure:"aws_cloud"`
}

func InitConfig(config *config.Config) *ConfigLocal {
	c := &ConfigLocal{
		MySQL:                config.MySQL,
		VpbankReconciliation: config.VpbankReconciliation,
		NameToReconcile:      config.NameToReconcile,
		AwsCloud:             config.AwsCloud,
	}
	return c
}
