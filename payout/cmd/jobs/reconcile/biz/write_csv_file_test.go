package biz

import (
	"context"
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"strconv"
	"testing"
	"time"

	"git.kafefin.net/backend/central-payout/cmd/jobs/reconcile/common"
	"git.kafefin.net/backend/central-payout/cmd/jobs/reconcile/models"
	models_ "git.kafefin.net/backend/central-payout/cmd/jobs/reconcile/models"
	"git.kafefin.net/backend/central-payout/cmd/jobs/reconcile/storage"
	"git.kafefin.net/backend/central-payout/config"
	"git.kafefin.net/backend/central-payout/internal/must"
	"git.kafefin.net/backend/kitchen/l"
	"git.kafefin.net/backend/kitchen/oteljaeger"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
)

var (
	header = []string{"RecordType", "RefNum", "BankId", "TransferType", "DebitAccount", "BenAccount", "BenName", "BenBankName", "BenBankCode", "Amount", "BankTime", "Checksum"}

	titleDummyDisbursementRecord = models.DisbursementRecord{
		RecordType:   header[0],
		RefNum:       header[1],
		BankId:       header[2],
		TransferType: header[3],
		DebitAccount: header[4],
		BenAccount:   header[5],
		BenName:      header[6],
		BenBankName:  header[7],
		BenBankCode:  header[8],
		Amount:       header[9],
		BankTime:     header[10],
		Checksum:     header[11],
	}

	dummyDisbursementRecord = models_.DisbursementRecord{
		RecordType:   "2",
		RefNum:       "refnum",
		BankId:       "bankid",
		TransferType: 2,
		DebitAccount: "0231000663066",
		BenAccount:   "1234556",
		BenName:      "123456",
		BenBankName:  "vietcombank",
		BenBankCode:  "Hung ne",
		Amount:       29999,
		BankTime:     "12345",
		Checksum:     "20202020202020202",
	}
)

func TestGenFileCsv(t *testing.T) {
	arrayData := make([]*models.DisbursementRecord, 0)
	arrayData = append(arrayData, &titleDummyDisbursementRecord)
	arrayData = append(arrayData, &dummyDisbursementRecord)
	biz := vPBDisbursementReconcileBiz{}
	result, err := biz.WriteCSVFile("file_result.csv", arrayData)
	if err != nil {
		t.Errorf(err.Error())
	}
	fmt.Println(result)
}

//func TestPushFile(t *testing.T) {
//	cfg := config.Load()
//
//	awsConfig := &aws.Config{
//		Region:      aws.String(cfg.AwsCloud.Region), // Replace with your desired region
//		Credentials: credentials.NewStaticCredentials(cfg.AwsCloud.AccessKey, cfg.AwsCloud.SecretKey, ""),
//	}
//
//	// Create a new session
//	sess, err := session.NewSession(awsConfig)
//	if err != nil {
//		fmt.Println("Error creating session:", err)
//		return
//	}
//
//	db := storage.NewStore(must.ConnectMySQL(cfg.MySQL))
//
//	s3Repo := storage.NewS3Client(sess)
//
//	bizJob := NewVPBDisbursementReconcileBiz(db, db, s3Repo, cfg)
//	file, err := os.Open("taofile.txt")
//	err = bizJob.UploadFileToS3(context.TODO(), true, "saladin", "taofile.txt", file)
//	if err != nil {
//		t.Errorf(err.Error())
//	}
//}

// func TestFlow(t *testing.T) {
// 	cfg := config.Load()
// 	db := storage.NewStore(must.ConnectMySQL(cfg.MySQL))
// 	// Create a new session
// 	awsConfig := &aws.Config{
// 		Region:      aws.String(cfg.AwsCloud.Region), // Replace with your desired region
// 		Credentials: credentials.NewStaticCredentials(cfg.AwsCloud.AccessKey, cfg.AwsCloud.SecretKey, ""),
// 	}

// 	sess, _ := session.NewSession(awsConfig)
// 	tp, shutdownFn, err := oteljaeger.JaegerProvider("saladin-user", cfg.OtelCollectorAddress)
// 	var ll = l.NewWithOtel(tp)
// 	defer func(ctx context.Context) {
// 		if err := shutdownFn(ctx); err != nil {
// 			ll.Error("shutdown tracing provider", l.Error(err))
// 		}
// 	}(ctx)

// 	if err != nil {

// 	}

// 	s3Repo := storage.NewS3Client(sess)
// 	bizJob := NewVPBDisbursementReconcileBiz(db, db, s3Repo, cfg)
// 	time := time.Now()
// 	if err := bizJob.JobPushFileSpecificDay(context.TODO(), &time); err != nil {
// 		t.Errorf(err.Error())
// 	}
// }

func TestHash(t *testing.T) {
	a := "0009"
	b := 4
	c := 52100000
	d := "SALADIN"
	e := "20221025154209"

	hasing_ := md5.Sum([]byte(fmt.Sprintf("%s%s%s%s%s%s", a, strconv.Itoa(b), strconv.Itoa(int(c)), d, e, d)))
	hashString := hex.EncodeToString(hasing_[:])
	if hashString != "85c39b48f9d1d43e882847a9314bc442" {
		t.Errorf(hashString)
	}

}

func TestFileName(t *testing.T) {
	currentTime, _ := common.ConvertYYYYMMDDToTime("2002-01-30")
	fileName := common.GenerateFileName("test", false, *currentTime)
	t.Logf(fileName)
}

func TestShorterBankId(t *testing.T) {
	bankId := "FT23234019610267/692702403657"
	expectBankId := common.ToReconcileBankID(bankId)
	if expectBankId != "FT23234019610267" {
		t.Errorf(expectBankId)
	}
}

func TestFlow(t *testing.T) {
	ctx := context.TODO()
	cfg := config.Load()
	configLocal := storage.InitConfig(cfg)
	fmt.Print(configLocal)

	tp, shutdownFn, err := oteljaeger.JaegerProvider("saladin-user", cfg.OtelCollectorAddress)
	var ll = l.NewWithOtel(tp)
	defer func(ctx context.Context) {
		if err := shutdownFn(ctx); err != nil {
			ll.Error("shutdown tracing provider", l.Error(err))
		}
	}(ctx)

	if err != nil {

	}

	db := storage.NewStore(must.ConnectMySQL(cfg.MySQL))
	// Create a new session
	awsConfig := &aws.Config{
		Region:      aws.String(cfg.AwsCloud.Region), // Replace with your desired region
		Credentials: credentials.NewStaticCredentials(cfg.AwsCloud.AccessKey, cfg.AwsCloud.SecretKey, ""),
	}

	sess, _ := session.NewSession(awsConfig)

	s3Repo := storage.NewS3Client(sess)

	bizJob := NewVPBDisbursementReconcileBiz(db, db, s3Repo, configLocal, ll)
	time := time.Now()
	if err := bizJob.JobPushFileSpecificDay(context.TODO(), &time); err != nil {

	}

}
