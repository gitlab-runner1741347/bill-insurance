package biz

import (
	"context"
	"crypto/md5"
	"encoding/csv"
	"encoding/hex"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"strconv"
	"time"

	"github.com/pkg/sftp"
	"golang.org/x/crypto/ssh"

	"git.kafefin.net/backend/central-payout/cmd/jobs/reconcile/common"
	models_ "git.kafefin.net/backend/central-payout/cmd/jobs/reconcile/models"
	"git.kafefin.net/backend/central-payout/cmd/jobs/reconcile/storage"
	"git.kafefin.net/backend/central-payout/internal/models"
	"git.kafefin.net/backend/central-payout/pkg/types"
	"git.kafefin.net/backend/kitchen/l"
)

type DisbursementReconcileStore interface {
}

type VpbTransactionResultStore interface {
	GetVpbTransactionResult(ctx context.Context, reconcileDateStart *time.Time, reconcileDateEnd *time.Time) ([]*models.VpbTransaction, error)
}

type AWSS3Store interface {
	UploadS3(isPublic bool, keyObject, bucket string, data []byte) error
}

type vPBDisbursementReconcileBiz struct {
	vbpResult             VpbTransactionResultStore
	disbursementReconcile DisbursementReconcileStore
	s3                    AWSS3Store
	config                *storage.ConfigLocal
	log                   types.CommonLogger
}

func NewVPBDisbursementReconcileBiz(vpBankResult VpbTransactionResultStore, disbursementReconcile DisbursementReconcileStore, s3 AWSS3Store, config *storage.ConfigLocal, log types.CommonLogger) *vPBDisbursementReconcileBiz {
	return &vPBDisbursementReconcileBiz{s3: s3, vbpResult: vpBankResult, config: config, disbursementReconcile: disbursementReconcile, log: log}
}

func (biz *vPBDisbursementReconcileBiz) GetSFTPFile(remotePath, remoteFileName, localFilePath string) error {
	// Connect to the server
	config := &ssh.ClientConfig{
		User: biz.config.VpbankReconciliation.Username,
		Auth: []ssh.AuthMethod{
			ssh.Password(biz.config.VpbankReconciliation.Password),
		},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}

	conn, err := ssh.Dial("tcp", fmt.Sprintf("%s:%d", biz.config.VpbankReconciliation.Address, biz.config.VpbankReconciliation.Port), config)
	if err != nil {
		biz.log.Error("Error when connect to server VpBank", l.Error(err))
		return err
	}

	defer conn.Close()

	sftpClient, err := sftp.NewClient(conn)
	if err != nil {
		biz.log.Error("Error when create sftp client to server VpBank", l.Error(err))
		return err
	}
	defer sftpClient.Close()

	remoteFilePath := fmt.Sprintf("%s/%s", remotePath, remoteFileName)

	// Open the remote file for reading
	remoteFile, err := sftpClient.Open(remoteFilePath)
	if err != nil {
		biz.log.Error("Error when open file remote", l.Error(err))
		return err
	}
	defer remoteFile.Close()

	// Create the local file for writing
	localFile, err := os.Create(localFilePath)
	if err != nil {
		biz.log.Error("Error when create localfile for writing", l.Error(err))
		return err
	}
	defer localFile.Close()

	// Copy the contents of the remote file to the local file
	_, err = io.Copy(localFile, remoteFile)
	if err != nil {
		biz.log.Error("Error when copy content from remote file to local file", l.Error(err))
		return err
	}
	return nil
}

func (biz *vPBDisbursementReconcileBiz) DeleteSpecifyFile(pathFile string) error {
	err := os.Remove(pathFile)
	if err != nil {
		biz.log.Error("Error when delete specify file", l.Error(err))
		return err
	}

	return nil
}

func (biz *vPBDisbursementReconcileBiz) UploadVPBsftp(remotePath, fileName, localFilePath string) error {
	biz.log.Info("")
	remoteFilePath := fmt.Sprintf("%s/%s", remotePath, fileName)
	biz.log.Info(fmt.Sprintf("remote file path to uploadVPBsftp: %s", remoteFilePath))
	config := &ssh.ClientConfig{
		User: biz.config.VpbankReconciliation.Username,
		Auth: []ssh.AuthMethod{
			ssh.Password(biz.config.VpbankReconciliation.Password),
		},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}

	// connect to server
	conn, err := ssh.Dial("tcp", fmt.Sprintf("%s:%d", biz.config.VpbankReconciliation.Address, biz.config.VpbankReconciliation.Port), config)
	if err != nil {
		biz.log.Error("Error when connect to server VpBank", l.Error(err))
		return err
	}

	defer conn.Close()

	sftpClient, err := sftp.NewClient(conn)
	if err != nil {
		biz.log.Error("Error when create new client sftp", l.Error(err))
		return err
	}
	defer sftpClient.Close()

	// Open the local file for reading
	localFile, err := os.Open(localFilePath)
	if err != nil {
		biz.log.Error("Error when open the local file for reading", l.Error(err))
		return err
	}
	defer localFile.Close()

	err = sftpClient.MkdirAll(fmt.Sprintf("%s/", remotePath))
	if err != nil {
		biz.log.Error("Error when create dir for save file", l.Error(err))
		return err
	}

	// Create the remote file on the SFTP server
	remoteFile, err := sftpClient.Create(remoteFilePath)
	if err != nil {
		biz.log.Error("Error when create file remote", l.Error(err))
		return err
	}
	defer remoteFile.Close()

	// Copy the contents of the local file to the remote file
	_, err = io.Copy(remoteFile, localFile)
	if err != nil {
		biz.log.Error("Error when copy content from localfile to remotefile", l.Error(err))
		return err
	}

	return nil
}

func (biz *vPBDisbursementReconcileBiz) UploadFileToS3(ctx context.Context, isPublic bool, bucket string, key string, data []byte) error {

	biz.log.Info(fmt.Sprintf("Param: %v, %v, %v, %v", isPublic, bucket, key, data))
	err := biz.s3.UploadS3(isPublic, key, bucket, data)
	if err != nil {
		biz.log.Error("Error when upload S3", l.Error(err))
		return err
	}

	return nil
}

func (biz *vPBDisbursementReconcileBiz) WriteCSVFile(localFilePath string, csvData []*models_.DisbursementRecord) (*os.File, error) {
	file, err := os.Create(localFilePath)
	if err != nil {
		biz.log.Error("Error when create csv file", l.Error(err))
		return nil, err
	}
	defer file.Close()

	writer := csv.NewWriter(file)
	result, err := common.ConvertStructSliceToStringSlice(csvData)
	if err != nil {
		biz.log.Error("Error when convert struct slice to string slice", l.Error(err))
		return nil, err
	}
	writer.WriteAll(result)
	writer.Flush()

	if writer.Error() != nil {
		biz.log.Error("Error when write from records to csv file", l.Error(err))
		return nil, writer.Error()
	}
	return file, nil
}

func (biz *vPBDisbursementReconcileBiz) JobPushFileSpecificDay(ctx context.Context, currentTime *time.Time) error {
	yesterday := currentTime.Add(-24 * time.Hour).UTC()
	startOfYesterday := time.Date(yesterday.Year(), yesterday.Month(), yesterday.Day(), 0, 0, 0, 0, yesterday.Location())
	endOfYesterday := time.Date(yesterday.Year(), yesterday.Month(), yesterday.Day(), 23, 59, 59, int(time.Second-time.Nanosecond), yesterday.Location())
	records, err := biz.ToVPBRecord(ctx, &startOfYesterday, &endOfYesterday, currentTime)

	if err != nil {

		biz.log.Error("Err when To VBP Record", l.Error(err))
		return err
	}
	fileName := common.GenerateFileName(biz.config.NameToReconcile, false, *currentTime)
	_, err = biz.WriteCSVFile(fileName, records)
	if err != nil {
		biz.log.Error("WriteCSVFile", l.Error(err))
		return err
	}

	err = biz.UploadVPBsftp(common.RemoteSendingPath, fileName, fileName)
	if err != nil {
		biz.log.Error("UploadVPBsftp", l.Error(err))
		return err
	}

	fileToUpload, err := os.Open(fileName)
	if err != nil {
		biz.log.Error("fileToUpload", l.Error(err))
		return err
	}

	dataFile, err := ioutil.ReadAll(fileToUpload)
	if err != nil {
		return err
	}

	fileName = fmt.Sprintf("vpbank/%s", fileName)

	biz.UploadFileToS3(ctx, false, biz.config.AwsCloud.Bucket, fileName, dataFile)

	return nil
}

func (biz *vPBDisbursementReconcileBiz) ToVPBRecord(ctx context.Context, reconcileDateStart *time.Time, reconcileDateEnd *time.Time, currentTime *time.Time) ([]*models_.DisbursementRecord, error) {
	var disbursementRecords []*models_.DisbursementRecord
	var totalAmount int64 = 0
	header := []string{"RecordType", "RefNum", "BankId", "TransferType", "DebitAccount", "BenAccount", "BenName", "BenBankName", "BenBankCode", "Amount", "BankTime", "Checksum"}
	disbursementRecords = append(disbursementRecords, &models_.DisbursementRecord{
		RecordType:   header[0],
		RefNum:       header[1],
		BankId:       header[2],
		TransferType: header[3],
		DebitAccount: header[4],
		BenAccount:   header[5],
		BenName:      header[6],
		BenBankName:  header[7],
		BenBankCode:  header[8],
		Amount:       header[9],
		BankTime:     header[10],
		Checksum:     header[11],
	})

	resultTSSTxts, err := biz.vbpResult.GetVpbTransactionResult(ctx, reconcileDateStart, reconcileDateEnd)
	if err != nil {
		biz.log.Error("GetVpbTransactionResult", l.Error(err))
		return disbursementRecords, err
	}

	for _, value := range resultTSSTxts {
		if err != nil {

			return disbursementRecords, err
		}

		recordType := "0002"
		refNum := value.ReferenceNumber
		bankId := common.ToReconcileBankID(value.TransactionID)
		transferType := common.ToReconcileTransferType(value.Type)
		debitAccount := value.SourceNumber
		benAccount := value.TargetNumber
		benName := value.BenName
		benBankName := value.BankName
		benBankCode := value.BankBin

		amount, _ := strconv.Atoi(value.Amount)

		// format the time
		bankTime, _ := common.ConvertToYYYYMMDD(value.TransactionDate)
		totalAmount += int64(amount)

		hashing := md5.Sum([]byte(fmt.Sprintf("%s%s%s%d%s%s%s%s%s%d%s%s",
			recordType, refNum, bankId, transferType, debitAccount, benAccount,
			benName, benBankName, benBankCode, amount, bankTime, biz.config.NameToReconcile)))

		// Convert the MD5 hash bytes to a string representation
		hashString := hex.EncodeToString(hashing[:])

		disbursementRecords = append(disbursementRecords, &models_.DisbursementRecord{
			RecordType:   recordType,
			RefNum:       refNum,
			BankId:       bankId,
			TransferType: transferType,
			DebitAccount: debitAccount,
			BenAccount:   benAccount,
			BenName:      benName,
			BenBankName:  benBankName,
			BenBankCode:  benBankCode,
			Amount:       amount,
			BankTime:     bankTime,
			Checksum:     hashString,
		})

	}

	recordTypeLast := "0009"
	totalRowLast := len(disbursementRecords) - 1
	v64 := common.FormatTime(time.Now())

	if s, err := strconv.ParseInt(v64, 10, 64); err == nil {
		fmt.Printf("%T, %v\n", s, s)
	}

	year := currentTime.Year()
	month := currentTime.Month()
	day := currentTime.Day()
	hour := currentTime.Hour()
	minute := currentTime.Minute()
	second := currentTime.Second()

	// Format to ensure single-digit values have leading zeros
	formattedTime := fmt.Sprintf("%04d%02d%02d%02d%02d%02d", year, month, day, hour, minute, second)
	hasing_ := md5.Sum([]byte(fmt.Sprintf("%s%s%s%s%s%s", recordTypeLast, strconv.Itoa(totalRowLast), strconv.Itoa(int(totalAmount)), biz.config.NameToReconcile, formattedTime, biz.config.NameToReconcile)))
	hashString := hex.EncodeToString(hasing_[:])
	disbursementRecords = append(disbursementRecords, &models_.DisbursementRecord{
		RecordType:   recordTypeLast,
		RefNum:       totalRowLast,
		BankId:       totalAmount,
		TransferType: biz.config.NameToReconcile,
		DebitAccount: formattedTime,
		BenAccount:   hashString,
		Amount:       "",
	})
	return disbursementRecords, nil
}
