package config_

import (
	"bytes"
	_ "embed"
	"strings"
	"time"

	kitconfig "git.kafefin.net/backend/kitchen/config"
	"git.kafefin.net/backend/kitchen/l"
	"github.com/spf13/viper"
)

var ll = l.New()

// Config holds all settings
//
//go:embed config.yaml
var defaultConfig []byte

type Config struct {
	AwsCloud   *AwsCloud        `yaml:"aws_cloud" mapstructure:"aws_cloud"`
	ApiGateway string           `yaml:"api_gateway" mapstructure:"api_gateway"`
	Name       string           `yaml:"name" map:"name"`
	Vpbank     *Vpbank          `yaml:"vpbank" mapstructure:"vpbank"`
	MySQL      *kitconfig.MySQL `yaml:"mysql" mapstructure:"mysql"`
	Sftp       *Sftp            `yaml:"sftp" mapstructure:"sftp"`
}

type AwsCloud struct {
	Region             string `yaml:"region" mapstructure:"region"`
	AccessKey          string `yaml:"access_key" mapstructure:"access_key"`
	SecretKey          string `yaml:"secret_key" mapstructure:"secret_key"`
	Bucket             string `yaml:"bucket" mapstructure:"bucket"`
	MediaBucket        string `yaml:"media_bucket" mapstructure:"media_bucket"`
	MediaPrivateBucket string `yaml:"media_private_bucket" mapstructure:"media_private_bucket"`
}

type Vpbank struct {
	Host                      string `json:"host" mapstructure:"host" yaml:"host"`
	Idn                       string `json:"idn" mapstructure:"idn" yaml:"idn"`
	BearerToken               string `json:"bearer_token" mapstructure:"bearer_token" yaml:"bearer_token"`
	DisbursementAccountNumber string `json:"disbursement_account_number" mapstructure:"disbursement_account_number" yaml:"disbursement_account_number"`
}

type Sftp struct {
	Host     string `json:"host" yaml:"host"`
	Port     int    `json:"port" yaml:"port"`
	Username string `json:"username" yaml:"username"`
	Password string `json:"password" yaml:"password"`
}

type MySQL struct {
	Username  string            `yaml:"username" mapstructure:"username"`
	Password  string            `yaml:"password" mapstructure:"password"`
	Protocol  string            `yaml:"protocol" mapstructure:"protocol"`
	Address   string            `yaml:"address" mapstructure:"address"`
	Database  string            `yaml:"database" mapstructure:"database"`
	Params    map[string]string `yaml:"params" mapstructure:"params"`
	Collation string            `yaml:"collation" mapstructure:"collation"`
	Loc       *time.Location    `yaml:"location" mapstructure:"loc"`
	TLSConfig string            `yaml:"tls_config" mapstructure:"tls_config"`

	Timeout      time.Duration `yaml:"timeout" mapstructure:"timeout"`
	ReadTimeout  time.Duration `yaml:"read_timeout" mapstructure:"read_timeout"`
	WriteTimeout time.Duration `yaml:"write_timeout" mapstructure:"write_timeout"`

	AllowAllFiles           bool   `yaml:"allow_all_files" mapstructure:"allow_all_files"`
	AllowCleartextPasswords bool   `yaml:"allow_cleartext_passwords" mapstructure:"allow_cleartext_passwords"`
	AllowOldPasswords       bool   `yaml:"allow_old_passwords" mapstructure:"allow_old_passwords"`
	ClientFoundRows         bool   `yaml:"client_found_rows" mapstructure:"client_found_rows"`
	ColumnsWithAlias        bool   `yaml:"columns_with_alias" mapstructure:"columns_with_alias"`
	InterpolateParams       bool   `yaml:"interpolate_params" mapstructure:"interpolate_params"`
	MultiStatements         bool   `yaml:"multi_statements" mapstructure:"multi_statements"`
	ParseTime               bool   `yaml:"parse_time" mapstructure:"parse_time"`
	GoogleAuthFile          string `yaml:"google_auth_file" mapstructure:"google_auth_file"`
}

func Load() *Config {
	cfg := &Config{}

	viper.SetConfigType("yaml")
	err := viper.ReadConfig(bytes.NewBuffer(defaultConfig))
	if err != nil {
		ll.Fatal("Failed to read viper config", l.Error(err))
	}

	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "__"))
	viper.AutomaticEnv()

	err = viper.Unmarshal(&cfg)
	if err != nil {
		ll.Fatal("Failed to unmarshal config", l.Error(err))
	}

	ll.Info("Config loaded", l.Object("config", cfg))
	return cfg
}
