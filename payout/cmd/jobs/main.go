package main

import (
	"git.kafefin.net/backend/central-payout/cmd/jobs/reconcile"
	"git.kafefin.net/backend/kitchen/l"

	"github.com/spf13/cobra"
)

var (
	ll = l.New()

	rootCmd = &cobra.Command{
		Use:           "job",
		SilenceErrors: true,
	}
)

func init() {
	cobra.OnInitialize(initConfig)
	rootCmd.AddCommand(demoCmd)
	rootCmd.AddCommand(reconcile.ReconcilationCmd)
}

func initConfig() {

}

func main() {
	if err := rootCmd.Execute(); err != nil {
		ll.Fatal("Job error", l.Error(err))
	}

	ll.Warn("Job succeeded")
}
