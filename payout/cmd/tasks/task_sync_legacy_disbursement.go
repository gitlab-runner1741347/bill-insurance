package main

import (
	"context"
	"time"

	"git.kafefin.net/backend/central-payout/internal/models"
	"git.kafefin.net/backend/central-payout/internal/must"
	"git.kafefin.net/backend/central-payout/pb"
	"git.kafefin.net/backend/central-payout/pkg/datetimeutils"
	"git.kafefin.net/backend/kitchen/l"
	"github.com/spf13/cobra"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

var (
	syncLegacyDisbursementCmd = &cobra.Command{
		Use:   "sync-legacy-disbursement",
		Short: "task syncLegacyDisbursement",
		RunE:  syncLegacyDisbursementFunc,
	}
)

func syncLegacyDisbursementFunc(cmd *cobra.Command, args []string) error {
	task := NewsyncLegacyDisbursementTask()
	return task.Run(context.TODO())
}

type syncLegacyDisbursementTask struct {
}

func NewsyncLegacyDisbursementTask() *syncLegacyDisbursementTask {
	return &syncLegacyDisbursementTask{}
}

func (t *syncLegacyDisbursementTask) Run(ctx context.Context) error {
	ll.Info("Run syncLegacyDisbursementTask")

	dbMySQL := must.ConnectMySQL(cfg.MySQL)
	dbPostgres := must.ConnectPostgres(cfg.Postgres)
	dryRun := false

	var legacyPaymentRequests []*LegacyPaymentRequest
	if err := dbPostgres.Table("payment_request").Find(&legacyPaymentRequests).Error; err != nil {
		ll.Error("Get payment_request", l.Error(err))
		return err
	}

	var legacyDisbursementTransactions []*LegacyDisbursementTransaction
	if err := dbPostgres.Table("disbursement_transaction").Find(&legacyDisbursementTransactions).Error; err != nil {
		ll.Error("Get disbursement_transaction", l.Error(err))
		return err
	}

	newPaymentRequests := toNewPaymentRequests(legacyPaymentRequests)
	if err := dbMySQL.Table("payment_request").Session(&gorm.Session{
		DryRun:               dryRun,
		FullSaveAssociations: true,
	}).Clauses(clause.OnConflict{
		UpdateAll: true,
	}).Save(&newPaymentRequests).Error; err != nil {
		ll.Error("Create payment_request", l.Error(err))
		return err
	}
	for _, m := range newPaymentRequests {
		if err := dbMySQL.Table("payment_request").Where("id = ?", m.ID).Updates(map[string]any{
			"created_at": m.CreatedAt,
			"updated_at": m.UpdatedAt,
		}).Error; err != nil {
			ll.Error("Update payment_request", l.Error(err))
		}
	}

	newPaymentTxns := toNewDisbursementTxns(legacyDisbursementTransactions)
	if err := dbMySQL.Table("payment_transaction").Session(&gorm.Session{
		DryRun:               dryRun,
		FullSaveAssociations: true,
	}).Clauses(clause.OnConflict{
		UpdateAll: true,
	}).Save(&newPaymentTxns).Error; err != nil {
		ll.Error("Create payment_transaction", l.Error(err))
		return err
	}
	for _, m := range newPaymentTxns {
		if err := dbMySQL.Table("payment_transaction").Where("id = ?", m.ID).Updates(map[string]any{
			"created_at": m.CreatedAt,
			"updated_at": m.UpdatedAt,
		}).Error; err != nil {
			ll.Error("Update payment_transaction", l.Error(err))
		}
	}

	newVPBTxns := toNewVPBTxns(legacyDisbursementTransactions)
	if err := dbMySQL.Table("vpb_transaction").Session(&gorm.Session{
		DryRun:               dryRun,
		FullSaveAssociations: true,
	}).Clauses(clause.OnConflict{
		UpdateAll: true,
	}).Save(&newVPBTxns).Error; err != nil {
		ll.Error("Create vpb_transaction", l.Error(err))
		return err
	}
	for _, m := range newVPBTxns {
		if err := dbMySQL.Table("vpb_transaction").Where("reference_number = ?", m.ReferenceNumber).Updates(map[string]any{
			"created_at": m.CreatedAt,
			"updated_at": m.UpdatedAt,
		}).Error; err != nil {
			ll.Error("Update vpb_transaction", l.Error(err))
		}
	}

	return nil
}

type LegacyPaymentRequest struct {
	Id                        int64
	CreatedBy                 string
	CreatedDate               time.Time
	UpdatedBy                 string
	UpdatedDate               time.Time
	Amount                    int64
	BankAccountNumber         string
	BankBranch                string
	BankId                    string
	BankName                  string
	BeneficiaryName           string
	ClaimNumber               string
	DebitAccount              string
	DisbursementOverallStatus string
	EventApprovedFireTime     string
	ExportedTime              string
	HasExported               string
	LastUpdatedUserId         string
	Message                   string
	Note                      string
	OrderId                   string
	OrderNumber               string
	OrderSource               string
	OriginRequestId           string
	PaymentOutType            string
	PolicyId                  string
	Product                   string
	Service                   string
	ShortBankName             string
	Status                    string
	TransferTime              string
	TransferType              string
	Campaign                  string
	Debtor                    string
	OriginRequestType         string
	Bin                       string
	CreatedDateTime           string
	CreatedDateNumber         string
	NoteOfAccountant          string
	NoteOfCeo                 string
	NoteOfOriginRequest       string
	ClaimId                   string
	CampaignName              string
}

var DebtorMap = map[string]pb.Debtor{
	"INSURER": pb.Debtor_DEBTOR_INSURER,
	"SALADIN": pb.Debtor_DEBTOR_SALADIN,
}

var StatusMap = map[string]pb.PaymentStatus{
	"WAITING_ACCOUNTANT": pb.PaymentStatus_PRS_WAITING_FOR_ACCOUNTANT,
	"WAITING_CEO":        pb.PaymentStatus_PRS_WAITING_FOR_CEO,
	"REJECTED":           pb.PaymentStatus_PRS_REJECTED,
	"APPROVED":           pb.PaymentStatus_PRS_APPROVED,
}

var MapTxnStatus = map[string]pb.PaymentTransactionStatus{
	"FAILED":  pb.PaymentTransactionStatus_PTS_FAILED,
	"SUCCESS": pb.PaymentTransactionStatus_PTS_SUCCESS,
	"NULL":    pb.PaymentTransactionStatus_PTS_UNSPECIFIED,
}

func toNewPaymentRequest(lpr *LegacyPaymentRequest) models.PaymentRequest {
	return models.PaymentRequest{
		ID:               lpr.Id,
		SrcID:            lpr.ClaimNumber,
		PaymentType:      "ClaimGITI",
		SldBankID:        0,
		AccountNumber:    lpr.BankAccountNumber,
		AccountName:      lpr.BeneficiaryName,
		BeneficiaryPhone: "",
		BankName:         lpr.BankName,
		BankBranch:       lpr.BankBranch,
		BankBin:          lpr.Bin,
		Debtor:           DebtorMap[lpr.Debtor],
		CampaignCode:     lpr.Campaign,
		CampaignName:     lpr.CampaignName,
		PaymentAmount:    lpr.Amount,
		SrcService:       "policy",
		CallbackURL:      "",
		Status:           StatusMap[lpr.Status],
		TxnStatus:        MapTxnStatus[lpr.DisbursementOverallStatus],
		SrcNote:          lpr.NoteOfOriginRequest,
		AccountantNote:   lpr.NoteOfAccountant,
		CeoNote:          lpr.NoteOfCeo,

		CreatedAt: lpr.CreatedDate.Add(-7 * time.Hour),
		UpdatedAt: lpr.UpdatedDate.Add(-7 * time.Hour),
	}
}

func toNewPaymentRequests(ls []*LegacyPaymentRequest) []models.PaymentRequest {
	var res []models.PaymentRequest
	for _, lpr := range ls {
		res = append(res, toNewPaymentRequest(lpr))
	}
	return res
}

type LegacyDisbursementTransaction struct {
	ReferenceNumber    string
	CreatedBy          string
	CreatedDate        time.Time
	Id                 string
	UpdatedBy          string
	UpdatedDate        time.Time
	Amount             int64
	DisbursementStatus string
	FtTransactionId    string
	OriginRequestId    string
	PaymentRequestId   int64
	RecievedResultTime string
	TransactionDate    *time.Time
	TransferResult     string
	Service            string
	TranferDate        *time.Time
	TransactionType    string
	CreatedDateTime    string
	CreatedAccountId   string
}

func toNewDisbursementTxn(ltxn *LegacyDisbursementTransaction) models.PaymentTransaction {
	return models.PaymentTransaction{
		ID:                       ltxn.ReferenceNumber,
		PaymentRequestID:         ltxn.PaymentRequestId,
		PaymentServiceProvider:   pb.PaymentServiceProvider_PSP_VPB,
		SldBankID:                0,
		PspBankID:                0,
		PspBankCode:              "",
		PspBankName:              "",
		PspBankShortName:         "",
		PspBin:                   "",
		BeneficiaryAccountNumber: "",
		BeneficiaryAccountName:   "",
		DebitAccountNumber:       "",
		PaymentAmount:            ltxn.Amount,
		Currency:                 "VND",
		TransactionDetail:        "",
		RefTxnID:                 ltxn.FtTransactionId,
		PartnerTransferResult:    ltxn.TransferResult,
		PartnerTransactionDate:   ltxn.TransactionDate,
		PartnerTransferDate:      ltxn.TranferDate,
		Status:                   MapTxnStatus[ltxn.DisbursementStatus],

		Model: models.Model{
			CreatedAt: ltxn.CreatedDate.Add(-7 * time.Hour),
			UpdatedAt: ltxn.UpdatedDate.Add(-7 * time.Hour),
		},
	}
}

func toNewDisbursementTxns(ls []*LegacyDisbursementTransaction) []models.PaymentTransaction {
	var res []models.PaymentTransaction
	for _, ltxn := range ls {
		res = append(res, toNewDisbursementTxn(ltxn))
	}
	return res
}

func toNewVPBTxn(ltxn *LegacyDisbursementTransaction) models.VpbTransaction {
	return models.VpbTransaction{
		ReferenceNumber: ltxn.ReferenceNumber,
		Service:         ltxn.Service,
		Type:            ltxn.TransactionType,
		SourceNumber:    "",
		TargetNumber:    "",
		BenName:         "",
		Amount:          "",
		Currency:        "VND",
		Remark:          "",
		BankID:          "",
		TransactionID:   ltxn.FtTransactionId,
		TransferResult:  ltxn.TransferResult,
		TransactionDate: datetimeutils.TimeToString(ltxn.TransactionDate, datetimeutils.VPBTransactionDateLayout),
		TransferDate:    datetimeutils.TimeToString(ltxn.TranferDate, datetimeutils.VPBTransactionDateLayout),
		BankName:        "",
		BankBin:         "",

		Model: models.Model{
			CreatedAt: ltxn.CreatedDate.Add(-7 * time.Hour),
			UpdatedAt: ltxn.UpdatedDate.Add(-7 * time.Hour),
		},
	}
}

func toNewVPBTxns(ls []*LegacyDisbursementTransaction) []models.VpbTransaction {
	var res []models.VpbTransaction
	for _, ltxn := range ls {
		res = append(res, toNewVPBTxn(ltxn))
	}
	return res
}
