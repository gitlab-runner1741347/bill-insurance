package main

import (
	"git.kafefin.net/backend/central-payout/config"
	"git.kafefin.net/backend/kitchen/l"

	"github.com/spf13/cobra"
)

var (
	ll = l.New()

	cfg *config.Config

	rootCmd = &cobra.Command{
		Use:           "task",
		SilenceErrors: true,
	}
)

func init() {
	cobra.OnInitialize(initConfig)
	rootCmd.AddCommand(demoCmd)
	rootCmd.AddCommand(vpbankCmd)
	rootCmd.AddCommand(syncLegacyDisbursementCmd)
	rootCmd.AddCommand(pubProcessPRCmd)
}

func initConfig() {
	cfg = config.Load()
}

func main() {
	if err := rootCmd.Execute(); err != nil {
		ll.Fatal("Task error", l.Error(err))
	}

	ll.Warn("Task succeeded")
}
