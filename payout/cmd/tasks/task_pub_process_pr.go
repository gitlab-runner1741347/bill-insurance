package main

import (
	"context"
	"fmt"
	"strconv"

	"git.kafefin.net/backend/central-payout/pkg/publisher"
	sqspubsub "git.kafefin.net/backend/central-payout/pkg/pubsub/sqs"
	"git.kafefin.net/backend/kitchen/l"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/spf13/cobra"
)

var (
	pubProcessPRCmd = &cobra.Command{
		Use:   "pub-process-pr",
		Short: "publish message to process payment request",
		RunE:  pubProcessPRFunc,
	}
)

func pubProcessPRFunc(cmd *cobra.Command, args []string) error {
	task := NewPubProcessPRTask()
	return task.Run(context.TODO(), args)
}

type PubProcessPRTask struct {
}

func NewPubProcessPRTask() *PubProcessPRTask {
	return &PubProcessPRTask{}
}

func (t *PubProcessPRTask) Run(ctx context.Context, args []string) error {
	ll.Warn("Start PubProcessPRTask")

	sqsSess := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
		Config: aws.Config{
			Region: aws.String("ap-southeast-1"),
		},
	}))
	sqsSvc := sqs.New(sqsSess)

	pub, err := sqspubsub.New(sqsSvc)
	if err != nil {
		return fmt.Errorf("initialize publisher client %w", err)
	}

	for _, v := range args {
		id, err := strconv.ParseInt(v, 10, 64)
		if err != nil {
			ll.Error("ParseInt", l.Error(err))
			continue
		}

		if err := publisher.PublishProcessPaymentTransactionMessage(context.TODO(), pub, cfg.ProcessPaymentTransactionQueueUrl, id); err != nil {
			ll.Error("PublishProcessPaymentTransactionMessage", l.Error(err))
		}
	}

	return nil
}
