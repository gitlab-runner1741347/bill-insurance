package main

import (
	"context"

	"git.kafefin.net/backend/kitchen/l"

	"git.kafefin.net/backend/product-configuration/pkg/disbursement"
	"github.com/spf13/cobra"
)

var (
	vpbankCmd = &cobra.Command{
		Use:   "vpbank",
		Short: "task vpbank",
		RunE:  vpbankFunc,
	}
)

func vpbankFunc(cmd *cobra.Command, args []string) error {
	task := NewVpbankTask()
	return task.Run(context.TODO())
}

type vpbankTask struct {
}

func NewVpbankTask() *vpbankTask {
	return &vpbankTask{}
}

func (t *vpbankTask) Run(ctx context.Context) error {
	ll.Info("Run vpbankTask")

	vp := disbursement.NewVpbankPayment(cfg.Vpbank.Host, cfg.Vpbank.Idn, cfg.Vpbank.BearerToken)
	ll.Info("vpbankTask", l.Object("banks", vp.ListBanks()))
	ll.Info("vpbankTask", l.Object("banks", vp.ListBanks()))

	return nil
}
