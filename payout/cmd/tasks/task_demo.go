package main

import (
	"context"

	"github.com/spf13/cobra"
)

var (
	demoCmd = &cobra.Command{
		Use:   "demo",
		Short: "task demo",
		RunE:  demoFunc,
	}
)

func demoFunc(cmd *cobra.Command, args []string) error {
	task := NewDemoTask()
	return task.Run(context.TODO())
}

type DemoTask struct {
}

func NewDemoTask() *DemoTask {
	return &DemoTask{}
}

func (t *DemoTask) Run(ctx context.Context) error {
	ll.Warn("Start DemoTask")
	return nil
}
