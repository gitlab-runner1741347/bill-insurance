// Code generated by protoc-gen-validate. DO NOT EDIT.
// source: cp_message.proto

package pb

import (
	"bytes"
	"errors"
	"fmt"
	"net"
	"net/mail"
	"net/url"
	"regexp"
	"sort"
	"strings"
	"time"
	"unicode/utf8"

	"google.golang.org/protobuf/types/known/anypb"
)

// ensure the imports are used
var (
	_ = bytes.MinRead
	_ = errors.New("")
	_ = fmt.Print
	_ = utf8.UTFMax
	_ = (*regexp.Regexp)(nil)
	_ = (*strings.Reader)(nil)
	_ = net.IPv4len
	_ = time.Duration(0)
	_ = (*url.URL)(nil)
	_ = (*mail.Address)(nil)
	_ = anypb.Any{}
	_ = sort.Sort
)

// Validate checks the field values on Bank with the rules defined in the proto
// definition for this message. If any rules are violated, the first error
// encountered is returned, or nil if there are no violations.
func (m *Bank) Validate() error {
	return m.validate(false)
}

// ValidateAll checks the field values on Bank with the rules defined in the
// proto definition for this message. If any rules are violated, the result is
// a list of violation errors wrapped in BankMultiError, or nil if none found.
func (m *Bank) ValidateAll() error {
	return m.validate(true)
}

func (m *Bank) validate(all bool) error {
	if m == nil {
		return nil
	}

	var errors []error

	// no validation rules for Id

	// no validation rules for Code

	// no validation rules for Name

	// no validation rules for ShortName

	// no validation rules for NameEn

	// no validation rules for Bin

	// no validation rules for Icon

	if len(errors) > 0 {
		return BankMultiError(errors)
	}

	return nil
}

// BankMultiError is an error wrapping multiple validation errors returned by
// Bank.ValidateAll() if the designated constraints aren't met.
type BankMultiError []error

// Error returns a concatenation of all the error messages it wraps.
func (m BankMultiError) Error() string {
	var msgs []string
	for _, err := range m {
		msgs = append(msgs, err.Error())
	}
	return strings.Join(msgs, "; ")
}

// AllErrors returns a list of validation violation errors.
func (m BankMultiError) AllErrors() []error { return m }

// BankValidationError is the validation error returned by Bank.Validate if the
// designated constraints aren't met.
type BankValidationError struct {
	field  string
	reason string
	cause  error
	key    bool
}

// Field function returns field value.
func (e BankValidationError) Field() string { return e.field }

// Reason function returns reason value.
func (e BankValidationError) Reason() string { return e.reason }

// Cause function returns cause value.
func (e BankValidationError) Cause() error { return e.cause }

// Key function returns key value.
func (e BankValidationError) Key() bool { return e.key }

// ErrorName returns error name.
func (e BankValidationError) ErrorName() string { return "BankValidationError" }

// Error satisfies the builtin error interface
func (e BankValidationError) Error() string {
	cause := ""
	if e.cause != nil {
		cause = fmt.Sprintf(" | caused by: %v", e.cause)
	}

	key := ""
	if e.key {
		key = "key for "
	}

	return fmt.Sprintf(
		"invalid %sBank.%s: %s%s",
		key,
		e.field,
		e.reason,
		cause)
}

var _ error = BankValidationError{}

var _ interface {
	Field() string
	Reason() string
	Key() bool
	Cause() error
	ErrorName() string
} = BankValidationError{}

// Validate checks the field values on Brand with the rules defined in the
// proto definition for this message. If any rules are violated, the first
// error encountered is returned, or nil if there are no violations.
func (m *Brand) Validate() error {
	return m.validate(false)
}

// ValidateAll checks the field values on Brand with the rules defined in the
// proto definition for this message. If any rules are violated, the result is
// a list of violation errors wrapped in BrandMultiError, or nil if none found.
func (m *Brand) ValidateAll() error {
	return m.validate(true)
}

func (m *Brand) validate(all bool) error {
	if m == nil {
		return nil
	}

	var errors []error

	// no validation rules for Code

	// no validation rules for Name

	// no validation rules for Icon

	if len(errors) > 0 {
		return BrandMultiError(errors)
	}

	return nil
}

// BrandMultiError is an error wrapping multiple validation errors returned by
// Brand.ValidateAll() if the designated constraints aren't met.
type BrandMultiError []error

// Error returns a concatenation of all the error messages it wraps.
func (m BrandMultiError) Error() string {
	var msgs []string
	for _, err := range m {
		msgs = append(msgs, err.Error())
	}
	return strings.Join(msgs, "; ")
}

// AllErrors returns a list of validation violation errors.
func (m BrandMultiError) AllErrors() []error { return m }

// BrandValidationError is the validation error returned by Brand.Validate if
// the designated constraints aren't met.
type BrandValidationError struct {
	field  string
	reason string
	cause  error
	key    bool
}

// Field function returns field value.
func (e BrandValidationError) Field() string { return e.field }

// Reason function returns reason value.
func (e BrandValidationError) Reason() string { return e.reason }

// Cause function returns cause value.
func (e BrandValidationError) Cause() error { return e.cause }

// Key function returns key value.
func (e BrandValidationError) Key() bool { return e.key }

// ErrorName returns error name.
func (e BrandValidationError) ErrorName() string { return "BrandValidationError" }

// Error satisfies the builtin error interface
func (e BrandValidationError) Error() string {
	cause := ""
	if e.cause != nil {
		cause = fmt.Sprintf(" | caused by: %v", e.cause)
	}

	key := ""
	if e.key {
		key = "key for "
	}

	return fmt.Sprintf(
		"invalid %sBrand.%s: %s%s",
		key,
		e.field,
		e.reason,
		cause)
}

var _ error = BrandValidationError{}

var _ interface {
	Field() string
	Reason() string
	Key() bool
	Cause() error
	ErrorName() string
} = BrandValidationError{}

// Validate checks the field values on Agent with the rules defined in the
// proto definition for this message. If any rules are violated, the first
// error encountered is returned, or nil if there are no violations.
func (m *Agent) Validate() error {
	return m.validate(false)
}

// ValidateAll checks the field values on Agent with the rules defined in the
// proto definition for this message. If any rules are violated, the result is
// a list of violation errors wrapped in AgentMultiError, or nil if none found.
func (m *Agent) ValidateAll() error {
	return m.validate(true)
}

func (m *Agent) validate(all bool) error {
	if m == nil {
		return nil
	}

	var errors []error

	// no validation rules for Code

	// no validation rules for City

	// no validation rules for Address

	// no validation rules for Phone

	// no validation rules for Index

	// no validation rules for Name

	if len(errors) > 0 {
		return AgentMultiError(errors)
	}

	return nil
}

// AgentMultiError is an error wrapping multiple validation errors returned by
// Agent.ValidateAll() if the designated constraints aren't met.
type AgentMultiError []error

// Error returns a concatenation of all the error messages it wraps.
func (m AgentMultiError) Error() string {
	var msgs []string
	for _, err := range m {
		msgs = append(msgs, err.Error())
	}
	return strings.Join(msgs, "; ")
}

// AllErrors returns a list of validation violation errors.
func (m AgentMultiError) AllErrors() []error { return m }

// AgentValidationError is the validation error returned by Agent.Validate if
// the designated constraints aren't met.
type AgentValidationError struct {
	field  string
	reason string
	cause  error
	key    bool
}

// Field function returns field value.
func (e AgentValidationError) Field() string { return e.field }

// Reason function returns reason value.
func (e AgentValidationError) Reason() string { return e.reason }

// Cause function returns cause value.
func (e AgentValidationError) Cause() error { return e.cause }

// Key function returns key value.
func (e AgentValidationError) Key() bool { return e.key }

// ErrorName returns error name.
func (e AgentValidationError) ErrorName() string { return "AgentValidationError" }

// Error satisfies the builtin error interface
func (e AgentValidationError) Error() string {
	cause := ""
	if e.cause != nil {
		cause = fmt.Sprintf(" | caused by: %v", e.cause)
	}

	key := ""
	if e.key {
		key = "key for "
	}

	return fmt.Sprintf(
		"invalid %sAgent.%s: %s%s",
		key,
		e.field,
		e.reason,
		cause)
}

var _ error = AgentValidationError{}

var _ interface {
	Field() string
	Reason() string
	Key() bool
	Cause() error
	ErrorName() string
} = AgentValidationError{}

// Validate checks the field values on Insurer with the rules defined in the
// proto definition for this message. If any rules are violated, the first
// error encountered is returned, or nil if there are no violations.
func (m *Insurer) Validate() error {
	return m.validate(false)
}

// ValidateAll checks the field values on Insurer with the rules defined in the
// proto definition for this message. If any rules are violated, the result is
// a list of violation errors wrapped in InsurerMultiError, or nil if none found.
func (m *Insurer) ValidateAll() error {
	return m.validate(true)
}

func (m *Insurer) validate(all bool) error {
	if m == nil {
		return nil
	}

	var errors []error

	// no validation rules for Code

	// no validation rules for Name

	// no validation rules for Icon

	for idx, item := range m.GetAgents() {
		_, _ = idx, item

		if all {
			switch v := interface{}(item).(type) {
			case interface{ ValidateAll() error }:
				if err := v.ValidateAll(); err != nil {
					errors = append(errors, InsurerValidationError{
						field:  fmt.Sprintf("Agents[%v]", idx),
						reason: "embedded message failed validation",
						cause:  err,
					})
				}
			case interface{ Validate() error }:
				if err := v.Validate(); err != nil {
					errors = append(errors, InsurerValidationError{
						field:  fmt.Sprintf("Agents[%v]", idx),
						reason: "embedded message failed validation",
						cause:  err,
					})
				}
			}
		} else if v, ok := interface{}(item).(interface{ Validate() error }); ok {
			if err := v.Validate(); err != nil {
				return InsurerValidationError{
					field:  fmt.Sprintf("Agents[%v]", idx),
					reason: "embedded message failed validation",
					cause:  err,
				}
			}
		}

	}

	if len(errors) > 0 {
		return InsurerMultiError(errors)
	}

	return nil
}

// InsurerMultiError is an error wrapping multiple validation errors returned
// by Insurer.ValidateAll() if the designated constraints aren't met.
type InsurerMultiError []error

// Error returns a concatenation of all the error messages it wraps.
func (m InsurerMultiError) Error() string {
	var msgs []string
	for _, err := range m {
		msgs = append(msgs, err.Error())
	}
	return strings.Join(msgs, "; ")
}

// AllErrors returns a list of validation violation errors.
func (m InsurerMultiError) AllErrors() []error { return m }

// InsurerValidationError is the validation error returned by Insurer.Validate
// if the designated constraints aren't met.
type InsurerValidationError struct {
	field  string
	reason string
	cause  error
	key    bool
}

// Field function returns field value.
func (e InsurerValidationError) Field() string { return e.field }

// Reason function returns reason value.
func (e InsurerValidationError) Reason() string { return e.reason }

// Cause function returns cause value.
func (e InsurerValidationError) Cause() error { return e.cause }

// Key function returns key value.
func (e InsurerValidationError) Key() bool { return e.key }

// ErrorName returns error name.
func (e InsurerValidationError) ErrorName() string { return "InsurerValidationError" }

// Error satisfies the builtin error interface
func (e InsurerValidationError) Error() string {
	cause := ""
	if e.cause != nil {
		cause = fmt.Sprintf(" | caused by: %v", e.cause)
	}

	key := ""
	if e.key {
		key = "key for "
	}

	return fmt.Sprintf(
		"invalid %sInsurer.%s: %s%s",
		key,
		e.field,
		e.reason,
		cause)
}

var _ error = InsurerValidationError{}

var _ interface {
	Field() string
	Reason() string
	Key() bool
	Cause() error
	ErrorName() string
} = InsurerValidationError{}

// Validate checks the field values on Beneficiary with the rules defined in
// the proto definition for this message. If any rules are violated, the first
// error encountered is returned, or nil if there are no violations.
func (m *Beneficiary) Validate() error {
	return m.validate(false)
}

// ValidateAll checks the field values on Beneficiary with the rules defined in
// the proto definition for this message. If any rules are violated, the
// result is a list of violation errors wrapped in BeneficiaryMultiError, or
// nil if none found.
func (m *Beneficiary) ValidateAll() error {
	return m.validate(true)
}

func (m *Beneficiary) validate(all bool) error {
	if m == nil {
		return nil
	}

	var errors []error

	// no validation rules for SldBankId

	// no validation rules for BankName

	// no validation rules for ShortName

	// no validation rules for AccountNumber

	// no validation rules for AccountName

	// no validation rules for Bin

	if len(errors) > 0 {
		return BeneficiaryMultiError(errors)
	}

	return nil
}

// BeneficiaryMultiError is an error wrapping multiple validation errors
// returned by Beneficiary.ValidateAll() if the designated constraints aren't met.
type BeneficiaryMultiError []error

// Error returns a concatenation of all the error messages it wraps.
func (m BeneficiaryMultiError) Error() string {
	var msgs []string
	for _, err := range m {
		msgs = append(msgs, err.Error())
	}
	return strings.Join(msgs, "; ")
}

// AllErrors returns a list of validation violation errors.
func (m BeneficiaryMultiError) AllErrors() []error { return m }

// BeneficiaryValidationError is the validation error returned by
// Beneficiary.Validate if the designated constraints aren't met.
type BeneficiaryValidationError struct {
	field  string
	reason string
	cause  error
	key    bool
}

// Field function returns field value.
func (e BeneficiaryValidationError) Field() string { return e.field }

// Reason function returns reason value.
func (e BeneficiaryValidationError) Reason() string { return e.reason }

// Cause function returns cause value.
func (e BeneficiaryValidationError) Cause() error { return e.cause }

// Key function returns key value.
func (e BeneficiaryValidationError) Key() bool { return e.key }

// ErrorName returns error name.
func (e BeneficiaryValidationError) ErrorName() string { return "BeneficiaryValidationError" }

// Error satisfies the builtin error interface
func (e BeneficiaryValidationError) Error() string {
	cause := ""
	if e.cause != nil {
		cause = fmt.Sprintf(" | caused by: %v", e.cause)
	}

	key := ""
	if e.key {
		key = "key for "
	}

	return fmt.Sprintf(
		"invalid %sBeneficiary.%s: %s%s",
		key,
		e.field,
		e.reason,
		cause)
}

var _ error = BeneficiaryValidationError{}

var _ interface {
	Field() string
	Reason() string
	Key() bool
	Cause() error
	ErrorName() string
} = BeneficiaryValidationError{}

// Validate checks the field values on Payment with the rules defined in the
// proto definition for this message. If any rules are violated, the first
// error encountered is returned, or nil if there are no violations.
func (m *Payment) Validate() error {
	return m.validate(false)
}

// ValidateAll checks the field values on Payment with the rules defined in the
// proto definition for this message. If any rules are violated, the result is
// a list of violation errors wrapped in PaymentMultiError, or nil if none found.
func (m *Payment) ValidateAll() error {
	return m.validate(true)
}

func (m *Payment) validate(all bool) error {
	if m == nil {
		return nil
	}

	var errors []error

	// no validation rules for Id

	// no validation rules for SrcId

	// no validation rules for PaymentType

	if all {
		switch v := interface{}(m.GetBeneficiary()).(type) {
		case interface{ ValidateAll() error }:
			if err := v.ValidateAll(); err != nil {
				errors = append(errors, PaymentValidationError{
					field:  "Beneficiary",
					reason: "embedded message failed validation",
					cause:  err,
				})
			}
		case interface{ Validate() error }:
			if err := v.Validate(); err != nil {
				errors = append(errors, PaymentValidationError{
					field:  "Beneficiary",
					reason: "embedded message failed validation",
					cause:  err,
				})
			}
		}
	} else if v, ok := interface{}(m.GetBeneficiary()).(interface{ Validate() error }); ok {
		if err := v.Validate(); err != nil {
			return PaymentValidationError{
				field:  "Beneficiary",
				reason: "embedded message failed validation",
				cause:  err,
			}
		}
	}

	// no validation rules for Debtor

	// no validation rules for CampaignCode

	// no validation rules for CampaignName

	// no validation rules for Status

	// no validation rules for PaymentStatus

	if len(errors) > 0 {
		return PaymentMultiError(errors)
	}

	return nil
}

// PaymentMultiError is an error wrapping multiple validation errors returned
// by Payment.ValidateAll() if the designated constraints aren't met.
type PaymentMultiError []error

// Error returns a concatenation of all the error messages it wraps.
func (m PaymentMultiError) Error() string {
	var msgs []string
	for _, err := range m {
		msgs = append(msgs, err.Error())
	}
	return strings.Join(msgs, "; ")
}

// AllErrors returns a list of validation violation errors.
func (m PaymentMultiError) AllErrors() []error { return m }

// PaymentValidationError is the validation error returned by Payment.Validate
// if the designated constraints aren't met.
type PaymentValidationError struct {
	field  string
	reason string
	cause  error
	key    bool
}

// Field function returns field value.
func (e PaymentValidationError) Field() string { return e.field }

// Reason function returns reason value.
func (e PaymentValidationError) Reason() string { return e.reason }

// Cause function returns cause value.
func (e PaymentValidationError) Cause() error { return e.cause }

// Key function returns key value.
func (e PaymentValidationError) Key() bool { return e.key }

// ErrorName returns error name.
func (e PaymentValidationError) ErrorName() string { return "PaymentValidationError" }

// Error satisfies the builtin error interface
func (e PaymentValidationError) Error() string {
	cause := ""
	if e.cause != nil {
		cause = fmt.Sprintf(" | caused by: %v", e.cause)
	}

	key := ""
	if e.key {
		key = "key for "
	}

	return fmt.Sprintf(
		"invalid %sPayment.%s: %s%s",
		key,
		e.field,
		e.reason,
		cause)
}

var _ error = PaymentValidationError{}

var _ interface {
	Field() string
	Reason() string
	Key() bool
	Cause() error
	ErrorName() string
} = PaymentValidationError{}

// Validate checks the field values on Paging with the rules defined in the
// proto definition for this message. If any rules are violated, the first
// error encountered is returned, or nil if there are no violations.
func (m *Paging) Validate() error {
	return m.validate(false)
}

// ValidateAll checks the field values on Paging with the rules defined in the
// proto definition for this message. If any rules are violated, the result is
// a list of violation errors wrapped in PagingMultiError, or nil if none found.
func (m *Paging) ValidateAll() error {
	return m.validate(true)
}

func (m *Paging) validate(all bool) error {
	if m == nil {
		return nil
	}

	var errors []error

	// no validation rules for Total

	// no validation rules for Offset

	if len(errors) > 0 {
		return PagingMultiError(errors)
	}

	return nil
}

// PagingMultiError is an error wrapping multiple validation errors returned by
// Paging.ValidateAll() if the designated constraints aren't met.
type PagingMultiError []error

// Error returns a concatenation of all the error messages it wraps.
func (m PagingMultiError) Error() string {
	var msgs []string
	for _, err := range m {
		msgs = append(msgs, err.Error())
	}
	return strings.Join(msgs, "; ")
}

// AllErrors returns a list of validation violation errors.
func (m PagingMultiError) AllErrors() []error { return m }

// PagingValidationError is the validation error returned by Paging.Validate if
// the designated constraints aren't met.
type PagingValidationError struct {
	field  string
	reason string
	cause  error
	key    bool
}

// Field function returns field value.
func (e PagingValidationError) Field() string { return e.field }

// Reason function returns reason value.
func (e PagingValidationError) Reason() string { return e.reason }

// Cause function returns cause value.
func (e PagingValidationError) Cause() error { return e.cause }

// Key function returns key value.
func (e PagingValidationError) Key() bool { return e.key }

// ErrorName returns error name.
func (e PagingValidationError) ErrorName() string { return "PagingValidationError" }

// Error satisfies the builtin error interface
func (e PagingValidationError) Error() string {
	cause := ""
	if e.cause != nil {
		cause = fmt.Sprintf(" | caused by: %v", e.cause)
	}

	key := ""
	if e.key {
		key = "key for "
	}

	return fmt.Sprintf(
		"invalid %sPaging.%s: %s%s",
		key,
		e.field,
		e.reason,
		cause)
}

var _ error = PagingValidationError{}

var _ interface {
	Field() string
	Reason() string
	Key() bool
	Cause() error
	ErrorName() string
} = PagingValidationError{}
