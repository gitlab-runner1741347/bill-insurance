FROM public.ecr.aws/docker/library/golang:1.19.9-alpine3.18 AS builder

WORKDIR /app
RUN apk update && apk upgrade && \
    apk add bash git openssh gcc libc-dev
COPY ./go.mod ./go.sum ./

ENV GOSUMDB off

ENV GOPRIVATE git.kafefin.net
RUN go mod download

COPY ./ ./
RUN go build -o /dist/server cmd/server/*.go
RUN go build -o /dist/jobs   cmd/jobs/*.go
RUN go build -o /dist/tasks  cmd/tasks/*.go

FROM public.ecr.aws/docker/library/alpine:3.18.0

RUN apk add --update ca-certificates tzdata curl pkgconfig && \
    cp /usr/share/zoneinfo/Asia/Ho_Chi_Minh /etc/localtime && \
    echo "Asia/Ho_Chi_Minh" > /etc/timezone && \
    rm -rf /var/cache/apk/*

COPY --from=builder /dist/server /app/bin/server
COPY --from=builder /dist/jobs   /app/bin/jobs
COPY --from=builder /dist/tasks  /app/bin/tasks

WORKDIR /app/bin
CMD ["/app/bin/server"]