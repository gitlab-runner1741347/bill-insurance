package main

import (
	"context"

	migratecentralpolicy "git.kafefin.net/backend/bill-insurance/cmd/jobs/migrate_central_policy"
	"git.kafefin.net/backend/bill-insurance/config"
	"git.kafefin.net/backend/bill-insurance/internal/external"
	"git.kafefin.net/backend/bill-insurance/internal/must"
	"git.kafefin.net/backend/kitchen/l"
	"github.com/spf13/cobra"
)

var (
	migrateCmd = &cobra.Command{
		Use:   "migrate-expired-policy",
		Short: "job migrate data to central policy",
		RunE:  migrateToCentralFunc,
	}
)

func migrateToCentralFunc(cmd *cobra.Command, args []string) error {
	task := NewMigrateToCentralTask()
	return task.Run(context.TODO())
}

type MigrateToCentralJob struct {
}

func NewMigrateToCentralTask() *MigrateToCentralJob {
	return &MigrateToCentralJob{}
}

func (t *MigrateToCentralJob) Run(ctx context.Context) error {
	ll.Info("Run Migrate Expired Policy To Central Job")
	var cfg = config.Load()
	var ll = l.New()

	db := must.ConnectPostgres(cfg.Postgres)
	centralPolicy := external.NewCentralPolicyClient(cfg.CentralPolicy, true)

	service := migratecentralpolicy.NewMigrateCentralPolicyService(
		ctx,
		ll,
		db,
		centralPolicy,
	)

	err := service.MicgrateDataToCentralPolicy(ctx)

	return err
}
