package migratecentralpolicy

import (
	"context"

	"git.kafefin.net/backend/bill-insurance/internal/external"
	"git.kafefin.net/backend/bill-insurance/internal/repositories"
	"git.kafefin.net/backend/bill-insurance/internal/repositories/postgresql"
	"git.kafefin.net/backend/kitchen/l"
	"gorm.io/gorm"
)

type ServiceMigrateCentralPolicyAction interface {
	MicgrateDateToCentralPolicy(ctx context.Context) error
}

type serviceMigrateCentralPolicy struct {
	log              l.Logger
	sldCentralPolicy external.CentralPolicyClient
	db               *gorm.DB
	repo             repositories.PostgresqlRepository
}

func NewMigrateCentralPolicyService(
	ctx context.Context,
	log l.Logger,
	db *gorm.DB,
	sldCentralPolicy external.CentralPolicyClient,
) *serviceMigrateCentralPolicy {
	return &serviceMigrateCentralPolicy{
		log:              log,
		sldCentralPolicy: sldCentralPolicy,
		db:               db,
		repo:             postgresql.NewPostgresSQLStore(log, db, nil),
	}
}
