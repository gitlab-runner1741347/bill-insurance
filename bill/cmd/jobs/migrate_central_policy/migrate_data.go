package migratecentralpolicy

import (
	"context"
	"time"

	"git.kafefin.net/backend/bill-insurance/internal/models"
	"git.kafefin.net/backend/bill-insurance/internal/pkg/convert"
	"git.kafefin.net/backend/kitchen/l"
	"gorm.io/gorm"
)

func (s *serviceMigrateCentralPolicy) MicgrateDataToCentralPolicy(ctx context.Context) error {
	var results []*models.BpPolicy
	timeNow := time.Now()

	result := s.db.Where("effective_to < ?", timeNow).FindInBatches(&results, 50, func(tx *gorm.DB, batch int) error {
		ids := make([]string, 0)
		for i := range results {
			ids = append(ids, results[i].PolicyID)
			results[i].PolicyStatus = "EXPIRED"
		}
		err := s.repo.UpdateStatusRecordsFromCretedOk(ctx, ids, "EXPIRED")
		if err != nil {
			s.log.Error("Error  UpdateStatus", l.Error(err))
		}

		_, err = s.sldCentralPolicy.UpsertPolicies(ctx, convert.ConvertBpPoliciesToCentralPolicies(results))
		return err
	})

	if result.Error != nil {
		s.log.Error("Error  MicgrateDateToCentralPolicy", l.Error(result.Error))
	}
	return result.Error
}
