package main

import (
	"git.kafefin.net/backend/kitchen/l"

	"github.com/spf13/cobra"
)

var (
	ll = l.New()

	rootCmd = &cobra.Command{
		Use:           "task",
		SilenceErrors: true,
	}
)

func init() {
	cobra.OnInitialize(initConfig)
	rootCmd.AddCommand(migrateCmd)
}

func initConfig() {

}

func main() {
	if err := rootCmd.Execute(); err != nil {
		ll.Fatal("Job error", l.Error(err))
	}

	ll.Warn("Job succeeded")
}
