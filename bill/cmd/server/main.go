package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"strings"

	"git.kafefin.net/backend/bill-insurance/config"
	"git.kafefin.net/backend/bill-insurance/internal/external"
	"git.kafefin.net/backend/bill-insurance/internal/must"
	"git.kafefin.net/backend/bill-insurance/internal/repositories/mysql"
	"git.kafefin.net/backend/bill-insurance/internal/repositories/postgresql"
	"git.kafefin.net/backend/bill-insurance/internal/services"
	"git.kafefin.net/backend/kitchen/l"
	"git.kafefin.net/backend/salad-kit/grpckit"
	"git.kafefin.net/backend/salad-kit/server"
	staffPb "git.kafefin.net/backend/saladin-staff/pb"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

func run(_ []string) error {
	var ctx = context.TODO()
	var cfg = config.Load()
	var ll = l.New()

	db := must.ConnectMySQL(cfg.MySQL)
	dbPostgres := must.ConnectPostgres(cfg.Postgres)

	_ = mysql.NewMySQLStore(ll, db, cfg)
	postgresqlRepos := postgresql.NewPostgresSQLStore(ll, dbPostgres, cfg)
	redis := must.ConnectRedis(cfg.Redis)
	// repos.AutoMigrate()

	// tokenSvc := vendorAuthen.NewTokenAuthenticator(ll, redis, 600, "bill-insurance:client-token")

	printCertClient := external.NewPrintCertClient(cfg.PrintCertAddress, true)
	sldMessageClient := external.NewSaladinMessageClient(cfg.SaladinMessageAddress, true)

	staffClient := external.NewSaladinStaffClient(cfg.SaladinStaffAddress, true)
	sldUserClient := external.NewSaladinUserClient(cfg.SaladinUserAddress, true)
	centralPolicy := external.NewCentralPolicyClient(cfg.CentralPolicy, true)
	billProtection := external.NewBillProtectionService(nil, cfg.SaladinBillProtectionAddress)
	saladinClaimHub := external.NewClaimHubClient(cfg.ClaimHubAddress, true)

	mw := NewCustomMiddleware(ll, nil, sldUserClient)
	srv, err := server.New(
		server.WithGrpcAddrListen(cfg.Server.GRPC),
		server.WithGatewayAddrListen(cfg.Server.HTTP),
		server.WithServiceServer(
			services.NewBillInsuranceService(
				ctx, cfg, ll,
				nil,
				postgresqlRepos,
				redis,
				printCertClient,
				sldMessageClient,
				sldUserClient,
				centralPolicy,
				billProtection,
				saladinClaimHub,
			),
		),

		server.WithGatewayServerMiddlewares(mw.MiddlewareHandleFunc),
		server.WithGrpcServerUnaryInterceptors(grpckit.MappingStaffResourceInterceptor(rsActionMap, validateAdminRsFn(ll, staffClient))),
		server.WithPassedHeader(func(h string) bool {
			return strings.HasPrefix(h, "X-")
		}),
	)

	if err != nil {
		return fmt.Errorf("initialize server %w", err)
	}

	if err := srv.Serve(ctx); err != nil {
		return fmt.Errorf("serving %w", err)
	}
	return nil

}

var rsActionMap = map[string]grpckit.ResourceAction{
	// AdminGetDetailPolicyByPolicyId
	"/bill_insurance.BillInsuranceAdmin/AdminGetDetailPolicyByPolicyId": {
		Resource: "srn:bill-insurance:policy/get",
		Action:   "read",
	},

	// AdminGetDetailPolicyByPolicyIdWithOrg
	"/bill_insurance.BillInsuranceAdmin/AdminGetDetailPolicyByPolicyIdWithOrg": {
		Resource: "srn:bill-insurance:$org:policy/get",
		Action:   "read",
	},

	// AdminListPolicies
	"/bill_insurance.BillInsuranceAdmin/AdminListPolicies": {
		Resource: "srn:bill-insurance:policy/list",
		Action:   "read",
	},

	// AdminListPoliciesWithOrg
	"/bill_insurance.BillInsuranceAdmin/AdminListPoliciesWithOrg": {
		Resource: "srn:bill-insurance:$org:policy/list",
		Action:   "read",
	},

	// Admin Update policy
	"/bill_insurance.BillInsuranceAdmin/AdminUpdatePolicy": {
		Resource: "srn:bill-insurance:policy/update",
		Action:   "write",
	},

	"/bill_insurance.BillInsuranceAdmin/AdminPartnerUpdatePolicy": {
		Resource: "srn:bill-insurance:$org:policy/update",
		Action:   "write",
	},

	"/bill_insurance.BillInsuranceAdmin/AdminGetChangeLog": {
		Resource: "srn:bill-insurance:policy/list",
		Action:   "read",
	},

	"/bill_insurance.BillInsuranceAdmin/AdminPartnerGetChangeLog": {
		Resource: "srn:bill-insurance:$org:policy/list",
		Action:   "read",
	},
}

func validateAdminRsFn(log l.Logger, staffClient external.SaladinStaffClient) func(context.Context, grpckit.ResourceAction) (grpckit.StaffPolicy, error) {
	return func(ctx context.Context, rs grpckit.ResourceAction) (grpckit.StaffPolicy, error) {
		md, ok := metadata.FromIncomingContext(ctx)
		if !ok {
			return nil, status.Error(codes.Internal, "invalid metadata")
		}

		mdAuthorizations := md.Get("authorization")
		if len(mdAuthorizations) == 0 {
			return nil, status.Error(codes.PermissionDenied, "invalid Authorization header")
		}
		tokenStrings := strings.Split(mdAuthorizations[0], "Bearer ")
		if len(tokenStrings) != 2 {
			return nil, status.Error(codes.PermissionDenied, "invalid Authorization header")
		}

		request := &staffPb.TokenPolicyRequest{
			Token:    tokenStrings[1],
			Action:   rs.Action,
			Resource: rs.Resource,
		}
		policy, err := staffClient.Client.TokenPolicy(context.Background(), request)
		if err != nil {
			log.Error("validateAdminRsFn call saladin-staff error", l.Error(err))
			return nil, status.Error(codes.PermissionDenied, err.Error())
		}

		if !policy.Active {
			return nil, status.Error(codes.PermissionDenied, codes.PermissionDenied.String())
		}

		return policy, nil
	}
}

func main() {
	if err := run(os.Args); err != nil {
		log.Fatal(err)
	}
}
