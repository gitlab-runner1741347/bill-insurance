package main

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"net/http"
	"strconv"
	"strings"

	"git.kafefin.net/backend/bill-insurance/config"
	"git.kafefin.net/backend/bill-insurance/internal/external"
	"git.kafefin.net/backend/kitchen/gwutils"
	"git.kafefin.net/backend/kitchen/l"
	vendorAuthen "git.kafefin.net/backend/kitchen/vendor_authen"
	userpb "git.kafefin.net/backend/saladin-central-proto/saladin_user"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"github.com/hashicorp/go-multierror"
	"github.com/pingcap/log"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type Middleware struct {
	log              l.Logger
	serviceVendorMap map[string]map[string]*config.SaladinVendor
	userClient       external.SaladinUserClient
}

const vendorPrefixPath = "/vendor/"

func NewCustomMiddleware(log l.Logger, svcVendorMap map[string]map[string]*config.SaladinVendor, userClient external.SaladinUserClient) *Middleware {
	return &Middleware{
		log:              log,
		serviceVendorMap: svcVendorMap,
		userClient:       userClient,
	}
}

func (m *Middleware) MiddlewareHandleFunc(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.HasPrefix(r.RequestURI, vendorPrefixPath) {
			if valid := m.authenVendorRequest(r); !valid {
				authenError(w, r)
				return
			}
		}

		if strings.HasPrefix(r.URL.Path, "/user") {
			header := r.Header

			authorizeHeader := header.Get("Authorization")

			if authorizeHeader == "" {
				m.log.Warn("Empty Authorization header")
				authenError(w, r)
				return
			}

			tokenStrings := strings.Split(authorizeHeader, "Bearer ")
			if len(tokenStrings) != 2 {
				m.log.Warn("Invalid Authorization header", l.String("Authorization header", authorizeHeader))
				authenError(w, r)
				return
			}

			userInfo, err := m.authenUserRequest(tokenStrings[1])
			if err != nil {
				m.log.Error("Empty Authorization header", l.Error(err))
				authenError(w, r)
				return
			}

			if !userInfo.GetActive() {
				authenError(w, r)
				return
			}

			r.Header.Set(gwutils.ToMetadataHeader(gwutils.UserIDKey), userInfo.GetSub())
			r.Header.Set(gwutils.ToMetadataHeader(gwutils.UserPhoneKey), userInfo.GetPhone())
		}

		next.ServeHTTP(w, r)
	})
}

type UserInfo interface {
	GetSub() string
	GetPhone() string
	GetActive() bool
}

func mapUserLoginToken(token string) *userpb.UserTokenIntrospectRequest {
	if len(token) == 32 {
		return &userpb.UserTokenIntrospectRequest{
			OldToken: token,
		}
	}

	return &userpb.UserTokenIntrospectRequest{
		Token: token,
	}
}

func (m *Middleware) authenUserRequest(loginToken string) (UserInfo, error) {
	request := mapUserLoginToken(loginToken)

	userInfo, err := m.userClient.UserTokenIntrospect(context.Background(), request)
	if err != nil {
		log.Error("authenUserRequest call saladin-user error", l.Error(err))
		return nil, multierror.Prefix(err, "authenUserRequest call saladin-user error")
	}

	return userInfo, nil
}

func (m *Middleware) authenVendorRequest(r *http.Request) bool {

	subURI := strings.TrimPrefix(r.RequestURI, vendorPrefixPath)
	subURIParts := strings.Split(subURI, "/")
	if len(subURIParts) == 0 {
		m.log.Error("authenVendorRequest invalid URI", l.String("request_uri", r.RequestURI))
		return false
	}
	serviceName := subURIParts[0]
	clientKeyMap := m.serviceVendorMap[serviceName]
	if clientKeyMap == nil {
		m.log.Error("authenVendorRequest invalid clientKeyMap", l.String("service_name", serviceName))
		return false
	}

	header := r.Header
	method := r.Method

	switch method {
	case http.MethodGet:
		path := r.URL.Path
		rawQuery := r.URL.RawQuery
		if rawQuery != "" {
			path = path + "?" + rawQuery
		}

		timestamp := header.Get(gwutils.TimestampHeader)
		clientKey := header.Get(gwutils.ClientKeyHeader)
		signature := header.Get(gwutils.SignatureHeader)

		serviceVendor := clientKeyMap[clientKey]
		if serviceVendor == nil {
			m.log.Warn("Invalid vendor", l.String("client_key", clientKey))
			return false
		}
		r.Header.Set(gwutils.VendorIDKey, strconv.Itoa(int(serviceVendor.VendorID)))

		if err := vendorAuthen.ValidateSignature(timestamp, clientKey, string(path), serviceVendor.ClientSecret, signature); err != nil {
			m.log.Warn("authenticateRequest err authen", l.Error(err))
			return false
		}

		return true
	case http.MethodPost, http.MethodPut, http.MethodPatch:
		reqBody, err := io.ReadAll(r.Body)
		if err != nil {
			m.log.Error("authenticateRequest err read req body", l.Error(err))
			return false
		}
		r.Body.Close()                                  // must close
		r.Body = io.NopCloser(bytes.NewBuffer(reqBody)) // must re-assign

		timestamp := header.Get(gwutils.TimestampHeader)
		clientKey := header.Get(gwutils.ClientKeyHeader)
		signature := header.Get(gwutils.SignatureHeader)

		serviceVendor := clientKeyMap[clientKey]
		if serviceVendor == nil {
			m.log.Warn("Invalid vendor", l.String("client_key", clientKey))
			return false
		}
		r.Header.Set(gwutils.VendorIDKey, strconv.Itoa(int(serviceVendor.VendorID)))

		if err := vendorAuthen.ValidateSignature(timestamp, clientKey, string(reqBody), serviceVendor.ClientSecret, signature); err != nil {
			m.log.Warn("authenticateRequest err authen", l.Error(err))
			return false
		}

		return true
	default:
		return false
	}

}

type errorBody struct {
	Code    uint32 `json:"code,omitempty"`
	Message string `json:"message,omitempty"`
}

func customHTTPError(w http.ResponseWriter, r *http.Request, err error) {
	const fallback = `{"message": "failed to marshal error message"}`

	w.Header().Set("Content-type", "application/json")
	e := errorBody{
		Code:    uint32(status.Code(err)),
		Message: status.Convert(err).Message(),
	}

	s := status.Convert(err)
	st := runtime.HTTPStatusFromCode(s.Code())
	if st == http.StatusInternalServerError {
		e.Message = "Có lỗi xảy ra. Vui lòng thử lại sau."
	}
	w.WriteHeader(st)
	jErr := json.NewEncoder(w).Encode(e)

	if jErr != nil {
		w.Write([]byte(fallback))
	}
}

func authenError(w http.ResponseWriter, r *http.Request) {
	customHTTPError(w, r, status.Error(codes.PermissionDenied, "Thông tin xác thực không hợp lệ."))
}
