package main

import (
	"context"

	migratecentralpolicy "git.kafefin.net/backend/bill-insurance/cmd/tasks/migrate_central_policy"
	"git.kafefin.net/backend/bill-insurance/config"
	"git.kafefin.net/backend/bill-insurance/internal/external"
	"git.kafefin.net/backend/bill-insurance/internal/must"
	"git.kafefin.net/backend/kitchen/l"

	"github.com/spf13/cobra"
)

var (
	migrateCmd = &cobra.Command{
		Use:   "migrate-central",
		Short: "task migrate data to central policy",
		RunE:  migrateToCentralFunc,
	}
)

func migrateToCentralFunc(cmd *cobra.Command, args []string) error {
	task := NewMigrateToCentralTask()
	return task.Run(context.TODO())
}

type MigrateToCentralTask struct {
}

func NewMigrateToCentralTask() *MigrateToCentralTask {
	return &MigrateToCentralTask{}
}

func (t *MigrateToCentralTask) Run(ctx context.Context) error {
	ll.Info("Run Migrate To Central Task")
	var cfg = config.Load()
	var ll = l.New()

	thisTaskConfig := migratecentralpolicy.NewMigrateCentralPolicyConfig(cfg.Postgres, cfg.CentralPolicy)

	db := must.ConnectPostgres(thisTaskConfig.PostgreSQL)
	centralPolicy := external.NewCentralPolicyClient(thisTaskConfig.CentralPolicy, true)

	service := migratecentralpolicy.NewMigrateCentralPolicyService(
		ctx,
		thisTaskConfig,
		ll,
		db,
		centralPolicy,
	)

	err := service.MicgrateDataToCentralPolicy(ctx)

	return err
}
