package migratecentralpolicy

import (
	"context"

	"git.kafefin.net/backend/bill-insurance/internal/external"
	"git.kafefin.net/backend/kitchen/l"
	"gorm.io/gorm"
)

type ServiceMigrateCentralPolicyAction interface {
	MicgrateDateToCentralPolicy(ctx context.Context) error
}

type serviceMigrateCentralPolicy struct {
	log              l.Logger
	config           *MigrateCentralPolicyConfig
	sldCentralPolicy external.CentralPolicyClient
	db               *gorm.DB
}

func NewMigrateCentralPolicyService(
	ctx context.Context,
	cfg *MigrateCentralPolicyConfig,
	log l.Logger,
	db *gorm.DB,
	sldCentralPolicy external.CentralPolicyClient,
) *serviceMigrateCentralPolicy {
	return &serviceMigrateCentralPolicy{
		log:              log,
		config:           cfg,
		sldCentralPolicy: sldCentralPolicy,
		db:               db,
	}
}
