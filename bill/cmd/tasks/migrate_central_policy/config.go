package migratecentralpolicy

import "git.kafefin.net/backend/kitchen/config"

type MigrateCentralPolicyConfig struct {
	PostgreSQL    *config.Postgres
	CentralPolicy string
}

func NewMigrateCentralPolicyConfig(postgresql *config.Postgres, centralPolicy string) *MigrateCentralPolicyConfig {
	return &MigrateCentralPolicyConfig{
		CentralPolicy: centralPolicy,
		PostgreSQL:    postgresql,
	}
}
