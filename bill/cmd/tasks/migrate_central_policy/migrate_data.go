package migratecentralpolicy

import (
	"context"

	"git.kafefin.net/backend/bill-insurance/internal/models"
	"git.kafefin.net/backend/bill-insurance/internal/pkg/convert"
	"git.kafefin.net/backend/kitchen/l"
	"gorm.io/gorm"
)

func (s *serviceMigrateCentralPolicy) MicgrateDataToCentralPolicy(ctx context.Context) error {
	var results []*models.BpPolicy
	result := s.db.FindInBatches(&results, 50, func(tx *gorm.DB, batch int) error {
		_, err := s.sldCentralPolicy.UpsertPolicies(ctx, convert.ConvertBpPoliciesToCentralPolicies(results))
		return err
	})

	if result.Error != nil {
		s.log.Error("Error  MicgrateDateToCentralPolicy", l.Error(result.Error))
	}
	return result.Error
}
