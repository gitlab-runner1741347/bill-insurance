# generate by sqlize

DROP TABLE IF EXISTS `policy`;

DROP TABLE IF EXISTS `bill`;

DROP TABLE IF EXISTS `claim_remain`;

DROP TABLE IF EXISTS `send_email_log`;

DROP TABLE IF EXISTS `policy_changelog`;

DROP TABLE IF EXISTS `policy_changelog_attribute`;

TRUNCATE schema_migrations;