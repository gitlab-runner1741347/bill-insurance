CREATE TABLE policy_changelog (
  id serial PRIMARY KEY,
  policy_id varchar(22) NOT NULL,
  policy_number varchar(50),
  action varchar(128),
  actor varchar(128),
  actor_name varchar(128),
  actor_type integer,
  key_path varchar(255),
  comment varchar(255), 
  created_at timestamp DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamp DEFAULT CURRENT_TIMESTAMP
);
 
 
 
 
CREATE TABLE policy_changelog_attribute (
  id serial PRIMARY KEY,
  changelog_id bigint, 
  key_path varchar(255),
  is_equal boolean,
  old_value jsonb,
  new_value jsonb,
  patch jsonb,
  created_at timestamp DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamp DEFAULT CURRENT_TIMESTAMP
);

-- Create an index for policy_id in policy_changelog
CREATE INDEX policy_changelog_policy_id_index ON policy_changelog (policy_id);

-- Create an index for changelog_id in policy_changelog_attribute
CREATE INDEX policy_changelog_attribute_changelog_id_index ON policy_changelog_attribute (changelog_id);
