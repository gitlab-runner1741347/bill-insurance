# generate by sqlize

CREATE TABLE `policy` (
 `id`                                    varchar(128) PRIMARY KEY,
 `order_id`                              varchar(128) COMMENT 'order id',
 `policy_number`                         varchar(128) COMMENT 'policy number',
 `total_bill_amount`                     int(11) COMMENT 'total bill amount',
 `total_payment_amount`                  int(11) COMMENT 'total payment amount',
 `total_loss_or_death_coverage_value`    int(11) COMMENT 'total loss or death coverage value',
 `total_whole_accident_coverage_value`   int(11) COMMENT 'total whole accident coverage value',
 `total_partial_accident_coverage_value` int(11) COMMENT 'total partial accident coverage value',
 `total_medical_coverage_value`          int(11) COMMENT 'total medical coverage value',
 `daily_in_hospital_coverage_value`      int(11) COMMENT 'daily in hospital coverage value',
 `max_days_of_in_hospital`               int(11) COMMENT 'max days of in hospital',
 `billing_month`                         int(11) COMMENT 'billing month',
 `billing_year`                          int(11) COMMENT 'billing year',
 `created_time_number`                   bigint(20) COMMENT 'created time number',
 `customer_address`                      varchar(128) COMMENT 'customer address',
 `customer_dob`                          varchar(128) COMMENT 'customer dob',
 `customer_email`                        varchar(128) COMMENT 'customer email',
 `customer_id`                           varchar(128) COMMENT 'customer id',
 `customer_name`                         varchar(128) COMMENT 'customer name',
 `customer_phone`                        varchar(128) COMMENT 'customer phone',
 `e_cert_url`                            varchar(128) COMMENT 'e cert url',
 `effective_from`                        datetime COMMENT 'effective from',
 `effective_to`                          datetime COMMENT 'effective to',
 `effective_time_number`                 bigint(20) COMMENT 'effective time number',
 `issued_date`                           datetime COMMENT 'issued date',
 `partner_org`                           varchar(128) COMMENT 'partner org',
 `payment_date`                          datetime COMMENT 'payment date',
 `policy_status`                         varchar(128) COMMENT 'policy status',
 `total_premium_amount`                  int(11) COMMENT 'total premium amount',
 `insurer`                               varchar(128) COMMENT 'insurer',
 `created_at`                            datetime DEFAULT CURRENT_TIMESTAMP(),
 `updated_at`                            datetime DEFAULT CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP()
);
CREATE INDEX `idx_order_id` ON `policy`(`order_id`);
CREATE INDEX `idx_policy_number` ON `policy`(`policy_number`);

CREATE TABLE `bill` (
 `id`                                    bigint(20) AUTO_INCREMENT PRIMARY KEY,
 `bill_id`                               varchar(128) COMMENT 'bill id',
 `bill_type`                             varchar(128) COMMENT 'bill type',
 `bill_amount`                           int(11) COMMENT 'bill amount',
 `payment_amount`                        int(11) COMMENT 'payment amount',
 `total_loss_or_death_coverage_value`    int(11) COMMENT 'total loss or death coverage value',
 `total_whole_accident_coverage_value`   int(11) COMMENT 'total whole accident coverage value',
 `total_partial_accident_coverage_value` int(11) COMMENT 'total partial accident coverage value',
 `total_medical_coverage_value`          int(11) COMMENT 'total medical coverage value',
 `daily_in_hospital_coverage_value`      int(11) COMMENT 'daily in hospital coverage value',
 `max_days_of_in_hospital`               int(11) COMMENT 'max days of in hospital',
 `premium_amount`                        int(11) COMMENT 'premium amount',
 `policy_number`                         varchar(128) COMMENT 'policy number',
 `policy_id`                             varchar(128) COMMENT 'policy id'
);
CREATE INDEX `idx_bill_id` ON `bill`(`bill_id`);
CREATE INDEX `idx_policy_id` ON `bill`(`policy_id`);

CREATE TABLE `claim_remain` (
 `policy_id`                            varchar(128) PRIMARY KEY COMMENT 'policy id',
 `updated_by`                           text COMMENT 'updated by',
 `order_id`                             text COMMENT 'order id',
 `created_by`                           text COMMENT 'created by',
 `claim_remain_loss_or_death_value`     bigint(20) COMMENT 'claim remain loss or death value',
 `claim_remain_whole_accident_value`    bigint(20) COMMENT 'claim remain whole accident value',
 `claim_remain_partial_accident_value`  bigint(20) COMMENT 'claim remain partial accident value',
 `claim_remain_medical_value`           bigint(20) COMMENT 'claim remain medical value',
 `claim_remain_daily_in_hospital_value` bigint(20) COMMENT 'claim remain daily in hospital value',
 `claim_remain_days_of_in_hospital`     bigint(20) COMMENT 'claim remain days of in hospital',
 `created_at`                           datetime DEFAULT CURRENT_TIMESTAMP(),
 `updated_at`                           datetime DEFAULT CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP()
);

CREATE TABLE `send_email_log` (
 `id`             bigint(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
 `policy_number`  varchar(128) COMMENT 'policy number',
 `idempotent_key` varchar(128) COMMENT 'idempotent key',
 `email_params`   json COMMENT 'email params',
 `status`         int(11) COMMENT 'status',
 `created_at`     datetime DEFAULT CURRENT_TIMESTAMP(),
 `updated_at`     datetime DEFAULT CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP()
);
CREATE INDEX `idx_policy_number` ON `send_email_log`(`policy_number`);
CREATE UNIQUE INDEX `idx_idempotent_key` ON `send_email_log`(`idempotent_key`);

CREATE TABLE `policy_changelog` (
 `id`            bigint(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
 `policy_id`     varchar(22) NOT NULL COMMENT 'policy id',
 `policy_number` varchar(50) COMMENT 'policy number',
 `action`        varchar(128) COMMENT 'action',
 `actor`         varchar(128) COMMENT 'actor',
 `actor_name`    varchar(128) COMMENT 'actor name',
 `actor_type`    int(11) COMMENT 'actor type',
 `key_path`      varchar(255) COMMENT 'key path',
 `comment`       text COMMENT 'comment',
 `created_at`    datetime DEFAULT CURRENT_TIMESTAMP(),
 `updated_at`    datetime DEFAULT CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP()
);
CREATE INDEX `idx_policy_id` ON `policy_changelog`(`policy_id`);

CREATE TABLE `policy_changelog_attribute` (
 `id`           bigint(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
 `changelog_id` bigint(20) UNSIGNED COMMENT 'changelog id',
 `key_path`     varchar(255) COMMENT 'key path',
 `is_equal`     tinyint(1) COMMENT 'is equal',
 `old_value`    json COMMENT 'old value',
 `new_value`    json COMMENT 'new value',
 `patch`        json COMMENT 'patch',
 `created_at`   datetime DEFAULT CURRENT_TIMESTAMP(),
 `updated_at`   datetime DEFAULT CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP()
);
CREATE INDEX `idx_changelog_id` ON `policy_changelog_attribute`(`changelog_id`);

TRUNCATE schema_migrations;
INSERT INTO schema_migrations (version, dirty) VALUES (1688476268882152576, false);