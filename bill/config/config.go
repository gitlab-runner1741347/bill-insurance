// YOU CAN EDIT YOUR CUSTOM CONFIG HERE

package config

import (
	"bytes"
	_ "embed"
	"fmt"
	"strings"

	"git.kafefin.net/backend/kitchen/config"
	"git.kafefin.net/backend/kitchen/l"
	"git.kafefin.net/backend/salad-kit/server"
	"github.com/spf13/viper"
)

var (
	ll = l.New()
)

// Config holds all settings
//
//go:embed config.yaml
var defaultConfig []byte

type Config struct {
	Base                         `mapstructure:",squash"`
	MySQL                        *config.MySQL    `yaml:"mysql" mapstructure:"mysql"`
	Postgres                     *config.Postgres `yaml:"postgres" mapstructure:"postgres"`
	Redis                        *config.Redis    `yaml:"redis" mapstructure:"redis"`
	Queue                        *Queue           `yaml:"queue" mapstructure:"queue"`
	PrintCertAddress             string           `yaml:"print_cert_address" mapstructure:"print_cert_address"`
	SaladinMessageAddress        string           `yaml:"saladin_message_address" mapstructure:"saladin_message_address"`
	SaladinStaffAddress          string           `yaml:"saladin_staff_address" mapstructure:"saladin_staff_address"`
	SaladinUserAddress           string           `yaml:"saladin_user_address" mapstructure:"saladin_user_address"`
	CentralPolicy                string           `yaml:"central_policy_address" mapstructure:"central_policy_address"`
	SaladinBillProtectionAddress string           `yaml:"saladin_bill_protection_address" mapstructure:"saladin_bill_protection_address"`
	ClaimHubAddress              string           `yaml:"claim_hub_address" mapstructure:"claim_hub_address"`
	ClaimURL                     string           `yaml:"claim_url" mapstructure:"claim_url"`
	FeatureFlag                  *FeatureFlag     `yaml:"feature_flag" mapstructure:"feature_flag"`
	BP                           *BP              `yaml:"bp" mapstructure:"bp"`
}

type Base struct {
	Server      ServerConfig `yaml:"server" mapstructure:"server"`
	Environment string
}

// Queue ...
type Queue struct {
	Address  string `yaml:"address" mapstructure:"address"`
	Username string `yaml:"username" mapstructure:"username"`
	Password string `yaml:"password" mapstructure:"password"`
}

func (q Queue) Uri() string {
	return fmt.Sprintf("amqp://%s:%s@%s", q.Username, q.Password, q.Address)
}

// ServerConfig hold http/grpc server config
type ServerConfig struct {
	GRPC server.Listen `json:"grpc" mapstructure:"grpc" yaml:"grpc"`
	HTTP server.Listen `json:"http" mapstructure:"http" yaml:"http"`
}

func Load() *Config {
	var cfg = &Config{}

	viper.SetConfigType("yaml")
	err := viper.ReadConfig(bytes.NewBuffer(defaultConfig))
	if err != nil {
		ll.Fatal("Failed to read viper config", l.Error(err))
	}

	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "__"))
	viper.AutomaticEnv()

	err = viper.Unmarshal(&cfg)
	if err != nil {
		ll.Fatal("Failed to unmarshal config", l.Error(err))
	}

	ll.Info("Config loaded", l.Object("config", cfg))
	return cfg
}

type FeatureFlag struct {
	EcertV2 bool `yamnl:"ecert_v2" mapstructure:"ecert_v2"`
}

type BP struct {
	InsuranceLookup        string `yaml:"insurance_lookup_url" mapstructure:"insurance_lookup_url"`
	PoliciesClaim          string `yaml:"get_policies_by_accident_time_url" mapstructure:"get_policies_by_accident_time_url"`
	ClaimRemain            string `yaml:"get_claim_remain_url" mapstructure:"get_claim_remain_url"`
	ClaimDetail            string `yaml:"claim_url" mapstructure:"claim_url"`
	PolicyKey              string `yaml:"policy_key" mapstructure:"policy_key"`
	UpdateClaimRemainQueue string `yaml:"update_claim_remain_queue" mapstructure:"update_claim_remain_queue"`
}

func (c Config) GetDetailClaimURL(phone, policyNumber string) string {
	return fmt.Sprintf("%sthongtin?phone=%s&policy_number=%s", c.ClaimURL, phone, policyNumber)
}
