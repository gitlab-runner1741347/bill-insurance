package config

type SaladinVendor struct {
	VendorID     uint32 `yaml:"vendor_id" mapstructure:"vendor_id"`
	VendorCode   string `yaml:"vendor_code" mapstructure:"vendor_code"`
	VendorSuffix string `yaml:"vendor_suffix" mapstructure:"vendor_suffix"`
	ClientKey    string `yaml:"client_key" mapstructure:"client_key"`
	ClientSecret string `yaml:"client_secret" mapstructure:"client_secret"`
}
