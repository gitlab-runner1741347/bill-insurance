package services

import (
	"context"
	"errors"

	"git.kafefin.net/backend/bill-insurance/internal/models"
	"git.kafefin.net/backend/bill-insurance/internal/pkg/convert"
	err2 "git.kafefin.net/backend/bill-insurance/internal/pkg/err"
	"git.kafefin.net/backend/bill-insurance/pb"
	"git.kafefin.net/backend/kitchen/l"
	"git.kafefin.net/backend/kitchen/utils"

	clamHubPb "git.kafefin.net/backend/saladin-central-proto/claim_hub"
	"google.golang.org/grpc/codes"
)

func (s *service) InternalGetPolicy(ctx context.Context, req *pb.InternalGetPolicyRequest) (*pb.InternalGetPolicyResponse, error) {
	policy, err := s.repos.GetPolicyByID(ctx, req.GetPolicyId())
	if err != nil {
		return nil, err
	}

	return &pb.InternalGetPolicyResponse{
		Policy: convert.PolicyModelToPb(policy),
	}, nil
}

func (s *service) handlePolicyEcert(ctx context.Context, idemtKey string) error {
	sendEmailLog, err := s.repos.GetByIdempotentKey(ctx, idemtKey)

	if err != nil {
		s.log.Error("HandlePolicyECert err", l.Error(err))
		return err
	}

	if sendEmailLog.Status == pb.SendEmailLogStatus_EMAIL_LOG_SENT {
		return nil
	}

	emailArgs := sendEmailLog.EmailParams.Data.V1

	if emailArgs == nil {
		return errors.New("invalid data")
	}

	if _, err := s.sldMessageClient.SendEmailTemplate(ctx, sendEmailLog.EmailParams.Data.V1); err != nil {
		return err
	}

	if _, err := s.repos.Save(ctx, sendEmailLog); err != nil {
		s.log.Error("SendEmailLogRepo.Save err", l.Error(err))
		return err
	}

	return nil
}

func (s *service) InternalGenerateEcertCallback(ctx context.Context, req *pb.InternalGenerateEcertCallbackRequest) (*pb.InternalGenerateEcertCallbackResponse, error) {
	if err := s.handlePolicyEcert(context.TODO(), req.GetIdempotentKey()); err != nil {
		s.log.Error("InternalGenerateEcertCallback err", l.Error(err))
	}

	return &pb.InternalGenerateEcertCallbackResponse{
		Message: codes.OK.String(),
	}, nil
}

func (s *service) InternalClaimRemain(ctx context.Context, req *pb.InternalClaimRemainRequest) (*pb.InternalClaimRemainResponse, error) {
	claimRemain, err := s.repos.GetClaimRemainByPolicyId(ctx, req.GetPolicyId())
	if err != nil {
		if err != err2.RecordNotFound {
			s.log.Error("Get data error")
			return &pb.InternalClaimRemainResponse{
				Code:    1,
				Message: "Error in database",
			}, err
		}
		s.log.Error("Record not found")
		return &pb.InternalClaimRemainResponse{
			Code:    1,
			Message: "Record not found",
		}, err
	}

	// set
	claimRemain.ClaimRemainLossOrDeathValue = req.GetLossOrDeathValue()
	claimRemain.ClaimRemainWholeAccidentValue = req.GetWholeAccidentValue()
	claimRemain.ClaimRemainPartialAccidentValue = req.GetPartialAccidentValue()
	claimRemain.ClaimRemainMedicalValue = req.GetMedicalValue()
	claimRemain.ClaimRemainDailyInHospitalValue = req.GetDailyInHospitalValue()
	claimRemain.ClaimRemainDaysOfInHospital = req.GetDaysOfInHospital()

	s.repos.SaveClaimRemain(ctx, claimRemain)

	return &pb.InternalClaimRemainResponse{
		Code:    0,
		Message: "Update successly",
	}, err
}

func (s *service) SyncPolicyWhenUpdatePolicy(ctx context.Context, policy *models.BpPolicy) (*pb.InternalExistPoliciesUserRequirementResponse, error) {
	record := []*models.BpPolicy{policy}
	_, err := s.sldCentralPolicy.UpsertPolicies(ctx, convert.ConvertBpPoliciesToCentralPolicies(record))
	if err != nil {
		s.log.Warn("error when UpsertPolicies", l.Error(err))
		return nil, err
	}

	return &pb.InternalExistPoliciesUserRequirementResponse{
		Code:    0,
		Message: "success",
	}, nil
}

func (s *service) InternalExistPoliciesUserRequirement(ctx context.Context, req *pb.InternalExistPoliciesUserRequirementRequest) (*pb.InternalExistPoliciesUserRequirementResponse, error) {
	if err := s.postgresqlRepos.UpdateUserIDByPhone(ctx, req.GetPhone(), req.GetUserId()); err != nil {
		s.log.Error("InternalExistPoliciesUserRequirement.UpdateUserIDLater. err", l.Error(err))
		return nil, err
	}

	record, err := s.postgresqlRepos.ListByUserId(ctx, req.GetUserId())
	if err != nil {
		s.log.Error("ListByUserId.err", l.Error(err))
		return nil, err
	}

	_, err = s.sldCentralPolicy.UpsertPolicies(ctx, convert.ConvertBpPoliciesToCentralPolicies(record))
	if err != nil {
		s.log.Warn("error when UpsertPolicies", l.Error(err))
		return nil, err
	}

	return &pb.InternalExistPoliciesUserRequirementResponse{
		Code:    0,
		Message: "success",
	}, nil
}

func (s *service) InternalGetClaimRemains(ctx context.Context, ids []string) (*clamHubPb.InternalClaimRemainsResponse, error) {
	return s.sldClaimHub.InternalClaimRemains(ctx, &clamHubPb.InternalClaimRemainsRequest{
		PolicyIds: ids,
	})
}

func (s *service) InternalGetPolices(ctx context.Context, req *pb.InternalGetPoliciesReqeust) (*pb.InternalGetPolicesResponse, error) {
	policies, err := s.postgresqlRepos.GetPoliciesByIds(ctx, req.GetIds())
	if err != nil {
		s.log.Error("InternalGetPolices.GetPoliciesByIds", l.Error(err))
		return nil, err
	}

	return &pb.InternalGetPolicesResponse{
		Code:    0,
		Message: "success",
		Data:    convert.ConvertPolicyToBpPolicies(policies),
	}, nil

}

func (s *service) InternalListPolicies(ctx context.Context, req *pb.InternalListPoliciesRequest) (*pb.InternalListPoliciesResponse, error) {
	req.CustomerPhone = utils.FormatPhoneNumber(req.GetCustomerPhone())
	policies, err := s.postgresqlRepos.InternalListPolicies(ctx, req)

	if err != nil {
		s.log.Error("InternalListPolicies.InternalPaginatePolicies", l.Error(err))
		return nil, err
	}

	return &pb.InternalListPoliciesResponse{
		Code:    0,
		Message: "success",
		Data: &pb.InternalListPoliciesResponse_Data{
			Policies: convert.ConvertPolicyToBpPolicies(policies),
		},
	}, nil

}
