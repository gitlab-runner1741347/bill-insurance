package services

import (
	"context"
	"errors"

	"git.kafefin.net/backend/bill-insurance/internal/models"
	"git.kafefin.net/backend/bill-insurance/internal/pkg/convert"
	"git.kafefin.net/backend/bill-insurance/internal/repositories"
	"git.kafefin.net/backend/bill-insurance/pb"
	"git.kafefin.net/backend/salad-kit/grpckit"
	"github.com/hashicorp/go-multierror"
	"github.com/r3labs/diff/v3"

	"git.kafefin.net/backend/kitchen/l"
	clamHubPb "git.kafefin.net/backend/saladin-central-proto/claim_hub"
)

var diffJSON, _ = diff.NewDiffer(diff.TagName("json"))

func billPolicyUpdateChangelog(
	ctx context.Context,
	staffEmail string,
	staffName string,
	policyID string,
	r repositories.PostgresqlRepository,
	diffChangelog diff.Changelog,
	changelogKeyPath string,
	action models.ActionType,
	comment string,
) error {
	if len(diffChangelog) == 0 {
		return nil
	}

	changelogModel := models.PolicyChangelog{
		PolicyID:  policyID,
		Action:    action,
		Actor:     staffEmail,
		ActorName: staffName,
		KeyPath:   changelogKeyPath,
		Comment:   comment,
	}
	if err := r.CreateOneChangeLog(ctx, &changelogModel); err != nil {
		return multierror.Prefix(err, "tripPolicyUpdateInfoChangelog PolicyChangelogRepo.Create error")
	}

	if _, err := r.BatchCreateChangeLogAtts(ctx, convert.ChangelogToAttributes(changelogModel.ID, diffChangelog)); err != nil {
		return multierror.Prefix(err, "tripPolicyUpdateInfoChangelog PolicyChangelogAttributeRepo.BatchCreate error")
	}

	return nil
}

func (s *service) AdminUpdatePolicy(ctx context.Context, req *pb.AdminUpdatePolicyRequest) (*pb.AdminUpdatePolicyResponse, error) {
	return s.commonAdminUpdatePolicy(ctx, req)
}

func (s *service) AdminPartnerUpdatePolicy(ctx context.Context, req *pb.AdminUpdatePolicyRequest) (*pb.AdminUpdatePolicyResponse, error) {
	return s.commonAdminUpdatePolicy(ctx, req)
}

func (s *service) validateUpdatePolicy(ctx context.Context, policy *models.BpPolicy) bool {
	if policy.PolicyStatus != "CREATED_OK" {
		return false
	}

	response, err := s.sldClaimHub.InternalGetStatusClaims(ctx, &clamHubPb.InternalGetStatusClaimsRequest{
		PolicyId: policy.PolicyID,
	})

	if err != nil {
		s.log.Error("ValidateUpdatePolicy.InternalGetStatusClaims", l.Error(err))
		return false
	}

	claims := response.Data

	for _, claim := range claims {
		if claim.Status != clamHubPb.ClaimStatus_REJECTED {
			return false
		}
	}

	return true

}

func (s *service) commonAdminUpdatePolicy(ctx context.Context, req *pb.AdminUpdatePolicyRequest) (*pb.AdminUpdatePolicyResponse, error) {

	if req.GetCustomerDob() == "" {
		return nil, errors.New("please fill customer_dob")
	}

	staff := grpckit.GetStaffPolicyFromContext(ctx).(grpckit.StaffPolicy)

	id := req.GetPolicyId()
	oldPolicy, err := s.postgresqlRepos.GetPolicyByID(ctx, id)
	if err != nil {
		s.log.Error("AdminUpdatePolicy.GetPolicyByID", l.Error(err))
		return nil, errors.New("can not update policy")
	}

	if ok := s.validateUpdatePolicy(ctx, oldPolicy); !ok {
		return nil, errors.New("can not update policy")
	}

	newPolicy := *oldPolicy

	newPolicy.CustomerName = req.GetCustomerName()
	newPolicy.CustomerDOB = req.GetCustomerDob()
	newPolicy.CustomerEmail = req.GetCustomerEmail()
	newPolicy.CustomerID = req.GetCustomerId()
	newPolicy.CustomerAddress = req.GetCustomerAddress()

	policyChangelog, err := diffJSON.Diff(
		convert.ConvertBpPolicyToPolicyToCompateProto(oldPolicy),
		convert.ConvertBpPolicyToPolicyToCompateProto(&newPolicy),
	)

	if len(policyChangelog) == 0 {
		return nil, errors.New("data is not changed")
	}

	if err = s.postgresqlRepos.Transaction(func(r repositories.PostgresqlRepository) error {

		_, err = s.postgresqlRepos.UpdatePolicy(ctx, newPolicy)
		if err != nil {
			s.log.Error("AdminUpdatePolicy.UpdatePolicy", l.Error(err))
			return errors.New("can not update policy")
		}

		if err != nil {
			return multierror.Prefix(err, "diffJSON.Diff policy")
		}

		if err := s.postgresqlRepos.UpdateUpdateVersion(ctx, newPolicy.PolicyID); err != nil {
			return err
		}

		if err := billPolicyUpdateChangelog(ctx, staff.GetEmail(), staff.GetName(), newPolicy.PolicyID, s.postgresqlRepos, policyChangelog, "policy", models.ActionUpdateInfo, req.GetComment()); err != nil {
			return multierror.Prefix(err, "billPolicyUpdateChangelog policy")
		}

		return nil
	}); err != nil {
		return nil, err
	}

	if err := s.sldBillProtection.ReGenerateEcert(ctx, newPolicy.OrderID); err != nil {
		s.log.Error("sldBillProtection.ReGenerateEcert", l.Error(err))
		return nil, err
	}

	newCtx := context.TODO()
	go s.SyncPolicyWhenUpdatePolicy(newCtx, &newPolicy)

	return &pb.AdminUpdatePolicyResponse{
		Code:    0,
		Message: "success",
	}, nil

}

func (s *service) commonAdminGetChangeLog(ctx context.Context, req *pb.AdminGetChangeLogRequest) (*pb.AdminGetChangeLogResponse, error) {
	id := req.GetPolicyId()
	changeLogs, err := s.postgresqlRepos.ListChangeLogByPolicyID(ctx, id)
	if err != nil {
		s.log.Error("AdminGetChangeLog.GetChangeLogByPolicyID", l.Error(err))
		return nil, err
	}

	pbChangLogs := convert.ModelsToChangelogPb(changeLogs)
	return &pb.AdminGetChangeLogResponse{
		Changelogs: pbChangLogs,
	}, nil

}

func (s *service) AdminGetChangeLog(ctx context.Context, req *pb.AdminGetChangeLogRequest) (*pb.AdminGetChangeLogResponse, error) {
	return s.commonAdminGetChangeLog(ctx, req)
}

func (s *service) AdminPartnerGetChangeLog(ctx context.Context, req *pb.AdminGetChangeLogRequest) (*pb.AdminGetChangeLogResponse, error) {
	return s.commonAdminGetChangeLog(ctx, req)
}

func (s *service) commonAdminGetDetailPolicyByPolicyId(ctx context.Context, req *pb.AdminGetDetailPolicyByPolicyIdRequest) (*pb.AdminGetDetailPolicyByPolicyIdResponse, error) {
	policy, err := s.postgresqlRepos.GetByPolicyIdAndOrg(ctx, req.GetPolicyId(), req.GetOrg())
	if err != nil {
		s.log.Error("commonAdminGetDetailPolicyByPolicyId.GetByPolicyIdAndOrg", l.Error(err))
		return nil, err
	}

	changeLogs, err := s.postgresqlRepos.ListChangeLogByPolicyID(ctx, req.GetPolicyId())
	if err != nil {
		s.log.Error("GetByPolicyIdAndOrg.ListChangeLogByPolicyID", l.Error(err))
		return nil, err
	}

	policyPb := convert.ConvertPolicyToBpPolicy(policy)

	pbChangLogs := convert.ModelsToChangelogPb(changeLogs)

	remain, err := s.postgresqlRepos.GetByOrderId(ctx, policy.OrderID)
	if err != nil {
		s.log.Error("UserGetPolicyDetail.GetByOrderId", l.Error(err))
		return nil, err
	}

	pbRemains := pb.BpClaimRemain{
		CurrentDailyInHospitalValue: int64(remain.ClaimRemainDailyInHospitalValue),
		CurrentDaysOfInHospital:     int64(remain.ClaimRemainDaysOfInHospital),
		CurrentLossOrDeathValue:     int64(remain.ClaimRemainLossOrDeathValue),
		CurrentMedicalValue:         int64(remain.ClaimRemainMedicalValue),
		CurrentPartialAccidentValue: int64(remain.ClaimRemainPartialAccidentValue),
		CurrentWholeAccidentValue:   int64(remain.ClaimRemainWholeAccidentValue),
	}

	return &pb.AdminGetDetailPolicyByPolicyIdResponse{
		Code:    0,
		Message: "success",
		Data: &pb.AdminGetDetailPolicyByPolicyIdResponse_Data{
			Policy:     policyPb,
			ChangeLogs: pbChangLogs,
			Remains:    &pbRemains,
		},
	}, nil
}

func (s *service) AdminGetDetailPolicyByPolicyId(ctx context.Context, req *pb.AdminGetDetailPolicyByPolicyIdRequest) (*pb.AdminGetDetailPolicyByPolicyIdResponse, error) {
	return s.commonAdminGetDetailPolicyByPolicyId(ctx, req)
}

func (s *service) AdminGetDetailPolicyByPolicyIdWithOrg(ctx context.Context, req *pb.AdminGetDetailPolicyByPolicyIdRequest) (*pb.AdminGetDetailPolicyByPolicyIdResponse, error) {
	return s.commonAdminGetDetailPolicyByPolicyId(ctx, req)
}

func (s *service) commonAdminListPolicies(ctx context.Context, req *pb.AdminListPoliciesRequest) (*pb.AdminListPoliciesResponse, error) {
	convert.CustomizeTimeToComparedExactly(req)
	policies, total, err := s.postgresqlRepos.PaginatePoliciesWithOrg(ctx, req)
	if err != nil {
		s.log.Error("commonAdminListPolicies.postgresqlRepos.PaginatePoliciesWithOrg", l.Error(err))
		return nil, err
	}

	pbPolicies := convert.ConvertPoliciesToSummaryBpPolicies(policies)

	return &pb.AdminListPoliciesResponse{
		Code:    0,
		Message: "success",
		Data: &pb.AdminListPoliciesResponse_Data{
			Paging: &pb.BillInsurancePaging{
				Total:         total,
				CurrentOffset: req.GetOffset(),
			},
			Policies: pbPolicies,
		},
	}, nil

}

func (s *service) AdminListPolicies(ctx context.Context, req *pb.AdminListPoliciesRequest) (*pb.AdminListPoliciesResponse, error) {
	return s.commonAdminListPolicies(ctx, req)
}

func (s *service) AdminListPoliciesWithOrg(ctx context.Context, req *pb.AdminListPoliciesRequest) (*pb.AdminListPoliciesResponse, error) {
	return s.commonAdminListPolicies(ctx, req)
}

func (s *service) GetPolicyEffective(ctx context.Context, req *pb.GetPolicyEffectiveRequest) (*pb.GetPolicyEffectiveResponse, error) {
	return nil, nil
}
