package services

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	_const "git.kafefin.net/backend/bill-insurance/internal/const"
	"git.kafefin.net/backend/bill-insurance/internal/models"
	"git.kafefin.net/backend/bill-insurance/internal/pkg/convert"
	"git.kafefin.net/backend/bill-insurance/internal/pkg/helper"
	models_ "git.kafefin.net/backend/bill-insurance/internal/pkg/helper/models"
	"git.kafefin.net/backend/bill-insurance/pb"
	"git.kafefin.net/backend/kitchen/l"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"time"
)

func (s *service) CalculatePremium(ctx context.Context, req *pb.CalculatePremiumRequest) (*pb.CalculatePremiumResponse, error) {
	bills := req.Bills
	arrayBill := make([]*pb.CalculatePremiumResponse_Data, 0)
	for _, value := range bills {
		billType := convert.MappingBillType(value.BillType.String())
		valueCalculator := helper.Calculate(_const.BillType(billType), value.BillAmount)
		jsonConvert, err := json.Marshal(valueCalculator)
		if err != nil {
			s.log.Error("Can not marshal to convert struct", l.Error(err))
			return nil, fmt.Errorf("Can not marshal to convert struct")
		}
		var dataResponse pb.CalculatePremiumResponse_Data
		if err = json.Unmarshal(jsonConvert, &dataResponse); err != nil {
			s.log.Error("Can not unmarshal to convert struct")
			err = errors.New("Can not unmashal to convert struct")
			return nil, err
		}
		arrayBill = append(arrayBill, &dataResponse)
	}
	return &pb.CalculatePremiumResponse{
		Data:    arrayBill,
		Message: "Calculate Successfully",
		Code:    0,
	}, nil
}

func (s *service) SubmitPolicy(ctx context.Context, req *pb.SubmitPolicyRequest) (*pb.SubmitPolicyResponse, error) {

	policyNumber := s.pnGenerator.GenNewPolicyNumber(ctx, "BILL_PRO1_E")
	if policyNumber == "" {
		return nil, status.Error(codes.Unknown, "Invalid generate policy number.")
	}
	policyId := s.biIdGenerator.Gen()

	billsRequest := req.Bills
	arrayBill := make([]models.Bill, 0)
	totalValueCalculator := models_.CalculatePremiumDTO{}
	for _, value := range billsRequest {
		billType := convert.MappingBillType(value.BillType.String())
		valueCalculator := helper.Calculate(_const.BillType(billType), value.BillAmount)
		bill := models.Bill{
			BillID:                            value.BillId,
			BillType:                          billType,
			BillAmount:                        int(value.BillAmount),
			PaymentAmount:                     int(value.PaymentAmount),
			TotalLossOrDeathCoverageValue:     int(valueCalculator.TotalLossOrDeathCoverageValue),
			TotalWholeAccidentCoverageValue:   int(valueCalculator.TotalWholeAccidentCoverageValue),
			TotalPartialAccidentCoverageValue: int(valueCalculator.TotalPartialAccidentCoverageValue),
			TotalMedicalCoverageValue:         int(valueCalculator.TotalMedicalCoverageValue),
			DailyInHospitalCoverageValue:      int(valueCalculator.DailyInHospitalCoverageValue),
			MaxDaysOfInHospital:               int(valueCalculator.MaxDaysOfInHospital),
			PremiumAmount:                     int(valueCalculator.PremiumAmount),

			// policy id, policy number
			PolicyID:     policyId,
			PolicyNumber: policyNumber,
		}
		totalValueCalculator = models_.CalculatePremiumDTO{
			PremiumAmount:                     totalValueCalculator.PremiumAmount + valueCalculator.PremiumAmount,
			TotalLossOrDeathCoverageValue:     totalValueCalculator.TotalLossOrDeathCoverageValue + valueCalculator.TotalLossOrDeathCoverageValue,
			TotalWholeAccidentCoverageValue:   totalValueCalculator.TotalWholeAccidentCoverageValue + valueCalculator.TotalWholeAccidentCoverageValue,
			TotalPartialAccidentCoverageValue: totalValueCalculator.TotalPartialAccidentCoverageValue + valueCalculator.TotalPartialAccidentCoverageValue,
			TotalMedicalCoverageValue:         totalValueCalculator.TotalMedicalCoverageValue + valueCalculator.TotalMedicalCoverageValue,
			DailyInHospitalCoverageValue:      totalValueCalculator.DailyInHospitalCoverageValue + valueCalculator.DailyInHospitalCoverageValue,
			MaxDaysOfInHospital:               totalValueCalculator.MaxDaysOfInHospital + valueCalculator.MaxDaysOfInHospital,
		}

		arrayBill = append(arrayBill, bill)
	}

	// new claim remain
	const (
		createdBy = _const.SystemCreated
	)

	newClaimRemain := models.ClaimRemain{
		Model:     models.Model{CreatedAt: time.Now(), UpdatedAt: time.Now()},
		CreatedBy: createdBy,
		UpdatedBy: createdBy,

		OrderId:                         req.GetOrderId(),
		PolicyId:                        policyId,
		ClaimRemainLossOrDeathValue:     totalValueCalculator.TotalLossOrDeathCoverageValue,
		ClaimRemainWholeAccidentValue:   totalValueCalculator.TotalWholeAccidentCoverageValue,
		ClaimRemainPartialAccidentValue: totalValueCalculator.TotalPartialAccidentCoverageValue,
		ClaimRemainMedicalValue:         totalValueCalculator.TotalMedicalCoverageValue,
		ClaimRemainDailyInHospitalValue: totalValueCalculator.DailyInHospitalCoverageValue,
		ClaimRemainDaysOfInHospital:     totalValueCalculator.MaxDaysOfInHospital,
	}

	// new policy
	// define to set for new policy, if you want to change, change here
	const (
		org     = _const.ZaloOrg
		insurer = _const.BaoVietInsurer
	)

	effectiveDate := convert.ConvertProtoTimestamp(req.PaymentDate).AddDate(0, 0, 30)
	newPolicy := models.Policy{
		OrderID: req.GetOrderId(),
		// poliynumber
		PolicyNumber:                      policyNumber,
		Id:                                policyId,
		TotalBillAmount:                   int(req.GetTotalBillAmount()),
		TotalPaymentAmount:                int(req.GetTotalPaymentAmount()),
		TotalLossOrDeathCoverageValue:     int(totalValueCalculator.TotalLossOrDeathCoverageValue),
		TotalWholeAccidentCoverageValue:   int(totalValueCalculator.TotalWholeAccidentCoverageValue),
		TotalPartialAccidentCoverageValue: int(totalValueCalculator.TotalPartialAccidentCoverageValue),
		TotalMedicalCoverageValue:         int(totalValueCalculator.TotalMedicalCoverageValue),
		DailyInHospitalCoverageValue:      int(totalValueCalculator.DailyInHospitalCoverageValue),
		MaxDaysOfInHospital:               int(totalValueCalculator.MaxDaysOfInHospital),
		BillingMonth:                      int(req.BillingMonth),
		BillingYear:                       int(req.BillingYear),
		Model:                             models.Model{CreatedAt: time.Now(), UpdatedAt: time.Now()},
		CreatedTimeNumber:                 time.Now().UnixNano(),
		CustomerAddress:                   req.GetCustomerAddress(),
		CustomerDOB:                       req.GetCustomerDob(),
		CustomerEmail:                     req.GetCustomerEmail(),
		CustomerID:                        req.GetCustomerId(),
		CustomerName:                      req.GetCustomerName(),
		CustomerPhone:                     req.GetCustomerPhone(),
		// ecert url
		EffectiveFrom: *convert.ConvertProtoTimestamp(req.PaymentDate),
		EffectiveTo:   effectiveDate,
		// de sau
		EffectiveTimeNumber: effectiveDate.UnixMilli(),
		IssuedDate:          time.Now(),
		PartnerOrg:          org,
		PaymentDate:         *convert.ConvertProtoTimestamp(req.PaymentDate),
		PolicyStatus:        _const.StatusCreatedOk,
		TotalPremiumAmount:  int(req.TotalPaymentAmount),
		Insurer:             insurer,
		Bills:               arrayBill,
		// ClaimRemain:
		ClaimRemain: newClaimRemain,
	}

	policy, err := s.repos.SavePolicy(ctx, &newPolicy)
	if err != nil {
		s.log.Error("Save Policy have error db")
		return nil, errors.New("Error When create new policy")
	}

	go s.processEcert(policy, s.cfg.ClaimURL, "hahaha")

	fmt.Println(policy)

	return &pb.SubmitPolicyResponse{
		Code:    0,
		Message: "Submit Policy success",
		Data: &pb.SubmitPolicyResponse_Data{
			PolicyId:      policy.Id,
			PremiumAmount: int64(policy.TotalPremiumAmount),
			CreatedAt:     convert.ConvertToProtoTimestamp(policy.CreatedAt),
			ECertUrl:      policy.ECertURL,
			EffectiveFrom: convert.ConvertToProtoTimestamp(policy.EffectiveFrom),
			EffectiveTo:   convert.ConvertToProtoTimestamp(policy.EffectiveTo),
			PolicyNumber:  policy.PolicyNumber,
		},
	}, nil
}

//policy, err := s.repos.SavePolicy(ctx, &newPolicy)

//if err != nil {
//	s.log.Error("Can not save new policy into database", l.Error(err))
//	return nil, fmt.Errorf("Can not save new policy into database")
//}

//	newClaimRemain := helper.CreateDefaultClaimRemainInValue(policy, _const.SystemCreated)

//claimRemain, err := s.repos.SaveClaimRemain(ctx, newClaimRemain)
//
//if err != nil {
//	s.log.Error("Can not insert save claim remain into database", l.Error(err))
//	return nil, fmt.Errorf("Can not insert new claim remain into database")
//}
//
//fmt.Println(claimRemain)
//
//go s.processEcert(&newPolicy, s.cfg.ClaimURL, _const.EmailShareBiPolicyId)
//
//fmt.Println(newPolicy)
//fmt.Println(policy)
//return &pb.SubmitPolicyResponse{
//	Code:    0,
//	Message: "Submit policy Ok",
//	Data: &pb.SubmitPolicyResponse_Data{
//		PolicyId:      policy.Id,
//		PremiumAmount: policy.PremiumAmount,
//		CreatedDate:   convert.ConvertToProtoTimestamp(policy.CreatedDate),
//		ECertUrl:      policy.ECertURL,
//		EffectiveFrom: convert.ConvertToProtoTimestamp(policy.EffectiveFrom),
//		EffectiveTo:   convert.ConvertToProtoTimestamp(policy.EffectiveTo),
//		PolicyNumber:  policy.PolicyNumber,
//	},
//}, nil
