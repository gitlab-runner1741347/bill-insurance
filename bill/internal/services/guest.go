package services

import (
	"context"
	"errors"
	"time"

	"git.kafefin.net/backend/bill-insurance/internal/pkg/convert"
	err2 "git.kafefin.net/backend/bill-insurance/internal/pkg/err"
	"git.kafefin.net/backend/bill-insurance/internal/pkg/helper"
	"git.kafefin.net/backend/bill-insurance/pb"
	"git.kafefin.net/backend/kitchen/l"
)

func (s *service) GuestSearchPolicies(ctx context.Context, req *pb.ClientSearchPolicyRequest) (*pb.ClientSearchPolicyResponse, error) {
	if req.GetIncidentTime() == nil || req.GetPhone() == "" {
		return nil, errors.New("fields cannot be empty")
	}
	accidentTimeToCompare := convert.ConvertProtoTimestamp(req.GetIncidentTime()).Add(time.Hour * 7)

	policies, err := s.postgresqlRepos.GetEffectivePoliciesExtension(ctx, req.GetPhone(), &accidentTimeToCompare)
	if err != nil {
		s.log.Error("GuestSearchPolicies.GetEffectivePoliciesExtension", l.Error(err))
		return nil, err
	}

	infos := make([]*pb.BpPolicyFulInfo, 0)

	bpBenefits := pb.TotalBenefitRemain{}

	for _, p := range policies {
		remain, err := s.postgresqlRepos.GetByOrderId(ctx, p.OrderID)
		if err != nil {
			s.log.Error("GuestSearchPolicies.GetByOrderId", l.Error(err))
			return nil, err
		}

		infos = append(infos, convert.ConvertPolicyAndRemainToPbPlcFullInfoWithMasking(p, remain))

		bpBenefits.LossOrDeath += int64(remain.ClaimRemainLossOrDeathValue)
		bpBenefits.WholeAccident += int64(remain.ClaimRemainWholeAccidentValue)
		bpBenefits.PartialAccident += int64(remain.ClaimRemainPartialAccidentValue)
		bpBenefits.MedicalValue += int64(remain.ClaimRemainMedicalValue)
		bpBenefits.HospitalValue += int64(remain.ClaimRemainDailyInHospitalValue) * int64(remain.ClaimRemainDaysOfInHospital)

	}

	return &pb.ClientSearchPolicyResponse{
		Code:    0,
		Message: "success",
		Data: &pb.ClientSearchPolicyResponse_Data{
			Policies:           infos,
			TotalBenefitRemain: &bpBenefits,
		},
	}, nil
}

func (s *service) GuestGetPolicyDetail(ctx context.Context, req *pb.GuestGetPolicyDetailRequest) (*pb.GuestGetPolicyDetailResponse, error) {
	policy, err := s.postgresqlRepos.GetPolicyByID(ctx, req.GetPolicyId())
	if err != nil {
		s.log.Error("UserGetPolicyDetail.GetPolicyByID", l.Error(err))
		return nil, err
	}

	policyPb := convert.ConvertPolicyToBpPolicy(policy)

	remain, err := s.postgresqlRepos.GetByOrderId(ctx, policy.OrderID)
	if err != nil {
		s.log.Error("UserGetPolicyDetail.GetByOrderId", l.Error(err))
		return nil, err
	}

	pbRemains := pb.BpClaimRemain{
		CurrentDailyInHospitalValue: int64(remain.ClaimRemainDailyInHospitalValue),
		CurrentDaysOfInHospital:     int64(remain.ClaimRemainDaysOfInHospital),
		CurrentLossOrDeathValue:     int64(remain.ClaimRemainLossOrDeathValue),
		CurrentMedicalValue:         int64(remain.ClaimRemainMedicalValue),
		CurrentPartialAccidentValue: int64(remain.ClaimRemainPartialAccidentValue),
		CurrentWholeAccidentValue:   int64(remain.ClaimRemainWholeAccidentValue),
	}

	return &pb.GuestGetPolicyDetailResponse{
		Code:    0,
		Message: "success",
		Data: &pb.GuestGetPolicyDetailResponse_Data{
			Policy:  convert.MaskingBpPolicy(policyPb),
			Remains: &pbRemains,
		},
	}, nil
}

func (s *service) ListEffectivePolicies(ctx context.Context, req *pb.ListEffectivePoliciesRequest) (*pb.ListEffectivePoliciesResponse, error) {
	policies, total, err := s.repos.PaginateEffectivePolies(ctx, req)

	if err != nil {
		return nil, err
	}

	result := make([]*pb.BillInsurancePolicyDetail, 0)
	for _, value := range policies {

		arrayBills := make([]*pb.BillInsuranceBillDetail, 0)
		for _, bill := range value.Bills {
			element := &pb.BillInsuranceBillDetail{
				BillType:                          bill.BillType,
				BillAmount:                        int64(bill.BillAmount),
				PaymentAmount:                     int64(bill.PaymentAmount),
				TotalLossOrDeathCoverageValue:     int64(bill.TotalLossOrDeathCoverageValue),
				TotalWholeAccidentCoverageValue:   int64(bill.TotalWholeAccidentCoverageValue),
				TotalPartialAccidentCoverageValue: int64(bill.TotalPartialAccidentCoverageValue),
				TotalMedicalCoverageValue:         int64(bill.TotalMedicalCoverageValue),
				DailyInHospitalCoverageValue:      int64(bill.DailyInHospitalCoverageValue),
				MaxDaysOfInHospital:               int64(bill.MaxDaysOfInHospital),
				PremiumAmount:                     int64(bill.PremiumAmount),
				PolicyNumber:                      bill.PolicyNumber,
				PolicyId:                          bill.PolicyID,
				BillId:                            bill.BillID,
			}

			arrayBills = append(arrayBills, element)
		}

		result = append(result, &pb.BillInsurancePolicyDetail{
			Bills:                       arrayBills,
			PolicyId:                    value.Id,
			PolicyNumber:                value.PolicyNumber,
			OrderId:                     value.OrderID,
			PaymentDate:                 convert.ConvertToProtoTimestamp(value.PaymentDate),
			PolicyStatus:                value.PolicyStatus,
			PolicyStatusValue:           value.PolicyStatus,
			EffectiveFrom:               convert.ConvertToProtoTimestamp(value.EffectiveFrom),
			EffectiveTo:                 convert.ConvertToProtoTimestamp(value.EffectiveTo),
			CustomerName:                value.CustomerName,
			CustomerDob:                 value.CustomerDOB,
			CustomerId:                  value.CustomerID,
			CustomerAddress:             value.CustomerAddress,
			PremiumAmount:               int64(value.TotalPremiumAmount),
			BillAmount:                  int64(value.TotalBillAmount),
			TotalLossOrDeathValue:       value.ClaimRemain.ClaimRemainLossOrDeathValue,
			TotalWholeAccidentValue:     value.ClaimRemain.ClaimRemainWholeAccidentValue,
			TotalPartialAccidentValue:   value.ClaimRemain.ClaimRemainPartialAccidentValue,
			TotalMedicalValue:           value.ClaimRemain.ClaimRemainMedicalValue,
			DailyInHospitalValue:        value.ClaimRemain.ClaimRemainDailyInHospitalValue,
			MaxDaysOfInHospital:         value.ClaimRemain.ClaimRemainDaysOfInHospital,
			CreatedAt:                   convert.ConvertToProtoTimestamp(value.CreatedAt),
			BillingMonth:                int64(value.BillingMonth),
			BillingYear:                 int64(value.BillingYear),
			CreatedTimeNumber:           int64(value.CreatedTimeNumber),
			CustomerEmail:               value.CustomerEmail,
			ECertUrl:                    value.ECertURL,
			IssuedDate:                  convert.ConvertToProtoTimestamp(value.IssuedDate),
			PartnerOrg:                  value.PartnerOrg,
			Insurer:                     value.Insurer,
			CurrentDaysOfInHospital:     value.ClaimRemain.ClaimRemainDaysOfInHospital,
			CurrentWholeAccidentValue:   value.ClaimRemain.ClaimRemainWholeAccidentValue,
			CurrentPartialAccidentValue: value.ClaimRemain.ClaimRemainPartialAccidentValue,
			CurrentMedicalValue:         value.ClaimRemain.ClaimRemainMedicalValue,
			CurrentDailyInHospitalValue: value.ClaimRemain.ClaimRemainDailyInHospitalValue,
			CurrentLossOrDeathValue:     value.ClaimRemain.ClaimRemainLossOrDeathValue,
			PaymentAmount:               int64(value.TotalPaymentAmount),
			EffectiveTimeNumber:         int64(value.EffectiveTimeNumber),
			CommonStatus:                int32(convert.MapInternalStatusToCentralStatus[value.PolicyStatus]),
		})
	}

	return &pb.ListEffectivePoliciesResponse{
		Code:    0,
		Message: "Admin list effective Policies success",
		Data: &pb.ListEffectivePoliciesResponse_Data{
			Policies: result,
			Paging: &pb.BillInsurancePaging{
				Total:         total,
				CurrentOffset: req.GetOffset(),
			},
		},
	}, nil
}

func (s *service) SharePolicyByEmail(ctx context.Context, req *pb.SharePolicyByEmailRequest) (*pb.SharePolicyByEmailResponse, error) {
	polices, err := s.repos.GetPolicyByOrderId(ctx, req.GetOrderId())
	if err != nil {
		if err != err2.RecordNotFound {
			s.log.Error("Some thing went wrong when share mail get policy by order id")
			return nil, err
		}

		s.log.Error("SharePolicyByEmail Record not found")

		return nil, err
	}
	policy := polices[0]
	sendEmailLog, err := s.repos.GetByPolicyNumber(ctx, policy.PolicyNumber)
	if err != nil {
		return nil, err

		s.log.Error("Record not found")
		return nil, err
	}

	emailArgs := sendEmailLog.EmailParams.Data.V1

	if emailArgs == nil {
		return nil, err
	}

	// set email
	emailArgs.To[0].Email = req.ReceiverEmail

	// send email

	if _, err := s.sldMessageClient.SendEmailTemplate(ctx, sendEmailLog.EmailParams.Data.V1); err != nil {
		s.log.Error("Error when send email")
		return nil, err
	}

	s.repos.UpdateTurnSharePolicyByEmail(ctx, sendEmailLog.IdempotentKey, 1)

	s.log.Error("Share poliby by email success")
	return nil, err
}

func (s *service) InsuranceLookupUser(ctx context.Context, req *pb.InsuranceLookupUserRequest) (*pb.InsuranceLookupUserResponse, error) {
	policiesAll, total, err := s.repos.PaginateAllInsuranceLookup(ctx, req)
	if err != nil {
		return nil, err
	}
	policies, _, err := s.repos.PaginateInsuranceLookup(ctx, req)
	if err != nil {
		return nil, err
	}

	dataPolicies := helper.ConvertPoliciesToResponse(policies)

	dateOfIncident := convert.ConvertProtoTimestamp(req.GetDataOfIncident())

	policies, err = s.repos.GetPoliciesEffectiveByPhoneVsIncident(ctx, req.GetCustomerPhone(), dateOfIncident)
	benefitPaid := helper.GetBenefitPaid(policiesAll)

	return &pb.InsuranceLookupUserResponse{
		Code:    0,
		Message: "claim lookup user successfully",
		Data: &pb.InsuranceLookupUserResponse_Data{
			TotalBenefitPaid: &pb.DataTotalBenefitPaid{
				LossOrDeath:         benefitPaid.LossOrDeath,
				WholeAccident:       benefitPaid.WholeAccident,
				PartialAccident:     benefitPaid.PartialAccident,
				MedicalValue:        benefitPaid.MedicalValue,
				DailyInHospital:     benefitPaid.DailyInHospital,
				MaxDaysOfInHospital: benefitPaid.MaxDaysOfInHospital,
			},
			Policies: &pb.InsuranceLookupUserResponse_Policies{
				DataPolicies: dataPolicies,
				Paging: &pb.BillInsurancePaging{
					Total:         total,
					CurrentOffset: req.GetOffset(),
				},
			},
		},
	}, nil

}

func (s *service) GetClaimRemain(ctx context.Context, req *pb.GetClaimRemainRequest) (*pb.GetClaimRemainResponse, error) {
	claimremains, err := s.repos.GetClaimRemainsByPoliciesId(ctx, req.GetPoliciesId())
	if err != nil {
		s.log.Error("Can not get info claim remains")
		return nil, err
	}

	claimremainsResponse := make([]*pb.ClaimRemainDetail, 0)

	for _, value := range claimremains {
		claimremainResponse := pb.ClaimRemainDetail{
			PolicyId:                    value.PolicyId,
			OrderId:                     value.OrderId,
			CreatedAt:                   convert.ConvertToProtoTimestamp(value.CreatedAt),
			UpdatedAt:                   convert.ConvertToProtoTimestamp(value.UpdatedAt),
			CreatedBy:                   string(value.CreatedBy),
			CurrentLossOrDeathValue:     value.ClaimRemainLossOrDeathValue,
			CurrentWholeAccidentValue:   value.ClaimRemainWholeAccidentValue,
			CurrentPartialAccidentValue: value.ClaimRemainPartialAccidentValue,
			CurrentMedicalValue:         value.ClaimRemainMedicalValue,
			CurrentDailyInHospitalValue: value.ClaimRemainDailyInHospitalValue,
			CurrentDaysOfInHospital:     value.ClaimRemainDaysOfInHospital,
		}
		claimremainsResponse = append(claimremainsResponse, &claimremainResponse)
	}

	return &pb.GetClaimRemainResponse{
		Code:    0,
		Message: "get claimremains succeedly",
		Data: &pb.GetClaimRemainResponse_Data{
			ClaimRemains: claimremainsResponse,
		},
	}, nil
}
