package services

import (
	"git.kafefin.net/backend/bill-insurance/config"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
)

type Sqs struct {
	UpdateRemainQueue string
}

func NewSqs(config *config.Config) *Sqs {
	return &Sqs{
		UpdateRemainQueue: config.BP.UpdateClaimRemainQueue,
	}
}

func (s *Sqs) PushToQueue(msgBody string) error {
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
		Config:            aws.Config{Region: aws.String("ap-southeast-1")},
	}))

	svc := sqs.New(sess)
	_, err := svc.SendMessage(&sqs.SendMessageInput{
		MessageBody: &msgBody,
		QueueUrl:    &s.UpdateRemainQueue,
	})
	if err != nil {
		return err
	}
	return nil
}

func (s *Sqs) GetDataFromQueue() ([]*sqs.Message, error) {
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
		Config:            aws.Config{Region: aws.String("ap-southeast-1")},
	}))

	svc := sqs.New(sess)
	result, err := svc.ReceiveMessage(&sqs.ReceiveMessageInput{
		QueueUrl:            &s.UpdateRemainQueue,
		MaxNumberOfMessages: aws.Int64(10),
		VisibilityTimeout:   aws.Int64(60),
	})

	if err != nil {
		return nil, err
	}

	return result.Messages, nil
}
