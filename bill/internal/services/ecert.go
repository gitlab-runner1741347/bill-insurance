package services

import (
	"context"
	"errors"
	"git.kafefin.net/backend/bill-insurance/internal/models"
	convert_ "git.kafefin.net/backend/bill-insurance/internal/pkg/convert"
	"git.kafefin.net/backend/kitchen/l"
	"gorm.io/datatypes"
)

func (s *service) processEcert(policy *models.Policy, claimURL string, templateID string) {
	policies := make([]*models.Policy, 1)
	policies[0] = policy

	ctx := context.TODO()

	certResp, err := s.printCertClient.GenerateECertV3(context.TODO(), policies)
	if err != nil {
		s.log.Error("processECert error generate ecert", l.Error(err))
		return
	}

	if len(certResp.GetData()) == 0 {
		s.log.Error("processECert error generate ecert", l.Error(errors.New("Array does have element")))
		return
	}

	policy.ECertURL = certResp.GetData()[0].PrintedUrl

	if err := s.repos.UpdateECertURL(ctx, policy.Id, policy.ECertURL); err != nil {
		s.log.Error("processECert error record evert url", l.Error(err))
		return
	}

	if _, err := s.repos.CreateOne(ctx, &models.SendEmailLog{
		PolicyNumber:  policy.PolicyNumber,
		IdempotentKey: certResp.IdempotencyKey,
		EmailParams: datatypes.JSONType[models.SendEmailLogData]{
			Data: models.SendEmailLogData{V1: convert_.ToEcertEmailRequest(policy, claimURL, templateID)}},
	}); err != nil {
		s.log.Error("processECert SendEmailLogRepo.CreateOne error", l.Error(err))
		return
	}
}
