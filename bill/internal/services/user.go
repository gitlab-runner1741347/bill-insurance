package services

import (
	"context"
	"errors"

	"git.kafefin.net/backend/bill-insurance/internal/pkg/convert"
	"git.kafefin.net/backend/bill-insurance/pb"
	"git.kafefin.net/backend/kitchen/l"
)

func (s *service) UserSearchPolicies(ctx context.Context, req *pb.ClientSearchPolicyRequest) (*pb.ClientSearchPolicyResponse, error) {
	if req.GetIncidentTime() == nil || req.GetPhone() == "" {
		return nil, errors.New("fields cannot be empty")
	}
	policies, err := s.postgresqlRepos.GetEffectivePoliciesExtension(ctx, req.GetPhone(), convert.ConvertProtoTimestamp(req.GetIncidentTime()))
	if err != nil {
		s.log.Error("GuestSearchPolicies.GetEffectivePoliciesExtension", l.Error(err))
		return nil, err
	}

	infos := make([]*pb.BpPolicyFulInfo, 0)

	bpBenefits := pb.TotalBenefitRemain{}

	for _, p := range policies {
		remain, err := s.postgresqlRepos.GetByOrderId(ctx, p.OrderID)
		if err != nil {
			s.log.Error("GuestSearchPolicies.GetByOrderId", l.Error(err))
			return nil, err
		}

		infos = append(infos, convert.ConvertPolicyAndRemainToPbPlcFullInfoWithMasking(p, remain))

		bpBenefits.LossOrDeath += int64(remain.ClaimRemainLossOrDeathValue)
		bpBenefits.WholeAccident += int64(remain.ClaimRemainWholeAccidentValue)
		bpBenefits.PartialAccident += int64(remain.ClaimRemainPartialAccidentValue)
		bpBenefits.MedicalValue += int64(remain.ClaimRemainMedicalValue)
		bpBenefits.HospitalValue += int64(remain.ClaimRemainDailyInHospitalValue) * int64(remain.ClaimRemainDaysOfInHospital)

	}
	return &pb.ClientSearchPolicyResponse{
		Code:    0,
		Message: "success",
		Data: &pb.ClientSearchPolicyResponse_Data{
			Policies:           infos,
			TotalBenefitRemain: &bpBenefits,
		},
	}, nil
}

func (s *service) UserGetPolicyDetail(ctx context.Context, req *pb.UserGetPolicyDetailRequest) (*pb.UserGetPolicyDetailResponse, error) {
	policy, err := s.postgresqlRepos.GetPolicyByID(ctx, req.GetPolicyId())
	if err != nil {
		s.log.Error("UserGetPolicyDetail.GetPolicyByID", l.Error(err))
		return nil, err
	}

	changeLogs, err := s.postgresqlRepos.ListChangeLogByPolicyID(ctx, req.GetPolicyId())
	if err != nil {
		s.log.Error("UserGetPolicyDetail.GetChangeLogByPolicyID", l.Error(err))
		return nil, err
	}

	policyPb := convert.ConvertPolicyToBpPolicy(policy)

	pbChangLogs := convert.ModelsToChangelogPb(changeLogs)

	// remains, err := s.sldClaimHub.InternalClaimRemains(ctx, &clamHubPb.InternalClaimRemainsRequest{
	// 	PolicyIds: []string{req.GetPolicyId()},
	// })

	remain, err := s.postgresqlRepos.GetByOrderId(ctx, policy.OrderID)
	if err != nil {
		s.log.Error("UserGetPolicyDetail.GetByOrderId", l.Error(err))
		return nil, err
	}

	pbRemains := pb.BpClaimRemain{
		CurrentDailyInHospitalValue: int64(remain.ClaimRemainDailyInHospitalValue),
		CurrentDaysOfInHospital:     int64(remain.ClaimRemainDaysOfInHospital),
		CurrentLossOrDeathValue:     int64(remain.ClaimRemainLossOrDeathValue),
		CurrentMedicalValue:         int64(remain.ClaimRemainMedicalValue),
		CurrentPartialAccidentValue: int64(remain.ClaimRemainPartialAccidentValue),
		CurrentWholeAccidentValue:   int64(remain.ClaimRemainWholeAccidentValue),
	}

	return &pb.UserGetPolicyDetailResponse{
		Code:    0,
		Message: "success",
		Data: &pb.UserGetPolicyDetailResponse_Data{
			Policy:     policyPb,
			ChangeLogs: pbChangLogs,
			Remains:    &pbRemains,
		},
	}, nil
}
