package services

import (
	"context"

	"git.kafefin.net/backend/bill-insurance/config"
	"git.kafefin.net/backend/bill-insurance/internal/external"
	"git.kafefin.net/backend/bill-insurance/internal/pkg/policynumber"
	repositories_ "git.kafefin.net/backend/bill-insurance/internal/repositories"
	"git.kafefin.net/backend/bill-insurance/pb"
	"git.kafefin.net/backend/kitchen/id"
	"git.kafefin.net/backend/kitchen/l"
	pbCore "git.kafefin.net/backend/salad-kit/pb"
	"git.kafefin.net/backend/salad-kit/server"
	"github.com/go-redis/redis/v8"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/r3labs/diff/v3"
	"google.golang.org/grpc"
)

type serviceInterface interface {
	pb.BillInsuranceVendorServer
	pb.BillInsuranceAdminServer
	pb.BillInsuranceGuestServer
	pb.BillInsuranceInternalServer
}

//var _ serviceInterface = &service{}Server

type service struct {
	cfg *config.Config
	log l.Logger

	diff              *diff.Differ
	repos             repositories_.Repository
	postgresqlRepos   repositories_.PostgresqlRepository
	printCertClient   external.PrintCertClient
	sldMessageClient  external.SldMessageClient
	sldMediaClient    external.SaladinMediaClient
	pnGenerator       policynumber.PolicyNumberGenerator
	sldUserClient     external.SaladinUserClient
	sldCentralPolicy  external.CentralPolicyClient
	sldBillProtection external.BillProtectionSvc
	sldClaimHub       external.ClaimHubSvc
	sqs               *Sqs
	biIdGenerator     id.ID
}

func (s *service) Version(ctx context.Context, request *pbCore.VersionRequest) (*pbCore.VersionResponse, error) {
	return &pbCore.VersionResponse{Version: "1.0.0"}, nil
}

func (s *service) Liveness(context context.Context, req *pbCore.LivenessRequest) (*pbCore.LivenessResponse, error) {
	return &pbCore.LivenessResponse{
		Message: "ok",
	}, nil
}

func (s *service) ToggleReadiness(context context.Context, req *pbCore.ToggleReadinessRequest) (*pbCore.ToggleReadinessResponse, error) {
	return &pbCore.ToggleReadinessResponse{Message: "ok"}, nil
}

func (s *service) Readiness(context context.Context, req *pbCore.ReadinessRequest) (*pbCore.ReadinessResponse, error) {
	return &pbCore.ReadinessResponse{Message: "ok"}, nil
}

func NewBillInsuranceService(
	ctx context.Context,
	cfg *config.Config,
	log l.Logger,
	repos repositories_.Repository,
	postgresqlRepos repositories_.PostgresqlRepository,
	redis *redis.Client,
	printCertClient external.PrintCertClient,
	sldMessageClient external.SldMessageClient,
	sldUserClient external.SaladinUserClient,
	sldCentralPolicy external.CentralPolicyClient,
	sldBillProtection external.BillProtectionSvc,
	sldClaimHub external.ClaimHubSvc,

) server.ServiceServer {
	diffJSON, err := diff.NewDiffer(diff.TagName("json"))
	if err != nil {
		panic("")
	}
	return &service{
		cfg:               cfg,
		log:               log.Named("Bill Insurance"),
		pnGenerator:       policynumber.NewPNGenerator(log, redis, "bill-insurance:policy-number-counter"),
		diff:              diffJSON,
		repos:             repos,
		printCertClient:   printCertClient,
		sldMessageClient:  sldMessageClient,
		postgresqlRepos:   postgresqlRepos,
		sldUserClient:     sldUserClient,
		sldCentralPolicy:  sldCentralPolicy,
		sldBillProtection: sldBillProtection,
		sldClaimHub:       sldClaimHub,
	}
}

func (s *service) RegisterWithGrpcServer(server *grpc.Server) {
	pb.RegisterBillInsuranceVendorServer(server, s)
	pb.RegisterBillInsuranceInternalServer(server, s)
	pb.RegisterBillInsuranceAdminServer(server, s)
	pb.RegisterBillInsuranceGuestServer(server, s)
	pb.RegisterBillInsuranceUserServer(server, s)
	pbCore.RegisterHealthServiceServer(server, s)
}

func (s *service) RegisterWithMuxServer(ctx context.Context, mux *runtime.ServeMux, conn *grpc.ClientConn) error {
	if err := pb.RegisterBillInsuranceVendorHandler(ctx, mux, conn); err != nil {
		return err
	}
	if err := pb.RegisterBillInsuranceInternalHandler(ctx, mux, conn); err != nil {
		return err
	}
	if err := pb.RegisterBillInsuranceAdminHandler(ctx, mux, conn); err != nil {
		return err
	}
	if err := pb.RegisterBillInsuranceGuestHandler(ctx, mux, conn); err != nil {
		return err
	}

	if err := pb.RegisterBillInsuranceUserHandler(ctx, mux, conn); err != nil {
		return err
	}

	if err := pbCore.RegisterHealthServiceHandler(ctx, mux, conn); err != nil {
		return err
	}

	return nil
}

// Close ...
func (s *service) Close(ctx context.Context) {
}
