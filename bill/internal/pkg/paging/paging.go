package paging

type Paging struct {
	Limit  int
	Offset int
	IsNext bool
}

const DefaultPageSize = 10

func CalcPagingWithLastID(prevPage, currPage int) Paging {
	if currPage > prevPage {
		pageDiff := currPage - prevPage
		return Paging{
			Limit:  DefaultPageSize,
			Offset: (pageDiff - 1) * DefaultPageSize,
			IsNext: true,
		}
	} else if currPage < prevPage {
		pageDiff := prevPage - currPage
		return Paging{
			Limit:  DefaultPageSize,
			Offset: (pageDiff - 1) * DefaultPageSize,
			IsNext: false,
		}
	} else {
		return Paging{
			Limit:  DefaultPageSize,
			Offset: 0,
			IsNext: true,
		}
	}
}

func CalcPagingWithoutLastID(prevPage, currPage int) Paging {

	return Paging{
		Limit:  DefaultPageSize,
		Offset: (currPage - 1) * DefaultPageSize,
		IsNext: true,
	}
}

// check pageSize
func CalcOffset(currPage int, pageSize *int) (int, int) {
	if pageSize == nil {
		*pageSize = DefaultPageSize
	}

	if currPage <= 0 {
		return *pageSize, 0
	}
	if pageSize != nil {
		return *pageSize, (currPage - 1) * (*pageSize)
	}
	return *pageSize, (currPage - 1) * DefaultPageSize
}
