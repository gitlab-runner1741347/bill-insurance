package policynumber

import (
	"context"
	"fmt"
	_const "git.kafefin.net/backend/bill-insurance/internal/const"
	"git.kafefin.net/backend/kitchen/l"
	"github.com/go-redis/redis/v8"
	"math"
	"time"
)

type PolicyNumberGenerator interface {
	GenNewPolicyNumber(ctx context.Context, productCode _const.ProductCode) string
	FormatNewPNVersion(originPolicyNumber string, version uint32) string
}

type pnGenerator struct {
	log       l.Logger
	redis     *redis.Client
	keyPrefix string
}

func NewPNGenerator(log l.Logger, redis *redis.Client, keyPrefix string) PolicyNumberGenerator {
	if redis == nil {
		panic("NewPNGenerator redis nil")
	}

	return &pnGenerator{
		log:       log,
		redis:     redis,
		keyPrefix: keyPrefix,
	}
}

func (g *pnGenerator) getCounterKey(year int) string {
	return fmt.Sprintf("%s:%d", g.keyPrefix, year)
}

func uintToStrWithZero(n uint64, length int) string {
	upper := math.Pow10(length)
	s := fmt.Sprintf("%d", uint64(upper)+n)
	return s[1:]
}

// SBVR.2022.00000001 - SBVR.[year].[increment_number]
func formatPolicyNumber(
	count uint64, year int,
) string {
	countString := uintToStrWithZero(count, 8)

	return fmt.Sprintf("SBVR.%d.%s", year, countString)
}

func (g *pnGenerator) GenNewPolicyNumber(ctx context.Context, productCode _const.ProductCode) string {
	now := time.Now()
	currentYear := now.Year()

	counterKey := g.getCounterKey(currentYear)
	// get next count number
	count, err := g.redis.Incr(ctx, counterKey).Uint64()
	if err != nil {
		g.log.Error("GenNewPolicyNumber increase count error", l.Error(err))
		return ""
	}

	return formatPolicyNumber(count, currentYear)
}

func (g *pnGenerator) FormatNewPNVersion(originPolicyNumber string, version uint32) string {

	return fmt.Sprintf("%s-%d", originPolicyNumber, version)
}
