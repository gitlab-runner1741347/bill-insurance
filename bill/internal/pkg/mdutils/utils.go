package mdutils

import (
	"context"
	"errors"
	"fmt"
	"strconv"
	"strings"

	"google.golang.org/grpc/metadata"
)

// GetMDValueFromRequest receive the context and a context.Metadata key
// then return the value of that key
func GetMDValueFromRequest(ctx context.Context, k string) string {
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return ""
	}

	return strings.Join(md.Get(k), "")
}

func GetInt64FromMD(ctx context.Context, k string) (int64, error) {
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return 0, errors.New("cannot get metadata from context")
	}

	values := md.Get(k)
	if len(values) != 1 {
		return 0, fmt.Errorf("too many value %v", values)
	}

	return strconv.ParseInt(values[0], 10, 64)
}
