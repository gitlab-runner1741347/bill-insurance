package convert

import (
	"git.kafefin.net/backend/bill-insurance/internal/models"
	"git.kafefin.net/backend/bill-insurance/pb"
)

func PolicyModelToPb(m *models.Policy) *pb.BillInsurancePolicy {
	return &pb.BillInsurancePolicy{
		//OrderId:                   m.OrderId,
		//BillAmount:                m.BillAmount,
		//BillId:                    m.BillID,
		//BillType:                  m.BillType,
		//BillingMonth:              m.BillingMonth,
		//BillingYear:               m.BillingYear,
		//CreatedDate:               ConvertToProtoTimestamp(m.CreatedDate),
		//CreatedTimeNumber:         m.CreatedTimeNumber,
		//CustomerAddress:           m.CustomerAddress,
		//CustomerDob:               m.CustomerDOB,
		//CustomerEmail:             m.CustomerEmail,
		//CustomerId:                m.CustomerID,
		//CustomerName:              m.CustomerName,
		//CustomerPhone:             m.CustomerPhone,
		//DailyInHospitalValue:      m.DailyInHospitalValue,
		//ECertUrl:                  m.ECertURL,
		//EffectiveFrom:             ConvertToProtoTimestamp(m.EffectiveFrom),
		//EffectiveTo:               ConvertToProtoTimestamp(m.EffectiveTo),
		//IssuedDate:                ConvertToProtoTimestamp(m.IssuedDate),
		//MaxDaysOfInHospital:       m.MaxDaysOfInHospital,
		//Org:                       m.Org,
		//PaymentDate:               ConvertToProtoTimestamp(m.PaymentDate),
		//Id:                        m.Id,
		//PolicyStatus:              m.PolicyStatus,
		//PremiumAmount:             m.PremiumAmount,
		//Insurer:                   m.Insurer,
		//TotalLossOrDeathValue:     m.TotalLossOrDeathValue,
		//TotalMedicalValue:         m.TotalMedicalValue,
		//TotalPartialAccidentValue: m.TotalPartialAccidentValue,
		//TotalWholeAccidentValue:   m.TotalWholeAccidentValue,
		//PolicyNumber:              m.PolicyNumber,
	}
}
