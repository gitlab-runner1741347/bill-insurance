package convert

import "strings"

const (
	BillType = "BillType"
)

func SplitInput(input string, key string) string {
	key = key + "_"
	return strings.TrimPrefix(input, key)
}

func MappingBillType(billType string) string {
	return SplitInput(billType, BillType)
}
