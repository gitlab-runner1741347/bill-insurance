package convert

import (
	"git.kafefin.net/backend/bill-insurance/internal/models"
	printCertPb "git.kafefin.net/backend/print-cert/pb"
	"strconv"
)

func PolicyModelToPrintCertPb(m *models.Policy) []*printCertPb.PrintCertValues {
	return []*printCertPb.PrintCertValues{
		{
			Key:    "policy_number",
			Values: []string{m.PolicyNumber},
		},
		{
			Key:    "insured_day",
			Values: []string{strconv.FormatInt(m.IssuedDate.UnixNano(), 10)},
		},
		{
			Key:    "effective_from",
			Values: []string{strconv.FormatInt(m.EffectiveFrom.UnixNano(), 10)},
		},
		{
			Key:    "effective_to",
			Values: []string{strconv.FormatInt(m.EffectiveTo.UnixNano(), 10)},
		},
		{
			Key:    "name",
			Values: []string{m.CustomerName},
		},
		{
			Key:    "dob",
			Values: []string{m.CustomerDOB},
		},
		{
			Key:    "identity",
			Values: []string{m.CustomerID},
		},
		{
			Key:    "phone",
			Values: []string{m.CustomerPhone},
		},
		{
			Key:    "address",
			Values: []string{m.CustomerAddress},
		},
		{
			Key:    "premium_amount",
			Values: []string{string(m.TotalPremiumAmount)},
		},
		{
			Key:    "total_loss_or_death_value",
			Values: []string{string(m.TotalLossOrDeathCoverageValue)},
		},
		{
			Key:    "total_medical_value",
			Values: []string{string(m.TotalMedicalCoverageValue)},
		},
		{
			Key:    "daily_in_hospital_value",
			Values: []string{string(m.DailyInHospitalCoverageValue)},
		},
	}
}
