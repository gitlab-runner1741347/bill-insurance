package convert

import (
	"strings"
	"time"

	"git.kafefin.net/backend/bill-insurance/internal/models"
	"git.kafefin.net/backend/bill-insurance/pb"
	"github.com/r3labs/diff/v3"
	"google.golang.org/protobuf/types/known/structpb"
	"gorm.io/datatypes"
)

func ChangelogToAttributes(changelogID uint64, changelog diff.Changelog) []*models.PolicyChangelogAttribute {
	ret := make([]*models.PolicyChangelogAttribute, 0)

	for i := range changelog {
		m := models.PolicyChangelogAttribute{
			ChangelogID: changelogID,
			KeyPath:     strings.Join(changelog[i].Path, "."),
			OldValue:    datatypes.JSONType[interface{}]{Data: changelog[i].From},
			NewValue:    datatypes.JSONType[interface{}]{Data: changelog[i].To},
		}

		ret = append(ret, &m)
	}

	return ret
}

func ModelsToChangelogPb(m []*models.PolicyChangelog) []*pb.Changelog {
	rs := make([]*pb.Changelog, 0, len(m))

	for i := range m {
		rs = append(rs, PolicyChangelogToChangelogPb(*m[i]))
	}

	return rs
}

func PolicyChangelogToChangelogPb(changelog models.PolicyChangelog) *pb.Changelog {
	actorName := changelog.ActorName
	if actorName == "" {
		actorName = changelog.Actor
	}

	return &pb.Changelog{
		Action:    changelog.Action,
		Actor:     changelog.Actor,
		Comment:   changelog.Comment,
		CreatedAt: changelog.CreatedAt.Add(-7 * time.Hour).UnixMilli(),
		ActorName: actorName,
		KeyPath:   changelog.KeyPath,
		Changes:   ModelsToChangelogAttributePb(changelog.ChangelogAttributes),
	}
}

func ModelsToChangelogAttributePb(m []*models.PolicyChangelogAttribute) []*pb.ChangelogAttribute {
	rs := make([]*pb.ChangelogAttribute, 0, len(m))

	for i := range m {
		e, _ := ToPolicyChangelogAttributePb(*m[i])
		rs = append(rs, e)
	}

	return rs
}

func ToPolicyChangelogAttributePb(c models.PolicyChangelogAttribute) (*pb.ChangelogAttribute, error) {
	// oldValueJSON, _ := c.OldValue.Value()
	oldValue, err := structpb.NewValue(c.OldValue.Data)
	if err != nil {
		return nil, err
	}
	// newValueJSON, _ := c.NewValue.Value()
	newValue, err := structpb.NewValue(c.NewValue.Data)
	if err != nil {
		return nil, err
	}
	return &pb.ChangelogAttribute{
		KeyPath:  c.KeyPath,
		IsEqual:  c.IsEqual,
		OldValue: oldValue,
		NewValue: newValue,
		Patch:    ToChangelogAttribute_PatchPb(c.Patch.Data),
	}, nil
}

func ToChangelogAttribute_PatchPb(a models.AttributePatch) *pb.ChangelogAttribute_Patch {
	added, _ := structpb.NewList(a.Added)
	removed, _ := structpb.NewList(a.Removed)

	return &pb.ChangelogAttribute_Patch{
		Added:   added,
		Removed: removed,
	}
}
