package convert

import (
	"fmt"
	"git.kafefin.net/backend/bill-insurance/internal/models"
	sldMessagePb "git.kafefin.net/backend/saladin-message/pb"
)

func PolicyModelToMessagePb(m *models.Policy, claimURL string) []*sldMessagePb.Values {
	return []*sldMessagePb.Values{

		{
			Key:    "buyer_name",
			Values: []string{m.CustomerName},
		},
		//{
		//	Key:    "policy_id",
		//	Values: []string{m.Id},
		//},
		{
			Key:    "policy_number",
			Values: []string{m.PolicyNumber},
		},
		{
			Key:    "issuance_date",
			Values: []string{m.IssuedDate.String()},
		},
		{
			Key:    "effective_from",
			Values: []string{m.EffectiveFrom.String()},
		},
		{
			Key:    "effective_to",
			Values: []string{m.EffectiveTo.String()},
		},
		{
			Key:    "ecert_url",
			Values: []string{claimURL},
		},
	}
}

func ToEcertEmailRequest(
	policy *models.Policy,
	claimURL string,
	emailTemplateID string,
) *sldMessagePb.SendEmailTemplateRequest {
	policyParams := PolicyModelToMessagePb(policy, claimURL)

	return &sldMessagePb.SendEmailTemplateRequest{
		To:         []*sldMessagePb.EmailTo{{Name: policy.CustomerName, Email: policy.CustomerEmail}},
		TemplateId: emailTemplateID,
		Params:     policyParams,
		Attachments: []*sldMessagePb.EmailAttachment{
			{
				Name: fmt.Sprintf("%s.pdf", policy.PolicyNumber),
				Type: "application/pdf",
				Url:  policy.ECertURL,
			},
		},
		Source: "BI",
	}
}
