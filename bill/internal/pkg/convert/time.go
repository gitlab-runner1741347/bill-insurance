package convert

import (
	"fmt"
	"time"

	"github.com/golang/protobuf/ptypes"
	"google.golang.org/protobuf/types/known/timestamppb"
)

func ConvertProtoTimestamp(protoTimestamp *timestamppb.Timestamp) *time.Time {
	if protoTimestamp == nil {
		// Trường hợp protoTimestamp là nil, trả về zero value của time.Time
		return nil
	}

	timeValue, err := ptypes.Timestamp(protoTimestamp)
	if err != nil {
		// Xử lý lỗi nếu có
		fmt.Println("Lỗi chuyển đổi Timestamp:", err)
		return nil
	}

	return &timeValue
}

func ConvertToProtoTimestamp(timestamp time.Time) *timestamppb.Timestamp {
	protoTimestamp, err := ptypes.TimestampProto(timestamp)
	if err != nil {
		return nil
	}
	return protoTimestamp
}

func FormatToYYYYMMDD(timestamp time.Time) string {
	formattedTime := timestamp.Format("2006-01-02")
	return formattedTime
}

func ParseFromStringToTime(input string) (time.Time, error) {
	layout := "2006-01-02 15:04:05"
	parsedTime, err := time.Parse(layout, input)
	if err != nil {
		return time.Time{}, err
	}
	return parsedTime, nil
}

func ParseFromStringToProto(intput string) *timestamppb.Timestamp {
	time, err := ParseFromStringToTime(intput)
	if err != nil {
		return nil
	}

	return ConvertToProtoTimestamp(time)
}

func ConvertStringToProtoTime(inputString string) (*timestamppb.Timestamp, error) {
	parsedTime, err := time.Parse(time.RFC3339, inputString)
	if err != nil {
		return nil, err
	}

	protoTime := timestamppb.New(parsedTime)

	return protoTime, nil
}
