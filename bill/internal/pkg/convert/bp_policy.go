package convert

import (
	"strings"
	"time"

	"git.kafefin.net/backend/bill-insurance/internal/models"
	"git.kafefin.net/backend/bill-insurance/pb"
	centralPolicyPb "git.kafefin.net/backend/saladin-central-proto/central_policy"
	mask "github.com/anu1097/golang-masking-tool"
	"github.com/anu1097/golang-masking-tool/customMasker"
	"github.com/anu1097/golang-masking-tool/filter"
)

const (
	ProductCategory = "EMBEDDED_BILL_PROTECTION"
)

var (
	MapBillTypeToProductCode = map[string]string{
		"ELECTRIC":         "BILL_PRO1_E_BV",
		"WATER":            "BILL_PRO1_E_BV",
		"CONSUMER_FINANCE": "BILL_PRO2_E_BV",
		"EDUCATION":        "BILL_PRO2_E_BV",
		"TV":               "BILL_PRO3_E_BV",
		"INTERNET":         "BILL_PRO3_E_BV",
	}

	MapInternalStatusToCentralStatus = map[string]centralPolicyPb.CentralPolicyStatus{
		"CANCELLED":  centralPolicyPb.CentralPolicyStatus_CENTRAL_POLICY_STATUS_CANCEL,
		"CREATED_OK": centralPolicyPb.CentralPolicyStatus_CENTRAL_POLICY_STATUS_ENFORCE,
		"EXPIRED":    centralPolicyPb.CentralPolicyStatus_CENTRAL_POLICY_STATUS_EXPIRED,
	}
)

func FindProductCode(billType string) string {
	result, ok := MapBillTypeToProductCode[billType]
	if !ok {
		return "UN_DEFINE"
	}

	return result
}

func FindCentralStatus(status string) centralPolicyPb.CentralPolicyStatus {
	result, ok := MapInternalStatusToCentralStatus[status]
	if !ok {
		return centralPolicyPb.CentralPolicyStatus_CENTRAL_POLICY_STATUS_UNSPECIFIED
	}

	return result
}

func ConvertBpPolicyToCentralPolicy(policy *models.BpPolicy) *centralPolicyPb.CentralPolicyMessage {
	centralPolicy := &centralPolicyPb.CentralPolicyMessage{
		Id:              policy.PolicyID,
		PolicyNumber:    policy.PolicyNumber,
		EcertUrl:        policy.EcertUrl,
		UserId:          policy.UserID,
		ProductCode:     FindProductCode(policy.BillType),
		OwnerName:       policy.CustomerName,
		OwnerPhone:      policy.CustomerPhone,
		OwnerEmail:      policy.CustomerEmail,
		Fee:             int64(policy.PremiumAmount),
		Vendor:          policy.OrderSource,
		Provider:        policy.ProviderCode,
		Status:          FindCentralStatus(policy.PolicyStatus),
		ProductLine:     centralPolicyPb.PolicyProductLine_PPL_EMBEDDED,
		ProductCategory: ProductCategory,
		CreatedAt:       ConvertToProtoTimestamp(policy.CreatedDate.Add(-7 * time.Hour)),
		UpdatedAt:       ConvertToProtoTimestamp(policy.CreatedDate.Add(-7 * time.Hour)),
		IssuedAt:        ConvertToProtoTimestamp(policy.IssuedDate.Add(-7 * time.Hour)),
		EffectiveFrom:   ConvertToProtoTimestamp(policy.EffectiveFrom.Add(-7 * time.Hour)),
		EffectiveTo:     ConvertToProtoTimestamp(policy.EffectiveTo.Add(-7 * time.Hour)),
	}
	return centralPolicy
}

func ConvertBpPoliciesToCentralPolicies(policies []*models.BpPolicy) []*centralPolicyPb.CentralPolicyMessage {
	var centralPolicies []*centralPolicyPb.CentralPolicyMessage
	for _, value := range policies {
		centralPolicies = append(centralPolicies, ConvertBpPolicyToCentralPolicy(value))
	}

	return centralPolicies
}

func ConvertBpPolicyToPolicyToCompateProto(policy *models.BpPolicy) *pb.PolicyToCompare {
	return &pb.PolicyToCompare{
		PolicyId:        policy.PolicyID,
		CustomerName:    policy.CustomerName,
		CustomerAddress: policy.CustomerAddress,
		CustomerDob:     GetDOBIngoreHour(policy.CustomerDOB),
		CustomerEmail:   policy.CustomerEmail,
		CustomerId:      policy.CustomerID,
	}
}

func GetDOBIngoreHour(dob string) string {
	if strings.TrimSpace(dob) == "" {
		return ""
	}
	parts := strings.Split(dob, " ")[0]
	return strings.Split(parts, "T")[0]
}

func MappingStatusPolicy(status string) pb.PolicyStatus {
	switch status {
	case "CREATED_OK":
		return pb.PolicyStatus_CREATED_OK
	case "CANCELLED":
		return pb.PolicyStatus_CANCELLED
	case "EXPIRED":
		return pb.PolicyStatus_EXPIRED
	}

	return pb.PolicyStatus_UNSPECIFIED
}

func ConvertPolicyToSummaryBpPolicy(policy *models.BpPolicy) *pb.BpPolicySummary {
	return &pb.BpPolicySummary{
		PolicyId:          policy.PolicyID,
		PolicyNumber:      policy.PolicyNumber,
		OrderId:           policy.OrderID,
		CustomerPhone:     policy.CustomerPhone,
		CreatedAt:         ConvertToProtoTimestamp(policy.CreatedDate.Add(-7 * time.Hour)),
		BillAmount:        int64(policy.BillAmount),
		PremiumAmount:     int64(policy.PremiumAmount),
		PaymentAmount:     int64(policy.PaymentAmount),
		PolicyStatus:      int32(MappingStatusPolicy(policy.PolicyStatus)),
		PolicyStatusValue: MappingStatusPolicy(policy.PolicyStatus).String(),
		CommonStatus:      int32(MapInternalStatusToCentralStatus[policy.PolicyStatus]),
	}
}

func ConvertPoliciesToSummaryBpPolicies(policies []*models.BpPolicy) []*pb.BpPolicySummary {
	result := make([]*pb.BpPolicySummary, 0)
	for _, policy := range policies {
		result = append(result, ConvertPolicyToSummaryBpPolicy(policy))
	}

	return result
}

func ConvertPolicyToBpPolicies(policies []*models.BpPolicy) []*pb.BpPolicy {
	result := make([]*pb.BpPolicy, 0)
	for _, policy := range policies {
		result = append(result, ConvertPolicyToBpPolicy(policy))
	}

	return result
}

func ConvertClaimRemainToBpClaimRemain(remain *models.BpClaimRemain) *pb.BpClaimRemain {
	return &pb.BpClaimRemain{
		CurrentLossOrDeathValue:     int64(remain.ClaimRemainLossOrDeathValue),
		CurrentWholeAccidentValue:   int64(remain.ClaimRemainWholeAccidentValue),
		CurrentPartialAccidentValue: int64(remain.ClaimRemainPartialAccidentValue),
		CurrentMedicalValue:         int64(remain.ClaimRemainMedicalValue),
		CurrentDailyInHospitalValue: int64(remain.ClaimRemainDailyInHospitalValue),
		CurrentDaysOfInHospital:     int64(remain.ClaimRemainDaysOfInHospital),
	}
}

func ConvertPolicyAndRemainToPbPlcFullInfoWithMasking(policy *models.BpPolicy, remain *models.BpClaimRemain) *pb.BpPolicyFulInfo {
	return &pb.BpPolicyFulInfo{
		Policy: MaskingBpPolicy(ConvertPolicyToBpPolicy(policy)),
		Remain: ConvertClaimRemainToBpClaimRemain(remain),
	}
}

func ConvertPolicyAndRemainToPbPlcFullInfoWithoutMasking(policy *models.BpPolicy, remain *models.BpClaimRemain) *pb.BpPolicyFulInfo {
	return &pb.BpPolicyFulInfo{
		Policy: ConvertPolicyToBpPolicy(policy),
		Remain: ConvertClaimRemainToBpClaimRemain(remain),
	}
}

func ConvertPolicyToBpPolicy(policy *models.BpPolicy) *pb.BpPolicy {
	dob, _ := ConvertStringToProtoTime(policy.CustomerDOB)

	claimAvailableStartRequestTime, claimAvailableEndRequestTime := ProvideClaimableTimeForBill(policy.EffectiveFrom.Add(-7*time.Hour), policy.EffectiveTo.Add(-7*time.Hour))
	incidentAvailableStartRequestTime, incidentAvailableEndRequestTime := ProvideIncidentTimeForBill(policy.EffectiveFrom.Add(-7*time.Hour), policy.EffectiveTo.Add(-7*time.Hour))

	return &pb.BpPolicy{
		PolicyId:                          policy.PolicyID,
		PolicyNumber:                      policy.PolicyNumber,
		OrderId:                           policy.OrderID,
		PaymentDate:                       ConvertToProtoTimestamp(policy.PaymentDate.Add(-7 * time.Hour)),
		PolicyStatus:                      int32(MappingStatusPolicy(policy.PolicyStatus)),
		PolicyStatusValue:                 MappingStatusPolicy(policy.PolicyStatus).String(),
		EffectiveFrom:                     ConvertToProtoTimestamp(policy.EffectiveFrom.Add(-7 * time.Hour)),
		EffectiveTo:                       ConvertToProtoTimestamp(policy.EffectiveTo.Add(-7 * time.Hour)),
		CustomerName:                      policy.CustomerName,
		CustomerDob:                       dob,
		CustomerId:                        policy.CustomerID,
		CustomerPhone:                     policy.CustomerPhone,
		CustomerAddress:                   policy.CustomerAddress,
		PremiumAmount:                     int64(policy.PremiumAmount),
		BillAmount:                        int64(policy.BillAmount),
		TotalLossOrDeathValue:             int64(policy.TotalLossOrDeathValue),
		TotalWholeAccidentValue:           int64(policy.TotalWholeAccidentValue),
		TotalPartialAccidentValue:         int64(policy.TotalPartialAccidentValue),
		TotalMedicalValue:                 int64(policy.TotalMedicalValue),
		DailyInHospitalValue:              int64(policy.DailyInHospitalValue),
		MaxDaysOfInHospital:               int64(policy.MaxDaysOfInHospital),
		EcertUrl:                          policy.EcertUrl,
		CustomerEmail:                     policy.CustomerEmail,
		CreatedAt:                         ConvertToProtoTimestamp(policy.CreatedDate.Add(-7 * time.Hour)),
		Insurer:                           policy.ProviderCode,
		Partner:                           policy.OrderSource,
		BillId:                            policy.BillID,
		BillType:                          policy.BillType,
		BillingMonth:                      int32(policy.BillingMonth),
		BillingYear:                       int32(policy.BillingYear),
		PaymentAmount:                     int64(policy.PaymentAmount),
		ClaimAvailableStartRequestTime:    claimAvailableStartRequestTime.UnixMilli(),
		ClaimAvailableEndRequestTime:      claimAvailableEndRequestTime.UnixMilli(),
		IncidentAvailableStartRequestTime: incidentAvailableStartRequestTime.UnixMilli(),
		IncidentAvailableEndRequestTime:   incidentAvailableEndRequestTime.UnixMilli(),
		CommonStatus:                      int32(MapInternalStatusToCentralStatus[policy.PolicyStatus]),
	}
}

func MaskingBpPolicy(m *pb.BpPolicy) *pb.BpPolicy {
	maskTool := mask.NewMaskTool(
		filter.CustomFieldFilter("OrderId", customMasker.MID),
		filter.CustomFieldFilter("CustomerPhone", customMasker.MMobile),
		filter.CustomFieldFilter("CustomerEmail", customMasker.MEmail),
		filter.CustomFieldFilter("CustomerAddress", customMasker.MAddress),
		filter.CustomFieldFilter("CustomerId", customMasker.MID),
	)
	filteredData := maskTool.MaskDetails(m)
	return filteredData.(*pb.BpPolicy)
}

func CustomizeTimeToComparedExactly(req *pb.AdminListPoliciesRequest) {
	if req.GetFromDate() != nil {
		fromDate := ConvertProtoTimestamp(req.GetFromDate())
		req.FromDate = ConvertToProtoTimestamp(fromDate.AddDate(0, 0, 1))
	}

	if req.GetToDate() != nil {
		toDate := ConvertProtoTimestamp(req.GetToDate())
		req.ToDate = ConvertToProtoTimestamp(toDate.AddDate(0, 0, 1))
	}
}
