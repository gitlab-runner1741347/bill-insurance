package convert

import (
	"fmt"
	"time"
)

func minTime(times ...time.Time) (time.Time, error) {
	if len(times) == 0 {
		return time.Time{}, fmt.Errorf("no times provided")
	}

	min := times[0]

	for _, t := range times {
		if t.Before(min) {
			min = t
		}
	}

	return min, nil
}

func ProvideClaimableTimeForBill(startTime time.Time, endTime time.Time) (time.Time, time.Time) {

	claimAbleStartTime := startTime
	claimAbleEndTime := endTime.AddDate(0, 0, 30)

	return claimAbleStartTime, claimAbleEndTime
}

func ProvideIncidentTimeForBill(startTime time.Time, endTime time.Time) (time.Time, time.Time) {

	accidentTimeAllowedStartTime := startTime

	accidentTimeAllowedEndTime, _ := minTime(endTime, time.Now())

	return accidentTimeAllowedStartTime, accidentTimeAllowedEndTime
}
