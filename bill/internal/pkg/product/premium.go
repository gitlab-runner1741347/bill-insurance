package product

type ProductCode string

const (
	Trip01 ProductCode = "e_trip_pa_01"
	Trip02 ProductCode = "e_trip_pa_02"
)
