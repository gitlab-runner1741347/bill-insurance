package authen

import (
	"context"
	"fmt"
	"git.kafefin.net/backend/kitchen/l"
	"github.com/go-redis/redis/v8"
	"github.com/google/uuid"
	"time"
)

type TokenSvc interface {
	GenerateToken(ctx context.Context, vendorID uint32) (*Token, error)
	ValidateToken(ctx context.Context, token string) uint32
	InvalidateToken(ctx context.Context, token string) error
}

type Token struct {
	Value    string
	ExpireIn int32
}

type tokenService struct {
	log      l.Logger
	redis    *redis.Client
	tokenTTL int32
	prefix   string
}

func NewTokenSvc(log l.Logger, redis *redis.Client, ttl int32, prefix string) TokenSvc {
	return &tokenService{
		log:      log,
		redis:    redis,
		tokenTTL: ttl,
		prefix:   prefix,
	}
}

func genTokenRedisKey(prefix string, token string) string {
	return fmt.Sprintf("%s:%s", prefix, token)
}

func (s *tokenService) GenerateToken(ctx context.Context, vendorID uint32) (*Token, error) {
	uniqStr := uuid.New()
	token, err := Sign("", uniqStr.String())
	if err != nil {
		return nil, err
	}

	key := genTokenRedisKey(s.prefix, token)

	redisErr := s.redis.SetEX(ctx, key, vendorID, time.Duration(s.tokenTTL)*time.Second).Err()
	if redisErr != nil {
		return nil, redisErr
	}

	return &Token{
		Value:    token,
		ExpireIn: s.tokenTTL,
	}, nil
}

func (s *tokenService) ValidateToken(ctx context.Context, token string) uint32 {
	key := genTokenRedisKey(s.prefix, token)

	vendorID, err := s.redis.Get(ctx, key).Uint64()
	if err != nil {
		if err != redis.Nil {
			s.log.Error("Error getting value from Redis", l.Error(err))
		}

		return 0
	}

	return uint32(vendorID)
}

func (s *tokenService) InvalidateToken(ctx context.Context, token string) error {
	key := genTokenRedisKey(s.prefix, token)

	return s.redis.Del(ctx, key).Err()
}
