package authen

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"
	"fmt"
)

func GetEncodedPayload(timestamp, clientKey, content string) string {
	payload := fmt.Sprintf("%s.%s.%s", timestamp, clientKey, string(content))
	encodedPayload := base64.RawURLEncoding.EncodeToString([]byte(payload))

	return encodedPayload
}

func Sign(secret string, payload string) (string, error) {
	h := hmac.New(sha256.New, []byte(secret))
	_, err := h.Write([]byte(payload))
	if err != nil {
		return "", err
	}
	signature := hex.EncodeToString(h.Sum(nil))
	return signature, nil
}

func ValidateSignature(encodedPayload string, clientSecret string, signature string) bool {

	expectedSignature, _ := Sign(clientSecret, encodedPayload)

	return hmac.Equal([]byte(signature), []byte(expectedSignature))
}
