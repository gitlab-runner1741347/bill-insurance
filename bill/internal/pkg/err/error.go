package err

import (
	"errors"
)

var (
	RecordNotFound = errors.New("record not found")
)
