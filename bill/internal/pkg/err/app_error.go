package err

type AppError struct {
	StatusCode int
	RootErr    error
	Message    string
	Log        string
}

func NewFullErrorResponse(statusCode int, root error, msg, log string) *AppError {
	return &AppError{
		statusCode,
		root,
		msg,
		log,
	}
}

func (e *AppError) RootError() error {
	if err, ok := e.RootErr.(*AppError); ok {
		return err.RootError()
	}
	return e.RootErr
}

func ErrDB(err error) *AppError {
	return NewFullErrorResponse(0, err, "Some thing went wrong  with DB", "DB_ERR")
}

func ErrRecordNotFound(err error) *AppError {
	return NewFullErrorResponse(-1, err, "Record not found", "NF_ERR")
}

func (e *AppError) Error() string {
	// return string of root error
	return e.RootError().Error()
}
