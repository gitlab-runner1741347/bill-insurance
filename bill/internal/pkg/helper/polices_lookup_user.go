package helper

import (
	_const "git.kafefin.net/backend/bill-insurance/internal/const"
	"git.kafefin.net/backend/bill-insurance/internal/models"
	"git.kafefin.net/backend/bill-insurance/internal/pkg/convert"
	"git.kafefin.net/backend/bill-insurance/pb"
)

func SumTotalBenefitPaid(o *models.TotalBenefitPaid, e *models.ClaimRemain) {
	o.LossOrDeath = o.LossOrDeath + e.ClaimRemainLossOrDeathValue
	o.WholeAccident = o.WholeAccident + e.ClaimRemainWholeAccidentValue
	o.PartialAccident = o.PartialAccident + e.ClaimRemainPartialAccidentValue
	o.MedicalValue = o.MedicalValue + e.ClaimRemainMedicalValue
	o.DailyInHospital = o.DailyInHospital + e.ClaimRemainDailyInHospitalValue
	o.MaxDaysOfInHospital = o.MaxDaysOfInHospital + e.ClaimRemainDaysOfInHospital
}

func ConvertPoliciesToResponse(policies []*models.Policy) []*pb.DataPoliciesLookupUser {
	policiesResponse := make([]*pb.DataPoliciesLookupUser, 0)
	for _, value := range policies {
		var data = &pb.DataPoliciesLookupUser{
			PolicyId:                  value.Id,
			PolicyNumber:              value.PolicyNumber,
			OrderId:                   value.OrderID,
			PaymentDate:               convert.ConvertToProtoTimestamp(value.PaymentDate),
			PolicyStatus:              value.PolicyStatus,
			PolicyStatusValue:         _const.MappingStatusPolicy[_const.StatusPolicy(value.PolicyStatus)],
			EffectiveFrom:             convert.ConvertToProtoTimestamp(value.EffectiveFrom),
			EffectiveTo:               convert.ConvertToProtoTimestamp(value.EffectiveTo),
			CustomerName:              value.CustomerName,
			CustomerDob:               value.CustomerDOB,
			CustomerId:                value.CustomerID,
			CustomerPhone:             value.CustomerPhone,
			CustomerAddress:           value.CustomerAddress,
			PremiumAmount:             int64(value.TotalPremiumAmount),
			BillAmount:                int64(value.TotalBillAmount),
			TotalLossOrDeathValue:     int64(value.TotalLossOrDeathCoverageValue),
			TotalWholeAccidentValue:   int64(value.TotalWholeAccidentCoverageValue),
			TotalPartialAccidentValue: int64(value.TotalPartialAccidentCoverageValue),
			TotalMedicalValue:         int64(value.TotalMedicalCoverageValue),
			DailyInHospitalValue:      int64(value.DailyInHospitalCoverageValue),
			MaxDaysOfInHospital:       int64(value.MaxDaysOfInHospital),
			ECertUrl:                  value.ECertURL,
			CustomerEmail:             value.CustomerEmail,
			CreatedAt:                 convert.ConvertToProtoTimestamp(value.CreatedAt),
		}

		policiesResponse = append(policiesResponse, data)
	}
	return policiesResponse
}

func GetBenefitPaid(policies []*models.Policy) models.TotalBenefitPaid {
	var result models.TotalBenefitPaid
	for _, value := range policies {
		SumTotalBenefitPaid(&result, &value.ClaimRemain)
	}
	return result
}
