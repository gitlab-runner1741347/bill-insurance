package models_

type CalculatePremiumDTO struct {
	PremiumAmount                     int64 `json:"premium_amount"`
	TotalLossOrDeathCoverageValue     int64 `json:"total_loss_or_death_coverage_value"`
	TotalWholeAccidentCoverageValue   int64 `json:"total_whole_accident_coverage_value"`
	TotalPartialAccidentCoverageValue int64 `json:"total_partial_accident_coverage_value"`
	TotalMedicalCoverageValue         int64 `json:"total_medical_coverage_value"`
	DailyInHospitalCoverageValue      int64 `json:"daily_in_hospital_coverage_value"`
	MaxDaysOfInHospital               int64 `json:"max_days_of_in_hospital"`
}
