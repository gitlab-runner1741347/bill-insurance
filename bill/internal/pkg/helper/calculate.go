package helper

import (
	_const "git.kafefin.net/backend/bill-insurance/internal/const"
	models_ "git.kafefin.net/backend/bill-insurance/internal/pkg/helper/models"
	"math"
)

func Calculate(billType _const.BillType, am int64) *models_.CalculatePremiumDTO {
	structBillType, err := billType.Detail()
	if err != nil {
		return nil
	}
	productCode := structBillType.ProductCode.ProductCode

	switch productCode {
	case _const.BillPro1EBv:
		billAmount := float64(am)
		amount := billAmount / 100.0
		premiumAmount := int64(math.Min(math.Round(amount), 10000))
		value := int64(math.Min(100*billAmount, 100000000))
		return &models_.CalculatePremiumDTO{
			PremiumAmount:                     int64(math.Max(float64(premiumAmount), 1000)),
			TotalLossOrDeathCoverageValue:     value,
			TotalWholeAccidentCoverageValue:   value,
			TotalPartialAccidentCoverageValue: value,
			TotalMedicalCoverageValue:         int64(math.Min(2*billAmount, 2000000)),
			DailyInHospitalCoverageValue:      int64(math.Min(math.Round(0.5*billAmount), 500000)),
			MaxDaysOfInHospital:               3,
		}
	case _const.BillPro2EBv:
		return &models_.CalculatePremiumDTO{
			PremiumAmount:                     10000,
			TotalLossOrDeathCoverageValue:     100000000,
			TotalWholeAccidentCoverageValue:   100000000,
			TotalPartialAccidentCoverageValue: 100000000,
			TotalMedicalCoverageValue:         2000000,
			DailyInHospitalCoverageValue:      500000,
			MaxDaysOfInHospital:               3,
		}
	case _const.BillPro3EBv:
		return &models_.CalculatePremiumDTO{
			PremiumAmount:                     2000,
			TotalLossOrDeathCoverageValue:     20000000,
			TotalWholeAccidentCoverageValue:   20000000,
			TotalPartialAccidentCoverageValue: 20000000,
			TotalMedicalCoverageValue:         400000,
			DailyInHospitalCoverageValue:      100000,
			MaxDaysOfInHospital:               3,
		}
	default:
		return nil

	}

}
