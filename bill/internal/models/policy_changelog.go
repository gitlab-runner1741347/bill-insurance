package models

import "git.kafefin.net/backend/bill-insurance/pb"

type PolicyChangelog struct {
	ID                  uint64                      `sql:"type:BIGINT UNSIGNED;primary_key;auto_increment"`
	PolicyID            string                      `sql:"type:VARCHAR(22) NOT NULL;index"`
	PolicyNumber        string                      `sql:"type:VARCHAR(50);"`
	Action              string                      `sql:"type:VARCHAR(128)"`
	Actor               string                      `sql:"type:VARCHAR(128)"`
	ActorName           string                      `sql:"type:VARCHAR(128)"`
	ActorType           pb.ActorType                `sql:"type:INT"`
	KeyPath             string                      `sql:"type:VARCHAR(255)"`
	Comment             string                      `sql:"type:TEXT"`
	ChangelogAttributes []*PolicyChangelogAttribute `sql:"-" gorm:"foreignKey:ChangelogID"`
	Model               `sql:"squash"`
}

func (PolicyChangelog) TableName() string {
	return "policy_changelog"
}

const (
	SystemActor = "System"
)

type ActionType = string

const (
	ActionUpdateStatus ActionType = "UPDATE_STATUS"
	ActionUpdateInfo   ActionType = "UPDATE_INFO"
)
