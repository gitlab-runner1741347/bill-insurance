package models

import "time"

type Model struct {
	CreatedAt time.Time `sql:"type:DATETIME;default:CURRENT_TIMESTAMP"`
	UpdatedAt time.Time `sql:"type:DATETIME;default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}

func (m *Model) FormatCreatedAt(layout string) string {
	if m == nil || m.CreatedAt.IsZero() {
		return ""
	}

	return m.CreatedAt.Format(layout)
}

func (m *Model) FormatUpdatedAt(layout string) string {
	if m == nil || m.UpdatedAt.IsZero() {
		return ""
	}

	return m.UpdatedAt.Format(layout)
}

func AllModels() []interface{} {
	return []interface{}{
		Policy{},
		Bill{},
		ClaimRemain{},
		SendEmailLog{},
		PolicyChangelog{},
		PolicyChangelogAttribute{},
	}
}
