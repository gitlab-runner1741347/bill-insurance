package models

type TotalBenefitPaid struct {
	LossOrDeath         int64 `json:"loss_or_death"`
	WholeAccident       int64 `json:"whole_accident"`
	PartialAccident     int64 `json:"partial_accident"`
	MedicalValue        int64 `json:"medical_value"`
	DailyInHospital     int64 `json:"daily_in_hospital"`
	MaxDaysOfInHospital int64 `json:"max_days_of_in_hospital"`
}
