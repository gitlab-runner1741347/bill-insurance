package models

import (
	"git.kafefin.net/backend/bill-insurance/pb"
	sldMessagePb "git.kafefin.net/backend/saladin-message/pb"
	"gorm.io/datatypes"
	"time"
)

type SendEmailLogData struct {
	V1 *sldMessagePb.SendEmailTemplateRequest `json:"v1"`
}

type SendEmailLog struct {
	ID            uint64                               `sql:"type:BIGINT UNSIGNED;primary_key;auto_increment"`
	PolicyNumber  string                               `sql:"type:VARCHAR(128);index"`
	IdempotentKey string                               `sql:"type:VARCHAR(128);unique"`
	EmailParams   datatypes.JSONType[SendEmailLogData] `sql:"type:JSON"`
	Status        pb.SendEmailLogStatus                `sql:"type:INT"`

	CreatedAt time.Time `sql:"type:DATETIME;default:CURRENT_TIMESTAMP"`
	UpdatedAt time.Time `sql:"type:DATETIME;default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}

func (SendEmailLog) TableName() string {
	return "send_email_log"
}

type EmailState int

const (
	StateInit EmailState = iota
	StateSent
)
