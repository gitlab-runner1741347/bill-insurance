package models

import (
	"time"
)

// struct mapping database

type Policy struct {
	Id                                string    `sql:"primary_key;type:VARCHAR(128)"`
	OrderID                           string    `sql:"size:255;type:VARCHAR(128);not null;index"`
	PolicyNumber                      string    `sql:"size:255;type:VARCHAR(128);not null;index"`
	TotalBillAmount                   int       `sql:"not null"`
	TotalPaymentAmount                int       `sql:"not null"`
	TotalLossOrDeathCoverageValue     int       `sql:"not null"`
	TotalWholeAccidentCoverageValue   int       `sql:"not null"`
	TotalPartialAccidentCoverageValue int       `sql:"not null"`
	TotalMedicalCoverageValue         int       `sql:"not null"`
	DailyInHospitalCoverageValue      int       `sql:"not null"`
	MaxDaysOfInHospital               int       `sql:"not null"`
	BillingMonth                      int       `sql:"not null"`
	BillingYear                       int       `sql:"not null"`
	CreatedTimeNumber                 int64     `sql:"not null"`
	CustomerAddress                   string    `sql:"size:255;type:VARCHAR(128);not null"`
	CustomerDOB                       string    `sql:"size:255;type:VARCHAR(128);not null"`
	CustomerEmail                     string    `sql:"size:255;type:VARCHAR(128);not null"`
	CustomerID                        string    `sql:"size:255;type:VARCHAR(128);not null"`
	CustomerName                      string    `sql:"size:255;type:VARCHAR(128);not null"`
	CustomerPhone                     string    `sql:"size:255;type:VARCHAR(128);not null"`
	ECertURL                          string    `sql:"size:255;type:VARCHAR(128);not null"`
	EffectiveFrom                     time.Time `sql:"not null"`
	EffectiveTo                       time.Time `sql:"not null"`
	EffectiveTimeNumber               int64     `sql:"not null"`
	IssuedDate                        time.Time `sql:"not null"`
	PartnerOrg                        string    `sql:"size:255;type:VARCHAR(128);not null"`
	PaymentDate                       time.Time `sql:"not null"`
	PolicyStatus                      string    `sql:"size:255;type:VARCHAR(128);not null"`
	TotalPremiumAmount                int       `sql:"not null"`
	Insurer                           string    `sql:"size:255;type:VARCHAR(128);not null"`

	ClaimRemain ClaimRemain `sql:"-" gorm:"one2one:PolicyId"`
	Bills       []Bill      `sql:"-" gorm:"foreignKey:PolicyID"`
	Model       `sql:"squash"`
	//

}

func (b Policy) TableName() string {
	return "policy"
}
