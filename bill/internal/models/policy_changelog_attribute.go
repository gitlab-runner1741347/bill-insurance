package models

import "gorm.io/datatypes"

type AttributePatch struct {
	Added   []interface{} `json:"added"`
	Removed []interface{} `json:"removed"`
}

type ChangelogPatch = datatypes.JSONType[AttributePatch]
type ChangelogValue = datatypes.JSONType[interface{}]

type PolicyChangelogAttribute struct {
	ID          uint64         `sql:"type:BIGINT UNSIGNED;primary_key;auto_increment"`
	ChangelogID uint64         `sql:"type:BIGINT UNSIGNED;index"`
	KeyPath     string         `sql:"type:VARCHAR(255)"`
	IsEqual     bool           `sql:"type:TINYINT(1)"`
	OldValue    ChangelogValue `sql:"type:JSON"`
	NewValue    ChangelogValue `sql:"type:JSON"`
	Patch       ChangelogPatch `sql:"type:JSON"`
	Model       `sql:"squash"`
}

func (PolicyChangelogAttribute) TableName() string {
	return "policy_changelog_attribute"
}
