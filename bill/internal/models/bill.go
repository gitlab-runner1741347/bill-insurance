package models

type Bill struct {
	Id                                int64  `sql:"primary_key;auto_increment"`
	BillID                            string `sql:"type:VARCHAR(128);index"`
	BillType                          string `sql:"type:VARCHAR(128);not null"`
	BillAmount                        int    `sql:"not null"`
	PaymentAmount                     int    `sql:"not null"`
	TotalLossOrDeathCoverageValue     int    `sql:"not null"`
	TotalWholeAccidentCoverageValue   int    `sql:"not null"`
	TotalPartialAccidentCoverageValue int    `sql:"not null"`
	TotalMedicalCoverageValue         int    `sql:"not null"`
	DailyInHospitalCoverageValue      int    `sql:"not null"`
	MaxDaysOfInHospital               int    `sql:"not null"`
	PremiumAmount                     int    `sql:"not null"`
	PolicyNumber                      string `sql:"type:VARCHAR(128);not null"`
	PolicyID                          int    `sql:"type:VARCHAR(128);index;not null;constraint:OnUpdate:CASCADE,OnDelete:CASCADE"`
}

func (b Bill) TableName() string {
	return "bill"
}
