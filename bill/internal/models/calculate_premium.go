package models

import (
	"time"
)

// struct mapping data base

type CalculatePremium struct {
	ID            int64     `sql:"type:BIGINT"`
	BillAmount    int64     `sql:"type:BIGINT"`
	BillType      string    `sql:"type:VARCHAR(128)"`
	BillingMonth  int       `sql:"type:BIGINT"`
	BillingYear   int       `sql:"type:BIGINT"`
	CustomerDOB   time.Time `sql:"type:DATETIME"`
	CustomerPhone string    `sql:"type:VARCHAR(128)"`
	HandleStatus  string    `sql:"type:VARCHAR(128)"`
	RequestJSON   string    `sql:"type:VARCHAR(128)"`
	RequestTime   time.Time
}

func (b CalculatePremium) TableName() string {
	return "calculate_premium"
}
