package models

type ClaimRemain struct {
	CreatedBy                       string
	UpdatedBy                       string
	Model                           `sql:"squash"`
	OrderId                         string `sql:"size:255;not null"`
	PolicyId                        string `sql:"type:VARCHAR(128);not null;primary_key"`
	ClaimRemainLossOrDeathValue     int64  `sql:"not null"`
	ClaimRemainWholeAccidentValue   int64  `sql:"not null"`
	ClaimRemainPartialAccidentValue int64  `sql:"not null"`
	ClaimRemainMedicalValue         int64  `sql:"not null"`
	ClaimRemainDailyInHospitalValue int64  `sql:"not null"`
	ClaimRemainDaysOfInHospital     int64  `sql:"not null"`
}

func (ClaimRemain) TableName() string {
	return "claim_remain"
}
