package models

import "time"

type BpPolicy struct {
	OrderID                   string    `sql:"type:VARCHAR(100)" gorm:"primaryKey"`
	BillAmount                int       `sql:"type:INT"`
	BillID                    string    `sql:"type:VARCHAR(100)"`
	BillType                  string    `sql:"type:VARCHAR(30)"`
	BillingMonth              int       `sql:"type:INT"`
	BillingYear               int       `sql:"type:INT"`
	CreatedDate               time.Time `sql:"type:DATETIME;default:CURRENT_TIMESTAMP"`
	CreatedTimeNumber         int       `sql:"type:INT"`
	CustomerAddress           string    `sql:"type:VARCHAR(100)"`
	CustomerDOB               string    `sql:"type:VARCHAR(100)"`
	CustomerEmail             string    `sql:"type:VARCHAR(100)"`
	CustomerID                string    `sql:"type:VARCHAR(100)"`
	CustomerName              string    `sql:"type:VARCHAR(100)"`
	CustomerPhone             string    `sql:"type:VARCHAR(100)"`
	DailyInHospitalValue      int       `sql:"type:INT"`
	EcertUrl                  string    `sql:"type:VARCHAR(100)"`
	EffectiveFrom             time.Time `sql:"type:DATETIME"`
	EffectiveTo               time.Time `sql:"type:DATETIME"`
	IssuedDate                time.Time `sql:"type:DATETIME"`
	MaxDaysOfInHospital       int       `sql:"type:INT"`
	OrderSource               string    `sql:"type:VARCHAR(100)"`
	PaymentDate               time.Time `sql:"type:DATETIME"`
	PolicyID                  string    `sql:"type:VARCHAR(100)"`
	PolicyStatus              string    `sql:"type:VARCHAR(100)"`
	PolicyNumber              string    `sql:"type:VARCHAR(100)"`
	PremiumAmount             int       `sql:"type:INT"`
	PaymentAmount             int       `sql:"type:INT"`
	ProviderCode              string    `sql:"type:VARCHAR(100)"`
	TotalLossOrDeathValue     int       `sql:"type:INT"`
	TotalWholeAccidentValue   int       `sql:"type:INT"`
	TotalPartialAccidentValue int       `sql:"type:INT"`
	TotalMedicalValue         int       `sql:"type:INT"`
	EffectiveTimeNumber       int       `sql:"type:INT"`
	UserID                    int64     `sql:"type:INT"`
	VersionUpdate             int32     `sql:"type:INT"`
}

func (b BpPolicy) TableName() string {
	return "bp_policy"
}
