package models

import "time"

type BeneficiaryInformation struct {
	Id                int       `sql:"type:BIGINT"`
	CreatedBy         time.Time `sql:"type:DATETIME"`
	CreatedDate       time.Time `sql:"type:DATETIME"`
	UpdatedBy         time.Time `sql:"type:DATETIME"`
	UpdatedDate       time.Time `sql:"type:DATETIME"`
	BankAccountNumber string    `sql:"type:CHAR(128)"`
	BankAccountOwner  string    `sql:"type:CHAR(128)"`
	BankName          string    `sql:"type:CHAR(128)""`
}

func (BeneficiaryInformation) TableName() string {
	return "beneficiary_information "
}
