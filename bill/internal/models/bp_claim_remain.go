package models

type BpClaimRemain struct {
	OrderId                         string
	ClaimRemainDaysOfInHospital     int
	ClaimRemainLossOrDeathValue     int
	ClaimRemainMedicalValue         int
	ClaimRemainPartialAccidentValue int
	ClaimRemainWholeAccidentValue   int
	ClaimRemainDailyInHospitalValue int
}

func (b BpClaimRemain) TableName() string {
	return "bp_claim_remain"
}
