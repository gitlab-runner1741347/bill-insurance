package external

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"git.kafefin.net/backend/kitchen/l"
	sldMessagePb "git.kafefin.net/backend/saladin-message/pb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/credentials/insecure"
)

type SldMessageClient interface {
	sldMessagePb.SaladinMessageClient
}

type sldMessageClient struct {
	sldMessagePb.SaladinMessageClient
}

func NewSaladinMessageClient(address string, isInsecure bool) SldMessageClient {
	ll := l.New()
	var opts []grpc.DialOption
	if address != "" {
		opts = append(opts, grpc.WithAuthority(address))
	}

	if isInsecure {
		opts = append(opts, grpc.WithTransportCredentials(insecure.NewCredentials()))
	} else {
		systemRoots, err := x509.SystemCertPool()
		if err != nil {
			ll.Fatal("Failed get cert", l.Error(err))
		}

		cred := credentials.NewTLS(&tls.Config{
			RootCAs: systemRoots,
		})
		opts = append(opts, grpc.WithTransportCredentials(cred))
	}

	conn, err := grpc.DialContext(context.Background(), address, opts...)
	if err != nil {
		ll.Fatal("Failed to dial Base service", l.Error(err))
	}

	return sldMessageClient{
		sldMessagePb.NewSaladinMessageClient(conn),
	}
}
