package external

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	_const "git.kafefin.net/backend/bill-insurance/internal/const"
	"git.kafefin.net/backend/bill-insurance/internal/models"
	convert_ "git.kafefin.net/backend/bill-insurance/internal/pkg/convert"
	"git.kafefin.net/backend/kitchen/l"
	printCertPb "git.kafefin.net/backend/print-cert/pb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/credentials/insecure"
)

type PrintCertClient interface {
	GenerateECert(
		context.Context,
		*models.Policy,
	) (*printCertPb.GenerateECertResponse, error)
	GenerateECertV3(
		context.Context,
		[]*models.Policy,
	) (*printCertPb.GenerateECertV3Response, error)
}

type printCertClient struct {
	client printCertPb.PrintCertServiceClient
}

func NewPrintCertClient(address string, isInsecure bool) PrintCertClient {
	ll := l.New()
	var opts []grpc.DialOption
	if address != "" {
		opts = append(opts, grpc.WithAuthority(address))
	}

	if isInsecure {
		opts = append(opts, grpc.WithTransportCredentials(insecure.NewCredentials()))
	} else {
		systemRoots, err := x509.SystemCertPool()
		if err != nil {
			ll.Fatal("Failed get cert", l.Error(err))
		}

		cred := credentials.NewTLS(&tls.Config{
			RootCAs: systemRoots,
		})
		opts = append(opts, grpc.WithTransportCredentials(cred))
	}

	conn, err := grpc.DialContext(context.Background(), address, opts...)
	if err != nil {
		ll.Fatal("Failed to dial Base service", l.Error(err))
	}

	return printCertClient{
		client: printCertPb.NewPrintCertServiceClient(conn),
	}
}

func (p printCertClient) GenerateECert(
	ctx context.Context,
	policy *models.Policy,
) (*printCertPb.GenerateECertResponse, error) {
	//policyParams := convert_.PolicyModelToPrintCertPb(policy)

	//billTypeStruct, err := _const.BillType(policy.BillType).Detail()
	//if err != nil {
	//	return nil, err
	//}

	req := &printCertPb.GenerateECertRequest{
		//ProductCode:  string(billTypeStruct.ProductCode.ProductCode),
		//Params:       policyParams,
		//PolicyNumber: policy.OrderId,
	}

	return p.client.GenerateECert(ctx, req)
}

func (p printCertClient) GenerateECertV3(
	ctx context.Context,
	policies []*models.Policy,
) (*printCertPb.GenerateECertV3Response, error) {

	req := &printCertPb.GenerateECertV3Request{

		ClientId: _const.ClientID,
	}

	for index, value := range policies {
		policyParams := convert_.PolicyModelToPrintCertPb(value)

		// version 1: default bill[0]
		billTypeStruct, err := _const.BillType(value.Bills[0].BillType).Detail()
		if err != nil {
			return nil, err
		}
		req.Data = append(req.Data, &printCertPb.GenerateECertV3RequestData{
			ProductCode: string(billTypeStruct.ProductCode.ProductCode),
			Params:      policyParams,
			Index:       int32(index),
			FileName:    value.PolicyNumber + ".pdf",
		})
	}

	return p.client.GenerateECertV3(ctx, req)
}
