package external

import (
	"context"
	"crypto/tls"
	"crypto/x509"

	"git.kafefin.net/backend/kitchen/l"
	centralPolicyPb "git.kafefin.net/backend/saladin-central-proto/central_policy"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/credentials/insecure"
)

type CentralPolicyClient interface {
	UpsertPolicies(ctx context.Context, policies []*centralPolicyPb.CentralPolicyMessage) (*centralPolicyPb.InternalUpsertArrayPoliciesResponse, error)
}

type centralPolicyClient struct {
	client centralPolicyPb.CentralPolicyInternalClient
}

func NewCentralPolicyClient(address string, isInsecure bool) CentralPolicyClient {
	ll := l.New()
	var opts []grpc.DialOption
	if address != "" {
		opts = append(opts, grpc.WithAuthority(address))
	}

	if isInsecure {
		opts = append(opts, grpc.WithTransportCredentials(insecure.NewCredentials()))
	} else {
		systemRoots, err := x509.SystemCertPool()
		if err != nil {
			ll.Fatal("Failed get cert", l.Error(err))
		}

		cred := credentials.NewTLS(&tls.Config{
			RootCAs: systemRoots,
		})
		opts = append(opts, grpc.WithTransportCredentials(cred))
	}

	conn, err := grpc.DialContext(context.Background(), address, opts...)
	if err != nil {
		ll.Fatal("Failed to dial Base service", l.Error(err))
	}

	return centralPolicyClient{
		client: centralPolicyPb.NewCentralPolicyInternalClient(conn),
	}
}

func (p centralPolicyClient) UpsertPolicies(ctx context.Context, policies []*centralPolicyPb.CentralPolicyMessage) (*centralPolicyPb.InternalUpsertArrayPoliciesResponse, error) {
	req := &centralPolicyPb.InternalUpsertArrayPoliciesRequest{
		Policies: policies,
	}
	return p.client.InternalUpsertPolicies(ctx, req)
}
