package external

import (
	"context"
	"crypto/tls"
	"crypto/x509"

	"git.kafefin.net/backend/kitchen/l"
	pb "git.kafefin.net/backend/saladin-central-proto/saladin_user"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

type SaladinUserClient interface {
	pb.SaladinUserClient
}

type saladinUserClient struct {
	pb.SaladinUserClient
}

func NewSaladinUserClient(address string, insecure bool) SaladinUserClient {
	ll := l.New()
	var opts []grpc.DialOption
	if address != "" {
		opts = append(opts, grpc.WithAuthority(address))
	}

	if insecure {
		opts = append(opts, grpc.WithInsecure())
	} else {
		systemRoots, err := x509.SystemCertPool()
		if err != nil {
			ll.Fatal("Failed get cert", l.Error(err))
		}

		cred := credentials.NewTLS(&tls.Config{
			RootCAs: systemRoots,
		})
		opts = append(opts, grpc.WithTransportCredentials(cred))
	}

	conn, err := grpc.DialContext(context.Background(), address, opts...)
	if err != nil {
		ll.Fatal("Failed to dial Base service", l.Error(err))
	}

	return saladinUserClient{
		SaladinUserClient: pb.NewSaladinUserClient(conn),
	}
}
