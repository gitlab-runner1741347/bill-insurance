package external

import (
	"context"

	"git.kafefin.net/backend/kitchen/nap"
	"go.opentelemetry.io/contrib/instrumentation/net/http/otelhttp"
	"go.opentelemetry.io/otel/trace"
)

const (
	ReGenerateEcertPath = "/api/internal/update-e-cert"
)

type BillProtectionSvc interface {
	ReGenerateEcert(ctx context.Context, orderID string) error
}

type billProtectionClient struct {
	host string
	nap  *nap.Nap
}

func NewBillProtectionService(tp trace.TracerProvider, host string) BillProtectionSvc {
	nap := nap.NewOtel(otelhttp.WithTracerProvider(tp)).Base(host)
	s := billProtectionClient{
		host: host,
		nap:  nap,
	}

	return &s
}

func (b *billProtectionClient) ReGenerateEcert(ctx context.Context, orderID string) error {
	queryPr := map[string]string{
		"orderId": orderID,
	}
	resp, err := b.nap.SetContext(ctx).Get(ReGenerateEcertPath).QueryParams(queryPr).ReceiveSuccess(nil)
	if err != nil {
		return err
	}

	if resp.StatusCode != 200 {
		return err
	}

	return nil
}
