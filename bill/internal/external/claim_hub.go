package external

import (
	"context"
	"crypto/tls"
	"crypto/x509"

	"git.kafefin.net/backend/kitchen/l"
	clamHubPb "git.kafefin.net/backend/saladin-central-proto/claim_hub"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/credentials/insecure"
)

type ClaimHubSvc interface {
	InternalClaimRemains(ctx context.Context, req *clamHubPb.InternalClaimRemainsRequest) (*clamHubPb.InternalClaimRemainsResponse, error)
	InternalGetStatusClaims(ctx context.Context, req *clamHubPb.InternalGetStatusClaimsRequest) (*clamHubPb.InternalGetStatusClaimsResponse, error)
}

type claimHubClient struct {
	client clamHubPb.ClaimHubBPServiceClient
}

func NewClaimHubClient(address string, isInsecure bool) ClaimHubSvc {
	ll := l.New()
	var opts []grpc.DialOption
	if address != "" {
		opts = append(opts, grpc.WithAuthority(address))
	}

	if isInsecure {
		opts = append(opts, grpc.WithTransportCredentials(insecure.NewCredentials()))
	} else {
		systemRoots, err := x509.SystemCertPool()
		if err != nil {
			ll.Fatal("Failed get cert", l.Error(err))
		}

		cred := credentials.NewTLS(&tls.Config{
			RootCAs: systemRoots,
		})
		opts = append(opts, grpc.WithTransportCredentials(cred))
	}

	conn, err := grpc.DialContext(context.Background(), address, opts...)
	if err != nil {
		ll.Fatal("Failed to dial Base service", l.Error(err))
	}

	return claimHubClient{
		client: clamHubPb.NewClaimHubBPServiceClient(conn),
	}
}

func (c claimHubClient) InternalClaimRemains(ctx context.Context, req *clamHubPb.InternalClaimRemainsRequest) (*clamHubPb.InternalClaimRemainsResponse, error) {
	return c.client.InternalClaimRemains(ctx, req)
}

func (c claimHubClient) InternalGetStatusClaims(ctx context.Context, req *clamHubPb.InternalGetStatusClaimsRequest) (*clamHubPb.InternalGetStatusClaimsResponse, error) {
	return c.client.InternalGetStatusClaims(ctx, req)
}
