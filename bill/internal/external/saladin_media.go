package external

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"git.kafefin.net/backend/kitchen/l"
	"git.kafefin.net/backend/saladin-media/pb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

type SaladinMediaClient struct {
	Client pb.SaladinMediaClient
}

func NewSaladinMediaClient(address string, insecure bool) SaladinMediaClient {
	ll := l.New()
	var opts []grpc.DialOption
	if address != "" {
		opts = append(opts, grpc.WithAuthority(address))
	}

	if insecure {
		opts = append(opts, grpc.WithInsecure())
	} else {
		systemRoots, err := x509.SystemCertPool()
		if err != nil {
			ll.Fatal("Failed get cert", l.Error(err))
		}

		cred := credentials.NewTLS(&tls.Config{
			RootCAs: systemRoots,
		})
		opts = append(opts, grpc.WithTransportCredentials(cred))
	}

	conn, err := grpc.DialContext(context.Background(), address, opts...)
	if err != nil {
		ll.Fatal("Failed to dial Base service", l.Error(err))
	}

	return SaladinMediaClient{
		Client: pb.NewSaladinMediaClient(conn),
	}
}
