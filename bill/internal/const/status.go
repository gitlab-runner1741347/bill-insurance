package _const

type StatusPolicy = string

const (
	StatusCreatedOk = "CREATED_OK"
	StatusCancelled = "CANCELLED"
	StatusExpired   = "EXPIRED"
)

const (
	SystemCreated = "SYSTEM"
)

var MappingStatusPolicy = map[StatusPolicy]string{
	StatusCreatedOk: "Thành công",
	StatusCancelled: "Hủy",
	StatusExpired:   "Hết hiệu lực",
}
