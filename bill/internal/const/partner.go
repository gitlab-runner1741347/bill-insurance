package _const

import "github.com/cockroachdb/errors"

type Org = string

const (
	NonOrg  = ""
	ZaloOrg = "ZALOPAY"
)

const ProductDetailUrl = `https://www.saladin.vn/bao-hiem-lien-ket/bao-hiem-nguoi-thanh-toan`

type Insurer = string

const (
	BaoVietInsurer = "BAOVIET"
)

type ProductCode string
type StatusProductCode = string

const (
	BillPro1EBv ProductCode = "BILL_PRO1_E_BV"
	BillPro2EBv ProductCode = "BILL_PRO2_E_BV"
	BillPro3EBv ProductCode = "BILL_PRO3_E_BV"
)

const (
	Electric        BillType = "ELECTRIC"
	Water           BillType = "WATER"
	ConsumerFinance BillType = "CONSUMER_FINANCE"
	Education       BillType = "EDUCATION"
	Internet        BillType = "INTERNET"
	Tv              BillType = "TV"
)

const (
	Active   StatusProductCode = "Active"
	UnActive StatusProductCode = "UnActive"
)

type StructProductCode struct {
	ProductCode      ProductCode
	Name             string
	Insurer          Insurer
	ProductDetailUrl string
	Status           StatusProductCode
}

func (p ProductCode) Detail() (*StructProductCode, error) {
	var result StructProductCode
	value := p

	switch value {
	case BillPro1EBv:
		result = StructProductCode{
			ProductCode:      p,
			Name:             "Bảo hiểm thanh toán hóa đơn",
			Insurer:          BaoVietInsurer,
			ProductDetailUrl: ProductDetailUrl,
			Status:           Active,
		}
	case BillPro2EBv:
		result = StructProductCode{
			ProductCode:      p,
			Name:             "Bảo hiểm thanh toán hóa đơn",
			Insurer:          BaoVietInsurer,
			ProductDetailUrl: ProductDetailUrl,
			Status:           Active,
		}

	case BillPro3EBv:
		result = StructProductCode{
			ProductCode:      p,
			Name:             "Bảo hiểm thanh toán hóa đơn",
			Insurer:          BaoVietInsurer,
			ProductDetailUrl: ProductDetailUrl,
			Status:           Active,
		}
	default:
		return nil, errors.New("ProductCode not found")
	}
	return &result, nil
}

type BillType string
type BillTypeStruct struct {
	BillType    BillType
	Notes       string
	ProductCode StructProductCode
}

func (b BillType) Detail() (*BillTypeStruct, error) {
	value := b
	var result BillTypeStruct
	switch value {
	case Electric:
		var productCode ProductCode
		productCode = "BILL_PRO1_E_BV"
		structProductCode, err := productCode.Detail()
		if err != nil {
			return nil, err
		}

		result = BillTypeStruct{
			BillType:    b,
			Notes:       "Điện",
			ProductCode: *structProductCode,
		}

	case Water:
		var productCode ProductCode
		productCode = "BILL_PRO1_E_BV"
		structProductCode, err := productCode.Detail()
		if err != nil {
			return nil, err
		}

		result = BillTypeStruct{
			BillType:    b,
			Notes:       "Nước",
			ProductCode: *structProductCode,
		}

	case ConsumerFinance:
		var productCode ProductCode
		productCode = "BILL_PRO2_E_BV"
		structProductCode, err := productCode.Detail()
		if err != nil {
			return nil, err
		}

		result = BillTypeStruct{
			BillType:    b,
			Notes:       "Vay tiêu dùng",
			ProductCode: *structProductCode,
		}

	case Education:
		var productCode ProductCode
		productCode = "BILL_PRO2_E_BV"
		structProductCode, err := productCode.Detail()
		if err != nil {
			return nil, err
		}

		result = BillTypeStruct{
			BillType:    b,
			Notes:       "Học Phí",
			ProductCode: *structProductCode,
		}

	case Internet:
		var productCode ProductCode
		productCode = "BILL_PRO3_E_BV"
		structProductCode, err := productCode.Detail()
		if err != nil {
			return nil, err
		}

		result = BillTypeStruct{
			BillType:    b,
			Notes:       "Học Phí",
			ProductCode: *structProductCode,
		}
	case Tv:
		var productCode ProductCode
		productCode = "BILL_PRO3_E_BV"
		structProductCode, err := productCode.Detail()
		if err != nil {
			return nil, err
		}

		result = BillTypeStruct{
			BillType:    b,
			Notes:       "Truyền hình",
			ProductCode: *structProductCode,
		}

	default:
		return nil, errors.New("ProductCode not found")
	}

	return &result, nil

}
