package must

import (
	"git.kafefin.net/backend/bill-insurance/config"
	"git.kafefin.net/backend/kitchen/id"
	"git.kafefin.net/backend/kitchen/l"
)

func NewVendorIDGenMap(vendors []*config.SaladinVendor) map[uint32]*id.ID {
	vendorMap := make(map[uint32]*id.ID, len(vendors))
	for i := range vendors {
		idGenerator, err := id.New(vendors[i].VendorSuffix)
		if err != nil {
			ll.Fatal("error create id generator", l.Any("suffix", vendors[i].VendorSuffix))
		}
		vendorMap[vendors[i].VendorID] = idGenerator
	}

	return vendorMap
}
