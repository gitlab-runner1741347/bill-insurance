package repositories

import (
	"context"
	"time"

	"git.kafefin.net/backend/bill-insurance/internal/models"
	"git.kafefin.net/backend/bill-insurance/pb"
)

type Paging struct {
	Limit  int
	Offset int
}

func NewPaging(limit int64, offset int64) *Paging {
	if limit == 0 {
		return &Paging{
			Limit:  50,
			Offset: int(offset),
		}
	}
	return &Paging{
		Limit:  int(limit),
		Offset: int(offset),
	}
}

//var _ Repository = &mysql.Store{}

type Repository interface {
	Transaction(txFunc func(Repository) error) error
	PolicyRepository
	SendEmailLogRepository
	ClaimRemainRepository
	BillRepository
	Changelog
	ChangelogAttributes
}

type PostgresqlRepository interface {
	Transaction(txFunc func(PostgresqlRepository) error) error
	BpPolicy
	BpClaimRemain
	Changelog
	ChangelogAttributes
}

type BpPolicy interface {
	UpdateUserIDByPhone(ctx context.Context, phone string, userID int64) error
	GetLatestRecordByUserId(ctx context.Context, userId int64) (*models.BpPolicy, error)
	ListByUserId(ctx context.Context, userId int64) ([]*models.BpPolicy, error)
	GetPolicyByID(ctx context.Context, id string) (*models.BpPolicy, error)
	GetPolicyByPolicyNumber(ctx context.Context, policyNumber string) (*models.BpPolicy, error)
	UpdatePolicy(ctx context.Context, policy models.BpPolicy) (*models.BpPolicy, error)
	GetEffectivePolicies(ctx context.Context, phone string, time *time.Time) ([]*models.BpPolicy, error)
	GetEffectivePoliciesExtension(ctx context.Context, phone string, time *time.Time) ([]*models.BpPolicy, error)
	UpdateUpdateVersion(ctx context.Context, policyID string) error
	GetByPolicyIdAndOrg(ctx context.Context, id string, org string) (*models.BpPolicy, error)
	PaginatePoliciesWithOrg(ctx context.Context, req *pb.AdminListPoliciesRequest) ([]*models.BpPolicy, int64, error)
	GetPoliciesByIds(ctx context.Context, ids []string) ([]*models.BpPolicy, error)
	UpdateStatus(ctx context.Context, policyID string, status string) error
	UpdateStatusRecordsFromCretedOk(ctx context.Context, policyIDs []string, status string) error
	InternalListPolicies(ctx context.Context, req *pb.InternalListPoliciesRequest) ([]*models.BpPolicy, error)
}

type BpClaimRemain interface {
	GetByOrderId(ctx context.Context, orderID string) (*models.BpClaimRemain, error)
}

type Changelog interface {
	GetChangeLogByID(ctx context.Context, id int64) (*models.PolicyChangelog, error)
	CreateOneChangeLog(ctx context.Context, policy *models.PolicyChangelog) error
	ListChangeLogByPolicyID(ctx context.Context, policyID string) ([]*models.PolicyChangelog, error)
}

type ChangelogAttributes interface {
	GetChangeLogAttByID(ctx context.Context, id int64) (*models.PolicyChangelogAttribute, error)
	CreateOneChangeLogAtt(ctx context.Context, policy *models.PolicyChangelogAttribute) error
	BatchCreateChangeLogAtts(ctx context.Context, ms []*models.PolicyChangelogAttribute) ([]*models.PolicyChangelogAttribute, error)
}

type BillRepository interface {
	ListBillByPolicyId(ctx context.Context, policyID string) ([]*models.Bill, error)
	ListBillByOrderId(ctx context.Context, orderID string) ([]*models.Bill, error)
	GetBillByBillId(ctx context.Context, billID string) (*models.Bill, error)
}

type PolicyRepository interface {
	UpdateECertURL(ctx context.Context, policyID string, ecertURL string) error
	GetPolicyByID(ctx context.Context, id string) (*models.Policy, error)
	GetPolicyByIdWithPreloadBills(ctx context.Context, id string) (*models.Policy, error)
	GetPolicyByIdWithPreloadBillsWithOrg(ctx context.Context, id string, org string) (*models.Policy, error)
	GetPolicyByIDAndOrg(ctx context.Context, id string, org string) (*models.Policy, error)
	SavePolicy(ctx context.Context, m *models.Policy) (*models.Policy, error)
	CreateOnePolicy(ctx context.Context, m *models.Policy) (*models.Policy, error)
	PaginatePolicies(ctx context.Context, req *pb.AdminListPoliciesRequest) ([]*models.Policy, int64, error)
	PaginateEffectivePolies(ctx context.Context, req *pb.ListEffectivePoliciesRequest) ([]*models.Policy, int64, error)
	PaginateInsuranceLookup(ctx context.Context, req *pb.InsuranceLookupUserRequest) ([]*models.Policy, int64, error)
	FindByConditions(ctx context.Context, condition map[string]interface{}) (*models.Policy, error)
	GetPolicyByOrderId(ctx context.Context, orderID string) ([]*models.Policy, error)
	GetPoliciesEffectiveByPhoneVsIncident(ctx context.Context, customerPhone string, dateOfIncident *time.Time) ([]*models.Policy, error)
	PaginateAllInsuranceLookup(ctx context.Context, req *pb.InsuranceLookupUserRequest) ([]*models.Policy, int64, error)
	InsertArrayPolicy(ctx context.Context, policies []*models.Policy) error
}

type SendEmailLogRepository interface {
	GetByID(ctx context.Context, id int64) (*models.SendEmailLog, error)
	GetByIdempotentKey(ctx context.Context, key string) (*models.SendEmailLog, error)
	GetByPolicyNumber(ctx context.Context, policyNumber string) (*models.SendEmailLog, error)
	CreateOne(ctx context.Context, m *models.SendEmailLog) (*models.SendEmailLog, error)
	Save(ctx context.Context, m *models.SendEmailLog) (*models.SendEmailLog, error)
	UpdateTurnSharePolicyByEmail(ctx context.Context, key string, numbers int) error
}

type ClaimRemainRepository interface {
	SaveClaimRemain(ctx context.Context, m *models.ClaimRemain) (*models.ClaimRemain, error)
	CreateOneClaimRemain(ctx context.Context, m *models.ClaimRemain) (*models.ClaimRemain, error)
	GetClaimRemainByPolicyId(ctx context.Context, id string) (*models.ClaimRemain, error)
	GetClaimRemainByPolicyIdAndOrg(ctx context.Context, id string, org string) (*models.ClaimRemain, error)
	GetClaimRemainByOrderId(ctx context.Context, id string) (*models.ClaimRemain, error)
	ListClaimRemainByPolicyId(ctx context.Context, id string) ([]models.ClaimRemain, error)
	GetClaimRemainsByPoliciesId(ctx context.Context, ids []string) ([]*models.ClaimRemain, error)
}
