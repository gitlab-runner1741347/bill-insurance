package mysql

import (
	"context"

	"git.kafefin.net/backend/bill-insurance/internal/models"
	"gorm.io/gorm"
)

func (s *Store) GetByID(ctx context.Context, id int64) (*models.SendEmailLog, error) {
	db := s.db.WithContext(ctx)
	var result *models.SendEmailLog
	return result, db.First(result, "id = ?", id).Error
}

func (s *Store) GetByIdempotentKey(ctx context.Context, key string) (*models.SendEmailLog, error) {
	db := s.db.WithContext(ctx)
	var result *models.SendEmailLog
	return result, db.First(result, "idempotent_key = ?", key).Error
}

func (s *Store) GetByPolicyNumber(ctx context.Context, policyNumber string) (*models.SendEmailLog, error) {
	db := s.db.WithContext(ctx)
	var result *models.SendEmailLog
	return result, db.First(result, "policy_number = ?", policyNumber).Error
}

func (s *Store) CreateOne(ctx context.Context, m *models.SendEmailLog) (*models.SendEmailLog, error) {
	return m, s.db.WithContext(ctx).Create(m).Error
}

func (s *Store) Save(ctx context.Context, m *models.SendEmailLog) (*models.SendEmailLog, error) {
	return m, s.db.WithContext(ctx).Save(m).Error
}

func (s *Store) UpdateTurnSharePolicyByEmail(ctx context.Context, key string, numbers int) error {
	return s.db.WithContext(ctx).Table(models.SendEmailLog{}.TableName()).Where("idempotent_key = ?", key).UpdateColumn("share_count", gorm.Expr("share_count + ?", numbers)).Error
}
