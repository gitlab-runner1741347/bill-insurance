package mysql

import (
	"context"
	"time"

	_const "git.kafefin.net/backend/bill-insurance/internal/const"
	"git.kafefin.net/backend/bill-insurance/internal/models"
	"git.kafefin.net/backend/bill-insurance/internal/pkg/convert"
	"git.kafefin.net/backend/bill-insurance/internal/repositories"
	"git.kafefin.net/backend/bill-insurance/pb"
)

func (s *Store) FindByConditions(ctx context.Context, condition map[string]interface{}) (*models.Policy, error) {
	var m *models.Policy
	return m, s.db.WithContext(ctx).Where(condition).Error
}

func (s *Store) UpdateECertURL(ctx context.Context, policyID string, ecertURL string) error {
	return s.db.WithContext(ctx).Where("id = ?", policyID).Update("e_cert_url", ecertURL).Error
}

func (s *Store) GetPolicyByID(ctx context.Context, id string) (*models.Policy, error) {
	var m *models.Policy
	return m, s.db.WithContext(ctx).First(&m, "id = ?", id).Error
}

func (s *Store) GetPolicyByIDAndOrg(ctx context.Context, id string, org string) (*models.Policy, error) {
	var m *models.Policy
	return m, s.db.WithContext(ctx).First(&m, "id = ? AND (partner_org = ? OR insurer = ?)", id, org, org).Error
}

func (s *Store) GetPolicyByIdWithPreloadBills(ctx context.Context, id string) (*models.Policy, error) {
	var m *models.Policy

	err := s.db.WithContext(ctx).Preload("Bills").First(&m, "id = ?", id).Error
	return m, err
}

func (s *Store) GetPolicyByIdWithPreloadBillsWithOrg(ctx context.Context, id string, org string) (*models.Policy, error) {
	var m *models.Policy

	err := s.db.WithContext(ctx).Preload("Bills").First(&m, "id = ? AND (partner_org = ? OR insurer = ?)", id, org, org).Error
	return m, err

}

func (s *Store) GetPolicyByOrderId(ctx context.Context, orderID string) ([]*models.Policy, error) {
	var m []*models.Policy
	return m, s.db.WithContext(ctx).Find(&m, "order_id = ?", orderID).Error
}

func (s *Store) SavePolicy(ctx context.Context, m *models.Policy) (*models.Policy, error) {
	return m, s.db.WithContext(ctx).Save(m).Error
}

func (s *Store) CreateOnePolicy(ctx context.Context, m *models.Policy) (*models.Policy, error) {
	return m, s.db.WithContext(ctx).Create(m).Error
}

func (s *Store) PaginateInsuranceLookup(ctx context.Context, req *pb.InsuranceLookupUserRequest) ([]*models.Policy, int64, error) {
	var policies []*models.Policy
	var total int64

	db := s.db.WithContext(ctx).Model(&models.Policy{})
	if req.GetCustomerPhone() != "" {
		db.Where("customer_phone = ?", req.GetCustomerPhone())
	}

	if req.GetNid() != "" {
		db.Where("customer_id = ?", req.GetNid())
	}

	if req.GetDataOfIncident() != nil {
		accidentTime := convert.ConvertProtoTimestamp(req.GetDataOfIncident())
		db.Where("? BETWEEN policy.effective_from AND policy.effective_to", accidentTime)
	}

	db.Where("policy_status = ?", _const.StatusCreatedOk)

	paging := repositories.NewPaging(req.GetLimit(), req.GetOffset())
	db.Count(&total).Order("effective_to desc").Offset(paging.Offset).Limit(paging.Limit)
	err := db.Find(&policies).Error

	return policies, total, err
}

func (s *Store) PaginateAllInsuranceLookup(ctx context.Context, req *pb.InsuranceLookupUserRequest) ([]*models.Policy, int64, error) {
	var policies []*models.Policy
	var total int64

	db := s.db.WithContext(ctx).Model(&models.Policy{})
	if req.GetCustomerPhone() != "" {
		db.Where("customer_phone = ?", req.GetCustomerPhone())
	}

	if req.GetNid() != "" {
		db.Where("customer_id = ?", req.GetNid())
	}

	if req.GetDataOfIncident() != nil {
		accidentTime := convert.ConvertProtoTimestamp(req.GetDataOfIncident())
		db.Where("? BETWEEN policy.effective_from AND policy.effective_to", accidentTime)
	}

	db.Where("policy_status = ?", _const.StatusCreatedOk)

	db.Count(&total)
	err := db.Preload("ClaimRemain").Find(&policies).Error

	return policies, total, err
}

func (s *Store) PaginateEffectivePolies(ctx context.Context, req *pb.ListEffectivePoliciesRequest) ([]*models.Policy, int64, error) {
	db := s.db.WithContext(ctx).Table(models.Policy{}.TableName())
	var policies []*models.Policy
	var total int64

	//db.Preload("ClaimRemain")
	//db.Find(&policies)

	if req.GetCustomerPhone() != "" {
		db.Where("customer_phone = ?", req.GetCustomerPhone())
	}

	db.Where("? BETWEEN policy.effective_from AND policy.effective_to", time.Now())
	db.Count(&total).Order("effective_to desc")
	paging := repositories.NewPaging(req.GetLimit(), req.GetOffset())

	err := db.Preload("Bills").Preload("ClaimRemain").Offset(paging.Offset).Limit(paging.Limit).Find(&policies).Error

	return policies, total, err
}

func (s *Store) PaginatePolicies(ctx context.Context, req *pb.AdminListPoliciesRequest) ([]*models.Policy, int64, error) {
	var policies []*models.Policy
	var total int64

	db := s.db.WithContext(ctx).Table(models.Policy{}.TableName())

	if req.GetPolicyStatus() != pb.PolicyStatus_UNSPECIFIED {
		db.Where("policy_status = ?", req.PolicyStatus)
	}

	if req.GetPolicyId() != "" {
		db.Where("id = ?", req.PolicyId)
	}

	if req.GetOrderId() != "" {
		db.Where("order_id = ?", req.OrderId)
	}

	if req.GetPolicyNumber() != "" {
		db.Where("policy_number = ?", req.PolicyNumber)
	}

	if req.GetCustomerName() != "" {
		db.Where("customer_name = ?", req.CustomerName)
	}

	if req.GetCustomerPhone() != "" {
		db.Where("customer_phone = ?", req.CustomerPhone)
	}

	if req.GetFromDate() != nil {
		fromDateTime := convert.ConvertProtoTimestamp(req.GetFromDate())
		db.Where("policy.created_at>= ?", fromDateTime)
	}

	if req.GetToDate() != nil {
		toDateTime := convert.ConvertProtoTimestamp(req.GetToDate())
		db.Where("policy.created_at <= ?", toDateTime.AddDate(0, 0, 1))
	}

	paging := repositories.NewPaging(req.GetLimit(), req.GetOffset())

	db.Count(&total).Order("created_at desc").Offset(paging.Offset)

	return policies, total, db.Preload("Bills").Preload("ClaimRemain").Limit(paging.Limit).Find(&policies).Error
}

func (s *Store) GetPoliciesEffectiveByPhoneVsIncident(ctx context.Context, customerPhone string, dateOfIncident *time.Time) ([]*models.Policy, error) {
	db := s.db.WithContext(ctx).Table(models.Policy{}.TableName())
	var policies []*models.Policy
	if customerPhone != "" {
		db.Where("customer_phone = ?", customerPhone)
	}

	db.Preload("ClaimRemain") // Preload the "ClaimRemain" relationship

	if dateOfIncident != nil {
		db.Where("? BETWEEN policy.effective_from AND policy.effective_to", *dateOfIncident)
	}

	db.Where("policy_status = ?", _const.StatusCreatedOk)

	db.Order("effective_to DESC")

	return policies, db.Find(&policies).Error // Use "Find" instead of "First" to fetch multiple policies
}

func (s *Store) InsertArrayPolicy(ctx context.Context, policies []*models.Policy) error {
	db := s.db.WithContext(ctx).Table(models.Policy{}.TableName())

	result := db.Create(&policies)
	if result.Error != nil {
		return result.Error
	}

	return nil
}
