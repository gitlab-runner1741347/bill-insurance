package mysql

import (
	"context"

	"git.kafefin.net/backend/bill-insurance/internal/models"
)

func (s Store) GetChangeLogByID(ctx context.Context, id int64) (*models.PolicyChangelog, error) {
	var m models.PolicyChangelog

	return &m, s.db.WithContext(ctx).First(&m, "id = ?", id).Error
}

func (s Store) CreateOneChangeLog(ctx context.Context, policy *models.PolicyChangelog) error {
	return s.db.WithContext(ctx).Create(policy).Error
}

func (s Store) ListChangeLogByPolicyID(ctx context.Context, policyID string) ([]*models.PolicyChangelog, error) {
	var m []*models.PolicyChangelog
	err := s.db.WithContext(ctx).Table(models.PolicyChangelog{}.TableName()).Preload("ChangelogAttributes").Where("policy_id = ?", policyID).Order("created_at desc").Find(&m).Error

	if err != nil {
		return nil, err
	}

	return m, nil
}
