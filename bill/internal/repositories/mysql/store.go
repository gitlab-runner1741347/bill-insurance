package mysql

import (
	"git.kafefin.net/backend/bill-insurance/config"
	"git.kafefin.net/backend/bill-insurance/internal/models"
	"git.kafefin.net/backend/bill-insurance/internal/repositories"
	"git.kafefin.net/backend/kitchen/l"
	"gorm.io/gorm"
)

type Store struct {
	db     *gorm.DB
	logger l.Logger
	cfg    *config.Config
}

func NewMySQLStore(logger l.Logger, db *gorm.DB, cfg *config.Config) *Store {
	q := Store{
		logger: logger,
		db:     db,
		cfg:    cfg,
	}
	return &q
}

func (s *Store) Ping() error {
	sqlDB, err := s.db.DB()
	if err != nil {
		return err
	}
	return sqlDB.Ping()
}

func (s *Store) WithTx(tx *gorm.DB) *Store {
	nr := *s
	nr.db = tx
	return &nr
}

func (s *Store) Transaction(txFunc func(a repositories.Repository) error) (err error) {
	// start new transaction
	tx := s.db.Begin()
	defer func() {
		p := recover()
		switch {
		case p != nil:
			execErr := tx.Rollback().Error
			if execErr != nil {
				s.logger.Error("error exec rollback", l.Error(execErr))
			}
			panic(p) // re-throw panic after Rollback
		case err != nil:
			execErr := tx.Rollback().Error // err is non-nil; don't change it
			if execErr != nil {
				s.logger.Error("error exec rollback", l.Error(execErr))
			}
		default:
			err = tx.Commit().Error // err is nil; if Commit returns error update err
		}
	}()
	return txFunc(s.WithTx(tx))
}

func InitAutoMigrate(s *Store) error {

	if err := s.db.Exec("SET FOREIGN_KEY_CHECKS = 0").Error; err != nil {
		return err
	}

	s.db.DisableForeignKeyConstraintWhenMigrating = true

	err := s.db.AutoMigrate(&models.Policy{})
	if err != nil {
		return err
	}

	err = s.db.AutoMigrate(&models.ClaimRemain{})
	if err != nil {
		return err
	}
	err = s.db.AutoMigrate(&models.Bill{})
	if err != nil {
		return err
	}
	err = s.db.AutoMigrate(&models.SendEmailLog{})
	if err != nil {
		return err
	}
	err = s.db.AutoMigrate(&models.ClaimRemain{})
	if err != nil {
		return err
	}
	return nil
}

func (s *Store) AutoMigrate() error {
	err := s.Transaction(func(repo repositories.Repository) error {
		return InitAutoMigrate(s)
	})
	if err != nil {
		return err
	}
	return nil
}
