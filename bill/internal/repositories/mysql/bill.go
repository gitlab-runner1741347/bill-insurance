package mysql

import (
	"context"

	"git.kafefin.net/backend/bill-insurance/internal/models"
)

func (s *Store) ListBillByPolicyId(ctx context.Context, policyID string) ([]*models.Bill, error) {
	db := s.db
	var result []*models.Bill
	return result, db.WithContext(ctx).Find(&result, "policy_id", policyID).Error
}

func (s *Store) ListBillByOrderId(ctx context.Context, orderID string) ([]*models.Bill, error) {
	db := s.db
	var result []*models.Bill
	return result, db.WithContext(ctx).Find(&result, "order_id", orderID).Error
}

func (s *Store) GetBillByBillId(ctx context.Context, billID string) (*models.Bill, error) {
	db := s.db
	var result models.Bill
	return &result, db.WithContext(ctx).First(&result, "bill_id", billID).Error
}
