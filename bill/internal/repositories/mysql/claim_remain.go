package mysql

import (
	"context"

	"git.kafefin.net/backend/bill-insurance/internal/models"
)

func (s *Store) SaveClaimRemain(ctx context.Context, m *models.ClaimRemain) (*models.ClaimRemain, error) {

	tx := s.db.Begin() // Start a new transaction

	// Defer the rollback in case of error or panic
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()

	if err := tx.Save(m).Error; err != nil {
		tx.Rollback() // Rollback the transaction if there's an error
		return nil, err
	}

	if err := tx.Commit().Error; err != nil {
		tx.Rollback() // Rollback the transaction if there's an error
		return nil, err
	}

	return m, nil

}

func (s *Store) CreateOneClaimRemain(ctx context.Context, m *models.ClaimRemain) (*models.ClaimRemain, error) {
	tx := s.db.WithContext(ctx).Begin()

	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()

	if err := tx.Create(m).Error; err != nil {
		tx.Rollback()
		return nil, err
	}

	if err := tx.Commit().Error; err != nil {
		tx.Rollback()
		return nil, err
	}

	return m, nil
}

func (s *Store) GetClaimRemainByPolicyId(ctx context.Context, id string) (*models.ClaimRemain, error) {
	var m *models.ClaimRemain
	return m, s.db.WithContext(ctx).First(&m, "policy_id = ?", id).Error
}

func (s *Store) GetClaimRemainByPolicyIdAndOrg(ctx context.Context, id string, org string) (*models.ClaimRemain, error) {
	var m *models.ClaimRemain
	return m, s.db.WithContext(ctx).First(&m, "policy_id = ? AND (partner_org = ? OR insurer = ?)", id, org, org).Error
}

func (s *Store) GetClaimRemainByOrderId(ctx context.Context, id string) (*models.ClaimRemain, error) {
	var m *models.ClaimRemain
	return m, s.db.WithContext(ctx).First(&m, "order_id = ?", id).Error
}

func (s *Store) ListClaimRemainByPolicyId(ctx context.Context, id string) ([]models.ClaimRemain, error) {
	var result []models.ClaimRemain
	return result, s.db.WithContext(ctx).Find(&result, "[policy_id = ?", id).Error
}

func (s *Store) GetClaimRemainsByPoliciesId(ctx context.Context, ids []string) ([]*models.ClaimRemain, error) {
	var claimRemains []*models.ClaimRemain
	err := s.db.Where("policy_id IN (?)", ids).WithContext(ctx).Find(&claimRemains).Error
	if err != nil {
		return nil, err
	}
	return claimRemains, nil
}
