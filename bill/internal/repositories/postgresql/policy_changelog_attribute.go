package postgresql

import (
	"context"

	"git.kafefin.net/backend/bill-insurance/internal/models"
)

func (s *Store) GetChangeLogAttByID(ctx context.Context, id int64) (*models.PolicyChangelogAttribute, error) {
	var m models.PolicyChangelogAttribute

	err := s.db.WithContext(ctx).First(&m, "id = ?", id).Error
	if err != nil {
		return nil, err
	}

	return &m, nil
}

func (s *Store) CreateOneChangeLogAtt(ctx context.Context, policy *models.PolicyChangelogAttribute) error {
	return s.db.WithContext(ctx).Create(policy).Error
}

func (s *Store) BatchCreateChangeLogAtts(ctx context.Context, ms []*models.PolicyChangelogAttribute) ([]*models.PolicyChangelogAttribute, error) {
	return ms, s.db.WithContext(ctx).
		CreateInBatches(&ms, 100).Error
}
