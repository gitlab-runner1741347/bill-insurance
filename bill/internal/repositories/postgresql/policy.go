package postgresql

import (
	"context"
	"time"

	"git.kafefin.net/backend/bill-insurance/internal/models"
	"git.kafefin.net/backend/bill-insurance/internal/pkg/convert"
	"git.kafefin.net/backend/bill-insurance/internal/repositories"
	"git.kafefin.net/backend/bill-insurance/pb"
	"git.kafefin.net/backend/kitchen/utils"
	"gorm.io/gorm"
)

func commonSelectExffectiveRecord(store *gorm.DB, time *time.Time) *gorm.DB {
	return store.Where("? BETWEEN effective_from AND effective_to", time)
}

func (s *Store) GetEffectivePolicies(ctx context.Context, phone string, time *time.Time) ([]*models.BpPolicy, error) {
	var result []*models.BpPolicy
	db := s.db.WithContext(ctx).Table(models.BpPolicy{}.TableName())
	if phone != "" {
		db = db.Where("customer_phone = ?", phone)
	}

	commonSelectExffectiveRecord(db, time)

	err := db.Where("policy_status = ?", "CREATED_OK").Order("effective_to DESC").Find(&result).Error
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (s *Store) GetEffectivePoliciesExtension(ctx context.Context, phone string, time *time.Time) ([]*models.BpPolicy, error) {
	var result []*models.BpPolicy
	db := s.db.Table(models.BpPolicy{}.TableName()).WithContext(ctx)
	if phone != "" {
		db = db.Where("customer_phone = ?", utils.FormatPhoneNumber(phone))
	}

	commonSelectExffectiveRecord(db, time)
	err := db.Where("policy_status IN (?)", []string{"CREATED_OK", "EXPIRED"}).
		Order("effective_to DESC").
		Find(&result).Error

	if err != nil {
		return nil, err
	}
	return result, nil
}

func (s *Store) UpdateUserIDByPhone(ctx context.Context, phone string, userID int64) error {
	db := s.db.Table(models.BpPolicy{}.TableName()).WithContext(ctx)
	query := db.Where("customer_phone = ?", phone)

	return query.Update("user_id", userID).Error
}

func (s *Store) GetLatestRecordByUserId(ctx context.Context, userId int64) (*models.BpPolicy, error) {
	var policy models.BpPolicy
	err := s.db.WithContext(ctx).Where("user_id = ?", userId).Order("created_date desc").First(&policy).Error
	if err != nil {
		return nil, err
	}
	return &policy, nil
}

func (s *Store) ListByUserId(ctx context.Context, userId int64) ([]*models.BpPolicy, error) {
	var m []*models.BpPolicy
	return m, s.db.WithContext(ctx).Where("bp_policy.user_id = ?", userId).Find(&m).Error
}

func (s *Store) GetPolicyByID(ctx context.Context, id string) (*models.BpPolicy, error) {
	var policy models.BpPolicy
	err := s.db.Where("policy_id = ?", id).First(&policy).Error
	if err != nil {
		return nil, err
	}

	return &policy, nil
}

func (s *Store) GetPoliciesByIds(ctx context.Context, ids []string) ([]*models.BpPolicy, error) {
	var policies []*models.BpPolicy
	err := s.db.WithContext(ctx).Where("policy_id IN (?)", ids).Find(&policies).Error
	if err != nil {
		return nil, err
	}

	return policies, nil
}

func (s *Store) GetByPolicyIdAndOrg(ctx context.Context, id string, org string) (*models.BpPolicy, error) {
	var policy models.BpPolicy
	db := s.db.WithContext(ctx)

	commonFilterByOrg(db, org)

	err := db.Where("policy_id = ?", id).First(&policy).Error
	if err != nil {
		return nil, err
	}

	return &policy, nil
}

func (s *Store) GetPolicyByPolicyNumber(ctx context.Context, policyNumber string) (*models.BpPolicy, error) {
	var policy models.BpPolicy
	err := s.db.WithContext(ctx).Where("policy_number = ?", policyNumber).First(&policy).Error
	if err != nil {
		return nil, err

	}

	return &policy, nil
}

func (s *Store) UpdatePolicy(ctx context.Context, policy models.BpPolicy) (*models.BpPolicy, error) {
	err := s.db.WithContext(ctx).Where("policy_id = ?", policy.PolicyID).Updates(&policy).Error
	if err != nil {
		return nil, err
	}

	return &policy, nil
}

func (s *Store) UpdateUpdateVersion(ctx context.Context, policyID string) error {
	if err := s.db.WithContext(ctx).Table(models.BpPolicy{}.TableName()).Where("policy_id = ?", policyID).UpdateColumn("version_update", gorm.Expr("COALESCE(version_update, 0) + ?", 1)).Error; err != nil {
		return err
	}

	return nil
}

func (s *Store) UpdateStatus(ctx context.Context, policyID string, status string) error {
	if err := s.db.WithContext(ctx).Table(models.BpPolicy{}.TableName()).Where("policy_id = ?", policyID).Update("policy_status", status).Error; err != nil {
		return err
	}

	return nil
}

func (s *Store) UpdateStatusRecordsFromCretedOk(ctx context.Context, policyIDs []string, status string) error {
	if err := s.db.WithContext(ctx).Table(models.BpPolicy{}.TableName()).Where("policy_id IN (?) AND policy_status = ?", policyIDs, "CREATED_OK").Update("policy_status", status).Error; err != nil {
		return err
	}

	return nil
}

func (s *Store) InternalListPolicies(ctx context.Context, req *pb.InternalListPoliciesRequest) ([]*models.BpPolicy, error) {
	var policies []*models.BpPolicy

	db := s.db.WithContext(ctx).Table(models.BpPolicy{}.TableName())

	if req.GetCustomerPhone() != "" {
		db.Where("customer_phone IN (?)", []string{req.GetCustomerPhone(), "0" + req.GetCustomerPhone()[2:], req.GetCustomerPhone()[2:]})
	}

	if req.GetPolicyId() != "" {
		db.Where("policy_id = ?", req.GetPolicyId())
	}

	if req.GetCustomerId() != "" {
		db.Where("customer_id = ?", req.GetCustomerId())
	}

	if req.GetCustomerEmail() != "" {
		db.Where("customer_email = ?", req.GetCustomerEmail())
	}

	if len(req.GetStatus()) > 0 {
		stringStatuses := make([]string, 0)
		for _, status := range req.GetStatus() {
			stringStatuses = append(stringStatuses, status.String())
		}
		db.Where("policy_status IN (?)", stringStatuses)
	}

	if req.GetDateOfIncident() != nil {
		commonSelectExffectiveRecord(db, convert.ConvertProtoTimestamp(req.GetDateOfIncident()))
	}

	db.Order("created_date desc")

	return policies, db.Find(&policies).Error
}

func commonFilterByOrg(store *gorm.DB, org string) {
	if org != "" {
		store.Where("LOWER(order_source) = LOWER(?) OR LOWER(provider_code) = LOWER(?)", org, org)
	}

}

func (s *Store) PaginatePoliciesWithOrg(ctx context.Context, req *pb.AdminListPoliciesRequest) ([]*models.BpPolicy, int64, error) {
	var policies []*models.BpPolicy
	var total int64

	db := s.db.WithContext(ctx).Table(models.BpPolicy{}.TableName())

	commonFilterByOrg(db, req.GetOrg())

	if req.GetPolicyStatus() != pb.PolicyStatus_UNSPECIFIED {
		db.Where("policy_status = ?", req.PolicyStatus)
	}

	if req.GetPolicyId() != "" {
		db.Where("policy_id = ?", req.PolicyId)
	}

	if req.GetOrderId() != "" {
		db.Where("order_id = ?", req.OrderId)
	}

	if req.GetPolicyNumber() != "" {
		db.Where("policy_number = ?", req.PolicyNumber)
	}

	if req.GetCustomerName() != "" {
		db.Where("customer_name = ?", req.CustomerName)
	}

	if req.GetCustomerPhone() != "" {
		db.Where("customer_phone = ?", req.CustomerPhone)
	}

	if req.GetFromDate() != nil {
		fromDateTime := convert.ConvertProtoTimestamp(req.GetFromDate())
		db.Where("bp_policy.created_date >= ?", fromDateTime)
	}

	if req.GetToDate() != nil {
		toDateTime := convert.ConvertProtoTimestamp(req.GetToDate())
		db.Where("bp_policy.created_date <= ?", toDateTime.AddDate(0, 0, 1))
	}

	paging := repositories.NewPaging(req.GetLimit(), req.GetOffset())

	db.Count(&total).Order("created_date desc").Offset(paging.Offset)

	return policies, total, db.Limit(paging.Limit).Find(&policies).Error
}
