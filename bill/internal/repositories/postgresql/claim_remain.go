package postgresql

import (
	"context"

	"git.kafefin.net/backend/bill-insurance/internal/models"
)

func (s *Store) GetByOrderId(ctx context.Context, orderID string) (*models.BpClaimRemain, error) {
	var remain models.BpClaimRemain
	err := s.db.WithContext(ctx).Where("order_id = ?", orderID).First(&remain).Error
	if err != nil {
		return nil, err

	}

	return &remain, nil
}
